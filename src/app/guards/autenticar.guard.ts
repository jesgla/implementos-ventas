import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AutentificacionService } from '../services/autentificacion/autentificacion.service';
/**
 *Permite validar el ruteo de la aplicacion
 * @export
 * @class AutenticarGuard
 * @implements {CanActivate}
 */
@Injectable({
  providedIn: 'root'
})
export class AutenticarGuard implements CanActivate{
  /**
   *Creates an instance of AutenticarGuard.
   * @param {AutentificacionService} auth
   * @memberof AutenticarGuard
   */
  constructor(public auth: AutentificacionService, private route: Router) {}
 
  /**
   *Valida la ruta
   * @returns {boolean} valida existencia
   * @memberof AutenticarGuard
   */
  canActivate(): boolean {
    // Validamos si el usuario esta logeado.
    let resp = this.auth.isAuthenticated();

    if(resp){
      return true;
    }else{
      this.route.navigate(['/login']);
      return false
    }
  }
}

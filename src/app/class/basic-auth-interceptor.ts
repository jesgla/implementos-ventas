import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
var username: String = 'services';
var password: String = '0.=j3D2ss1.w29-';
@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
    authdata: any = window.btoa(username + ':' + password);
    constructor() { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
     
        var v = { 'Authorization': `Basic ${this.authdata}`, 'Access-Control-Allow-Headers': 'Authorization, Access-Control-Allow-Headers' };
        if (request.url.includes('api/carro')) {
            v['Access-Control-Allow-Origin'] = '*';
        }
        const headers = new HttpHeaders(v);
        const cloneReq = request.clone({ headers });
     

        return next.handle(cloneReq);
    }
}
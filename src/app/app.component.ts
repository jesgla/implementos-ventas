import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { Component, OnDestroy } from '@angular/core';
import { AlertController } from '@ionic/angular';

import { Platform, ToastController, ModalController, Events} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AutentificacionService } from './services/autentificacion/autentificacion.service';
import { AcercaDePage } from './pages/Modals/acerca-de/acerca-de.page';
import { DataLocalService } from './services/dataLocal/data-local.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { VendedorService } from './services/vendedor/vendedor.service';
import { Network } from '@ionic-native/network/ngx';
import { Insomnia } from '@ionic-native/insomnia/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { Subscription } from 'rxjs';
import { SeccionesService } from 'src/app/services/secciones/secciones.service';
import { MenuController } from '@ionic/angular';
import { CategoriasArticulosService } from 'src/app/services/categoriasArticulos/categorias-articulos.service';
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';
import { NotificationsService } from './services/notifications/notifications.service';
import { UsuarioService } from './services/usuario/usuario.service';
import { Button } from 'protractor';

/**
 *Componente que se ejecuta al iniciar la aplicacion
 * @export
 * @class AppComponent
 */
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnDestroy{
  /**
   *Listado de menu
   * @memberof AppComponent
   */
  listaMenu : any[];
  listaMenuCategorias: any;
  vendedor: any;
  nivelUsuario: any;
  ruta: string;
  cliente: Cliente;
  seccionSelected$: Subscription;
  menuCategoria$: Subscription;

  constructor(
    public events: Events,
    private geolocation: Geolocation,
    public backgroundMode: BackgroundMode,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public toastController: ToastController,
    private authenticationService: AutentificacionService,
    private router: Router,
    public modalController: ModalController,
    private dataLocal: DataLocalService,
    private vendedorService: VendedorService,
    private _seccionService: SeccionesService,
    public _catMenu: CategoriasArticulosService,
    private network: Network,
    private menu: MenuController,
    private insomnia: Insomnia,
    private _estadoTiendaService: EstadoTiendaService,
    private fcm: FCM,
    private _NotificationsService: NotificationsService,
    private _UsuarioService: UsuarioService,
    public  AlertController: AlertController
  ) {
    this.ruta = '';
    this.events.publish('actualizar', true);
    this.listaMenu=[];
    this.initializeApp();

    this._catMenu.getCategoriasMenu();

    this.seccionSelected$ = this._seccionService.seccionObservable$.subscribe( async redirect => {
      this.cliente = null;
      await this.obtenerUsuario(redirect);
    })

    // nos suscribimos a una observable para saber cuando es necessario mostrar el menu asociado a las categorias en la pagina de menu.
    this.menuCategoria$ = this._catMenu.menuObservable$.subscribe( showMenu => {
      showMenu ?  this.openMenuCategorias() : this.closeMenuCategorias();
    });
  }

  ngOnDestroy(){
    this.seccionSelected$ ?  this.seccionSelected$.unsubscribe() : '';
    this.menuCategoria$? this.menuCategoria$.unsubscribe() : '';
  }

  /**
   *Inicio de aplicacion
   * @memberof AppComponent
   */
  async initializeApp() {
    this.platform.ready().then(async () => {

      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      this.checkInterval();
      this.refreshFCMtoken();

      this.setNotificationsToken();
    
      this.fcm.onTokenRefresh().subscribe(token => {
        this._NotificationsService.setFireBaseToken(token);
      });

      this.fcm.onNotification().subscribe(async data => {
        if (data.wasTapped) {
          if(data.seccion && data.landing_page){
            await this.dataLocal.setSeccion(data.seccion);
            await this._seccionService.selectedSeccion(false);
            this._NotificationsService.setLandingPage(data.landing_page);
            this.router.navigate([data.landing_page]);
          }
        } else {
          if(data.seccion){
            await this.dataLocal.setSeccion(data.seccion);
            await this._seccionService.selectedSeccion(false);
          }
          let url = data.landing_page? data.landing_page : null;
          this.presentAlertNotification(data.title, data.body, url );
        }
      });

      this.insomnia.keepAwake()
      .then(
        () => console.log('success'),
        () => console.log('error')
      );
      this.authenticationService.authenticationState.subscribe(async (state) => {
        if (state) {
          await this.dataLocal.setUserLoged();
          let seccion = await this.dataLocal.getItem('seccion');
          if(seccion){
            await this.obtenerUsuario();
            if(this._NotificationsService.landing_pageByNotification && this._NotificationsService.landing_pageByNotification.length > 0){
              this.router.navigate([this._NotificationsService.landing_pageByNotification]);
              this._NotificationsService.landing_pageByNotification = null;
            }
          }else{
            this.router.navigate(['home']); 
          }
        } 
      });
       this.network.onDisconnect().subscribe(async () => {
        await this.dataLocal.setItem('conexion',false);
        await this.dataLocal.presentToast('Sin conexión','danger');
         
       });
       this.network.onConnect().subscribe(async() => {
        await this.dataLocal.setItem('conexion',true)
        if(!this._estadoTiendaService.GetScanningStatus()){
          await this.dataLocal.presentToast('Conectado nuevamente','success');
        }
       });

    });

   this.platform.pause.subscribe(async () => {    
      this.authenticationService.authenticationState.subscribe((state) => {
        if (state) {
          this.guardarGeoRecursivo();
        }
      });
    });
    
    this.dataLocal.clienteReload$.subscribe( () => {
      this.obtenerCliente();
    })
  }

  async checkInterval() {
    setInterval(async () =>{
      this._NotificationsService.updateToken(this._UsuarioService.getUserLoged());

      await this.authenticationService.isValidVersion();
      if(!this.authenticationService.checkVersion.error && !this.authenticationService.checkVersion.valid){
        this.authenticationService.logout();
      }
    }, 1000 * 60 * 60 * 8);
  }

  async refreshFCMtoken(){
    this.setNotificationsToken();
  }
  
  async guardarGeoRecursivo() {
    await this.guardarPosicionVendedor();
    setTimeout(async () => {
      await this.guardarPosicionVendedor();
      await this.guardarGeoRecursivo()
    }, 60000 * 5);
  } 

  async abrirModal() {
    const modal = await this.modalController.create({
      component: AcercaDePage,
    });
    return await modal.present();
  }

  setNotificationsToken(){
    this.fcm.getToken().then(token => {
      this._NotificationsService.setFireBaseToken(token);
    });
  }

  /**
  *Permite obtener un usuario
  * @memberof AppComponent
  */
  async obtenerUsuario(redirect: boolean = true) {
    let seccion = await this.dataLocal.getItem('seccion');

    if(!seccion){
      this.router.navigate(['home']); 
      return;
    }
    console.log('seccion',seccion)
    switch(seccion){
    /**#################################
     * vendedores
     * #################################*/
      case 'vendedores':
              if (!this._UsuarioService.isZonalAccount()) {
                this.listaMenu = [
                  { icono: 'home', nombre: 'Objetivo', directTo: '/tabs/objetivos' },
                  this.getClienteMenu(),
                  { icono: 'calendar', nombre: 'Calendario', directTo: '/tabs/calendario' },
                  { icono: 'logo-pinterest', nombre: 'Oportunidades', directTo: '/tabs/oportunidad' },
                  { icono: 'list-box', nombre: 'Catalogo', directTo: '/tabs/catalogo' },
                  { icono: 'cart', nombre: 'Carro Compras', directTo: '/carro-compra' },
                ]
                if(!this.cliente && redirect){
                  this.router.navigate(['tabs', 'objetivos']);
                }
              } else {
                this.listaMenu = [
                  { icono: 'home', nombre: 'Zona', directTo: '/tabs/zonas' },
                  { icono: 'call', nombre: 'Llamadas', directTo: '/tabs/llamadas' },
                  { icono: 'cube', nombre: 'Combos', directTo: '/tabs/combos' },
                  { icono: 'checkbox-outline', nombre: 'Visitas', directTo: '/tabs/visitas' },
/*                   { icono: 'logo-pinterest', nombre: 'Oportunidades', directTo: '/tabs/oportunidad' }, */
                  { icono: 'person', nombre: 'Vendedores', directTo: '/tabs/vendedores' },
                ]
                if(!this.cliente && redirect){
                  this.router.navigate(['tabs', 'zonas']);
                }
          
              } 
          break;
    /**#################################
     * tiendas
     * #################################*/
      case 'tiendas':
            this.listaMenu = [
              { icono: 'qr-scanner', nombre: 'Detalle Productos', directTo: '/tabs/hua' },
              ...this.isUserBodegueroOrCajero(),
              ...this.AccesoVerAsignarDocumentos(),
            ];
            if(redirect) this.router.navigate(['tabs', 'hua']);
          break;
    /**#################################
     * bodegas
     * #################################*/
      case 'bodegas':
        console.log('entre aL CASE bodegas');
          this.listaMenu = [
            /* { icono: 'home', nombre: 'Inicio', directTo: '/bdga-inicio-page' }, */
            { icono: 'archive', nombre: 'Recibir Mercancías', directTo: '/recibo-mercancias' },
            { icono: 'bus', nombre: 'Picking Pendientes', directTo: '/picking-pendientes' },
            /* { icono: 'bus', nombre: 'Despachos Pendientes', directTo: 'retiros-pendientes/OV-1732077' }, */
            /* { icono: 'megaphone', nombre: 'Registrar Sobrantes', directTo: '/registro-sobrantes' }, */
            /* { icono: 'paper', nombre: 'Quiebre de Stock', directTo: '/tabs/quiebre' }, */
          ];
          if(redirect) this.router.navigate(['picking-pendientes']);
          break;
      default: 
          console.error('Seccion no habilitada');
          if(redirect) this.router.navigate(['home']); 
          break;
    }
  }
  cerrarSesion() {
    this.authenticationService.logout();
  }

  /**
   *Permite almacenar la geolocalizacion
   * @memberof AppComponent
   */
  async guardarPosicionVendedor() {

    let resp = await this.geolocation.getCurrentPosition();

    let geolocalizacion = {
      latitud: resp.coords.latitude,
      longitud: resp.coords.longitude
    }

    await this.vendedorService.guardarGeolocalizacion(geolocalizacion, this._UsuarioService.getUserLoged());
  }


  /**
 * permite retornar el objeto asociado al submenu de clientes
 * @memberof AppComponent
 */
  getClienteMenu(){
    if(this.cliente){
      return { icono: 'person', nombre: 'Cliente', children: 
                [
                  { icono: 'checkbox-outline', nombre: 'Ir a Check-in', directTo: '/registro-visita' },
                  { icono: 'list-box', nombre: 'Lista Clientes', directTo: '/tabs/clientes' }
                ]
              }
    }else{
      return { icono: 'person', nombre: 'Cliente', directTo: '/tabs/clientes' }
    }  
  }

  async obtenerCliente(){
    this.cliente = await this.dataLocal.getCliente();
  }

  hideMenu(){
    if(this.router.url == '/login' || this.router.url == '/home'){
      return true;
    }else{
      return false;
    }
  }

  showMenuCat(){
    if(this.router.url != '/tabs/catalogo'){
      return true;
    }else{
      false
    }
  }

    /**
   * Metodo destinado a eliminar la seccion albergada en e localstorage y luego ir a la pagina home.
   * @memberof AppComponent
   */
  async cambiarSeccion(){
    this.listaMenu = [];
    await this.dataLocal.removeSeccion();
    this.router.navigate(['home']);
  }

  /**
   * Metodo dque permite mostrar el menu asociado a las categorias que se muestran en el catalogo
   * @memberof AppComponent
   */
  openMenuCategorias() {
    this.menu.open('categoriaMenu');
  }

  /**
   * Metodo dque permite ocultar el menu asociado a las categorias que se muestran en el catalogo
   * @memberof AppComponent
   */
  closeMenuCategorias(){
    this.menu.close('categoriaMenu');
  }

  /**
 * Metodo que permite ejecutar la busqueda de un categoria, desde el menu de categoria hacia la pagina de categoria.
 * @memberof AppComponent
 */
  buscarCategoria(categoria: any){
    let cat: string = categoria.url;
    this._catMenu.buscarCategoria(cat);
  }

  buscarFiltro(filtro: any, tipo){
    this._catMenu.buscarFiltro(filtro.title, tipo);
  }

  isUserBodegueroOrCajero(){
    if(this._UsuarioService.isUserAllowedToSaveDocs()){
      return  [
        { icono: 'clipboard', nombre: 'Respaldar Entrega', directTo: '/tabs/respaldo-entrega' },
        { icono: 'save', nombre: 'Respaldar Compra', directTo: 'respaldo-compras' },
        { icono: 'folder', nombre: 'Mis Respaldos', directTo: '/mis-respaldos' },
      ]
    }
    return [];
  }

  AccesoVerAsignarDocumentos(){
    if(this._UsuarioService.AccesoVerAsignarDocumentos()){
      return [
        { icono: 'document', nombre: 'Última Milla', directTo: '/tabs/asignacion-documentos' },
      ]
    }
    return [];
  }

  async presentAlertNotification(title: string, msg: string, url?: string) {
    let buttons: any[] = this.createNotificationsAlertButtons(url);

    const alert = await this.AlertController.create({
      cssClass: 'my-custom-class',
      header: title,
      message: msg,
      buttons: buttons,
    });

    await alert.present();
  }

  createNotificationsAlertButtons(url: string){
    if(!url || url.length < 1) return ['OK']

    let buttons: any[] = [];

    buttons.push(
      {
        text: 'OK',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          
        }
      }
    )

    buttons.push( 
      {
        text: 'Revisar',
        handler: () => {
          this.router.navigate([url]);
        }
      }
    )
    return buttons;
  }

}

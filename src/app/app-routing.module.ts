import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AutenticarGuard } from './guards/autenticar.guard';

const routes: Routes = [
  // global 
  { path: '', redirectTo: 'home', pathMatch: 'full', canActivate:[AutenticarGuard]},
  { path: 'login', loadChildren: './pages/loginPage/login/login.module#LoginPageModule' },
  { path: 'tabs', canActivate:[AutenticarGuard], loadChildren:'./tabs/tabs.module#TabsPageModule'},
  { path: 'home', loadChildren: './pages/homePage/home.module#HomePageModule' , canActivate:[AutenticarGuard]},
  { path: '', loadChildren: './pages/loginPage/login/login.module#LoginPageModule',canActivate:[AutenticarGuard] },

  // vendedor
  { path: 'clientes', loadChildren: './pages/clientePage/pages/clientes/clientes.module#ClientesPageModule', canActivate:[AutenticarGuard] },
  { path: 'detalle-cliente', loadChildren: './pages/clientePage/pages/detalle-cliente/detalle-cliente.module#DetalleClientePageModule',canActivate:[AutenticarGuard] },
  { path: 'registro-visita', loadChildren: './pages/visitaPage/pages/registro-visita/registro-visita.module#RegistroVisitaPageModule',canActivate:[AutenticarGuard] },
  { path: 'carro-compra', loadChildren: './pages/CarroPages/pages/carro-compra/carro-compra.module#CarroCompraPageModule',canActivate:[AutenticarGuard] },
  { path: 'resumen-ventas', loadChildren: './pages/vendedorPage/page/resumen-ventas/resumen-ventas.module#ResumenVentasPageModule',canActivate:[AutenticarGuard] },
  { path: 'detalle-vendedor', loadChildren: './pages/vendedorPage/page/detalle-vendedor/detalle-vendedor.module#DetalleVendedorPageModule',canActivate:[AutenticarGuard] },
  { path: 'notificacion/:price', loadChildren: './pages/notificacionesPage/pages/notificacion/notificacion.module#NotificacionPageModule',canActivate:[AutenticarGuard] },
  { path: 'propuesta', loadChildren: './pages/propuestaPage/propuesta.module#PropuestaPageModule', canActivate:[AutenticarGuard] },

  // Tiendas
  { path: 'mis-respaldos', loadChildren: './pages/respaldo-documentosPage/historial-respaldos/historial-respaldos.module#HistorialRespaldosPageModule', canActivate:[AutenticarGuard]  },
  { path: 'respaldo-compras', loadChildren: './pages/respaldo-documentosPage/respaldo-documentos/respaldo-documento.module#RespaldoDocumentoPageModule' , canActivate:[AutenticarGuard]},

  // Bodegas
  { path: 'bdga-inicio-page', loadChildren: './pages/bodegaPages/pages/bdga-inicio-page/bdga-inicio-page.module#BdgaInicioPagePageModule',canActivate:[AutenticarGuard] },
  { path: 'recibo-mercancias', loadChildren: './pages/bodegaPages/pages/recibo-mercancias/recibo-mercancias.module#ReciboMercanciasPageModule',canActivate:[AutenticarGuard] },
  { path: 'picking-pendientes', loadChildren: './pages/bodegaPages/pages/picking-pendientes/picking-pendientes.module#PickingPendientesPageModule',canActivate:[AutenticarGuard] },
  { path: 'retiros-pendientes/:OV', loadChildren: './pages/bodegaPages/pages/retiro-pendiente/retiro-pendiente-page.module#retiroPendientePageModule', canActivate:[AutenticarGuard]},


  /*{ path: 'recibo-mercancias-discrepancia', loadChildren: './pages/bodegaPages/pages/recibo-mercancias-discrepancia/recibo-mercancias-discrepancia.module#ReciboMercanciasDiscrepanciaPageModule' },
  { path: 'recibo-mercancias-resumen-confirm', loadChildren: './pages/bodegaPages/pages/recibo-mercancias-resumen-confirm/recibo-mercancias-resumen-confirm.module#ReciboMercanciasResumenConfirmPageModule' },
  { path: 'modal-imagen', loadChildren: './pages/bodegaPages/pages/modal-imagen/modal-imagen.module#ModalImagenPageModule' },
  { path: 'modal-ov', loadChildren: './pages/bodegaPages/pages/modal-ov/modal-ov.module#ModalOvPageModule' },*/
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes , { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {}

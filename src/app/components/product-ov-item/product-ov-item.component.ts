import { Component, Input, OnInit } from '@angular/core';

// interface
import { detalleOV } from 'src/app/interfaces/bodega/interfaces';

@Component({
  selector: 'app-product-ov-item',
  templateUrl: './product-ov-item.component.html',
  styleUrls: ['./product-ov-item.component.scss'],
})
export class ProductOvItemComponent implements OnInit {
  @Input() articulo: detalleOV;
  viewDetail: boolean = false;

  constructor() { }

  ngOnInit() {}

}

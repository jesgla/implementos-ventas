import { Component, Input, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-popover-filtro-zonas',
  templateUrl: './popover-filtro-zonas.component.html',
  styleUrls: ['./popover-filtro-zonas.component.scss'],
})
export class PopoverFiltroZonasComponent implements OnInit {
  @Input() zonas: any[];

  constructor(private PopoverController: PopoverController) { }

  ngOnInit() {
  }

  /**
  * @author ignacio zapata  \"2020-11-10\
  * @desc Metodo que se llama cuando se hace click en una zona y le setea la propiedad selected a true ademas cierra el popover y devuelve el dato zona
  * @params string con el nombre de la zona seleccionada.
  * @return
  */
  setFiltroZona(zona){
    for(let item of this.zonas){
      item.selected = item.nombre && item.nombre === zona? true : false;
    }

    this.PopoverController.dismiss(zona);
  }

}

import { Component, Input, OnInit } from '@angular/core';

// interface
import { DocumentoRespaldo } from '../../interfaces/respaldoDocs/respaldo';

@Component({
  selector: 'app-detalle-documento',
  templateUrl: './detalle-documento.component.html',
  styleUrls: ['./detalle-documento.component.scss'],
})
export class DetalleDocumentoComponent implements OnInit {
  @Input() documento: DocumentoRespaldo;

  constructor() { }

  ngOnInit() {
    
  }

}

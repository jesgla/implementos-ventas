import { Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { Subscription } from 'rxjs';

//ionic
import { PopoverController  } from '@ionic/angular';

// Services
import { FiltrosQuiebresService } from 'src/app/services/filtrosQuiebres/filtros-quiebres.service';

// interfaces
import { parametrosUrl } from 'src/app/interfaces/articuloQuiebre/articuloQuiebre';
import { audit } from 'rxjs/operators';

@Component({
  selector: 'app-popover-filtros-quiebre',
  templateUrl: './popover-filtros-quiebre.component.html',
  styleUrls: ['./popover-filtros-quiebre.component.scss'],
})
export class popOverFiltrosQuiebreComponent implements OnInit {
  @Output() setFiltrosEvent = new EventEmitter;
  @Input() parametros: parametrosUrl;
  yaQuebrados: boolean = false;
  selectedCategoria = '';
  selectFiltroText: string = 'Productos Assortment';

  SelectfiltroOptions: any = [{nombre: 'Productos Assortment', value: 1},{nombre: 'Tipologia 1-2 Tienda', value: 2},{nombre: 'Tipologia 1-2 Compañía', value: 3}]

  constructor( public _filtrosQuiebreService: FiltrosQuiebresService, private PopoverController: PopoverController) { 

  }

  ngOnInit() {
    // Seteamos los valores que se mostrarán en el popover.
    this.selectedCategoria = this.parametros.categoria.toLowerCase();
    this.parametros.quebrados == 1? this.yaQuebrados = true : this.yaQuebrados = false;
    if(this.parametros.filtro === 1) this.selectFiltroText = 'Productos Assortment';
    if(this.parametros.filtro === 2) this.selectFiltroText = 'Tipologia 1-2 Tienda';
    if(this.parametros.filtro === 3) this.selectFiltroText = 'Tipologia 1-2 Compañía';
  }


  /**
  * @author ignacio zapata 25-08-2020
  * @desc  Setea la propiedad yaquebrados cuando se hace clic en el checkbox para filtrar por quiebre o sin quiebre.
  * @params event ionchange de un ion-checkbox.  
  * @return
  */
  setYaQuebrados(value){
    this.yaQuebrados = value.detail.checked;
    if(this.yaQuebrados){
      this.parametros.quebrados = 1; 
    }else{
      this.parametros.quebrados = 0 ;
    }
  }

  /**
  * @author ignacio zapata 25-08-2020
  * @desc  Setea la propiedad categoria de la variable parametros, selecciona una categoria desde el select de categorias.
  * @params value del select de categorias 
  * @return
  */
  setCategoriaFiltro(value){
    this.parametros.categoria = value;
  }

  /**
  * @author ignacio zapata  \"2020-08-26\
  * @desc se encarga de quitar el filtro de categoriast.
  * @params 
  * @return
  */
  limpiarCategoria(){
    this.parametros.categoria = '';
    this.selectedCategoria = '';
  }

  /**
  * @author ignacio zapata  \"2020-08-26\
  * @desc metodo que se encarga de cerrar el popover y luego emitir evento que volverá a cargar los productos con quiebre dependiendo los filtros seleccionados.
  * @params 
  * @return
  */
  aplicarFiltros(){
    this.PopoverController.dismiss();
    this.setFiltrosEvent.emit(this.parametros);
  }

  /**
  * @author ignacio zapata  \"2020-08-26\
  * @desc Metodo que se acciona cuando se presiona el boton limpiar filtros. reestablece los filtros a los valores por default y luego emite evento para llamar denuevo al servicio
  * @params 
  * @return
  */
  limpiarFiltros(){
    this.parametros.categoria = '';
    this.parametros.quebrados = 1;
    this.PopoverController.dismiss();
    this.setFiltrosEvent.emit(this.parametros);
  }

  /**
  * @author ignacio zapata  \"2020-09-23\
  * @desc metodo que se emite cuando cambia de valor el selec asociado al filtro. 
  * @params  recibe la id de la opcion del select que ha sido seleccionado.
  * @return
  */
  filtroOnChage(value){
    this.selectFiltroText = this.SelectfiltroOptions.filter( opcion => opcion.value === value)[0].nombre;
    this.parametros.filtro = value;
  }

}

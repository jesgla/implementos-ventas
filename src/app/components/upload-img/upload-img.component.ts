import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

declare var window:any;

@Component({
  selector: 'app-upload-img',
  templateUrl: './upload-img.component.html',
  styleUrls: ['./upload-img.component.scss'],
})
export class UploadImgComponent implements OnInit, OnDestroy {
  @Output() imageEventEmitter = new EventEmitter;
  imgObject: {image: any, imageData: any}; 

  constructor(private camera: Camera, private _DataLocalService: DataLocalService) { }

  ngOnInit() {}

  ngOnDestroy(){
    this.clearImage();
  }

  takePhoto(tipo){
    this.imgObject = null;

    const options: CameraOptions = {
      quality: 50,
      targetWidth: 1000,
      targetHeight: 1000,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType[tipo]
    };

    this.procesarImagen(options);
  }

  procesarImagen(options:CameraOptions){
    let images: any[]= [];
    
    this.camera.getPicture(options).then( ( imageData ) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;

      images.push(imageData);

      this.imgObject = {"image":base64Image,"imageData":images};
    }, (err) => {
      console.error(err);
      this._DataLocalService.presentToast('No ha sido posible capturar la imagen', 'danger');
    });

  }

  clearImage(){
    this.imgObject = null;
  }
  
  returnImage(){
    this.imageEventEmitter.emit(this.imgObject);
    this.ngOnDestroy();
  }
}

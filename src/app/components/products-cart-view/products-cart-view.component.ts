// Angular
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

//ionic
import { ModalController } from '@ionic/angular';

// Interfaces
import { Articulo } from 'src/app/interfaces/articulo/articulo'

// Componentes
import { DetalleArticuloPage } from 'src/app/pages/Modals/detalle-articulo/detalle-articulo.page';

@Component({
  selector: 'app-products-cart-view',
  templateUrl: './products-cart-view.component.html',
  styleUrls: ['./products-cart-view.component.scss'],
})
export class ProductsCartViewComponent implements OnInit {

  /**
   * listado con articulos
   * @type { Articulo[] }
   * @memberof ProductsCartViewComponent
   */
  @Input() articulo: Articulo;

  /**
   * listado con articulos
   * @type { boolean }
   * @memberof ProductsCartViewComponent
   */
  @Input() isCotizacion: boolean = false;

  /**
 * listado con articulos
 * @type { EventEmitter }
 * @memberof ProductsCartViewComponent
 */
  @Output() recargarCarro = new EventEmitter();


  /**
   *Creates an instance of CarroCompraPage.
   * @param {ModalController} modalController
   * @memberof ProductsCartViewComponent
   */
  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  /**
 * Permite visualizar el detalle del articulo seleccionado
 * @param {*} articulo
 * @memberof CarroCompraPage
 */
  async detalleArticulo(articulo) {
    // Sino es cotizaicon, entonces se permite ver y editar el articulo.
    if(!this.isCotizacion){
      const modal = await this.modalController.create({
        component: DetalleArticuloPage,
        componentProps: {
          articulo,
          boton: true,
          cantidad: articulo.cantidad
        }
      });
      await modal.present();
      const { data } = await modal.onDidDismiss();
  
      if (data.recargar) {
        this.recargarCarro.emit();
      }
    }
  }

}

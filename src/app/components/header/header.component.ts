import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})

/**
 *Componente dinamico que recibe el titulo e indice para cambiar slider
 * @export
 * @class HeaderComponent
 */
export class HeaderComponent{

  /**
   * Objeto nativo slider 
   * @type {*}
   * @memberof HeaderComponent
   */
  @Input() slides:any;

  /**
   * Cadena que permite identificar el componente señalado
   * @type {string}
   * @memberof HeaderComponent
   */
  @Input()  titulo:string;

  /**
   *Creates an instance of HeaderComponent.
   * @memberof HeaderComponent
   */
  constructor() { }

  /**
   * Permite cambiar pestaña slider
   * @param {*} value
   * @memberof HeaderComponent
   */
  evento(value){
    value? this.slides.slidePrev(): this.slides.slideNext();
  }
}

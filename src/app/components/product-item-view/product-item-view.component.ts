import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Articulo } from '../../interfaces/articulo/articulo';

//ionic
import { ModalController, AlertController, NavController, Events } from '@ionic/angular';

//components
import { DetalleArticuloPage } from '../../pages/Modals/detalle-articulo/detalle-articulo.page';

@Component({
  selector: 'app-product-item-view',
  templateUrl: './product-item-view.component.html',
  styleUrls: ['./product-item-view.component.scss'],
})
export class ProductItemViewComponent implements OnInit {

  @Input() articulo: Articulo;
  @Output() actualizaCarro = new EventEmitter();

  constructor(public modalController: ModalController,
    public alertController: AlertController,
    private navCtrl: NavController) { }

  ngOnInit() {
  }

  async abrirModal() {
    const modal = await this.modalController.create({
      component: DetalleArticuloPage,
      componentProps: {
        articulo: this.articulo,
        boton: false,
        cantidad: 1,
      }
    },
    );

    await modal.present();
    const { data } = await modal.onWillDismiss();
    data.recargar ? await this.obtenerCarro() : null;
  }
  
  obtenerCarro(){
    this.actualizaCarro.emit();
  }

}

//angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

//ionic
import { IonicModule } from '@ionic/angular';

//components
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { CalendarioComponent } from './calendario/calendario.component';
import { ProductItemViewComponent } from './product-item-view/product-item-view.component';
import { ProductsCartViewComponent } from './products-cart-view/products-cart-view.component';
import { popOverFiltrosQuiebreComponent } from './popover-filtros-quiebre/popover-filtros-quiebre.component';
import { PopoverFiltroZonasComponent } from './popover-filtro-zonas/popover-filtro-zonas.component';
import { UploadImgComponent } from'./upload-img/upload-img.component';

import { DetalleArticuloPageModule} from '../pages/Modals/detalle-articulo/detalle-articulo.module'
import { DetalleArticuloPage } from '../pages/Modals/detalle-articulo/detalle-articulo.page';
import { IconComponent } from '../components/icon/icon.component';

import { DetalleDocumentoComponent } from './detalle-documento/detalle-documento.component';
import { ProductOvItemComponent } from 'src/app/components/product-ov-item/product-ov-item.component';
 
// pipes
import { PipesModule } from 'src/app/pipes/pipes.module';


@NgModule({
  entryComponents:[ DetalleArticuloPage ],
  declarations: [
    HeaderComponent,
    MenuComponent,
    CalendarioComponent,
    ProductItemViewComponent,
    ProductsCartViewComponent,
    popOverFiltrosQuiebreComponent,
    PopoverFiltroZonasComponent,
    IconComponent,
    DetalleDocumentoComponent,
    ProductOvItemComponent,
    UploadImgComponent,
  ],
  exports:[
    HeaderComponent,
    MenuComponent,
    CalendarioComponent,
    ProductItemViewComponent,
    ProductsCartViewComponent,
    popOverFiltrosQuiebreComponent,
    PopoverFiltroZonasComponent,
    IconComponent,
    DetalleDocumentoComponent,
    ProductOvItemComponent,
    UploadImgComponent,
    ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    PipesModule,
    DetalleArticuloPageModule,
  ]
})
export class ComponentsModule { }

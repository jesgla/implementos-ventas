import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-tabla-llamandos',
  templateUrl: './tabla-llamandos.component.html',
  styleUrls: ['./tabla-llamandos.component.scss'],
})
export class TablaLlamandosComponent implements OnInit {
  @Input() informeLlamado: any;
  @Input() lstVendedores: any;

  @Input() indice: number
  @Input() filtro: string;
  @Input() slidesLlamada: IonSlides;
  @Input() sucursal: string;
  @Input() slidesLlamados: IonSlides;
  @Output() public brake = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {

  }

  enviarObjeto(objeto) {
    if(objeto.index < 2){
      this.brake.emit(objeto);
    }
  }
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LlamadasTabPage } from './llamadas-tab.page';
import { TablaLlamandosComponent } from './components/tabla-llamandos/tabla-llamandos.component';

const routes: Routes = [
  {
    path: '',
    component: LlamadasTabPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LlamadasTabPage, TablaLlamandosComponent]
})
export class LlamadasTabPageModule {}

import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';

// interfaces
import { Objetivos } from 'src/app/interfaces/vendedor/objetivos';

// Servicio
import { SupervisorService } from 'src/app/services/supervisor/supervisor.service';
import {  LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-llamadas-tab',
  templateUrl: './llamadas-tab.page.html',
  styleUrls: ['./llamadas-tab.page.scss'],
})

export class LlamadasTabPage implements OnInit {
  listaObjetivos: Objetivos[];
  /**
   *elemento slider
   * @type {IonSlides}
   * @memberof DetalleClientePage
   */
  @ViewChild(IonSlides, { static: false }) slidesLlamados: IonSlides;

  /**
   *color de elemento
   * @type {*}
   * @memberof DetalleClientePage
   */
  color: any;
  rango: string = 'Diario';
  /**
   *arreglo de paginas de slider
   * @memberof DetalleClientePage
   */
  paginas = [
    { svg: 'call', index: 0, cheked: true, filtro: '' },
    { svg: 'person', index: 1, disabled: true, cheked: false, filtro: '' },
  ];

  /**
   *configuracion de slider
   * @memberof DetalleClientePage
   */
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  indice: number;
  indiceActual: number = 0;

  informeLlamado: any;
  lstVendedores : any;
  lstClientes: any;
  zonaActual: string;

  constructor( private supervisorService: SupervisorService, public _loadingService: LoadingService, ) {
    this.indice = 1;
  }
  ngOnInit() {

  }
 
  /**
      *metodo que se ejecuta al llamar el componente
      * @memberof ObjetivosTabPage
      */
  async ionViewWillEnter() {

    await this._loadingService.presentLoading('Cargando Datos..');
 
    let info = await this.supervisorService.InformeLlamados(null);
    this.informeLlamado= this.prepararDatos(info)
    
    this._loadingService.hideLoading();
  }


  async filtrar(event) {

    this.rango =event.detail.value;

    await this._loadingService.presentLoading("Cargando Datos..");

    if(this.rango=='Diario'){
      let info = await this.supervisorService.InformeLlamados(null);
      this.informeLlamado= await this.prepararDatos(info)
    
    }else{
      let info = await this.supervisorService.InformeLlamados(30);
      this.informeLlamado= await this.prepararDatos(info)
    }

    // Actualizamos los datos de los vendedores.
    if(this.indiceActual == 1 && this.zonaActual){
      let zona : any = this.getZona(this.zonaActual);
      this.lstVendedores = zona && zona.vendedores ? zona.vendedores : [];
    }

    this._loadingService.hideLoading();
  }

  prepararDatos(informe){
    let promedio =0;
    let objeto =null;
    let arr =[];
    let sumaR =0;
    let sumaL =0;
    promedio = informe.realizados.NORTE*100/informe.total.NORTE;
    sumaR +=informe.realizados.NORTE
    sumaL +=informe.total.NORTE
    
    objeto ={
      zona : 'Norte',
      realizados:informe.realizados.NORTE,
      total :informe.total.NORTE,
      promedio :promedio,
      vendedores: informe.detalle.NORTE
    }
    arr.push(objeto);

    promedio = informe.realizados.CENTRO*100/informe.total.CENTRO;    
    sumaR +=informe.realizados.CENTRO
    sumaL +=informe.total.CENTRO

    objeto ={
      zona : 'Centro',
      realizados:informe.realizados.CENTRO,
      total :informe.total.CENTRO,
      promedio :promedio,
      vendedores: informe.detalle.CENTRO

    }
    arr.push(objeto);

    promedio = informe.realizados.CENTROSUR*100/informe.total.CENTROSUR;    
    sumaR +=informe.realizados.CENTROSUR
    sumaL +=informe.total.CENTROSUR

    objeto ={
      zona : 'Centro Sur',
      realizados:informe.realizados.CENTROSUR,
      total :informe.total.CENTROSUR,
      promedio :promedio,
      vendedores: informe.detalle.CENTROSUR

    }
    arr.push(objeto);

    promedio = informe.realizados.SUR*100/informe.total.SUR;
    sumaR +=informe.realizados.SUR
    sumaL +=informe.total.SUR

    objeto ={
      zona : 'Sur',
      realizados:informe.realizados.SUR,
      total :informe.total.SUR,
      promedio :promedio,
      vendedores: informe.detalle.SUR

    }
    arr.push(objeto);

    promedio = informe.realizados.TELEVENTA*100/informe.total.TELEVENTA;
    sumaR +=informe.realizados.TELEVENTA
    sumaL +=informe.total.TELEVENTA
    objeto ={
      zona : 'TeleVenta',
      realizados:informe.realizados.TELEVENTA,
      total :informe.total.TELEVENTA,
      promedio :promedio,
      vendedores: informe.detalle.TELEVENTA

    }
    arr.push(objeto);

    let promedioTotal= sumaR*100/sumaL;
    
    let infomeLlamado ={
      zonas:arr,
      totalRealizadas:sumaR,
      totalLlamadas:sumaL,
      promedioGeneral: promedioTotal

    }
    return infomeLlamado
  }
  
  async  habilitar(objeto) {

    this.indiceActual = objeto.index;
    objeto.index==1? this.lstVendedores = objeto.zona.vendedores:this.lstClientes = objeto.vendedor.clientes ;
    objeto.index==1? this.zonaActual = objeto.zona.zona : null;
    this.paginas[objeto.index].disabled = false;
    this.paginas[objeto.index].cheked = true;
    let index = this.paginas[objeto.index].index;
    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false :pag.cheked=true
    })
    
    objeto.slides.lockSwipes(false);
    objeto.slides.slideTo(objeto.index);
    objeto.slides.lockSwipeToNext(true);
    objeto.slides.lockSwipes(true);
  }

  /**
    *metodo que se ejecuta despues de iniciar la vista
    * @memberof DetalleClientePage
    */
  ngAfterViewInit() {
    this.slidesLlamados.lockSwipes(true);

  }

  /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(index, slide) {
  
   this.indiceActual = index;

    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false : pag.cheked=true
    });

    slide.lockSwipes(false);
    slide.slideTo(index);
    slide.lockSwipes(true);

    window.scrollTo({ top: 0});
  }

  /**
  * @author ignacio zapata  \"2020-11-13\
  * @desc funcion para retornar la zona desde el informe. esto con el objetivo de corregir bug cuando se cambia entre dia o mes y los datos no se actualizan en pantalla.
  * @params zona en string.
  * @return
  */
  getZona(zona: string){
    let resp: any;
    if(this.informeLlamado && this.informeLlamado.zonas){
      let filtro = this.informeLlamado.zonas.filter(informe => informe.zona === zona);
      resp = filtro && filtro[0]? filtro[0] : [];
    }
    return resp; 
  }
}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuiebreTabPage } from './quiebre-tab.page';

describe('QuiebreTabPage', () => {
  let component: QuiebreTabPage;
  let fixture: ComponentFixture<QuiebreTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuiebreTabPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuiebreTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

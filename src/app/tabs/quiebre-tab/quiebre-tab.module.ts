import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QuiebreTabPage } from './quiebre-tab.page';

import { QuiebrePageModule } from 'src/app/pages/quiebrePage/quiebre.module';
import { QuiebrePage } from 'src/app/pages/quiebrePage/quiebre.page';

const routes: Routes = [
  {
    path: '',
    component: QuiebreTabPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    QuiebrePageModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([{ path: '', component: QuiebrePage }])
  ],
  declarations: [QuiebreTabPage]
})
export class QuiebreTabPageModule {}

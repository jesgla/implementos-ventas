import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VendedorTabPage } from './vendedor-tab.page';
import { VendedorPageModule } from 'src/app/pages/vendedorPage/page/vendedor/vendedor.module';

const routes: Routes = [
  {
    path: '',
    component: VendedorTabPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VendedorPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VendedorTabPage]
})
export class VendedorTabPageModule {}

import { Component, OnInit, ViewChild} from '@angular/core';
import { IonSlides } from '@ionic/angular';

// Servicios
import { EventoService } from 'src/app/services/evento/evento.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-checkin-tab',
  templateUrl: './checkin-tab.page.html',
  styleUrls: ['./checkin-tab.page.scss'],
})
export class CheckinTabPage implements OnInit {
  
  @ViewChild(IonSlides, { static: false }) slidesCheckIns: IonSlides;

  paginas = [
    { svg: 'pin', index: 0, cheked: true, filtro: '' },
    { svg: 'home', index: 1, disabled: true, cheked: false, filtro: '' },
    { svg: 'person', index: 2, disabled: true, cheked: false, filtro: '' },
  ];

  detalleTitulo: string = '';

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  indice: number = 1;
  indiceActual: number = 0;
  rango: string = 'Mensual';
  codSucursal: string;

  informeCheckInZonas: any = {
    mensual: [],
    dia: []
  }

  informeCheckInSucursal: any = {
    mensual: [],
    dia: []
  }

  informeCheckInVendedores: any = {
    mensual: [],
    dia: []
  }

  informeMensualZonas: any[] = [];

  constructor(private _eventoService: EventoService, private _loadingService: LoadingService) { }

  async ngOnInit() {
  }

  async ionViewWillEnter() {
    await this._loadingService.presentLoading("Cargando Datos..");

    // Obtenemos los checkin asociados a casa zona.
    this.informeCheckInZonas.mensual = await this._eventoService.obtenerInformeCheckIn('mes', 'zona');
    this.informeCheckInZonas.dia = await this._eventoService.obtenerInformeCheckIn('dia', 'zona');
    this.paginas[0].filtro = 'Zonas'

    this._loadingService.hideLoading();
  }

  /**
  * @author ignacio zapata  \"2020-09-14\
  * @desc Metodo que permite mostrar todos los checkin de sucursales asociadas a una zona en especifico.
  * @params  
  * @return
  */
  async MostrarDetalle(objeto){
    await this._loadingService.presentLoading("Cargando Datos..")

    this.indiceActual = objeto.index;

    if(objeto.index==1) {
      // Se traen los datos de los check-in asociados a las sucursales pertenecientes a la sucursal que va la variable objeto.dato
      this.informeCheckInSucursal.mensual = [];
      this.informeCheckInSucursal.dia = [];

      this.informeCheckInSucursal.mensual = await this._eventoService.obtenerInformeCheckIn('mes', 'sucursal', objeto.dato );
      this.informeCheckInSucursal.dia = await this._eventoService.obtenerInformeCheckIn('dia', 'sucursal' , objeto.dato );
      this.paginas[1].filtro = 'Zona '+objeto.dato;

      // Resetamos los valodres de los checkin por sucursal.
      this.paginas[2].disabled = true;

    }else if(objeto.index==2){
      // Se traen los datos de los check-in asociados a los vendedores que hicieron checkin y que pertenecen al cod. sucursal que viene en la var objeto.dato
      this.informeCheckInVendedores.mensual = [];
      this.informeCheckInVendedores.dia = [];

      this.informeCheckInVendedores.mensual = await this._eventoService.obtenerInformeCheckIn('mes', 'nombreVendedor', objeto.dato );
      this.informeCheckInVendedores.dia = await this._eventoService.obtenerInformeCheckIn('dia', 'nombreVendedor' , objeto.dato );
      this.paginas[2].filtro = 'Sucursal - '+objeto.dato;
      this.codSucursal = objeto.dato;
    }

    this.paginas[objeto.index].disabled = false;
    this.paginas[objeto.index].cheked = true;

    let index = this.paginas[objeto.index].index;
    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false :pag.cheked=true
    })

    this._loadingService.hideLoading();
    
    objeto.slides.lockSwipes(false);
    objeto.slides.slideTo(objeto.index);
    objeto.slides.lockSwipeToNext(true);
    objeto.slides.lockSwipes(true);

    this.getDetalleTitulo(objeto.index);
  }

  /**
  * @author ignacio zapata  \"2020-09-14\
  * @desc Metodo encargado de actualizar la variable rango, cuando se ejecuta el onchange del ion-segment de diario y mensual.
  * @params  value del segment filtro. tipo string.
  * @return
  */
  filtrar(event){
    this.rango = event;
  }

    /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(index, slide) {
    if(index < 2){
      this.paginas[2].disabled = true;
    }

    this.indiceActual = index;
  
    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false : pag.cheked=true
    });

    this.getDetalleTitulo(index);
 
    slide.lockSwipes(false);
    slide.slideTo(index);
    slide.lockSwipes(true);

    window.scrollTo({ top: 0});
 
   }


  /**
  * @author ignacio zapata  \"2020-08-31\
  * @desc Metodo encargado de actualizar el string detalledeTitulo cada ves que se cambia entre paginas. ya sea manual o automaticamente.
  * @params indice seleccionado de tipo number.
  * @return
  */
  getDetalleTitulo(indice){
    this.paginas.forEach(pagina => {
      pagina.index == indice? this.detalleTitulo = pagina.filtro : '';
    })
  }

}

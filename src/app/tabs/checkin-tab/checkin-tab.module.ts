import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckinTabPage } from './checkin-tab.page';

import { TablaCheckinComponent } from 'src/app/tabs/checkin-tab/components/tabla-checkin/tabla-checkin.component';
import { MapaPage } from 'src/app/pages/Modals/mapa/mapa.page';
import { MapaPageModule }  from 'src/app/pages/Modals/mapa/mapa.module';

const routes: Routes = [
  {
    path: '',
    component: CheckinTabPage
  }
];

@NgModule({
  imports: [
    MapaPageModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckinTabPage, TablaCheckinComponent],
  entryComponents: [MapaPage],
})
export class CheckinTabPageModule {}

import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

// modals
import { MapaPage } from 'src/app/pages/Modals/mapa/mapa.page';

// Servicios
import { EventoService } from 'src/app/services/evento/evento.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-tabla-checkin',
  templateUrl: './tabla-checkin.component.html',
  styleUrls: ['./tabla-checkin.component.scss'],
})
export class TablaCheckinComponent implements OnInit, OnChanges{
  @Input() informeCheckIn: any;

  @Input() indice: number
  @Input() filtro: string;
  @Input() slidesCheckIn: IonSlides;
  @Input() sucursal: string;
  @Input() slidesCheckIns: IonSlides;
  @Input() detalleTitulo: string = '';
  @Output() public brake = new EventEmitter<any>();

  FiltroDatos: any[] = [];
  tipo: string = 'Zona';

  
  constructor(private ModalController: ModalController, private _eventoService: EventoService, private _LoadingService: LoadingService) { }

  ngOnInit() {
    this.setTipo();
  }

  ngOnChanges(change: SimpleChanges){

    if(change.filtro){
      this.FiltroDatos =   change.filtro.currentValue === 'Mensual'? this.informeCheckIn.mensual : this.informeCheckIn.dia;
    }
  }

  setTipo(){
    if(this.indice === 0) this.tipo = 'Zona';
    if(this.indice === 1) this.tipo = 'Sucursal';
    if(this.indice === 2) this.tipo = 'Vendedor';
  }

  enviarObjeto(objeto, rut?) {
    if(objeto.dato != 'TOTAL' && this.indice < 2 && objeto.cant > 0){
      this.brake.emit( objeto );
    }else if(this.indice == 2 && objeto.dato != 'TOTAL'){
      this.showMapClientAddress(rut, objeto.dato, this.sucursal);
    }
  }

  async showMapClientAddress(rut, vendedor, sucursal){
    let tipo = this.filtro === 'Mensual'? 'mes' : 'dia';
    await this._LoadingService.presentLoading('Cargando visitas...');
    let visitas = await this._eventoService.obtenerCheckIn(rut, tipo);

    this._LoadingService.hideLoading();

    if(visitas && visitas.length > 0){
      const modal = await this.ModalController.create({
        component: MapaPage,
        componentProps: {
          tipoSolicitud: 'checkinVendedor',
          rutVendedor: rut,
          nombreVendedor: vendedor,
          checkIns: visitas,
          sucursal: sucursal,
        }
      });
      await modal.present();
      const { data } = await modal.onDidDismiss();
  
      if (data.reset) {
  
      } 
    }
  }
}

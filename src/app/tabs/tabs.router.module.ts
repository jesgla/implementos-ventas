import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';


const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'zonas',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./zona-tab/zona-tab.module').then(m => m.ZonaTabPageModule)
          }
        ]
      },
      {
        path: 'llamadas',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./llamadas-tab/llamadas-tab.module').then(m => m.LlamadasTabPageModule)
          }
        ]
      },
      {
        path: 'visitas',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./checkin-tab/checkin-tab.module').then(m => m.CheckinTabPageModule)
          }
        ]
      },
      {
        path: 'combos',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./combos-tab/combos-tab.module').then(m => m.CombosTabPageModule)
          }
        ]
      },
      {
        path: 'objetivos',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./objetivos-tab/objetivos-tab.module').then(m => m.ObjetivosTabPageModule)
          }
        ]
      },
     
      {
        path: 'vendedores',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./vendedor-tab/vendedor-tab.module').then(m => m.VendedorTabPageModule)
          }
        ]
      },
      {
        path: 'clientes',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./clientes-tab/clientes-tab.module').then(m => m.ClientesTabPageModule)
          }
        ]
      },
      {
        path: 'calendario',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./calendario-tab/calendario-tab.module').then(m => m.CalendarioTabPageModule)
          }
        ]
      },
      {
        path: 'catalogo',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./catalogo-tab/catalogo-tab.module').then(m => m.CatalogoTabPageModule)
          }
        ]
      },
      {
        path: 'oportunidad',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./oportunidades-tab/oportunidades-tab.module').then(m => m.OportunidadesTabPageModule)
          }
        ]
      },
      {
        path: 'hua',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./hua-tab/hua-tab.module').then(m => m.HuaTabPageModule)
          }
        ]
      },
      {
        path: 'quiebre',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./quiebre-tab/quiebre-tab.module').then(m => m.QuiebreTabPageModule)
          }
        ]
      },
      {
        path: 'respaldo-entrega',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('src/app/pages/respaldo-documentosPage/respaldo-documentos/respaldo-documento.module').then(m => m.RespaldoDocumentoPageModule)
          }
        ]
      },
      {
        path: 'asignacion-documentos',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('src/app/pages/asignacion-documentosPage/asignacion-documentos.module').then(m => m.AsignacionDocumentosPageModule)
          }
        ]
      },
      /* {
        path: '',
        redirectTo: '/objetivos',
        pathMatch: 'full'
      } */
    ]
  },
  /* {
    path: '',
    redirectTo: '/objetivos',
    pathMatch: 'full'
  } */
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}

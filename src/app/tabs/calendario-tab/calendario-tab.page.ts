import { Component, OnInit } from '@angular/core';
import { CalendarioComponent } from 'src/app/components/calendario/calendario.component';
import { CalendarioPage } from 'src/app/pages/CalendarioPage/page/calendario/calendario.page';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
/**
 *componente que permite visualizar la agenda del vendedor
 *
 * @export
 * @class CalendarioTabPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-calendario-tab',
  templateUrl: './calendario-tab.page.html',
  styleUrls: ['./calendario-tab.page.scss'],
})
export class CalendarioTabPage implements OnInit {

  /**
   *estado del vendedor
   * @type {boolean}
   * @memberof CalendarioTabPage
   */
  clienteSeleccionado: boolean;

  /**
   *Creates an instance of CalendarioTabPage.
   * @param {DataLocalService} dataLocal
   * @memberof CalendarioTabPage
   */
  constructor(private dataLocal : DataLocalService) { 
    
  }
  
  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof CalendarioTabPage
   */
  ngOnInit() {

    
  }

  /**
   *metodo que se ejecuta al llamar el componente
   * @memberof CalendarioTabPage
   */
  async ionViewWillEnter(){
   

    await this.ValidarCliente();
  }

  /**
   *permite validar si selecciono un cliente
   * @memberof CalendarioTabPage
   */
  async ValidarCliente() {
    
    let cliente = await this.dataLocal.getCliente();
  
    if(!cliente ){
        this.clienteSeleccionado= false
    }else{
      this.clienteSeleccionado=true
    }
   
    
  }
}

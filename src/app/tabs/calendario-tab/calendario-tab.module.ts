import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';


import { CalendarioTabPage } from './calendario-tab.page';
import { CalendarioPageModule } from 'src/app/pages/CalendarioPage/page/calendario/calendario.module';

const routes: Routes = [
  {
    path: '',
    component: CalendarioTabPage
  }
];

@NgModule({
  imports: [
    CalendarioPageModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  
  declarations: [CalendarioTabPage]
})
export class CalendarioTabPageModule {}

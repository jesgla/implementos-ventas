import { Component, OnInit } from '@angular/core';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Events } from '@ionic/angular';
/**
 *Tab de catalogo
 * @export
 * @class CatalogoTabPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-catalogo-tab',
  templateUrl: './catalogo-tab.page.html',
  styleUrls: ['./catalogo-tab.page.scss'],
})

export class CatalogoTabPage implements OnInit {
  items: number;
  /**
   *Creates an instance of CatalogoTabPage.
   * @memberof CatalogoTabPage
   */
  constructor(public events : Events ,
    private dataLocal : DataLocalService) { 

    this.events.subscribe('actualizar', (data) =>{
      this.ionViewWillEnter()
  

    });
  }
  /**
   *Metodo que se ejecuta al iniciar el componente
   *
   * @memberof CatalogoTabPage
   */
  ngOnInit() {
  }
  async ionViewWillEnter() {

    await this.obtenerCarro();
  }
  async obtenerCarro() {

    let carro = await this.dataLocal.getCarroCompra();
    carro? this.items = carro.length : this.items = 0;
  }
}

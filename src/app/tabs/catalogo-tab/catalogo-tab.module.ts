import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CatalogoTabPage } from './catalogo-tab.page';
import { ArticulosPage } from 'src/app/pages/articulosPage/page/articulos/articulos.page';
import { ArticulosPageModule } from 'src/app/pages/articulosPage/page/articulos/articulos.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: CatalogoTabPage
  }
];

@NgModule({
  imports: [
    ArticulosPageModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([{ path: '', component: CatalogoTabPage }])
  ],
  declarations: [
    CatalogoTabPage]
})
export class CatalogoTabPageModule {}

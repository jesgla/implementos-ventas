import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesTabPage } from './clientes-tab.page';

describe('ClientesTabPage', () => {
  let component: ClientesTabPage;
  let fixture: ComponentFixture<ClientesTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesTabPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { LoadingController, AlertController } from '@ionic/angular';
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ModalController } from '@ionic/angular';


// modals
import { MapaPage } from 'src/app/pages/Modals/mapa/mapa.page';

/**
 *Tab de cliente
 * @export
 * @class ClientesTabPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-clientes-tab',
  templateUrl: './clientes-tab.page.html',
  styleUrls: ['./clientes-tab.page.scss'],
})
export class ClientesTabPage {
  /**
   *permite validar si obtiene listado de cliente
   * @type {boolean}
   * @memberof ClientesTabPage
   */
  obtener: boolean;

  /**
   *Objeto de vendedor
   * @type {Vendedor}
   * @memberof ClientesTabPage
   */
  vendedor: Vendedor;
  /**
   *Listado de clientes
   *
   * @type {Cliente[]}
   * @memberof ClientesTabPage
   */
  listaClientes: Cliente[];

  /**
   *Creates an instance of ClientesTabPage.
   * @param {ClienteService} clienteService
   * @param {LoadingController} loadingController
   * @param {AutentificacionService} authService
   * @param {DataLocalService} dataLocal
   * @param {AlertController} alertController
   * @memberof ClientesTabPage
   */
  constructor(
    private clienteService: ClienteService,
    public loadingController: LoadingController,
    private authService: AutentificacionService,
    private dataLocal: DataLocalService,
    public alertController: AlertController,
    private ModalController:ModalController
  ) {
    this.vendedor = new Vendedor();
  }

  /**
   *permite obtener los clientes del vendedor
   * @returns
   * @memberof ClientesTabPage
   */
  async obtenerCliente() {
    this.obtener = true;
    const loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    // await loading.present();

    this.listaClientes = await this.clienteService.obtenerClientes(this.vendedor);
    this.listaClientes.sort((a, b) => {
      if (a.nombre > b.nombre) {
        return 1;
      }
      if (a.nombre < b.nombre) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    //console.log('res',res)
    //this.listaClientes  =[];
    //await loading.dismiss();

    this.obtener = false;
    return this.listaClientes;
  }
  /**
   *Metodo que se ejecuta cada vez que se llama el componente
   *
   * @memberof ClientesTabPage
   */
  async ionViewWillEnter() {
    await this.obtenerUsuario();
    await this.obtenerCliente();
    this.listaClientes = await this.clienteService.calcularDistancia(this.listaClientes);
  }

  /**
   *Permite obtener al usuario
   * @memberof ClientesTabPage
   */
  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
    this.vendedor = JSON.parse(usuario);
   
    if (this.vendedor.codBodega == 'AREA ADMINISTRACION' || this.vendedor.codBodega == '') {
      let vt =await this.dataLocal.getItem('vendedor');
      this.vendedor.codEmpleado = vt._id;
     
    }
  }

  /**
  * @author ignacio zapata  \"2020-08-28\
  * @desc Metodo que permite mostrar el modal que se encarga de mostrar el mapa de googlemaps.
  * @params 
  * @return
  */
  async showMapClientAddress(){
    const modal = await this.ModalController.create({
      component: MapaPage,
      componentProps: {
        tipoSolicitud: 'direccionClientes',
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();

    if (data.reset) {

    } 
  }
}

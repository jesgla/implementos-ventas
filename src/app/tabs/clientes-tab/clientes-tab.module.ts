import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ClientesTabPage } from './clientes-tab.page';
import { ClientesPage } from 'src/app/pages/clientePage/pages/clientes/clientes.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { ClienteComponentsModule } from 'src/app/pages/clientePage/components/cliente-components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';


import { MapaPageModule }  from 'src/app/pages/Modals/mapa/mapa.module';
import { MapaPage } from 'src/app/pages/Modals/mapa/mapa.page';



@NgModule({
  imports: [
    MapaPageModule,
    PipesModule,
    ComponentsModule,
    ClienteComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([{ path: '', component: ClientesTabPage }])
  ],
  entryComponents: [MapaPage],
  declarations: [
    ClientesTabPage,
    ClientesPage
  ]
})
export class ClientesTabPageModule {}

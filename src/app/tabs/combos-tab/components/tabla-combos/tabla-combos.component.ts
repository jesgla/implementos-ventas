import { Component, OnInit, Input, Output , EventEmitter} from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-tabla-combos',
  templateUrl: './tabla-combos.component.html',
  styleUrls: ['./tabla-combos.component.scss'],
})
export class TablaCombosComponent implements OnInit {
  @Input() listado: any;
  @Input() lstVendedores: any;
  @Input()index : number;
  @Input() indice: number
  @Input() filtro: string;
  @Input() slidesCombos: IonSlides;
  @Input() sucursal: string;
  @Input() slidesLlamados: IonSlides;
  @Input()titulo : string;
  @Output() public brake = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {

  }

  enviarObjeto(objeto) {
    this.brake.emit(objeto);
  }
}
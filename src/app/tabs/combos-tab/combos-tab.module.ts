import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CombosTabPage } from './combos-tab.page';
import { TablaCombosComponent } from './components/tabla-combos/tabla-combos.component';

const routes: Routes = [
  {
    path: '',
    component: CombosTabPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CombosTabPage, TablaCombosComponent]
})
export class CombosTabPageModule {}

import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';

// Servicio
import { SupervisorService } from 'src/app/services/supervisor/supervisor.service';
import {  LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-combos-tab',
  templateUrl: './combos-tab.page.html',
  styleUrls: ['./combos-tab.page.scss'],
})
export class CombosTabPage implements OnInit {
  @ViewChild(IonContent, {static: false}) content: IonContent;

  lstCombos : any;
  obtener: boolean;
  rango: string = 'Mensual';
  lstTiendas: any;
  lstVendedores: any;
  indiceActual: number = 0;

  paginas = [
    { svg: 'pin', index: 0, cheked: true },
    { svg: 'home', index: 1, disabled: true, cheked: false },
    { svg: 'man', index: 2, disabled: true, cheked: false }
  ];
  constructor(private supervisorServices: SupervisorService, private _LoadingService: LoadingService) { }

  async ngOnInit() {

  }
  async ionViewWillEnter() {
    this.obtener = true;
    this.lstCombos = await this.supervisorServices.informeCombos();
    this.obtener = false;

  }
  filtrar(event) {
    this.rango =event.detail.value;
   
  }
  async  habilitar(objeto) {
    if(objeto.index < 3){
 
    this.indiceActual = objeto.index;

    this.paginas[objeto.index].disabled = false;
    this.paginas[objeto.index].cheked = true;
    let index = this.paginas[objeto.index].index;

    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false :pag.cheked=true
    }) 
    objeto.index==1?
    this.lstTiendas = objeto.zona:
    this.lstVendedores = objeto.zona;

    objeto.slides.lockSwipes(false); 
    objeto.slides.slideTo(objeto.index);
    objeto.slides.lockSwipeToNext(true);
    objeto.slides.lockSwipes(true);
    this.content.scrollToTop();
    }
  }

  cambioPagina(index, slide) {
    if(index < 2){
      this.paginas[2].disabled = true;
    }

    this.indiceActual = index;
   
    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false : pag.cheked=true
    });
  

    slide.lockSwipes(false);
    slide.slideTo(index);
    slide.lockSwipes(true);

    window.scrollTo({ top: 0});

  }
}

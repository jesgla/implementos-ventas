import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';
import { SupervisorService } from 'src/app/services/supervisor/supervisor.service';
import { Objetivos } from 'src/app/interfaces/vendedor/objetivos';
import { IonSlides } from '@ionic/angular';
import { Informe } from 'src/app/interfaces/informe/informe';

import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-zona-tab',
  templateUrl: './zona-tab.page.html',
  styleUrls: ['./zona-tab.page.scss'],
})
export class ZonaTabPage {
  listaObjetivos: Objetivos[];
  /**
   *elemento slider
   * @type {IonSlides}
   * @memberof DetalleClientePage
   */
  @ViewChild(IonSlides, { static: false }) slidesAdmin: IonSlides;

  /**
   *color de elemento
   * @type {*}
   * @memberof DetalleClientePage
   */
  color: any;
  rango: string = 'Mensual';
  /**
   *arreglo de paginas de slider
   * @memberof DetalleClientePage
   */
  paginas = [
    { svg: 'pin', index: 0, cheked: true, filtro: '' },
    { svg: 'home', index: 1, disabled: true, cheked: false, filtro: '' },
    { svg: 'apps', index: 2, disabled: true, cheked: false, filtro: '' },
  ];

  /**
   *configuracion de slider
   * @memberof DetalleClientePage
   */
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  indice: number;
  totalVenta: number;
  totalMeta: number;
  promedio: number;
  sucursales: Objetivos[];
  canales: Objetivos[];
  vendedores: Objetivos[];
  totalVentaSuc: number;
  totalMetaSuc: number;
  promedioSuc: number;
  totalVentaCan: number;
  totalMetaCan: number;
  promedioCan: number;
  promedioVen: number;
  totalMetaVen: number;
  totalVentaVen: number;
  detalleTitulo: string = '';
  indiceActual: number = 0;
 
  resumenZona :Informe;
  resumenSucursal :Informe;
  resumenTienda :Informe;
  constructor(
    private authService: AutentificacionService,
    private supervisorService: SupervisorService,
    private _loadingService: LoadingService ) {
    this.indice = 1;
    this.totalVenta = 0;
    this.totalMeta = 0;
    this.promedio = 0;
    this.totalVentaSuc = 0
    this.totalMetaSuc = 0
    this.promedioSuc = 0
    this.totalVentaCan = 0
    this.totalMetaCan = 0
    this.promedioCan = 0
    this.totalVentaVen = 0
    this.totalMetaVen = 0
    this.promedioVen = 0
    this.resumenZona = new Informe();
    this.resumenSucursal = new Informe();
    this.resumenTienda = new Informe();
  }
  ngOnInit() {

  }
 
  /**
      *metodo que se ejecuta al llamar el componente
      * @memberof ObjetivosTabPage
      */
  async ionViewWillEnter() {
    await this.actualizarDatos();
  }

  async getDatosZona(){
    this.listaObjetivos = await this.supervisorService.obtenerZonas('', 'ZONA');

    this.resumenZona = await this.setInforme('Zonas',this.listaObjetivos);
  }

  setInforme(titulo,objetivos){
    let informe = new Informe();
    informe.titulo=titulo;
    let arr =[];
    objetivos.map(async (zona) => {
      if(zona.nombreVendedor){
        let porcentaje = Math.trunc(zona.Venta / zona.MetaAcumulada * 100);
        let porcentajeD =Math.trunc(zona.VentaDia / zona.MetaDia * 100);
        zona.porcentaje = 0;
  
        if (isFinite(porcentaje) && !isNaN(porcentaje)) {
  
          zona.porcentaje = porcentaje;
        }
        if (isFinite(porcentajeD) && !isNaN(porcentajeD)) {
  
          zona.porcentajeD = porcentajeD;
        }
        informe.totalVenta += zona.Venta;
        informe.totalMeta += zona.MetaAcumulada;
        let promedio = informe.totalVenta / informe.totalMeta * 100;
        informe.promedio = (isFinite(promedio) && !isNaN(promedio)) ? promedio : 0;
  
        informe.totalVentaD += zona.VentaDia;
        informe.totalMetaD += zona.MetaDia;
        let promedioD = informe.totalVentaD / informe.totalMetaD * 100;
        informe.promedioD = (isFinite(promedioD) && !isNaN(promedioD)) ? promedioD : 0;
        
        arr.push(zona)
      }
      
      
    });
    informe.body=arr;
    return informe;
  }

  filtrar(event) {
    this.rango =event.detail.value;
  }

  async  habilitar(objeto) {
    
    this.indiceActual = objeto.indice;
    this.updateChecked(objeto);

    if(objeto.indice <= 2){
      await this._loadingService.presentLoading('Cargando Datos..');

      switch (objeto.tipo) {
        case 'SUCURSAL':
          this.sucursales = await this.supervisorService.obtenerZonas(objeto.zona.nombreVendedor, objeto.tipo);
    
          this.resumenSucursal = await this.setInforme('Tiendas',this.sucursales);
          break;
        case 'CANAL':
          this.canales = await this.supervisorService.obtenerZonas(objeto.zona.nombreVendedor, objeto.tipo);
          this.resumenTienda = await this.setInforme('Ventas',this.canales);

      
          break;
        case 'VENDEDOR':
          this.vendedores = await this.supervisorService.obtenerZonas(objeto.zona.nombreVendedor, objeto.tipo);
          this.vendedores.map(async (zona) => {
            let porcentaje = Math.trunc(zona.Venta / zona.MetaAcumulada * 100);

            zona.porcentaje = 0;
            if (isFinite(porcentaje) || isNaN(porcentaje)) {
              zona.porcentaje = porcentaje;
            }

            this.totalVentaVen += zona.Venta;
            this.totalMetaVen += zona.MetaAcumulada;

            let promedio = this.totalVentaVen / this.totalMetaVen * 100;
            this.promedioVen = isFinite(promedio) && isNaN(promedio) ? promedio : 0;
          })
          break;
      }

      this.paginas[objeto.indice].disabled = false;
      this.paginas[objeto.indice].cheked = true;
      let index = this.paginas[objeto.indice].index;
   
      this.paginas.map(async (pag) => {
        pag.index != index ? pag.cheked = false : null;
      })
      
      objeto.slides.lockSwipes(false);
      objeto.slides.slideTo(objeto.indice);
      objeto.slides.lockSwipeToNext(true);
      objeto.slides.lockSwipes(true);

      this.getDetalleTitulo(objeto.indice);
      this._loadingService.hideLoading();
    }

    window.scrollTo({ top: 0});
  }

  /**
    *metodo que se ejecuta despues de iniciar la vista
    * @memberof DetalleClientePage
    */
  ngAfterViewInit() {
    this.slidesAdmin.lockSwipes(true);

  }

  /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(index, slide) {
    if(index < 2){
      this.paginas[2].disabled = true;
    }

    this.indiceActual = index;

    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false : pag.cheked = true;
    });
    this.getDetalleTitulo(index);

    slide.lockSwipes(false);
    slide.slideTo(index);
    slide.lockSwipes(true);
    window.scrollTo({ top: 0});

  }
 
  logoutUser() {
    this.authService.logout();
  }

  /**
  * @author ignacio zapata  \"2020-08-31\
  * @desc Metodo encargado de actualizar el ion-segment-button que se selecciona al presionar una fila desde las tablas.
  * @params dato de tipo number, asociado al indice que fue seleccionado para ser mostrado.
  * @return
  */
  updateChecked(objeto){
    let index = objeto.indice
    this.paginas.forEach(pagina => {
      pagina.index == index ?  pagina.filtro = objeto.zona.nombreVendedor: '';
      pagina.index === ( index -1 )? pagina.cheked = true : pagina.cheked = false;
    })
  }

  /**
  * @author ignacio zapata  \"2020-08-31\
  * @desc Metodo encargado de actualizar el string detalledeTitulo cada ves que se cambia entre paginas. ya sea manual o automaticamente.
  * @params indice seleccionado de tipo number.
  * @return
  */
  getDetalleTitulo(indice){
    this.paginas.forEach(pagina => {
      pagina.index == indice? this.detalleTitulo = pagina.filtro : '';
    })
  }

  async actualizarDatos(){
    this.resumenZona = new Informe();
    this.paginas[1].disabled = true;

    await this._loadingService.presentLoading('Cargando Datos...');
    await this.getDatosZona();
    this._loadingService.hideLoading();
  }
}


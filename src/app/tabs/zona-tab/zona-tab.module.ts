import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ZonaTabPage } from './zona-tab.page';
import { ObjetivoAdministradorPageModule } from 'src/app/pages/objetivosPage/page/objetivo-administrador/objetivo-administrador.module';

import { ComponentsModule } from 'src/app/components/components.module';
import { TablaTiendasZonalComponent } from './components/tabla-tiendas-zonal/tabla-tiendas-zonal.component';

import { PipesModule } from 'src/app/pipes/pipes.module';



@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ObjetivoAdministradorPageModule,
    RouterModule.forChild([{path: '',component: ZonaTabPage}])
  ],
  declarations: [ZonaTabPage, TablaTiendasZonalComponent]
})
export class ZonaTabPageModule {}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonaTabPage } from './zona-tab.page';

describe('ZonaTabPage', () => {
  let component: ZonaTabPage;
  let fixture: ComponentFixture<ZonaTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZonaTabPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonaTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

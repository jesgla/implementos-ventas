import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-tabla-tiendas-zonal',
  templateUrl: './tabla-tiendas-zonal.component.html',
  styleUrls: ['./tabla-tiendas-zonal.component.scss'],
})
export class TablaTiendasZonalComponent implements OnInit {
  @Input() resumen : any;
  @Input() indice : number
  @Input() filtro : string;
  @Input() detalleTitulo: string = '';
  @Input() slidesAdmin: IonSlides;
  @Input() sucursal : string;
  @Output() public brake = new EventEmitter<any>();
  @Output() public actualizarDatos = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  enviarObjeto(objeto){
    if(objeto.indice < 3){
      this.brake.emit(objeto);
    }
  }

  /**
  * @author ignacio zapata  \"2020-11-09\
  * @desc Metodo utilizado para emitir un eventemitter que permite actualizar los datos del informe de ventas.
  * @params 
  * @return
  */
  ActualizarDatos(){
    if(this.indice == 1){
      this.actualizarDatos.emit();
    }
  }
}

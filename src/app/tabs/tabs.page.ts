import { Component, OnInit, OnDestroy } from '@angular/core';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

// servicios
import { UsuarioService } from '../services/usuario/usuario.service';
import { SeccionesService } from 'src/app/services/secciones/secciones.service'
import { DataLocalService } from '../services/dataLocal/data-local.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})

export class TabsPage implements OnInit, OnDestroy{

  seccion: string = '';
  tabs: any[];
  tabsVendedor: any[];
  tabsAdmin: any[];
  tabsTiendas: any[];
  tabsBodegas: any[];
  hideTabs: boolean = false;
  seccionSelected1$: Subscription;

  constructor(
    private dataLocal: DataLocalService,
    public backgroundMode: BackgroundMode,
    public platform: Platform,
    private _seccionService: SeccionesService,
    private _UsuarioService: UsuarioService) {
  
    this.tabsVendedor = [
      { nombre: 'Obj.', router: 'objetivos', icon: 'home', selected: 'selected' },
      { nombre: 'Clientes', router: 'clientes', icon: 'person' },
      { nombre: 'Agenda', router: 'calendario', icon: 'calendar' },
      { nombre: 'Oportunidad', router: 'oportunidad', icon: 'logo-pinterest' },
      { nombre: 'Catálogo', router: 'catalogo', icon: 'list-box' },
    ];

    this.tabsAdmin = [
      { nombre: 'Zona', router: 'zonas', icon: 'home', selected: 'selected' },
      { nombre: 'Llamada', router: 'llamadas', icon: 'call' },
      { nombre: 'Combos', router: 'combos', icon: 'cube' },
      { nombre: 'Visita', router: 'visitas', icon: 'checkbox-outline'},
 /*      { nombre: 'Oport', router: 'oportunidad', icon: 'logo-pinterest' }, */
      { nombre: 'VT', router: 'vendedores', icon: 'person' },
    ]

    this.tabsTiendas = [
      {nombre: 'Detalles SKU', router: 'hua', icon: 'qr-scanner',  selected: 'selected' },
      ...this.isUserBodegueroOrCajero(),
      ...this.AccesoVerAsignarDocumentos(),
      /* {nombre: 'Quiebre', router: 'quiebre', icon: 'paper' } */
    ]


    this.tabsBodegas = [
      {nombre: 'Detalles SKU', router: 'hua', icon: 'qr-scanner',  selected: 'selected' },
      /* {nombre: 'Quiebre', router: 'quiebre', icon: 'paper' } */
    ]

    // Seteamos una observable que verifique cuando la seccion cambia, con el objetivo de actualizar los tabs.
    this.seccionSelected1$ = this._seccionService.seccionObservable$.subscribe( async redirect => {
      this.tabs = [];
      await this.cargarTabs();
    });
  }

  /**
   *Metodo que se ejecuta al iniciar el componente
   * @memberof TabsPage
   */
  async ngOnInit() {

  }

  async ionViewWillEnter(){
    await this.cargarTabs();
  }

  /**
   *Metodo que se ejecuta al destruir el componente
   * @memberof TabsPage
   */
  ngOnDestroy(){
    this.seccionSelected1$.unsubscribe();
  }

  async cargarTabs() {
    this.seccion = await this.dataLocal.getItem('seccion');
    this.setTheme();

    switch(this.seccion){
      case 'vendedores':
          this.tabs = (!this._UsuarioService.isZonalAccount())? this.tabsVendedor : this.tabsAdmin;
          break;
      case 'tiendas':
          this.tabs = this.tabsTiendas;
          break;
      case 'bodegas':
          this.tabs = this.tabsBodegas;
          break;
      default: 
          console.error('Seccion no habilitada');
          break;
    }

  }

  setTheme(){
    let root = document.documentElement;

    if(this.seccion === 'tiendas'){
      root.style.setProperty('--ion-color-theme', '#005db9');

    }else if(this.seccion === 'vendedores'){
      root.style.setProperty('--ion-color-theme', '#303138');

    }
  }

  isUserBodegueroOrCajero(){
    if(this._UsuarioService.isUserAllowedToSaveDocs()) return [{nombre: 'Respaldar Entrega', router: 'respaldo-entrega', icon: 'clipboard' }]
    return [];
  }

  AccesoVerAsignarDocumentos(){
    if(this._UsuarioService.AccesoVerAsignarDocumentos()) return [{nombre: 'Ultima Milla', router: 'asignacion-documentos', icon: 'car' }];
    return [];
  }
}

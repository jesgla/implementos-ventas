import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OportunidadesTabPage } from './oportunidades-tab.page';
import { OportunidadesPageModule } from '../../pages/oportunidadesPage/oportunidades.module';
import { OportunidadesPage } from '../../pages/oportunidadesPage/oportunidades.page';

const routes: Routes = [
  {
    path: '',
    component: OportunidadesTabPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OportunidadesPageModule,
    RouterModule.forChild([{ path: '', component: OportunidadesPage }])
  ],
  declarations: [OportunidadesTabPage]
})
export class OportunidadesTabPageModule {}

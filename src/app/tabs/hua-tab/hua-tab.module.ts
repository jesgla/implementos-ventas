import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HuaTabPage } from './hua-tab.page';
import {  HuaPageModule } from 'src/app/pages/HuaPage/hua.module';

const routes: Routes = [
  {
    path: '',
    component: HuaTabPage
  }
];

@NgModule({
  imports: [
    HuaPageModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([{ path: '', component: HuaTabPage }])
  ],
  declarations: [HuaTabPage]
})
export class HuaTabPageModule {}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HuaTabPage } from './hua-tab.page';

describe('ProductosTabPage', () => {
  let component: HuaTabPage;
  let fixture: ComponentFixture<HuaTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HuaTabPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HuaTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

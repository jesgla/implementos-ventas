import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ObjetivosTabPage } from './objetivos-tab.page';
import {NgxEchartsModule} from 'ngx-echarts';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { ObjetivoVendedorPageModule } from 'src/app/pages/objetivosPage/page/objetivo-vendedor/objetivo-vendedor.module';

@NgModule({
  imports: [
    /* ComponentsModule,
    PipesModule,
    NgxEchartsModule, */
    CommonModule,
    FormsModule,
    IonicModule,
    ObjetivoVendedorPageModule,
    RouterModule.forChild([{ path: '', component: ObjetivosTabPage }])
  ],
  declarations: [ObjetivosTabPage]
})
export class ObjetivosTabPageModule {}

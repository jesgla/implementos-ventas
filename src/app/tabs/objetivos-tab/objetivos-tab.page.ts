import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { Objetivos } from 'src/app/interfaces/vendedor/objetivos';
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';
import { Evento } from 'src/app/interfaces/cliente/evento';
import { EventoService } from 'src/app/services/evento/evento.service';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
/**
 *componente de objetivos diarios del vendedor
 * @export
 * @class ObjetivosTabPage
 * @implements {OnInit}
 */
@Component({
    selector: 'app-objetivos-tab',
    templateUrl: './objetivos-tab.page.html',
    styleUrls: ['./objetivos-tab.page.scss'],
})
export class ObjetivosTabPage {
  


}

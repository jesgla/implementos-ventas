import { Injectable } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ToastController, NavController } from '@ionic/angular';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Cliente } from 'src/app/interfaces/cliente/cliente';

/**
 *Servicio para ejecutar funciones nativas
 * @export
 * @class PluginService
 */
@Injectable({
  providedIn: 'root'
})

export class PluginService {

  /**
   *objeto cliente
   * @type {Cliente}
   * @memberof PluginService
   */
  detalleCliente: Cliente;

  /**
   *Creates an instance of PluginService.
   * @param {CallNumber} callNumber
   * @param {ToastController} toastController
   * @param {SocialSharing} socialSharing
   * @param {LaunchNavigator} launchNavigator
   * @param {NavController} navCtrl
   * @memberof PluginService
   */
  constructor(
    private callNumber: CallNumber,
    public toastController: ToastController,
    private socialSharing: SocialSharing,
    private launchNavigator: LaunchNavigator,
    private navCtrl: NavController) { }

  /**
   *Permite ejecutar plugin segun llamado
   * @param {*} accion
   * @param {*} cliente
   * @memberof PluginService
   */
  ejecutarSocialShare(accion, cliente) {
  
    let fono = cliente.contactos[0].telefono;
    let correo = cliente.contactos[0].correo
    switch (accion) {
      case 'Llamar':

        this.callNumber.callNumber(fono, true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
        break;

      case 'Whatsapp':

        this.socialSharing.shareViaWhatsAppToReceiver(fono, 'Hola')
          .then((resp) => {
            console.log('resp', resp)
          })
          .catch((error) => {
            console.log('error', error)
          })
        break;
      case 'Correo':
        this.socialSharing.shareViaEmail('Consulta ', 'Hola', [correo])
          .then((resp) => {
            console.log('se envio', resp)
          })
          .catch((error) => {
            console.log('error', error)
          })
        break;

      case 'Check-in':
  
        this.navCtrl.navigateForward(['/tabs/objetivos']);
        break;
    }
  }

  /**
   *Permite abrir aplicacion de navegacion
   * @param {*} detalleCliente
   * @memberof PluginService
   */
  abrirNavegacion(detalleCliente) {
    /* let geolocalizacion = `${detalleCliente.direccionDefaultLatitud},${detalleCliente.direccionDefaultLongitud}`;
    let lng =  Number(detalleCliente.direccionDefaultLongitud.replace(',','.'));
    let lat = Number(detalleCliente.direccionDefaultLatitud.replace(',','.'));
    if(geolocalizacion.length<5){
      this.presentToast();
      return
    }   */
    this.launchNavigator.navigate(detalleCliente.direcciones[0].direccionCompleta); 
  }
  abrirDireccion(direccion){
  
    this.launchNavigator.navigate(direccion.direccionCompleta);
  }
  /**
   *Permite enviar un mensaje al usuario
   * @memberof PluginService
   */
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'CLIENTE NO TIENE UBICACIÓN ENCONTRADA. NO ES POSIBLE USAR EL MAPA!!',
      duration: 2000
    });
    toast.present();
  }
}

import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FiltrosQuiebresService {
  filtrosQuiebres: any[];
  filtroAplicado: any;
  CategoriasFiltros: any[];
  showFiltros: boolean = false;

  menuFiltrosQuiebreSubject$: Subject<any> = new Subject();
  menuFiltrosQuiebreObservable$: Observable<any> = this.menuFiltrosQuiebreSubject$.asObservable();


  constructor() {

   }

  
  /**
  * @author ignacio zapata 25-08-2020
  * @desc  Metodo que se ejecuta cuando se cambia de filtro, seleccionando una nueva opcion en el select de filtros.
  * @params nombre del filtro seleccionado "string"  
  * @return
  */
   FiltrarQuiebre(data: string){
    let filtro = this.filtrosQuiebres.filter(item => item.nombre.trim() === data.trim());
    filtro[0] ? this.filtroAplicado = filtro[0] : '';
  }

  showMenuFiltrosQuiebre(event){
    this.menuFiltrosQuiebreSubject$.next( event );
  }


  /**
  * @author ignacio zapata 25-08-2020
  * @desc  Metodo que se gatilla para actualizar las categorias que se mostrarán en el filtro de los productos quebrados.
  * @params array con nombres de categorias en string.  
  * @return
  */
  setCategoriasFiltro(categorias){
    this.CategoriasFiltros = [];

    this.CategoriasFiltros.push(...categorias)
  }

}

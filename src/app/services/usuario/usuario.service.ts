import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

// interfaces
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';

//servicios
import { NotificationsService } from '../notifications/notifications.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private LogedUser: Vendedor;
  private JwtLogedUser: string;

  private userReloadSubject$: Subject<any> = new Subject();
  readonly userReload$: Observable<any> = this.userReloadSubject$.asObservable();

  constructor(private _NotificationsService: NotificationsService) { }

  setUserLoged(user: Vendedor){
    if(user){
      this.LogedUser = user;
      this._NotificationsService.updateToken(user); 
    }
  }

  getUserLoged() :Vendedor{
    return this.LogedUser;
  }

  setJwtUserLoged(jwt: string){
    if(!jwt){
       console.error('Token no se informa');
       return;
    }
    this.JwtLogedUser = jwt;
  }

  getTokenUserLoged() :string{
    return this.JwtLogedUser;
  }


  reloadUser(){
    this.userReloadSubject$.next();
  }

  
  isZonalAccount(): boolean{
    if(this.LogedUser.codBodega != 'AREA ADMINISTRACION' && this.LogedUser.codBodega != '') return false;
    return true;
  }

  removeUserLoged(){
    this.LogedUser = null;
    this.JwtLogedUser = null;
  }

  /**
  * @author ignacio zapata  \"2021-04-09\
  * @desc Permite validar si el usuario tiene perfil de bodeguero, cajero, vt o venta de meson. para respaldar documentos de entrega.
  */
  isUserAllowedToSaveDocs(){
    if(this.LogedUser.codPerfil == 4 || this.LogedUser.codPerfil == 5 || this.LogedUser.codPerfil == 7 || this.LogedUser.codPerfil == 10) return true;
    return false;
  }

  AccesoVerAsignarDocumentos(){
    if(this.LogedUser.codPerfil == 7 || this.LogedUser.codPerfil == 5 || this.LogedUser.codPerfil == 6 ) return true;
    return false;
  }
}
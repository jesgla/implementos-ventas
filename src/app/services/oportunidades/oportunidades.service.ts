import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service'

//environment
import { environment } from 'src/environments/environment';

//interfaces
import { Oportunidad, oportunidades_conceptos } from 'src/app/interfaces/oportunidades/oportunidades';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OportunidadesService {
  filtros : {estado: string, periodo: string, orderBy: string, tipoContacto: string} = {estado: 'abiertas', periodo: 'todos', orderBy: 'fecha_limite', tipoContacto: 'todos'};

  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });

  constructor(private httpClient: HttpClient,private _dataLocal: DataLocalService) { }

  /**
  * @author ignacio zapata  \"2020-11-18\
  * @desc Metodo utilizado para obtener los conceptos de creacion y cierre asociados a las oportunidades.
  * @params variable tipo, string.
  * @return
  */
  async getOportunidadesConceptos(tipo){
    let url = `${environment.endPointApiMovil}/oportuniades_conceptos/?tipo=${tipo}`;
    let resp: oportunidades_conceptos[] = [];

    try{
        resp = await this.httpClient.get(url).pipe(
        map( (resp : any) => {
          if(resp && !resp.error && resp.data){
            return resp.data;
          }else{
            console.log('error obteniendo los datos', resp);
            return [];
          }
        }
      )).toPromise();
    }catch(e){
      console.log('No ha sido posible obtener los conceptos de oportunidades');
      this._dataLocal.presentToast('No ha sido posible obtener los conceptos de oportunidades desde el serviddor', 'danger');
    }
    return resp;
  }

  async setNewOportunidad(oportunidad : Oportunidad){
    let url = `${environment.endPointApiMovil}/newOportunidad`;

    try{
      let consulta = await this.httpClient.post<any>(url, { oportunidad: oportunidad},  { headers: this.header }).toPromise();
      consulta && !consulta.error ? console.log('oportunidad creada exitosamente.') : console.log('error creando la oportunidad', consulta.msg); 
    }catch(e){
      console.log('error creando la oportunidad', e);
    }
  }

  async getOportunidades(filtros: any){
    let url = `${environment.endPointApiMovil}/getOportunidades`;
    let resp: Oportunidad[] = [];

    try{
      resp = await this.httpClient.post<any>(url, filtros,  { headers: this.header }).pipe(
        map(resp => {
          return resp.data;
        })
      ).toPromise();

    }catch(e){
      console.log('No ha sido posible obtener las oportunidades...', e );
      this._dataLocal.presentToast('No ha sido posible obtener las oportunidades', 'danger');
    }
    return resp;
  }

  async finalizarOportunidad(oportunidad: Oportunidad){
    let url = `${environment.endPointApiMovil}/finalizarOportunidad`;
    let resp: Oportunidad[] = [];

    try{
      resp = await this.httpClient.post<any>(url, oportunidad,  { headers: this.header }).pipe(
        map(resp => {
          if(resp.error){
            this._dataLocal.presentToast('Error intentando finalizar la oportunidad', 'danger');
            return resp;
          }else{
            this._dataLocal.presentToast('Oportunidad finalizada con éxito.', 'success');
            return resp;
          }
        })
      ).toPromise();
    }catch(e){
      console.log('No ha sido posible finalizar la Oportunidad.', e);
      this._dataLocal.presentToast('No ha sido posible finalizar la oportunidad.', 'danger');
    }
    return resp;
  }

  async getResumenOportunidadVendedor(rutVendedor, fecha: string){
    let url = `${environment.endPointApiMovil}/getResumenOportunidadesVendedor/?rutVendedor=${rutVendedor}&fecha_creacion=${fecha}`;
    let resp;

    try{
      resp = await this.httpClient.get<any>(url).pipe(
        map(resp => {
          if(resp.error) this._dataLocal.presentToast('Error intentando obtener el resumen de oportunidades', 'danger');
          return resp.data;
        })
      ).toPromise();
    }catch(e){
      console.log('Error intentando obtener el resumen de oportunidades.', e);
      this._dataLocal.presentToast('Error intentando obtener el resumen de oportunidades', 'danger');
      resp = {}
    }
    return resp;
  }
}

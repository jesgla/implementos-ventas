import { TestBed } from '@angular/core/testing';

import { RespaldoDocsService } from './respaldo-docs.service';

describe('RespaldoDocsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RespaldoDocsService = TestBed.get(RespaldoDocsService);
    expect(service).toBeTruthy();
  });
});

import { LoadingService } from './../loading/loading.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

// servicios
import { DataLocalService } from './../dataLocal/data-local.service';

// interfaces
import { Respaldo, DocumentoRespaldo, MotivoNoEntrega } from 'src/app/interfaces/respaldoDocs/respaldo';

@Injectable({
  providedIn: 'root'
})
export class RespaldoDocsService {
  private misDocumentosReloadSubject$: Subject<any> = new Subject();
  readonly misDocumentosReload$: Observable<any> = this.misDocumentosReloadSubject$.asObservable();

  constructor(private htpp: HttpClient, private _dataLocal: DataLocalService, private _LoadingService: LoadingService) { }

  async saveRespaldo(respaldo: Respaldo, tipo = 'guardado'): Promise<boolean>{
    let respuesta: boolean = false;

    try{
      // let url: string =  `${environment.endPointApiMovil}/respaldos/saveDocumento`;
      let url: string =  `${environment.endPointApiMovil}/respaldos/saveDocumento`;

      let consulta: any = await this.htpp.post(url, {respaldo: respaldo}).toPromise();
      if(!consulta.error){
        this._dataLocal.presentToast(`Documento ha sido ${tipo} exitosamente.`, 'success');
        respuesta = true;
      }else{
        this._dataLocal.presentToast(`El documento no ha sido ${tipo}. Favor volver a intentar`, 'danger', 2000);
      }
    }catch(e){
      this._dataLocal.presentToast(`El documento no ha sido ${tipo}. Favor volver a intentar`, 'danger',2000);
    }

    return respuesta;
  }

  async getMisRespaldos(codEmpleado): Promise<DocumentoRespaldo[]>{
    let respuesta: DocumentoRespaldo[] = []; 

    try{
      let url: string =  `${environment.endPointApiMovil}/respaldos/getMisRespaldos/?codEmpleado=${codEmpleado}`;
    
      let consulta: any= await this.htpp.get(url).toPromise();
      if(consulta.error) this._dataLocal.presentToast('No ha sido posible cargar el historial de respaldos', 'danger');
      respuesta = consulta.data;
    }catch(e){
      this._dataLocal.presentToast('No ha sido posible cargar el historial de respaldos', 'danger');
      console.log('ha ocurrido problemas intentando obtener el historial de respaldos', e);
      respuesta = [];
    }
    
      return respuesta;
  }
  

  async getDocumentoDisponibleDetails(tipo: string, folio: number){
    let respuesta;

    try{

      let url: string =  `${environment.endPointApiMovil}/respaldos/getDocumentoDisponible/?folio=${folio}&tipo=${tipo}`;

      let consulta: any= await this.htpp.get(url).toPromise();

      if(!consulta.error && consulta.data && consulta.data[0]){
        respuesta = consulta.data[0];
      }else{
        respuesta = null;
      }
    }catch(e){
      this._dataLocal.presentToast('No ha sido posible conectar con el servidor', 'danger');
      console.log('No ha sido posible obtener el respaldo del documento consultado', e);
    }
    return respuesta;
  }

  async getDetalleDocumentoRespaldado(intRegistro: number, tipo: string){
    let respuesta;

    try{
      let url: string =  `${environment.endPointApiMovil}/respaldos/getDocumento/?intRegistro=${intRegistro}&tipo=${tipo}`;
  
      let consulta: any = await this.htpp.get(url).toPromise();
      if(consulta && !consulta.error){
        respuesta = consulta.data[0];
      }
    }catch(e){
      console.log('No ha sido posible encontrar el detalle del documento folio: '+intRegistro, e);
      this._dataLocal.presentToast('No ha sido posible cargar el detalle del documento respaldado', 'danger');
    }

    return respuesta;
  }


  async getTransportistas(texto: string){
    let data = [];
    try{
      // let url: string =  `${environment.endPointApiMovil}/respaldos/transportistas?texto=${texto}`;
      let url: string =  `${environment.endPointApiMovil}/respaldos/transportistas?texto=${texto}`;

      let consulta: any = await this.htpp.get(url).toPromise();
      if(consulta && !consulta.error){
        data = consulta.data;
      }else{
        this._dataLocal.presentToast('No ha sido posible obtener los transportistas desde AX', 'danger', 2000);
      }
    }catch(e){
      console.log('No ha sido posible contactar con el servidor para obtener transportitas', e);
      this._dataLocal.presentToast('No ha sido posible contactar con el servidor para obtener transportitas','danger',2000);
    }
    return data;
  }

  async getMisDocumentos(usuarioId: number){
    let data = [];
    try{
      let url = `${environment.endPointApiMovil}/respaldos/getMisDocumentos?usuarioId=${usuarioId}`;

      let consulta: any = await this.htpp.get(url).toPromise();
      if(consulta && !consulta.error){
        data = consulta.data;
      }else{
        this._dataLocal.presentToast('No ha sido posible obtener mis documentos', 'danger', 2000);
      }
    }catch(e){
      console.log('No ha sidp posible obtener mis documentos', e);
      this._dataLocal.presentToast('No ha sido posible obtener mis documentos', 'danger', 2000);
    }
    return data;
  }

  async getMotivoNoEntrega(tipo: number){
    let data: MotivoNoEntrega[] = [];

    try{
      let url = `${environment.endPointApiMovil}/respaldos/getMotivoNoEntrega/?idMotivo=${tipo}`;
      let consulta: any = await this.htpp.get(url).toPromise();
      if(consulta && !consulta.error){
        data = consulta.data;
      }else{
        this._dataLocal.presentToast('No ha sido posible cargar los motivos de no entrega', 'danger');
      }
    }catch(e){
      console.log('No ha sido posible obtener los motivos de no entrega', e);
      this._dataLocal.presentToast('No ha sido posible cargar los motivos de no entrega', 'danger');
    }

    return data;
  }

  reloadMisDocumentos(){
    this.misDocumentosReloadSubject$.next();
  }

}

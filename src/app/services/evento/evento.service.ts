import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { Evento } from 'src/app/interfaces/evento/evento';

import { DataLocalService } from 'src/app/services/dataLocal/data-local.service'

import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

/**
 *Servicio de eventos
 * @export
 * @class EventoService
 */
export class EventoService {
  /**
   *objeto de evento
   * @type {Evento}
   * @memberof EventoService
   */
  crearEvento: Evento;

  /**
   *Creates an instance of EventoService.
   * @param {HttpClient} httpClient
   * @memberof EventoService
   */
  constructor(
    private httpClient: HttpClient,
    private _DataLocalService: DataLocalService,
  ) {
    this.crearEvento = new Evento();
  }

  /**
   *header para peticiones post
   * @memberof EventoService
   */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json',
      'response-Type': 'json'
    }

  );

  /**
   *Permite obtener eventos del cliente
   * @param {*} vendedor
   * @returns
   * @memberof EventoService
   */
  async obtenerEventosCliente(vendedor, fechas, estado) {
    let consulta = null;
   
    let parametros = {
      strRutVendedor: vendedor.rut.replace('.', '').replace('.', ''),
      //fecha: await this.ordenarFecha(fecha)
      fechaInicio: fechas.inicio,
      fechaTermino: fechas.termino,
      estado: estado
    }

    let url = `${environment.endPointApiMovil}/obtenerEventos`;

    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }

  /**
   *Permite generar un evento de cliente
   * @param {*} objeto
   * @returns
   * @memberof EventoService
   */
  async generarEvento(evento: Evento) {
    let consulta = null;
    let url = `${environment.endPointApiMovil}/agendarVisita`;
    let parametros = this.setearObjeto(evento);
 
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
  
    return consulta;
  }

  /**
   *Permite ordenar la fecha para almacenar
   * @param {Date} fechaCompleta
   * @returns
   * @memberof EventoService
   */
  ordenarFecha(fechaCompleta: any) {
    
    fechaCompleta = new Date(fechaCompleta);
    let formatoFecha;

      let dia = fechaCompleta.getDate();
      let mes = fechaCompleta.getMonth() + 1;
      let anio = fechaCompleta.getFullYear();
      let hora = fechaCompleta.getHours();
      let min = fechaCompleta.getMinutes();
      mes = mes > 12 ? 1 : mes;
      let diaSrt = dia < 10 ? '0' + dia : dia;
      let mesStr = mes < 10 ? '0' + mes : mes;
      let horaStr = hora < 10 ? '0' + hora : hora;
      let minStr = min < 10 ? '0' + min : min;

      formatoFecha = `${anio}${mesStr}${diaSrt}${horaStr}${minStr}`

      return formatoFecha
  }

  /**
   *Permite llenar objeto de evento
   * @param {*} objeto
   * @returns
   * @memberof EventoService
   */
  setearObjeto(objeto: Evento) {

    if(objeto.observaciones != "CHECK-IN"){
      let fechaCompleta = new Date(objeto.fechaCreacion);
      let formatoFecha;

      formatoFecha = fechaCompleta;
      formatoFecha.setHours(formatoFecha.getUTCHours() - 3);

      objeto.fechaCreacionMongo = formatoFecha;

    }

    this.crearEvento = new Evento();
    this.crearEvento.idEvento = objeto.idEvento ? objeto.idEvento : '';
    this.crearEvento.refVisita = objeto.refVisita ? objeto.refVisita : '';
    this.crearEvento.tipoEvento = objeto.tipoEvento ? objeto.tipoEvento : '';
    this.crearEvento.fechaCreacion = objeto.fechaCreacion ? this.ordenarFecha(objeto.fechaCreacion) : '';
    this.crearEvento.fechaEjecucion = objeto.fechaEjecucion ? this.ordenarFecha(objeto.fechaEjecucion) : '';
    this.crearEvento.fechaProgramada = objeto.fechaProgramada ? this.ordenarFecha(objeto.fechaProgramada) : '';
    this.crearEvento.observaciones = objeto.observaciones ? objeto.observaciones : '';
    this.crearEvento.rutEmisor = objeto.rutEmisor ? objeto.rutEmisor : '';
    this.crearEvento.rutVendedor = objeto.rutVendedor ? objeto.rutVendedor : '';
    this.crearEvento.rutCliente = objeto.rutCliente ? objeto.rutCliente : '';
    this.crearEvento.estado = objeto.estado ? objeto.estado : '';
    this.crearEvento.direccion = objeto.direccion ? objeto.direccion : '';
    this.crearEvento.latitud = objeto.latitud ? objeto.latitud : '';
    this.crearEvento.longitud = objeto.longitud ? objeto.longitud : '';
    this.crearEvento.detalle = objeto.detalle ? objeto.detalle : '';
    this.crearEvento.comentario = objeto.comentario ? objeto.comentario: '';
    this.crearEvento.zona = objeto.zona? objeto.zona : '';
    this.crearEvento.sucursal = objeto.sucursal? objeto.sucursal : '';
    this.crearEvento.nombreCliente = objeto.nombreCliente ? objeto.nombreCliente : '';
    this.crearEvento.nombreVendedor = objeto.nombreVendedor ? objeto.nombreVendedor : '';
    this.crearEvento.referenciaEvento = objeto.referenciaEvento ? objeto.referenciaEvento : '';
    this.crearEvento.fechaCreacionMongo = objeto.fechaCreacionMongo? objeto.fechaCreacionMongo : null;
    this.crearEvento.origenEvento = objeto.origenEvento? objeto.origenEvento : null;

    return this.crearEvento;
  }

  /**
  * @author ignacio zapata  \"2020-09-10\
  * @desc Metodo utilizado para obtener la los checkins realizados por un vendedor.
  * @params  rut del vendedor "string", "tipo" de tipo string que permite filtrar por el dia actual o el primer del mes presente.
  * @return array con los objetos de checkin.
  */
  async obtenerCheckIn(rut: string, tipo: string){
    
    let consulta;

    let fecha: Date = this.setFechaReporte(tipo);

    let parametros = {rut: rut,tipo: tipo,fecha: fecha}

    let url = `${environment.endPointApiMovil}/getCheckInVendedor`;

    try{
      consulta = await this.httpClient.post(url, parametros, { headers: this.header })
                            .pipe( map( (resp: any) => {
                              if(!resp.error){
                                return resp.data;
                              }else{
                                console.log("ha ocurrido un error obteniendo los checkin", resp);
                                return [];
                              }
                            })).toPromise();
    }catch(e){
      console.log("error al obtener los checkin.", e);
      consulta = [];
      this._DataLocalService.presentToast('Error al obtener los check-in','danger');
    }
    return consulta;
  }

  async obtenerInformeCheckIn(tipo_fecha, tipo, filtro = ''){
    let consulta;

    let fecha: Date = this.setFechaReporte(tipo_fecha);

    let url = `${environment.endPointApiMovil}/getCheckInInforme/?tipo=${tipo}&filtro=${filtro}&fecha=${fecha}`;    

    try{
      consulta = await this.httpClient.get(url, { headers: this.header })
      .pipe(
        map((resp: any) => {
          if(resp && !resp.error){
            return resp.data;
          }else{
            return [];
          }
        })
      )
      .toPromise();
    }catch(e){
      console.log("error al intentar obtener el informe de los checkin", e);
      this._DataLocalService.presentToast("Error al intentar obtener el informe de los check-in", 'danger');
      consulta = [];
    }
    return consulta;
  }


  /**
  * @author ignacio zapata  \"2020-09-11\
  * @desc funcion utilizada para obtener la fecha de inicio del mes actual o la fecha de hoy, con el objetivo de filtrar por dia o mes
  * @params tipo de tipo string
  * @return valor de tipo date.
  */
  setFechaReporte(tipo): Date{
    let hoy = new Date();
    let fecha: Date;
    // Seteamos la fecha que se utilizará para filtrar los checkin. si es dia se obtiene la fecha de hoy a las 0 horas, si es mes es desde el dia 1.
    if(tipo === 'mes'){
      fecha = new Date(hoy.getUTCFullYear(), hoy.getUTCMonth(), 1, 0);
    }else if(tipo === 'dia'){
      fecha = new Date(hoy.getUTCFullYear(), hoy.getUTCMonth(), hoy.getUTCDate(), 0);
    }

    return fecha;
  }

  async sendErrorEmail(msg: string, errorTipo: string){
    try{
      let parametros = {
          para: 'ignacio.zapata@implementos.cl',
          asunto: `[App Movil] Error - ${errorTipo}`, 
          html: msg,
          bbc: 'pablo.navarro@implementos.cl'
      }

      let url = `${environment.endPointApiCarro}/enviarmail`;
  
      let respuesta = await this.httpClient.post(url, parametros).toPromise();
      console.log('respuesta', respuesta);
  }catch(e){
      console.log('No se ha podido enviar correo de error', e.toString());
  }
  }



}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

// servicios
import { DataLocalService } from '../dataLocal/data-local.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';

  /**
   *Creates an instance of EventoService.
   * @param { HttpClient } httpClient
   * @param { DataLocalService } dataLocal
   * @param { ClienteService }  clienteService
   * @param { VendedorService } vendedorService
   * @memberof EventoService
   */


@Injectable({
  providedIn: 'root'
})
export class CotizacionService {

  constructor(private dataLocal: DataLocalService, private clienteService: ClienteService, private http: HttpClient, private vendedorService: VendedorService) { }


  async generarCotizacion(parametros: any){
    let respuesta = await this.clienteService.crearCotizacion(parametros);

    if(respuesta[0].resultado){
      this.dataLocal.eliminarDatos('carro');
/*    this.dataLocal.eliminarDatos('cliente'); */
      this.dataLocal.presentToast('Cotización creada exitosamente', 'success');
    }

    return respuesta;
  }

  async confirmarCotizacion(respuesta, vendedor){
    let convertir = await this.vendedorService.convertirCotizacion(vendedor, respuesta);
     
    if (convertir) {
      this.dataLocal.presentToast('Cotización convertida exitosamente', 'success');
    }
    return convertir;
  }


  // Funcion utilizada para obtener el total de una cotizacion generada desde un check-IN
  getTotalCotizacion(detalleCoti: string){
    let total: number = 0;

    let items: any = detalleCoti.split('|')
      items.forEach(item => {
        if(item.length > 0){
          let arreglo = item.split('@');
          total = total + ( Math.round( arreglo[2] * arreglo[1])); 
        }
      });
    return total;
  }

}

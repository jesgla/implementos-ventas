import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

// services
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  public landing_pageByNotification: string;
  private tokenFireBase: string = 'fKZGCwJyhBM:APA91bHhDEh7lWCJ6zlYH1hWjxwCOzZYGlrKIHAxQwAymA0M-tlRhJl-dpXn3CkH0llcCI9Z-j_4DyEdGExJGW63cf95X2FL2CTeEd4SbZTnYjwy2K5093rTvB3EtZG2vTDnJqh-CtQI';

  constructor(private _http: HttpClient, private Platform: Platform) { }

  async updateToken(vendedor: Vendedor){

    if(!vendedor || !this.tokenFireBase) return;
    
    let url = `${environment.endPointApiNotificaciones}/tokens/setToken`;

    let data = {
      token: this.tokenFireBase? this.tokenFireBase : null,
      rutEmpleado: vendedor.rut? vendedor.rut.replace('.','').replace('.','') : null,
      nombre: vendedor.nombre? vendedor.nombre : '',
      codBodega: vendedor.codBodega? vendedor.codBodega : '',
      sistema: this.Platform.is('ios')? 'ios' : 'android',
      codPerfil: vendedor.codPerfil? vendedor.codPerfil : null,
      codEmpleado: vendedor.codEmpleado? vendedor.codEmpleado : null,
    }
    await this._http.post(url, data).toPromise();
  }

  getFireBaseToken(): string{
    return this.tokenFireBase;
  }

  setFireBaseToken(token: string){
    this.tokenFireBase = token;
  }

  setLandingPage(landingPage: string){
    this.landing_pageByNotification = landingPage;
  }

}

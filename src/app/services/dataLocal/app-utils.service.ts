import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AppUtilsService {

  loading: HTMLIonLoadingElement;

  constructor( private alertCtrl : AlertController,
               private loadingCtrl: LoadingController,
               public toastController: ToastController ) { }


  async presentarAlertaSimple( titulo:string, mensaje:string , textoBoton:string='Aceptar', cssClass:string='' ) {
    /** cssClass:  alert-danger , alert-warning */
    const alert = await this.alertCtrl.create({
      cssClass: cssClass,
      header: titulo,
      subHeader: '',
      message: mensaje,
      buttons: [textoBoton]
    });

    return await alert.present();
  }

  /**************************************************
   * CONFIRM
   */
  async presentAlertConfirm(header:string='',message:string='', fnAceptar:any, fnCancelar:any ,txtAceptar:string='Aceptar',txtCancelar:string='Cancelar') {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: header,
      message: message,
      buttons: [
        {
          text: txtCancelar,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            if(typeof fnCancelar === 'function'){
              fnCancelar();
            }
          }
        }, {
          text: txtAceptar,
          handler: () => {
            console.log('Confirm Okay');
            if(typeof fnAceptar === 'function'){
              fnAceptar();
            }
          }
        }
      ]
    });

    return await alert.present();
  }

  /**************************************************
   * LOADING
   */
  async presentLoading( message: string ){
    this.loading = await this.loadingCtrl.create({
      message: message
    });
    await this.loading.present();
  }
  async closeLoading( ){
    this.loading.dismiss();
  }


  /**************************************************
   * TOAST
   */
  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Lectura correcta',
      message: 'Click to Close',
      position: 'top',
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }
  async presentToast( message: string,color) {
    const toast = await this.toastController.create({
      message,
      duration: 1200,
      position: 'top',
      color: color
    });
    toast.present();
  }

  
}

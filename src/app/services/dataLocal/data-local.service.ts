//angular
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

//ionic
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';

//interfaces
import { Articulo } from 'src/app/interfaces/articulo/articulo';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor'

// services
import { ArticulosService } from 'src/app/services/articulos/articulos.service';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})

export class DataLocalService {
  carro : Articulo[]=[];
  nuevoArticulo: Articulo;

  private clienteReloadSubject$: Subject<any> = new Subject();
  readonly clienteReload$: Observable<any> = this.clienteReloadSubject$.asObservable();

  /**
   *Creates an instance of DataLocalService.
   * @param {Storage} storage
   * @param {ToastController} toastController
   * @memberof DataLocalService
   */
  constructor(private storage: Storage,
              public toastController: ToastController,
              private _ArticulosService: ArticulosService,
              private _UsuarioService: UsuarioService) { 
    this.storage.set('carro',[]);
    this.storage.set('geo',[]);
    storage.remove('cliente');
  }

  /**
   *Permite almacenar el carro de compras
   * @param {Articulo} articulo
   * @param {boolean} estado
   * @returns
   * @memberof DataLocalService
   */
  async setCarroCompra(articulo:Articulo,estado: boolean){
    this.carro =await this.storage.get('carro');

    let existe = this.carro.find(articuloA => articuloA.sku === articulo.sku);

    if(!existe){
      this.carro.unshift(articulo)
      this.carro = this.carro.filter(art=>art.cantidad>0);
  
      this.storage.set('carro',this.carro)
    }else{

      existe.cantidad = articulo.cantidad;
      existe.precioEscaladoNeto = articulo.precioEscaladoNeto;
      existe.precioNeto = articulo.precioNeto;

      this.carro = this.carro.filter(art=>art.cantidad>0);
      this.presentToast( this.carro.length.toString(),'success' )
  
     
      this.storage.set('carro',this.carro);

    }
    this.presentToast( 'Articulo Agregado','success' )
    return await this.storage.get('carro');
    
  }

  async clienteReloadSubject(){
    this.clienteReloadSubject$.next();
  }

  /**
   *Almacena un cliente en ambiente local
   *
   * @param {Cliente} cliente
   * @memberof DataLocalService
   */
  async setCliente(cliente: Cliente) {
    let esClienteActual = await this.valClienteActual(cliente);
    
    if(!esClienteActual ){
      await this.storage.remove('carro');
      await this.storage.remove('referencia');
      await this.eliminarDatos('cliente'); 
      this.storage.set('carro',[]);

      await this.storage.set('cliente',cliente);
      this.clienteReloadSubject();
    }
  }

    /**
   * funcion que retorna true si el cliente que se acaba de seleccionar, es el mismo al cliente actualmente seleccionado.
   *
   * @param { boolean } boolean
   * @memberof DataLocalService
   */
  async valClienteActual(cliente : Cliente){
    let resp = false;
    let clienteActual = await this.getCliente()
    if(clienteActual && clienteActual.rut === cliente.rut){
      resp =  true;
    } 
    return resp;
  }

  /**
   *Permite consultar por el carro de compras
   * @returns
   * @memberof DataLocalService
   */
  async getCarroCompra(){
    return await this.storage.get('carro');
  }

  /**
   *Permite mostrar un mensaje al usuario
   * @param {string} message
   * @memberof DataLocalService
   */
  async presentToast( message: string,color, duration: number = 1800) {
    const toast = await this.toastController.create({
      message,
      duration: duration,
      position: 'bottom',
      color: color
    });
    toast.present();
  }

  /**
   *Permite eliminar un articulo del carro de compra
   * @param {*} sku
   * @memberof DataLocalService
   */
  async eliminarArticulo(sku: any) {
    this.carro =await this.storage.get('carro');
 
    this.carro = this.carro.filter(articulo => articulo.sku!= sku);

    this.storage.set('carro',this.carro);
    this.presentToast( 'Articulo Eliminado','success' )

  }

  /**
   *Permite obtener un cliente
   * @returns
   * @memberof DataLocalService
   */
  async getCliente() {
    return await this.storage.get('cliente');
  }

  /**
   *Permite eliminar el carro de compras
   * @param {string} dato
   * @memberof DataLocalService
   */
  async eliminarDatos(dato: string) {
    
    this.storage.remove(dato);
    if(dato==='carro'){
      this.storage.set('carro',[]);
    }
  }

  /**
   *Almacena la referencia del check-in
   * @param {*} id
   * @memberof DataLocalService
   */
  async setIdReferencia(id) {
    this.storage.set('referencia',id);
    this.presentToast( 'registro check-in exitoso','success' );
  }

  /**
   *Permite obtener un elemento de la local storage
   * @param {string} elemento
   * @returns
   * @memberof DataLocalService
   */
  async getItem(elemento: string){
    return await this.storage.get(elemento);
  }

  /**
   *Permite ordenar la fecha para almacenar 
   * @param {Date} fechaCompleta
   * @param {boolean} tipoFecha
   * @returns
   * @memberof DataLocalService
   */
  ordenarFecha(fechaCompleta: Date,tipoFecha : boolean) {
 
    fechaCompleta = new Date(fechaCompleta);

    let dia  = fechaCompleta.getDate();
    let mes  = fechaCompleta.getMonth()+1;
    let anio = fechaCompleta.getFullYear();
    let hora = fechaCompleta.getHours();
    let min  = fechaCompleta.getMinutes();
    mes = mes>12?1:mes;
    let diaSrt = dia<10?'0'+dia:dia;
    let mesStr = mes<10?'0'+mes:mes;
    let horaStr = hora<10?'0'+hora:hora;
    let minStr = min<10?'0'+min:min;

    let formatoFecha =null;
    if(tipoFecha){
      formatoFecha =`${anio}${mesStr}${diaSrt}`;
    }else{
      formatoFecha =`${anio}${mesStr}${diaSrt}${horaStr}${minStr}`;
    }

    return formatoFecha

  } 

  async setItem(key: string, datos: any) {
    await this.storage.set(key,datos);
  }

  // Verifica si ya existe un articulo en el carro, con el objetivo de devolver la cantidad anterior.
  async valItemOnCart(articulo){
    this.carro =await this.storage.get('carro');

    if(this.carro){
      let existe = this.carro.find(articuloA => articuloA.sku === articulo.sku);
      if(existe){
        return existe.cantidad;
      }else{
        return 1;
      }
    }else{
      return 1;
    }

  }

  // Permite setea una variable asociada a la seccion seleccionada por el usuario.
  async setSeccion(tipo: string){
    this.storage.set('seccion', tipo);
  }

  async clearDatos(){
    await this.storage.remove('carro');
    await this.storage.remove('cliente');
  }

  // Remueve los datos el localstorage, con el objetivo realizar el cambio de seccion.
  async removeSeccion(){
    this.clearDatos();
    await this.storage.remove('seccion');
  }

  async setUserLoged(){
    let usuario = await this.getItem('auth-token');
    await this.clienteReloadSubject();
    if(JSON.parse(usuario)) await this._UsuarioService.setUserLoged(JSON.parse(usuario));
  }
}

import { Direccion } from './../../interfaces/direccion/direccion';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

// Interface
import { detalleHua } from 'src/app/interfaces/hua/hua'

@Injectable({
  providedIn: 'root'
})
export class EstadoTiendaService {

  public sucursales: any[] = [];
  scanningStatus: boolean;
  selectSucurales: any[] = [];

  constructor(private http: HttpClient, private _dataLocal: DataLocalService) { }

  async getDetalleHua(sku: string, sucursal: string){
    let respuesta;

    const url = `${environment.endPointApiCatalogo}/estadoarticulotienda?sku=${sku}&sucursal=${sucursal}`;
    let consulta = await this.http.get(url).toPromise().then( (resp: any) => {
      if(!resp.error){
        if(resp.data && resp.data._id){
          respuesta = resp.data;
        }
      }else{
        this._dataLocal.presentToast("se ha generado un problema al intentar obtener los detalles del hua para este articulo.", 'danger');
      }
    });
    // Seteamos el stock del centro de distribucion y stock actual en sucursal.
    respuesta? respuesta.stockCD = await this.getStock(respuesta.sku, 'CDD-CD1') : '';
    respuesta? respuesta.disponibilidad = await this.getStock(respuesta.sku, sucursal) : '';
    return respuesta;
  }

  // Obtiene la cantidad de stock asociada a un producto y al centro de distribucion.
  async getStock(sku:string, sucursal: string){

    let respuesta: number;
    const url = `${environment.endPointApiCarro}/stockb2b?sku=${sku}&sucursal=${sucursal}`
    let consulta = await this.http.get(url).toPromise().then( resp => {
      if(resp){
        respuesta = resp[0].cantidad;
      }else{
        respuesta = NaN;
      }
    });
    return respuesta;
  }

  // Obtiene la lista de todas las sucursales
/*   async getSucursales(){
    
    this.selectSucurales = [];

    let url = `${environment.endPointApiLogistica}/tiendas`;
    let sucursales: any[] = [];

    await this.http.get(url).toPromise().then( (resp: any) => {
      if(resp && !resp.error){
        resp.data.forEach(tienda => {
          sucursales.push({codigo: tienda.codigo, nombre: tienda.zona});
        });
      }else{
        this._dataLocal.presentToast('error al obtener las tiendas', 'warning');
      }
    })
    this.sucursales = sucursales;
    await this.getBodegas();
  } */

  /**
  * @author ignacio zapata  \"2021-02-25\
  * @desc Obtiene la lista de bodegas disponibles
  * @params 
  * @return array de bodegas con codigo y nombre en string
  */
  async getBodegas(){
    let url = `${environment.endPointApiLogistica}/bodegas`;
    let respuesta:  {codigo: String, nombre: string}[] = [];
    
    try{
      await this.http.get(url).toPromise().then( (resp: any) => {
        if(!resp || resp.error ) this._dataLocal.presentToast('Ha ocurrido un error intentanto cargar las bodegas', 'danger');
        if(resp && !resp.error) this.sucursales = resp.data;
      })
    }catch(e){
      this._dataLocal.presentToast('Ha ocurrido un error intentanto cargar las bodegas', 'danger');
    }
    return respuesta;
  }

  async getProductoUbicacion(codBodega: string, sku: string){
    let resp = 'Indefinida';

    try{
      let url = `${environment.endPointApiCatalogo}/productoUbicacion/?sku=${sku}&codBodega=${codBodega}`;
 
      let consulta: any = await this.http.get(url).toPromise();
      if(consulta && consulta.data){
        resp = consulta.data;
      }
    }catch(e){
      this._dataLocal.presentToast('No se ha podido cargar la ubicacion del producto', 'danger');
      console.log('No se ha podido cargar la ubicacion del producto', e);
    }

    return resp;
  }

  // Metodo utilizado para setear sucursales a seleccionar.
  async setSucursales(codBodega: string, codSucursal: string){
    this.selectSucurales = [];
    try{
      if(codBodega == 'AREA ADMINISTRACION' || codBodega == ''){
        this.selectSucurales =  this.sucursales;
      }else{
        this.selectSucurales.push({ codigo: codBodega, nombre: codSucursal });
      }
    }catch(e){
      this._dataLocal.presentToast('Ha ocurrido un error intentanto cargar las sucursales', 'danger');
    }
  }

  // Metodo que nos permite retornar el promedio de ventas por semana, validando la division por 0
  getSemanasVentas(disp, promedio){
    let resp;
    promedio != 0 ? resp = disp / promedio : resp = 0;
    return resp.toFixed(2);
  }

  setScanningStatus(value: boolean){
    this.scanningStatus = value;
  }

  GetScanningStatus(){
    return this.scanningStatus;
  }

}

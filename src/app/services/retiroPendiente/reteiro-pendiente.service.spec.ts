import { TestBed } from '@angular/core/testing';

import { ReteiroPendienteService } from './reteiro-pendiente.service';

describe('ReteiroPendienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReteiroPendienteService = TestBed.get(ReteiroPendienteService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Subject, Observable} from 'rxjs';

//variables de entorno
import { environment } from 'src/environments/environment';

// servicios
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { LoadingService } from './../loading/loading.service';

// interface
import { retiroPendiente } from 'src/app/interfaces/bodega/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ReteiroPendienteService {
  private pickingReloadSubject$: Subject<any> = new Subject();
  readonly pickingReloadObservable$: Observable<any> = this.pickingReloadSubject$.asObservable();

  constructor(private httpClient: HttpClient, private _dataLocal: DataLocalService, private _loadingService: LoadingService) { }

  async obtenerRetiroPendiente(folio: string){
    let resp: retiroPendiente;

    try{
      await this._loadingService.presentLoading('Cargando Retiro Pendiente');
      let url = `${environment.endPointApiMovil}/picking/retirosPendientes/?ordenVenta=${folio}`;
      
      let consulta: any = await this.httpClient.get(url).toPromise();
      if(consulta && !consulta.error){
        resp = consulta.data;
      }else{
        this._dataLocal.presentToast('No ha sido posible obtener el retiro pendiente asociado a la ov ingresada', 'danger');
      }
    }catch(e){
      this._dataLocal.presentToast('No ha sido posible cargar los datos del retiro pendiente', 'danger');
    }
    this._loadingService.hideLoading();
    return resp;
  }



  async getRetirosPendientesByBodega(codBodega: string, listoParaEntrega: boolean){
    let resp: retiroPendiente[];

    try{
      let url = `${environment.endPointApiMovil}/picking/retirosPendientes/?codBodega=${codBodega}&listoParaEntrega=${listoParaEntrega}`;

      let consulta: any = await this.httpClient.get(url).toPromise();
      if(consulta && !consulta.error){
        resp = consulta.data;
      }else{
        this._dataLocal.presentToast('No ha sido posible obtener el retiro pendiente asociado a la ov ingresada', 'danger');
      }

    }catch(e){
      this._dataLocal.presentToast('No ha sido posible cargar los datos del retiro pendiente', 'danger');
    }

    return resp;
  }

  async setResponsableRetiroPendiente(idRetiroPendiente: string, nombreUsuario: string ){
    let responseStatus: boolean = false;
    try{
      let url = `${environment.endPointApiMovil}/picking/setResponsableRetiroPendiente/`;

      let consulta: any = await this.httpClient.post(url, {_id: idRetiroPendiente, responsable: nombreUsuario}).toPromise();
      
      if(consulta && !consulta.error && consulta.msg === 'OK'){
        this._dataLocal.presentToast('Responsable asignado exitosamente', 'success');
        responseStatus = true;
      }

      if(consulta && !consulta.error && consulta.msg != 'OK'){
        this._dataLocal.presentToast(consulta.msg, 'warning');
      }

      if(!consulta || consulta.error){
        this._dataLocal.presentToast('No ha sido posible registrar responsable para el retiro pendiente seleccionado', 'danger');
      }
    }catch(e){
      this._dataLocal.presentToast('No ha sido posible registrar responsable para el retiro pendiente seleccionado', 'danger');
    }

    return responseStatus;
  }

  async setListoParaRetiro(_id: string, folio: string){
    let response: boolean = false;
    try{
      let pickingListo = await this.valPickingEstado(folio);

      if(!pickingListo){
        return;
      }

      let url = `${environment.endPointApiMovil}/picking/finalizaRetiroPendiente/`;

      let consulta: any = await this.httpClient.post(url, {_id: _id}).toPromise();
      if(consulta && !consulta.error){
        this._dataLocal.presentToast('Retiro pendiente, finalizado exitosamente', 'success');
        response = true;
      }else{
        this._dataLocal.presentToast('No ha podido finalizar el retiro pendiente', 'danger');
      }

    }catch(e){
      this._dataLocal.presentToast('No ha sido posible pasar el retiro tienda a estado listo para entregar', 'danger');
    }

    return response;
  }

  async valPickingEstado(folio: string){
    let resp: boolean = false;
    try{
      let url = `${environment.endPointApiMovil}/picking/valEstadoPicking?ordenVenta=${folio}`;
      let consulta: any = await this.httpClient.get(url).toPromise();
      if(consulta && !consulta.error){
        resp = consulta.data;
        if(!resp){
          this._dataLocal.presentToast('Para cerrar el retiro pendiente, primero es necesario finalizar el picking en sistema AX.', 'danger', 2400);
        }
      }else{
        this._dataLocal.presentToast('No ha sido posible verificar estado del picking asociado a la OV.', 'danger');
      }
    }catch(e){
      console.log('No ha sido posible verificar estado de ov');
      this._dataLocal.presentToast('No ha sido posible verificar estado del picking asociado a la OV.', 'danger');
    }
    return resp;
  }

  reloadBandejaPicking(){
    this.pickingReloadSubject$.next();
  }


}

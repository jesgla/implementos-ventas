import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

// interfaces
import { ArticuloQuiebre, SucursalQuiebre, parametrosUrl } from 'src/app/interfaces/articuloQuiebre/articuloQuiebre';

// Servicios
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { FiltrosQuiebresService } from 'src/app/services/filtrosQuiebres/filtros-quiebres.service';


@Injectable({
  providedIn: 'root'
})
export class QuiebreStockService {

  totalPaginas: number;
  filtrosAnteriores: any = {
    quebrados: 1,
    filtro: 0
  }

  constructor(private htpp: HttpClient, private _dataLocal: DataLocalService, private _FiltrosQuiebresService: FiltrosQuiebresService) { }

  async getProductosQuiebre(parametros: parametrosUrl ){

    let params = await this.setURLQuebrados(parametros);
 
    let url = `${environment.endPointApiCatalogo}/revisarQuiebre/?sucursal=${parametros.sucursal}&&pageSize=15&quiebre=${parametros.quebrados}&assortment=${parametros.filtro}&ordensemanas=1&page=${parametros.page}${params.categoria}`;

    let respuesta: ArticuloQuiebre[] = [];

    try{
      let consulta: any = await this.htpp.get(url).pipe( 
        map((result: any) =>{
          // Seteamos las imagenes para cada producto.
          if(result.data && result.data.length > 0){
            
            // seteamos el total de paginas
            this.totalPaginas = result.paginacion.paginas;

            if(this.valChangedSoloQuebrados(parametros)){
              this._FiltrosQuiebresService.setCategoriasFiltro(result.uen);
            }

            result.data.forEach( (producto: ArticuloQuiebre) => {
              this.setPerdidaProducto(producto);
              producto.img = `https://images.implementos.cl/img/250/${producto.sku}-1.jpg`;
            });
          }
          return result;
      } )).toPromise()

      if(consulta && !consulta.error){
        consulta.data.length > 0? respuesta = consulta.data : respuesta = [];
      }else{
        this._dataLocal.presentToast("Error al obtener los articulos con quiebre", "danger");
      }
    }catch(e){
      this._dataLocal.presentToast("Error al obtener los articulos con quiebre", "danger");
      console.log('error al consultar los articulos en quiebre', e);
    }
    return respuesta;
  }

  async getQuiebreSucursales(){
    let respuesta: SucursalQuiebre[] = [];

    const url = `${environment.endPointApiCatalogo}/informequiebrestock`;

    try{
      let consulta: any = await this.htpp.get(url).toPromise();
      if(consulta && !consulta.error){
        respuesta = consulta.data;
      }else{
        this._dataLocal.presentToast(consulta.msg,'danger');
      }
    }catch(e){
      this._dataLocal.presentToast("Ha ocurrido un error al consultar los datos de quiebre de stock", "danger");
      console.log("error al consultar quiebre de stock", e);
    }
    return respuesta;
  }

  /**
  * @author ignacio zapata  \"2020-08-26\
  * @desc obtenemos los queryparams para categoria y tipologia de forma dinamica.
  * @params objeto de tipo parametrosUrl.
  * @return string para formar el url del servicio que llama a los items quebrados.
  */
  setURLQuebrados(parametros: parametrosUrl){
    let categoria: string = '';
    
    parametros.categoria ? categoria = `&uen=${parametros.categoria}`  : '';

    return {categoria}
  }

  /**
  * @author ignacio zapata  \"2020-08-27\
  * @desc Funcion utilizada para validar si se ha hecho cambio en el filtro de SoloQuebrados.
  * @params objeto de tipo parametrosUrlm, con el dato de los filtros actuales. 
  * @return boolean 
  */
  valChangedSoloQuebrados(parametros: parametrosUrl){
    let respuesta: boolean = false;

    if( parametros.quebrados != this.filtrosAnteriores.quebrados || parametros.filtro != this.filtrosAnteriores.filtro ){
      respuesta = true;
    }

    this.filtrosAnteriores.quebrados = parametros.quebrados; 
    this.filtrosAnteriores.filtro = parametros.filtro
    return respuesta;
  }

  setPerdidaProducto(producto: ArticuloQuiebre){
     producto.perdidaCalculada = producto.precio * (producto.avg + producto.sd)
    
  }
}

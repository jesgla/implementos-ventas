import { Injectable } from '@angular/core';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service'

// interfaces 
import { Articulo } from 'src/app/interfaces/articulo/articulo';

@Injectable({
  providedIn: 'root'
})
export class CarroService {

  constructor(private _Localdata: DataLocalService) { }

  /**
   * Permite obtener los datos del carro y sus montos asociados.
   * @param {*} carro
   * @memberof CarroService
   */
  async obtenerCarro() {

    let carro = await this._Localdata.getCarroCompra();
    if(carro){
      let montos = await this.calcularMontos(carro);
      return {'carro': carro, 'total': montos.total, 'subTotal': montos.subTotal, 'iva': montos.iva};
    }else{
      return {'carro': 0, 'total': 0, 'subTotal': 0, 'iva': 0};
    }

  }

  /**
   * Permite calcular el monto de los productos cargados al carro
   * @param {*} carro
   * @memberof CarroService
   */
  async calcularMontos(carro: any) {

    let total = 0;
    let subTotal = 0;
    let iva = 0;
    carro.map((articulo: Articulo) => {

      let precioSIva: number;
      let ivaArt: number;

      if(!articulo.precio_escala){
        precioSIva = Number(articulo.precioNeto);
        ivaArt = Number(articulo.precioNeto * 1.19) - precioSIva;
      }else{
        precioSIva = Number(articulo.precioEscaladoNeto);
        ivaArt = Number(articulo.precioEscaladoNeto * 1.19) - precioSIva;
      }
      
      subTotal += Math.round(precioSIva * articulo.cantidad);
      iva += Math.round(ivaArt * articulo.cantidad);
    })

    total = Math.round(subTotal + iva);
    return ({'total': total, 'subTotal': subTotal , 'iva': iva});
  }

}

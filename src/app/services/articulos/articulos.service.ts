//angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { map, filter } from 'rxjs/operators';

//environment
import { environment } from 'src/environments/environment';

//interfaces
import { Articulo } from 'src/app/interfaces/articulo/articulo';


@Injectable({
  providedIn: 'root'
})

/**
 * Servicio de articulos
 * @export
 * @class ArticulosService
 */
export class ArticulosService {

  /**
   *Creates an instance of ArticulosService.
   * @param {HttpClient} httpClient
   * @memberof ArticulosService
   */
  constructor(private httpClient: HttpClient,private storage: Storage) { }
  /**
   * header para ejecutar consultas post
   * @memberof ArticulosService
   */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });

  /**
   *Permite obtener listado de articulos
   * @param {*} parametros
   * @returns
   * @memberof ArticulosService
   */
  async obtenerArticulos(parametros) {

    let consulta = null;
    let cli : any = await this.storage.get('cliente');

     const url = `${environment.endPointApiMovil}/busquedaProductos`; 
    cli? parametros.rutCliente = cli.rut: null; 

    consulta = await this.httpClient.post<Articulo>(url, parametros, { headers: this.header }).toPromise();
  
    return consulta;
  }

  /**
   *Permite obtener stock de un producto por almacen
   * @param {*} parametros
   * @returns
   * @memberof ArticulosService
   */
  async obtenerStockTiendas(parametros) {
    let consulta = null;

    const url = `${environment.endPointApiCarro}/stockb2b?sku=${parametros.sku}`
    consulta = await this.httpClient.get(url).toPromise();

    return consulta;
  }

  async obtenerPropuesta(parametros){
    const url = `${environment.endPointApiMovil}/obtenerPropuesta`;

    let consulta = await this.httpClient.post(url, parametros , { headers: this.header }).toPromise();
    return consulta;
  }

  // Obtiene la lista de precios escala para un producto en especifico.
  async getListPreciosEscala(sku:string, sucursal: string){
    sucursal = encodeURI(sucursal);
    const url =  `${environment.endPointApiCarro}/buscaprecioescala?sucursal=${sucursal}&sku=${sku}`; 
    let consulta = await this.httpClient.get(url).toPromise();
    return consulta;
  }

  // obtiene el precio de un producto dependiendo la cantidad seleccionada.
  async getPrecioEscala(sku: string, sucursal: string, cantidad: number){
    sucursal = encodeURI(sucursal);
    let precio: number = undefined;
    const url =  `${environment.endPointApiCarro}/buscaprecio?sku=${sku}&sucursal=${sucursal}&cantidad=${cantidad}`; 
    let consulta = await this.httpClient.get(url).toPromise().then( (resp : any) => {
      if(resp.precio) precio = resp.precio.precio;
    });
    return precio;
  }

 
  // obtiene el precio neto de un precio con iva
  getSinIva(precio: number){
    return Math.round( precio / 1.19);
  }

  // Funcion que permite buscar la matriz de productos cuando se busca por un sku, desde el catalogo
  async getMatrizProducto(sku: string, parametros?: any){

    const url = `${environment.endPointApiCatalogo}/matrizproducto?sku=${sku}`;

    let consulta: any = await this.httpClient.get(url).toPromise().then( (resp: any) => {
      if(!resp.error && resp.data.length > 0){

        // Seteamos los sku devueltos por la api matrizProducto y luego lo enviamos como parametros a la funcion buscarSku.
        let arreglo = []
        resp.data.forEach(articulo => {
          arreglo.push(articulo.sku);
        });

        if(parametros){
          parametros.sku = arreglo;
          return this.buscarSku(parametros);
        }else{
          return arreglo; 
        }
      }else{
        return [];
      }
    });
    return consulta;
  }

  // Funcion que permite buscar articulos enviandole un array de sku's y los datos del vendedor, rut cliente y sucursal.
  async buscarSku(parametros: any){
    const url = `${environment.endPointApiMovil}/busquedaSku`;

    let consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise().then( (resp: any) => {
      if(!resp.error && resp.data.items.length > 0){
        return resp.data.items;
      }else{
        return [];
      }
    });
    return consulta;
  }

  

}

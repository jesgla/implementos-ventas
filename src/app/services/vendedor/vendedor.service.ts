import { Vendedor } from './../../interfaces/vendedor/vendedor';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators'

import { environment } from 'src/environments/environment';

import { Objetivos } from 'src/app/interfaces/vendedor/objetivos';
import { DataLocalService } from '../dataLocal/data-local.service';

@Injectable({
  providedIn: 'root'
})

/**
 *Servicio vendedor
 * @export
 * @class VendedorService
 */
export class VendedorService {

  /**
   *Creates an instance of VendedorService.
   * @param {HttpClient} httpClient
   * @param {DataLocalService} dataLocal
   * @memberof VendedorService
   */
  constructor(private httpClient: HttpClient, private dataLocal: DataLocalService) { }

  /**
   *Header para ejecutar funciones post
   * @memberof VendedorService
   */
  header = new HttpHeaders({
    "Accept": "application/json",
    "Content-Type": "application/json",
    "cache-control": "no-cache"
  },
  );

  /**
   *Permite obtener los objetivos del vendedor
   * @param {*} vendedor
   * @returns objetivos del vendedor
   * @memberof VendedorService
   */
  async obtenerObjetivos(vendedor) {
    let consulta = null
    const url = `${environment.endPointApiMovil}/objetivosVendedor`;
    let fecha = await this.dataLocal.ordenarFecha(new Date(), true);

    let parametros = {
      "rutVendedor": vendedor.rut.replace('.', '').replace('.', ''),
      "fechaResumen": fecha
    }
    consulta = await this.httpClient.post(url, parametros).toPromise();

    return consulta;
  }

  /**
   *Permite almacenar la geolocalizacion del vendedor
   * @param {*} geo
   * @param {*} vendedor
   * @returns
   * @memberof VendedorService
   */
  async guardarGeolocalizacion(geo, vendedor) {
    let consulta = null;
    const url = `${environment.endPointApiLocalizacion}/setlocalizacion`;

    let parametros = {
      "fecha": this.formatoFecha(),
      "sistema": "1",
      "llave": `${vendedor.rut.replace('.', '').replace('.', '')}-AppMovil`,
      "usuario": vendedor.rut.replace('.', '').replace('.', ''),
      "latitud": geo.latitud.toString(),
      "longitud": geo.longitud.toString()

    }
   

    consulta = await this.httpClient.post(url, parametros, { responseType: 'text' }).toPromise();
   
    return consulta;
  }

  async obtenerVendedores() {
    let consulta = null
    const url = `${environment.endPointApiMovil}/obtenerVendedores`;

    consulta = await this.httpClient.get(url).toPromise();
    
    consulta.data.sort((a, b) => a.rutEmpleado < b.rutEmpleado ? -1 : null)
    return consulta.data;
  }

  async obtenerEstadisticaVendedor(rut, periodo, tipo, fecha) {
    let consulta = null
    let parametros = {

      rutVendedor: rut,
      periodo: periodo,
      tipoPeriodo: tipo,
      fechaResumen: fecha
    }
    const url = `${environment.endPointApiMovil}/obtenerEstadisticasVendedor`;
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }


  async obtenerDetalleVendedor(rut, fecha) {
    let consulta = null
    let parametros = {

      rutVendedor: rut,
      fechaDetalle: fecha
    }
    const url = `${environment.endPointApiMovil}/obtenerDetalleVendedor`;
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
  

    return consulta[0];
  }

  async convertirCotizacion(vendedor, respuesta) {
    let consulta = null
    let parametros = {
      rutCliente:vendedor.rut.replace('.','').replace('.',''),
      folio:respuesta[0].resultado

    }
    const url = `${environment.endPointApiMovil}/convertirCotizacion`;
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }

  async enviarCotizacion(parametros) {
    
    let consulta = null;
    try{
      const url = `${environment.endPointApiCarro}/enviarmailCotizaciones`; 
    
      consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
      return consulta;

    }catch(e){
      console.log(e);
      return {error: true, msg: 'error'};
    }

  }

  /**
   * 
   *Permite ordenar la fecha para almacenar
   * @returns fecha
   * @memberof VendedorService
   */
  formatoFecha() {
    let fecha = new Date();
    let dia = fecha.getDate();
    let mes = fecha.getMonth() + 1;
    let anio = fecha.getFullYear();
    let hora = fecha.getHours();
    let min = fecha.getMinutes();
    let seg = fecha.getSeconds();
    let diaSrt = dia < 10 ? '0' + dia : dia;
    let mesStr = mes < 10 ? '0' + mes : mes;
    let horaStr = hora < 10 ? '0' + hora : hora;
    let minStr = min < 10 ? '0' + min : min;
    let segStr = seg < 10 ? '0' + seg : seg;

    let formatoFecha = `${anio}-${mesStr}-${diaSrt} ${horaStr}:${minStr}:${segStr}`;

    return formatoFecha
  }

  async getZonaVendedor(CodSucursal: string){
    let respuesta;
    const url = `${environment.endPointApiMovil}/getZonaVendedor/?sucursal=${CodSucursal}`;

    try{
      let consulta = await this.httpClient.get(url).pipe(
        map( (resp: any) => {
          return resp.data[0]? resp.data[0].zona : '';
        })
      ).toPromise();

        respuesta = consulta;
    }catch(e){
      console.log("error al intentar obtener los datos de la zona");
      respuesta = '';
    }

    return respuesta;
  }  
}
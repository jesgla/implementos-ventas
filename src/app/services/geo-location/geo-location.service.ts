import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class GeoLocationService {

  lat: number = 0;
  lng: number = 0;
  
  constructor(private httpClient: HttpClient) { }

   public getPosition(): Observable<Position> {
    return Observable.create(
      (observer) => {
      navigator.geolocation.watchPosition((pos: Position) => {
        this.lat =  pos.coords.latitude;
        this.lng = pos.coords.longitude;
        observer.next(pos);
      }),
      () => {
          console.log('Position is not available');
      },
      {
        enableHighAccuracy: true
      };
    });
  }

  getCoordenadas(){
    return {lat: this.lat, lng: this.lng};
  }
  
  /**
  * @author ignacio zapata  \"2020-11-11\
  * @desc funcion utilizada para encontrar la sucursal asociada a la sucursal en donde se hizo el checkin y ademas se traen las otras sucursales de esa misma zona_comercial.
  * @params  sucursal de tipo string.
  * @return retorna array con objeto con las siguientes propiedades: nombre, latitud, longitud, direccion.
  */
  async getTiendas(sucursal){
    let url: string = `${environment.endPointApiLogistica}/tiendas`;
    let respuesta: any[] = [];

    try{
      respuesta = await this.httpClient.get(url).pipe(
                  map( (tiendas: any) => {
                    let posTiendas : any[] = [];
                    // buscamos la sucursal asociada al checkin.
                    if(tiendas && !tiendas.error && tiendas.data && tiendas.data.length > 0){
                      let zonaComercial: number;
                      for(let tienda of tiendas.data){
                        if(tienda.codigo === sucursal){
                          posTiendas.push({nombre: tienda.nombre, latitud: tienda.lat, longitud: tienda.lon, direccion: tienda.direccion})
                          zonaComercial = tienda.zona_comercial;
                        }
                      }
                      // buscamos las sucursales pertenecientes a la zona comercial en donde se hizo el checkin.
                      for(let tienda of tiendas.data){
                        if(tienda.zona_comercial === zonaComercial && tienda.codigo != sucursal ){
                          posTiendas.push({nombre: tienda.nombre, latitud: tienda.lat, longitud: tienda.lon, direccion: tienda.direccion})
                        }
                      }
                    }
                    return posTiendas;
                  })
                ).toPromise();
    }catch(e){
      console.log("No ha sido posible obtener las tiendas.");
    }
    return respuesta;
  }
}

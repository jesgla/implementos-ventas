import { Injectable } from '@angular/core';
import { observable, Subject, BehaviorSubject, Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class SeccionesService {

  seccionSubject$: Subject<any> = new Subject();
  readonly seccionObservable$: Observable<any> = this.seccionSubject$.asObservable();

  constructor() { }

  selectedSeccion(redirect: boolean = true){
    this.seccionSubject$.next(redirect);
  }
}

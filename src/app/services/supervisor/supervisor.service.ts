import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataLocalService } from '../dataLocal/data-local.service';

//environment
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SupervisorService {

  constructor(private httpClient: HttpClient, private dataLocal: DataLocalService) { }
  /**
     * header para ejecutar consultas post
     * @memberof ArticulosService
     */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });

  async obtenerZonas(sector, tipoSector) {
    let consulta = null;
    const url = `${environment.endPointApiMovil}/obtenerZonas`;
    let usuario =  JSON.parse(await this.obtenerUsuario());
 
    let parametros = {
      "rutEmisor": usuario.rut.replace('.', '').replace('.', ''),
      "fechaResumen": this.formatoFecha(),
      "sector": sector,
      "tipoSector": tipoSector,
      "token": "new-app"

    }
    try{
      consulta = await this.httpClient.post(url, parametros, { headers:this.header }).toPromise();
      if (consulta.error) { return [] };
    }catch(e){
      consulta = [];
      this.dataLocal.presentToast( 'No ha sido posible obtener las ventas', 'danger');
    }

    return consulta;
  }

  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');

    return usuario;
  }

  async informeCombos() {
    let consulta = null;
    let usuario = JSON.parse(await this.obtenerUsuario());

    let fecha = new Date();
    const url = `${environment.endPointApiMovil}/obtenerCombosZonas?rutZonal=${usuario.rut.replace('.', '').replace('.', '')}&fechaResumen=${fecha}`;

    try{
      consulta = await this.httpClient.get(url, { headers: this.header }).toPromise();
      if (consulta.error) { return [] }
    }catch(e){
      console.log('error obteniendo los combos');
      this.dataLocal.presentToast('No ha sido posible obtener los combos', 'danger');
    }


    return consulta.data;
  }

  /**
 *Permite ordenar la fecha para almacenar
 * @returns fecha
 * @memberof VendedorService
 */
  formatoFecha() {
    let fecha = new Date();
    let dia = fecha.getDate();
    let mes = fecha.getMonth() + 1;
    let anio = fecha.getFullYear();
    let hora = fecha.getHours();
    let min = fecha.getMinutes();
    let seg = fecha.getSeconds();
    let diaSrt = dia < 10 ? '0' + dia : dia;
    let mesStr = mes < 10 ? '0' + mes : mes;
    let horaStr = hora < 10 ? '0' + hora : hora;
    let minStr = min < 10 ? '0' + min : min;
    let segStr = seg < 10 ? '0' + seg : seg;

    let formatoFecha = `${anio}${mesStr}${diaSrt}`;

    return formatoFecha
  }

  async InformeLlamados(rango) {
    let consulta = null;
    const url = `${environment.endPointApiMovil}/llamadozona`;
    let usuario = await this.obtenerUsuario();
    let parametros = this.rangoFechas(rango);

    try{
      consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
      if (consulta.error) { return [] }
    }catch(e){
      consulta = [];
    }
    
    return consulta.data;
  }

  rangoFechas(rango) {
    let parametros = {
      inicio: null,
      termino: null
    }
    if (!rango) {
      parametros.inicio = new Date().setHours(0, 0, 0, 0);
      parametros.termino = new Date().setHours(23, 59, 59, 59);
    } else {
      let date = new Date();
      let fechacontresdiasmas=date.getTime()-(30*24*60*60*1000); 
      parametros.inicio = new Date(fechacontresdiasmas).setHours(0, 0, 0, 0);
      parametros.termino= new Date().setHours(23, 59, 59, 59); 
/*       console.log('ultima', new Date(parametros.inicio)); */
    }
/*     console.log('parametros', parametros); */
    return parametros
  }
}

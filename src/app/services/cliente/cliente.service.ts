//angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//ionic
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

//variables de entorno
import { environment } from 'src/environments/environment';

//interfaces
import { Cliente, Respuesta } from 'src/app/interfaces/cliente/cliente';
import { Evento } from 'src/app/interfaces/cliente/evento';
import { Pedido } from 'src/app/interfaces/cliente/pedido';
import { Flota } from 'src/app/interfaces/cliente/flota';
import { Direccion } from 'src/app/interfaces/direccion/direccion';

//extras
import * as turf from '@turf/turf';
import { map } from 'rxjs/operators';

// servicios
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { LoadingService } from './../loading/loading.service';

// interfaces
import { detalleOV } from 'src/app/interfaces/bodega/interfaces';


@Injectable({
  providedIn: 'root'
})

/**
 *Servicio de cliente
 * @export
 * @class ClienteService
 */
export class ClienteService {
  
  /**
   *Objeto cliente
   * @memberof ClienteService
   */
  cliente = null;

  /**
   *latitud de la geolocalizacion
   * @type {number}
   * @memberof ClienteService
   */
  latitud: number;

  /**
   *longitud de la geolocalizacion
   * @type {number}
   * @memberof ClienteService
   */
  longitud: number;

  /**
   *Creates an instance of ClienteService.
   * @param {HttpClient} httpClient
   * @param {Geolocation} geolocation
   * @param {NativeGeocoder} nativeGeocoder
   * @param { DataLocalService } _dataLocal
   * @memberof ClienteService
   */
  constructor(private httpClient: HttpClient,
      private geolocation: Geolocation,
      private nativeGeocoder: NativeGeocoder,
      private _dataLocal: DataLocalService,
      private _loadingService: LoadingService) { }

  /**
   *header para las peticiones post
   * @memberof ClienteService
   */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });

  /**
   *Configuracion de geolicalizacion
   * @type {NativeGeocoderOptions}
   * @memberof ClienteService
   */
  options: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

  /**
   *Permite obtener los clientes de un vendedor
   * @param {*} vendedor
   * @returns
   * @memberof ClienteService
   */
  async obtenerClientes(vendedor) {
    let consulta = null
   
    const url =`${environment.endPointApiMovil}/listarCO?codigo=${vendedor.codEmpleado}`;
    consulta = await this.httpClient.get<Respuesta>(url, { headers: this.header }).toPromise();
    
    
    return consulta.data;
  }

  /**
   *Permite devolver un cliente seleccionado
   * @param {*} cliente
   * @returns
   * @memberof ClienteService
   */
  async detalleCliente(cliente) {
    this.cliente = cliente;
    return await this.cliente;
  }

  /**
   *Permite obtener un cliente por su rut
   * @param {*} cliente
   * @returns
   * @memberof ClienteService
   */
  async obtenerCliente(cliente) {
    let consulta = null;
    let parametros = {
      rut: cliente.rutCliente.replace('.', '').replace('.', ''),
      token: cliente.rutCliente

    }
    const url = `${environment.endPointApiMovil}/GetDatosCliente`;
   
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
  
    return consulta.data;
  }

  /**
   *Permite obtener el listado de eventos por cliente
   * @param {*} vendedor
   * @param {*} cliente
   * @returns listado de eventos
   * @memberof ClienteService
   */
  async obtenerEventos(vendedor, cliente) {
    let consulta = null
    let parametros = {
      rutVendedor: vendedor.rut.replace('.', '').replace('.', ''),
      rutCliente: cliente.rut.replace('.', '').replace('.', '')

    }
 

    const url = `${environment.endPointApiMovil}/listadoEventos`;
    consulta = await this.httpClient.post<Evento>(url, parametros, { headers: this.header }).toPromise();
   
    return consulta;
  }

  /**
   *Permite obtener el listado de pedidos por cliente
   * @param {*} vendedor
   * @param {*} cliente
   * @returns
   * @memberof ClienteService
   */
  async obtenerPedidos(vendedor, cliente) {
    let consulta = null;
    let parametros = {
      rutVendedor: vendedor.rut,
      rutCliente: cliente.rut

    }
    const url = `${environment.endPointApiMovil}/listadoPedidos`;
    try{
      consulta = await this.httpClient.post<Pedido>(url, parametros, { headers: this.header }).toPromise();
    }catch(e){
      this._dataLocal.presentToast('No se han podido cargar las ov.', 'danger')
      console.log('No se ha podido obtener respuesta asociada  las ov', e);
      consulta = [];
    }

    return consulta;
  }

  /**
   *Permite obtener la flota de un cliente
   * @param {*} cliente
   * @returns
   * @memberof ClienteService
   */
  async obtenerFlota(cliente) {
    let consulta = null
    let parametros = {
      rutCliente: cliente.rut

    }

    const url = `${environment.endPointApiMovil}/listadoFlota`;
    consulta = await this.httpClient.post<Flota>(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }

  /**
   *Permite obtener la forma de pago de los clientes
   * @param {*} rutCliente
   * @returns
   * @memberof ClienteService
   */
  async obtenerFormaPago(recid) {
    let consulta = null
    let respuesta: any[] = []
    const url = `${environment.endPointApiMovil}/formaPago`;
    
    try{
      consulta = await this.httpClient.post(url, { recid }, { headers: this.header })
    .pipe(
      map( (resp: any) => {
        if(resp && !resp.error){
          respuesta = resp.data
        }else{
          this._dataLocal.presentToast("Error al cargar las formas de pago", 'danger');
          respuesta = [];
        }
      })
    )
    .toPromise();
    }catch(e){
      console.log('error al cargar las formas de pago', e);
      this._dataLocal.presentToast("Error al cargar las formas de pago", 'danger');
      respuesta = [];
    }
    
    return respuesta;
  }

  /**
   *Permite crear una cotizacion para un cliente
   *
   * @param {{ codTipoFormaPago: string; rutVendedor: string; rutCliente: string; rutEmisor: string; codSucursal: string; detalle: string; }} parametros
   * @returns
   * @memberof ClienteService
   */
  async crearCotizacion(parametros: { codTipoFormaPago: string; rutVendedor: string; rutCliente: string; rutEmisor: string; codSucursal: string; detalle: string; codOC: string }) {
    let consulta = null
    const url = `${environment.endPointApiMovil}/crearEvento`;
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
  
    return consulta;
  }
  /**
   *Permite registrar el check-in de cliente
   * @param {*} parametros
   * @returns
   * @memberof ClienteService
   */
  async crearVisita(parametros) {

    let consulta = null
    const url = `${environment.endPointApiMovil}/agendarVisita`;
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }

  /**
   *Permite calcular la distancia entre el vendedor y el cliente
   * @param {*} clientes
   * @returns
   * @memberof ClienteService
   */
  async calcularDistancia(clientes) {
    let coordenas = await this.geolocation.getCurrentPosition();

    let { latitude, longitude } = coordenas.coords;
    this.latitud = latitude;
    this.longitud = longitude;

    for (let index = 0; index < clientes.length; index++) {
      const cliente = clientes[index];
      /* if (cliente.direcciones && cliente.direcciones.length > 0) {
   
        cliente.direcciones[0].direccionCompleta = cliente.direcciones[0].direccionCompleta.replace('%', '')
        this.nativeGeocoder.forwardGeocode(cliente.direcciones[0].direccionCompleta, this.options)
          .then((result: NativeGeocoderResult[]) => {
       
            cliente.latitud = result[0].latitude;
            cliente.longitud = result[0].longitude;

            let posicionVendedor = turf.point([this.longitud, this.latitud]);
            let posicionCliente = turf.point([cliente.longitud, cliente.latitud]);

            let distancia = turf.distance(posicionVendedor, posicionCliente, { units: 'kilometers' });
            cliente.distancia = Math.round(distancia);
          })
          .catch((error: any) => console.log(error));
      } */
   
      let posicionVendedor = turf.point([this.longitud, this.latitud]);
      let posicionCliente = turf.point([cliente.longitud, cliente.latitud]);

      let distancia = turf.distance(posicionVendedor, posicionCliente, { units: 'kilometers' });

      cliente.distancia = Math.round(distancia);
      cliente.creditoV = Math.round(Number(cliente.credito) / 1000000);
      cliente.ventaV = Math.round(Number(cliente.creditoUtilizado) / 1000000);
    }
    return clientes;
  }

  async obtenerDetallePedido(folio: string) {
    let respuesta: detalleOV[] = [];

    const url = `${environment.endPointApiMovil}/detallePedido?&folio=${folio}`;

    try{
      await this._loadingService.presentLoading('Cargando detalle Pedido.');
      await this.httpClient.get(url, {headers:this.header} ).pipe(
        map((resp: any) => {
          if(resp && !resp.error){
            respuesta = resp.data
          }else{
            this._dataLocal.presentToast("Error al obtener el detalle del pedido", "danger");
          }
        })
      ).toPromise();
    }catch(e){
      this._dataLocal.presentToast("Error al obtener el detalle del pedido", "danger");
      console.log('error cargando el detalle de la ov '+folio, e);
    }
    this._loadingService.hideLoading();
    return respuesta;
  }
  
}

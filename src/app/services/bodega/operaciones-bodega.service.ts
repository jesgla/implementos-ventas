import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { DataLocalService } from '../dataLocal/data-local.service';
import { AppUtilsService } from '../dataLocal/app-utils.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OperacionesBodegaService {

  //@TODO: cargar desde property 
  //apiUrl = "http://192.168.1.104:3800/api/movil";
  //apiUrl = "https://dev-api.implementos.cl/api/mobile";
  apiUrl = `${environment.endPointApiMovil}`;
  
  
  apiUrlGetGuiaTR = `${this.apiUrl}/recibos/getGuia`;
  apiUrlGetOC = `${this.apiUrl}/recibos/getOc`;
  apiUrlGetGuiasLPN = `${this.apiUrl}/recibos/getGuiasLpn`;

  //apiUrlTemporalRecibirGuia = `${this.apiBase}/recibo/guardar`;
  apiUrlRecibirDocumento = `${this.apiUrl}/recibos/documento`;
  apiUrlRecibirOV = `${this.apiUrl}/recibos/ov`;

  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });

  documentosCargados=[];


  constructor(  private http: HttpClient
              , private dataLocal:DataLocalService
              , private fileTransfer: FileTransfer
              , private appUtilsService: AppUtilsService
              ) { }

  analizarCodigo( lectura : string ){
    return lectura;
  }

  async obtenerTrFromNroGuia( nroGuia:string ){
    let consulta = null
    let respuesta: any
    
    
    var url = `${this.apiUrlGetGuiaTR}?folioGuia=${nroGuia}`;
    let token_jwt = await this.dataLocal.getItem('jwt');
    url += `&token=${token_jwt}`;
    
    
    try{
      respuesta = await this.http.get(url).toPromise();
      console.log(respuesta);
      if(respuesta.error){
        //throw new Error( respuesta.errorDetalle || 'No se pudo obtener documento' )
        await this.dataLocal.presentToast(respuesta.errorDetalle,'danger',3000);
        return null;
      }
    }catch(e){
      console.log('error al cargar la info', e);
      await this.dataLocal.presentToast('Error al conectar con el servidor','danger',3000);
      return null;
    }
    
    return respuesta;
  }

  async obtenerOrdenDeCompra( nroOc:string ){
    let consulta = null
    let respuesta: any
    
    var url = `${this.apiUrlGetOC}?numeroOC=${nroOc}`;
    let token_jwt = await this.dataLocal.getItem('jwt');
    url += `&token=${token_jwt}`;

    try{
      respuesta = await this.http.get(url).toPromise();
      console.log(respuesta);
      if(respuesta.error){
        //throw new Error( respuesta.errorDetalle || 'No se pudo obtener documento' )
        await this.dataLocal.presentToast(respuesta.errorDetalle,'danger',3000);
        return null;
      }
    }catch(e){
      console.log('error al cargar la info', e);
      await this.dataLocal.presentToast('Error al conectar con el servidor','danger',3000);
      return null;
    }
    
    return respuesta;
  }

  async obtenerGuiasLPN( nroLPN:string ){
    let consulta = null
    let respuesta: any
    
    var url = `${this.apiUrlGetGuiasLPN}?lpn=${nroLPN}`;
    let token_jwt = await this.dataLocal.getItem('jwt');
    url += `&token=${token_jwt}`;

    
    try{
      respuesta = await this.http.get(url).toPromise();
      console.log(respuesta);
      if(respuesta.error){
        //throw new Error( respuesta.errorDetalle || 'No se pudo obtener documento' )
        await this.dataLocal.presentToast(respuesta.errorDetalle,'danger',3000);
        return null;
      }
    }catch(e){
      console.log('error al cargar la info', e);
      await this.dataLocal.presentToast('Error al conectar con el servidor','danger',3000);
      return null;
    }
    
    return respuesta;
  }

  async recibirItemsGuia( recibo ){
    let consulta = null
    let respuesta: any
    
    let url = `${this.apiUrlRecibirDocumento}`;
    let token_jwt = await this.dataLocal.getItem('jwt');
    url += `?token=${token_jwt}`;

    recibo.estado = "true";
    recibo.mensaje = "recibir";
    recibo.recurso = "guia";
    
    

    try{
      respuesta = await this.http.post(url,recibo,{ headers: this.header } ).toPromise();
      console.log(respuesta);
      
      
    }catch(e){
      console.log('error al cargar la info', e);
    }

    if(respuesta.error){
      await this.dataLocal.presentToast( (respuesta.errorDetalle || 'ERROR' ),'danger',3000);
    }    
    
    if(respuesta && respuesta.data && respuesta.data.estado){
      return respuesta.data.estado;
    }
    return null;
  }

  async recibirItemsOc( recibo ){
    let consulta = null
    let respuesta: any

    let url = `${this.apiUrlRecibirDocumento}`;
    let token_jwt = await this.dataLocal.getItem('jwt');
    url += `?token=${token_jwt}`;

    recibo.estado = "true";
    recibo.mensaje = "recibir";  
    recibo.recurso = "oc";
    
    try{
      respuesta = await this.http.post(url,recibo,{ headers: this.header } ).toPromise();
      console.log(respuesta);
      
    }catch(e){
      console.log('error al cargar la info', e);
    }
    
    if(respuesta.error){
      await this.dataLocal.presentToast( (respuesta.errorDetalle || 'ERROR' ),'danger',3000);
    }    
    
    if(respuesta && respuesta.data && respuesta.data.estado){
      return respuesta.data.estado;
    }

    return null;
  }

  async realizarCierreGuia( documento ){
    let consulta = null
    let respuesta: any
    
    let url = `${this.apiUrlRecibirDocumento}`;
    let token_jwt = await this.dataLocal.getItem('jwt');
    url += `?token=${token_jwt}`;
    
    let json = {
      "estado":"true",
      "mensaje":"cerrartr",
      "recurso":"guia",
      "identificador":documento.documentoNro,
      "documento":documento.documentoRaiz, 
      "almacen_envia":documento.dataTR.almacen_envia,
      "almacen_recibe":documento.dataTR.almacen_recibe,
      "items":[ ]
      }

      try{
        respuesta = await this.http.post(url,json,{ headers: this.header } ).toPromise();
        console.log(respuesta);
        
      }catch(e){
        console.log('error al cargar la info', e);
      }
      
      return respuesta.data.estado;
  }



  async subirImagenTemporalRecibo( imgData:string, dataInfo:any ){
    
    let token_jwt = await this.dataLocal.getItem('jwt');

    const options: FileUploadOptions = {
      fileKey: 'archivo',
      headers:{
        'x-token': token_jwt
      },
      params:{
        ...dataInfo
      }
    };

   const fileTransfer: FileTransferObject = this.fileTransfer.create();
    
   let url = `${this.apiUrl}/recibos/temporal/upload`;
    
   try{
      console.log('comienza upload. imgData:',imgData);
      let resp = await fileTransfer.upload( imgData, url , options).then((res)=>{console.log('dentro then',res); return res;});
      console.log('termina la subida. Resp:',resp); 
      return resp;
   }catch(e){
      console.error('Error al subir imagen: ',e);
      return false;
   }
  }

  generarUuid(){
    let random = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    return random;
  }

  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('jwt');

    return usuario;
  }


  async recibirOV(guia:number,ov:string, almacenEnvia:string, almacenRecibe:string){
    console.log('service.recibirOV[INI] guia: ',guia,'OV:',ov, 'almacenEnvia',almacenEnvia, 'almacenRecibe',almacenRecibe);
    console.log('se consume servicio');

    let consulta = null
    let respuesta: any
    
    let url = `${this.apiUrlRecibirOV}`;
    let token_jwt = await this.dataLocal.getItem('jwt');
    url += `?token=${token_jwt}`;
    
    let json = {
        guia: guia,
        ov: ov,
        almacenEnvia:almacenEnvia,
        almacenRecibe:almacenRecibe
      }
      try{
        respuesta = await this.http.post(url,json,{ headers: this.header } ).toPromise();
        console.log('respuesta',respuesta);
        
        if(respuesta.error){
          throw new Error(respuesta.errorDetalle);
        }        
        
      }catch(e){
        console.log('error en el servicio', e);
        await this.appUtilsService.presentToast(e.toString(),'danger');
        return false;
      }


    return true;
  }

  

}

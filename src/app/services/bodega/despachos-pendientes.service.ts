import { Injectable } from '@angular/core';
import { DataLocalService } from '../dataLocal/data-local.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators'
import { environment } from 'src/environments/environment';

// interfaces
import { despachoPendiente } from 'src/app/interfaces/bodega/interfaces'

@Injectable({
  providedIn: 'root'
})
export class DespachosPendientesService {

  constructor(private http: HttpClient, private dataLocal:DataLocalService, private _DataLocalService: DataLocalService) { }

  async getDespachosPendientes(codBodega: string){
    let url = `${environment.endPointApiLogistica}/despachos-pendientes?codBodega=${codBodega}`;

    try{
      return await this.http.get(url).pipe( map( (result: any) =>{
        if(result && !result.error){
          return result.data;
        }else{
          this._DataLocalService.presentToast("No ha sido posible obtener los despachos pendientes.", "danger");
          console.log('No ha sido posible obtener los despachos pendientes '+result.error);
          return []
        }
      })
      ).toPromise();
    }catch(e){
        this._DataLocalService.presentToast("No ha sido posible obtener los despachos pendientes.", "danger");
        console.log('No ha sido posible obtener los despachos pendientes '+e);
        return [];
    }
  }

}

//angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject, Observable} from 'rxjs';

//ionic
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';

//services
import { DataLocalService } from '../dataLocal/data-local.service';
import { UsuarioService } from './../usuario/usuario.service';
import { NotificationsService } from './../notifications/notifications.service';

//environment
import { environment } from 'src/environments/environment';

//iterfaces
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';

/**
 * constante 
 */
const TOKEN_KEY = 'auth-token';
const TOKEN_JWT = 'jwt';
/**
 *Servicio que permite obtener sesion de usuario
 * @export
 * @class AutentificacionService
 */
@Injectable({
  providedIn: 'root'
})

export class AutentificacionService {
  authenticationState = new BehaviorSubject(false);

    /**
   *validacion de sesion 
   * @memberof AutentificacionService
   */
  app_version: string = '1.12.0';
  checkVersion: {error: boolean, valid: boolean};
  

  constructor(private router: Router,
    private storage: Storage,
    private plt: Platform,
    private httpClient: HttpClient,
    private dataLocal: DataLocalService,
    private _UsuarioService: UsuarioService,
    private _NotificationsService: NotificationsService)
    {
    this.plt.ready().then(() => {
      this.initLogin();
    });
  }

  async initLogin(){
    await this.isValidVersion();
    if(!this.checkVersion.error && this.checkVersion.valid){
      this.checkToken();
    }else{
      this.logout();
    }
  }

  /**
   *Permite validar si existe usuario logueado
   * @returns objeto de vendedor
   * @memberof AutentificacionService
   */
  async checkToken() {
    let credenciales = await this.storage.get('CREDENCIALES');

    if(credenciales) {
      credenciales =  JSON.parse(credenciales);
      this.login(credenciales);
    }
  }

  /**
   *Permite consultar por usuario que quiere iniciar sesion en la aplicacion
   * @param {*} usuario
   * @returns
   * @memberof AutentificacionService
   */
  async login(usuario) {
   
    if(await this.isValidVersion()){
      let consulta = null;
      try{
        const url = `${environment.endPointApiMovil}/loginUsuario`;
        consulta = await this.httpClient.post<Vendedor>(url, usuario).toPromise();

        if(consulta && consulta.error) return this.dataLocal.presentToast(consulta.msg,'danger');
      
        if (consulta.length > 0) {
          console.log('respuesta',consulta);
          await this.storage.remove(TOKEN_KEY);
          await this.storage.remove(TOKEN_JWT);
          await this.storage.remove('CREDENCIALES');
          this.storage.set(TOKEN_JWT, consulta[(consulta.length-1)]['jwt']);

          return this.storage.set(TOKEN_KEY, JSON.stringify(consulta[0])).then(() => {
            this.storage.set('CREDENCIALES', JSON.stringify(usuario));
            this.authenticationState.next(true);
          });
        } else {
          this.dataLocal.presentToast('Usuario o contraseña incorrecta','danger');
        }
      }catch(e){
        this.dataLocal.presentToast(e.error? e.error.trim() : 'Error al conectar con el servidor', 'danger');
        console.log('error login', e);
      }
    }
  }

  /**
   *Permite cerrar sesion en la aplicacion  
   * @returns
   * @memberof AutentificacionService
   */
  async logout() {
    await this.dataLocal.removeSeccion();
    await this.storage.clear();
    await this.dataLocal.eliminarDatos('vendedor');

    await this._UsuarioService.removeUserLoged();
    this._NotificationsService.landing_pageByNotification = null;
    await this.router.navigate(['login']);

    return await this.authenticationState.next(false);
  }

  /**
   *Permite autenticar el usuario logueado
   * @returns
   * @memberof AutentificacionService
   */
  isAuthenticated() {
    return this.authenticationState.value;
  }


  async isValidVersion(){
    try{
      let url = `${environment.endPointApiMovil}/valAppVersion/?app=implementos_ventas&version=${this.app_version}&platform=${this.plt.is('ios')? 'ios' : 'android'}`;

      let resp = await this.httpClient.get<{error: boolean, data: boolean, msg: string}>(url).toPromise();
      this.checkVersion = resp? {error: resp.error, valid: resp.data} : {error: true, valid: false}
    }catch(e){
      console.log("No ha sido posible verificar la version de la aplicacion ", e);
      this.dataLocal.presentToast('Ha ocurrido un error al intentar validar versión', 'danger');
      this.checkVersion = {error: true, valid: false};
    }

    return this.checkVersion.valid;
   }

}

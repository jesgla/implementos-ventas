// angular
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

//environment
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriasArticulosService {

  menuCategorias : any[] = [];
  menuCategoriasXproducto: any[] = [];
  menuFiltros: any[] = [];
  showFiltros: boolean = false;

  selectedCategoria: string = '';
  selectedFiltros: any[] = [];

  buscarMenuSubject$: Subject<any> = new Subject();
  readonly buscarMenuObserver$: Observable<any> = this.buscarMenuSubject$.asObservable();

  menuSubject$: Subject<any> = new Subject();
  readonly menuObservable$: Observable<any> = this.menuSubject$.asObservable();

  constructor(private http: HttpClient) { }

  async getCategorias(){

  }

  async getCategoriasMenu() {
    const url = `${environment.endPointApiCms}/categories/categorias-header/`;
    let consulta: any = await this.http.get(url).toPromise();
    if(consulta){
      this.menuCategorias = consulta.data;
    }else{
      this.menuCategorias = [];
    }
  }

  // se ejecuta cuando es requiere buscar al seleccionar una categoria desde el menu de categorias.
  buscarCategoria(categoria: string){
    categoria = categoria.split('/').join('');
    this.selectedCategoria = categoria;
    this.buscarMenuSubject$.next({categoria: this.selectedCategoria});
  }

  buscarFiltro(filtro: string, tipo: string){
    this.selectedFiltros.push({filtro: filtro, tipo: tipo});
    this.buscarMenuSubject$.next({categoria: this.selectedCategoria});
  }

  setFiltroUrl(){
    let url = '';
    if(this.selectedFiltros){
      this.selectedFiltros.forEach( filtro => {
        url = url + `&filter_${filtro.tipo}=${encodeURIComponent(filtro.filtro)}`
      })
    }
    return url;
  }

  // Metodo para setear el menu de categorias, dependiendo de las categorias que se trae dentro de una busqueda de productos.
  setCategoriasXbusqueda(categoriasArticulos: any[]){
    this.menuCategoriasXproducto = categoriasArticulos;
  }

  // Permite setear los filtros que se mostrarán luego de haber realizado una busqueda en el catalogo.
  setFiltrosXbusqueda(filtros: any[]){
    this.menuFiltros = [];
    var keyNames = Object.keys(filtros);
    keyNames.forEach( filtro => {
       this.setFiltrosArray(filtro, filtros[filtro]);
    });
    this.showFiltros = true;
  }

  showFiltrosSubmenu(){
    this.showFiltros = true;
  }

  setFiltrosArray(filtroName: string, filtroValues: any[]){
    let filtros: any[] = [];

    filtroValues.forEach( filtro => {
      filtros.push({title: filtro});
    })

    this.menuFiltros.push({title: filtroName, children: filtros });
  }
  
  async mostrarMenu(show: boolean = true){
    this.menuSubject$.next(show);
  }

  // Metodo utilizado para resetear los filtros aplicados.
  resetFiltros(){
    this.menuFiltros = [];
    this.showFiltros = false;
    this.menuCategoriasXproducto = [];
    this.selectedCategoria = '';
    this.selectedFiltros = [];
  }

  // Funcion que permite validar si el filtro  ya esta seleccionado, con el fin de no volver a mostrarlo en la lista de filtros.
  valSelectedFiltro(Filtro: string){
    let resp: boolean = false;
    let existe = this.selectedFiltros.filter( filtro => filtro.tipo === Filtro);
    existe[0]? resp = true : resp = false;

    return resp;
  }

  limpiarFiltros(){
    this.resetFiltros();
    this.buscarMenuSubject$.next({categoria: this.selectedCategoria});
  }

  // Metodo que nos permite quitar la categoria ya seleccionada. eliminando tambien los filtros seleccionados y luego realizando la busqueda nuevamente si existe un texto en el buscador.
  quitarCategoria(){
    this.selectedFiltros = []
    this.selectedCategoria = '';
    this.buscarMenuSubject$.next({categoria: this.selectedCategoria});
  }

  // Metodo que nos permite quitar un filtro ya seleccionado y luego emitir la busqueda denuevo.
  quitarFiltro(filtroAplicado: string){
    this.selectedFiltros = this.selectedFiltros.filter(filtro => filtro.filtro != filtroAplicado);
    this.setFiltroUrl();
    this.buscarMenuSubject$.next({categoria: this.selectedCategoria});
  }
}

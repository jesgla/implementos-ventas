export interface ArticuloQuiebre{
    sku: string;
    sucursal: string;
    cantidad: string;
    assortment: number;
    minimo: number;
    categoria: string,
    tipoTienda: number;
    tipoCia: number;
    img?: string;
    costo: number;
    costoTotal: number;
    semanasVenta: number;
    promedioVenta: number;
    Vigente: boolean;
    quiebre: boolean;
    precio: number;
    uen: string;
    pendienteEntrega: number;
    pendienteRecepcion: number;
    mes: string;
    semana: string;
    calculable: number;
    venta: number;
    promedioVentaSemana: number;
    sd: number;
    avg: number;
    stockCD: number;
    disponibilidad: number;
    perdidaCalculada?: number;
}

export interface SucursalQuiebre{
    _id: string;
    Total: number;
    TotalconStock: number;
    CostoTotalconStock: number;
    Assortment: number;
    CostoAssortment: number;
    QuebradosAssortment: number;
    PerdidaAssortment: number;
    OcumuladoAssortment: number;
    DisponibilidadAssorment: number;
    CiaTipo1y2: number;
    CostoCiaTipo1y2: number;
    QuebradosCiaTipo1y2: number;
    PerdidaCiaTipo1y2: number; 
    OcumuladoCiaTipo1y2: number; 
    DisponibilidadCia: number; 
    TiendaTipo1y2: number; 
    QuebradosTiendaTipo1y2: number; 
    PerdidaTiendaTipo1y2: number; 
    OcumuladoTiendaTipo1y2: number; 
    DisponibilidadTienda: number; 
}

export interface parametrosUrl{
    page: number,
    sucursal: string;
    categoria: string;
    quebrados?: number;
    filtro: number,
}
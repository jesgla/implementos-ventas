/**
 * clase de articulos
 * @export
 * @class Articulo
 * @ignore
 */

export class Articulo {
  _id: string;
  sku: string;
  categoria: string;
  cod_id_proveedor: string;
  costo_cero: number;
  costo_financiero: number;
  estado: string;
  fabricante: string;
  id_categoria: number;
  id_linea: number;
  id_marca: string;
  sku_matriz: string;
  linea: string;
  marca: string;
  nombre: string;
  numero_parte: string;
  atributos: Atributo[];
  p_minimo: string;
  product: number;
  uen: string;
  unit_id: string;
  fullText: string;
  descripcion: string;
  image_recid: number;
  images: Images[];
  precio_escala: boolean;
  matriz: Matriz[];
  precio: Precio;
  cantidad?:number;
  stockorden:number =0;
  precioNeto?: number;
  precioComunNeto?: number;
  precioEscaladoNeto?: number;
}

/**
 *interface de precio de producto
 * @ignore
 * @interface Precio
 */
interface Precio {
  desde: number;
  rut: string;
  sucursal: string;
  hasta: number;
  ingreso: string;
  precio: number;
  recid: number;
  vigencia: string;
}

/**
 *
 * @ignore
 * @interface Atributo
 */
interface Atributo {
  nombre: string;
  valor: string;
  interno: number;
  recid: number;
}

/**
 *
 * @ignore
 *
 * @interface RootObject
 */
interface RootObject {
  items: Item[];
  totalRegistros: number;
  paginaActual: number;
  totalPaginas: number;
  mostrandoDesde: number;
  mostrandoHasta: number;
  filter: any[];
  category: any[];
}

/**
 *
 *
 * @ignore
 * @interface Item
 */
interface Item {
  _id: string;
  sku: string;
  categoria: string;
  cod_id_proveedor: string;
  costo_cero: number;
  costo_financiero: number;
  estado: string;
  fabricante: string;
  id_categoria: number;
  id_linea: number;
  id_marca: string;
  sku_matriz: string;
  linea: string;
  marca: string;
  nombre: string;
  numero_parte: string;
  atributos: Atributo[];
  p_minimo: string;
  product: number;
  uen: string;
  unit_id: string;
  fullText: string;
  descripcion: string;
  image_recid: number;
  precio_escala: boolean;
  matriz: Matriz[];
  precio: Precio;
}

/**
 *
 *
 * @ignore
 * @interface Precio
 */
interface Precio {
  desde: number;
  rut: string;
  sucursal: string;
  hasta: number;
  ingreso: string;
  precio: number;
  recid: number;
  vigencia: string;
}

/**
 *
 *
 * @ignore
 * @interface Matriz
 */
interface Matriz {
  sku: string;
}

/**
 *
 *
 * @ignore
 * @interface Atributo
 */
interface Atributo {
  nombre: string;
  valor: string;
  interno: number;
  recid: number;
}

/**
 *
 *
 * @ignore
 * @interface Images
 */
interface Images{
  150: any[];
  250: any[];
  450: any[];
  600: any[];
  1000: any[];
  2000: any[];
}
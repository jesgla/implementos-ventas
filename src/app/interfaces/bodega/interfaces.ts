
/**
 *
 *
 * @export
 * @ignore
 * @interface LecturaRecibo
 */
export interface LecturaRecibo {
    tipo: "GUIA-TR" | "OC" | "OV" | "LPN" | "PRODUCTO";
    numeroDocumento: string;
  }

  export interface despachoPendiente {
    tipo: string;
    documento: string;
    dlvdate: Date;
    diasretraso: number;
    origen: string;
    destino: string;
    estado: string;
    modoDespacho: string;
    pickingrouteid: number;
    address: string;
    monto: number;
    cantidad: number;
    vigente: boolean;
    notificacion: number;
  }

  export interface detalleOV{
    sku: string;
    nombre: string;
    cantidad: number;
    precio: number;
    descuento: number;
    neto: number,
    iva: number;
    total: number;
    direccion?: string;
    p_minimo?: number;  
  }

  export interface retiroPendiente{
    _id: string;
    ordenVenta: string;
    responsable: string;
    createdAt: Date;
    updatedAt: Date;
    listoParaEntrega: boolean;
    fechaSetResponsable: Date;
    fechaSetListoRetiro: Date;
    nombrecliente: string;
    rutCliente: string;
    cantNotificaciones: number;
    fechaCompromiso: Date;
    codBodega: string;
  }
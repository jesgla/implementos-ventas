import { Objetivos } from '../vendedor/objetivos';

export class Informe {
    titulo:string ;
    header?:any= ['Zona', 'Venta MM', 'Meta MM', '%'];
    headerLlamadas?:any= ['Zona', 'Llamadas realizadas', 'Total llamadas'];
    body: Objetivos[];
    totalVenta:number= 0;
    totalMeta:number=  0;
    promedio:number=  0;
    totalVentaD:number=  0;
    totalMetaD: number= 0;
    promedioD: number= 0;
  }

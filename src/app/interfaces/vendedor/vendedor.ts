/**
 *
 *
 * @export
 * @ignore
 * @class Vendedor
 */
export class Vendedor {
  codUsuario: number;
  codEmpleado: number;
  codSucursal: string;
  rut: string;
  codBodega: string;
  codPerfil: number;
  nombre: string;
  nombreSucursal: string;
  codCliente: number;
  _id: number;
  nombreCompleto: string;
  rutEmpleado: string;
  vigente: number;
  perfil: number;
}
/**
 *
 *
 * @export
 * @ignore
 * @interface Direccion
 */
export interface Direccion {
  codDireccion: string;
  direccion: string;
  numero?: any;
  codComuna: number;
  codLocalidad: number;
  codProvincia: number;
  codRegion: number;
  comuna: string;
  localidad: string;
  provincia?: any;
  ciudad: string;
  telefono?: any;
  aprobada: number;
  validada: number;
  tipo: number;
  codPostal?: any;
  latitud: string;
  longitud: string;
}
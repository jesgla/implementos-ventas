export interface oportunidades_conceptos {
    nombre: string;
    tipo: string;
    estado: number;
    creacion: Date;
} 

export interface Oportunidad{
    id_visita: string;
    monto_magnitud: number;
    concepto_creacion: string;
    concepto_cierre: string;
    fecha_limite: Date;
    fecha_cierre: Date;
    fecha_creacion: Date;
    rutVendedor: string;
    rutCliente: string;
    clienteNombre: string;
    OportunidadOpen: boolean;
    probabilidad: string;
    oportunidadConcretada: boolean;
    zonaVendedor: string;
    sucursalVendedor: string;
    notaVenta: any[];
    productos: any[];
    codEmpleado: string;
    vendedorNombre?: string;
    origenEvento?: string;
}

export interface ResumenOportunidades{
    montoTotal: number;
    cantidad: number;
    cantidadTotalWhatsapp?: number;
    cantidadTotalVisita?:  number;
    cantidadTotalLlamada?:  number;
    cantidadTotalVideoconferencia?:  number;
    montoTotalWhatsapp?: number;
    montoTotalVisita?: number;
    montoTotalLlamada?: number;
    montoTotalVideoconferencia?: number;
    montoTotalAlta: number;
    montoTotalMedia: number;
    montoTotalBaja: number;
    cantTotalAlta: number;
    cantTotalMedia: number;
    cantTotalBaja: number;
    cerradas: {
        total: number;
        totalWhatsapp: number;
        totalVisita: number;
        totalLlamada: number;
        totalVideoconferencia: number;
        montoTotalWhatsapp: number;
        montoTotalVisita: number;
        montoTotalLlamada: number;
        montoTotalVideoconferencia: number;
        totalConcretadas: number;
        totalNoConcretadas: number;
        montoConcretadas: number;
        montoNoConcretadas: number;
        montoTotal: number;
        montoTotalAlta: number;
        montoTotalMedia: number;
        montoTotalBaja: number;
        cantTotalAlta: number;
        cantTotalMedia: number;
        cantTotalBaja: number;
        conceptosCierre: [{
            concepName: string;
            concepCantidad: number;
            concepMontoTotal: number;
            concepCantWhatsapp?: number;
            concepCantVisita?: number;
            concepCantLlamada?: number;
            concepCantVideoconferencia?: number;
            concepMontoWhatsapp?: number;
            concepMontoVisita?: number;
            concepMontoLlamada?: number;
            concepMontoAltaVideoconferencia?: number;
            concepMontoAlta: number;
            concepMontoMedia: number;
            concepMontoBaja: number;
            concepCantAlta: number;
            concepCantMedia: number;
            concepCantBaja: number;
        }],
        conceptosCreacion: [{
            concepName: string;
            concepCantidad: number;
            concepMontoTotal?: number;
            concepCantWhatsapp?: number;
            concepCantVisita?: number;
            concepCantLlamada?: number;
            concepCantVideoconferencia?: number;
            concepMontoWhatsapp?: number;
            concepMontoVisita?: number;
            concepMontoLlamada?: number;
            concepMontoAltaVideoconferencia?: number;
            concepMontoAlta: number;
            concepMontoMedia: number;
            concepMontoBaja: number;
            concepCantAlta: number;
            concepCantMedia: number;
            concepCantBaja: number;
        }]
    },
    abiertas:{
        total: number;
        totalWhatsapp?: number;
        totalVisita?: number;
        totalLlamada?: number;
        totalVideoconferencia?: number;
        montoTotalWhatsapp?: number;
        montoTotalVisita?: number;
        montoTotalLlamada?: number;
        montoTotalVideoconferencia?: number;
        montoTotal: number;
        montoTotalMedia: number;
        montoTotalBaja: number;
        cantTotalAlta: number;
        cantTotalMedia: number;
        cantTotalBaja: number;
        conceptosCreacion: [{
            concepName: string;
            concepCantidad: number;
            concepMontoTotal: number;
            concepCantWhatsapp?: number;
            concepCantVisita?: number;
            concepCantLlamada?: number;
            concepCantVideoconferencia?: number;
            concepMontoWhatsapp?: number;
            concepMontoVisita?: number;
            concepMontoLlamada?: number;
            concepMontoAltaVideoconferencia?: number;
            concepMontoAlta: number;
            concepMontoMedia: number;
            concepMontoBaja: number;
            concepCantAlta: number;
            concepCantMedia: number;
            concepCantBaja: number;
        }]
    }
}
/**
 * clase de hua
 * @export
 * @class Hua
 * @ignore
 */

import { StringMap } from '@angular/compiler/src/compiler_facade_interface';

 export interface detalleHua {
    "_id": string;
    "sku": string;
    "nombre": string;
    "categoria": string;
    "estado": string;
    "direccion": string;
    "gmroi": string;
    "cantidad": number;
    "cartel": number;
    "caras": number;
    "area": string;
    "departamento": string;
    "pasillo": string;
    "lineal": string;
    "metro": string;
    "posicion": string;
    "sucursal": string;
    "tipologiaClacom": number;
    "tipologiaTienda": number;
    "ventaPromedioMesual": string;
    "ventaPromedioSemanal": string;
    "stockMaximoReal": number;
    "stockMinimoReal": number;
    "pendienteEntrega": number;
    "pendienteRecepcion": number;
    "PVP": number;
    "disponibilidad": number;
    "stockMinimoPropuesto": number;
    "stockMaximoPropuesto": number;
    "assortment": string;
    "ventaS1": number;
    "ventaS2": number;
    "ventaS3": number;
    "ventaS4": number;
    "gmroiTienda": string;
    "img": string;
    "stockCD": number;
    "cantidadUltimaCompra": number;
    "fechaUltimaCompra": string;
    "cantidadUltimaReposicion": number;
    "fechaUltimaReposicion": string;
    "cantidadUltimaVenta": number;
    "fechaUltimaVenta": string;
    "cantidadEmpuje": number;
 }
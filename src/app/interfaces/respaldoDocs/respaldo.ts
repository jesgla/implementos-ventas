
export interface Respaldo {
  intRegistro : any;
  intConductor: number;
  intEstadoDocumento : number;
  intPagoCaja: number;
  strTipoDocumento: string;
  strImagen64?: any[];
  strObservacion?: string;
  intCodMotivo?: number;
}


export interface DocumentoRespaldo{
  intRegistro: number;
  strSalesID: string;
  strTipoDoc: string;
  strFolioDoc: string;
  strDireccionDesp: string;
  intConductor: number;
  strNombreConductor: string;
  strPerfilConductor: string;
  strNombreCliente: string;
  strRutCliente: string;
  intEstadoDocumento: number;
  strNombreEstado: string;
  intCodMotivo: number;
  strNombreMotivo: string;
  strObservacion: string;
  dtFechaEstado: Date;
  dtFechaDocumento: Date;
  strHoraRecibo: string;
  strImagen64: any[];
  intPagoCaja: number;
  strTipoDocumento: string;
  strCodGeorefencia: string;
}

export interface Transportista{
  strUsuario: string;
  strNombreUsuario: string;
  intPerfilUsuario: number;
  intCodUsuario: number;
  intCodEmpleado: number;
}

export interface MotivoNoEntrega{
  intCodMotivo: number;
  strNombreMotivo: string;
}
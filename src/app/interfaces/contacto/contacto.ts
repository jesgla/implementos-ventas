/**
 *
 *
 * @export
 * @ignore
 * @interface Contacto
 */
export interface Contacto {
  codCliente: number;
  rut?: any;
  nombre: string;
  apellidop?: any;
  apellidom?: any;
  email: string;
  telefono: string;
  tipo?: any;
  tipoPago?: any;
  direccionDefault?: any;
  direccionDefaultLatitud?: any;
  direccionDefaultLongitud?: any;
  direccionDefaultvalidada: number;
  credito?: any;
  venta?: any;
  meta?: any;
  fechaUltimaVisita?: any;
  fechaProximaVisita?: any;
  direccion?: any;
  codEstado: number;
  codGiro: number;
  estado?: any;
  nombreVendedor?: any;
}
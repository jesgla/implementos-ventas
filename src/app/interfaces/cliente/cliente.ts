/* export class Cliente {
  codCliente: number;
  rut?: string;
  nombre?: string;
  apellidop?: any;
  apellidom?: any;
  email?: any;
  telefono?: any;
  tipo?: any;
  tipoPago?: any;
  direccionDefault: string;
  direccionDefaultLatitud: string;
  direccionDefaultLongitud: string;
  direccionDefaultvalidada: number;
  credito: string;
  venta: string;
  meta: string;
  fechaUltimaVisita: string;
  fechaProximaVisita?: any;
  direccion?: any;
  codEstado: number;
  codGiro: number;
  estado: string;
  nombreVendedor?: any;
  distancia?:number;
  creditoM? : number;
  ventaM? : number;
} */

/**
 * @ignore
 */
export class Respuesta {
  error: boolean;
  msg: string;
  data: Cliente[];
}
/**
 *@ignore
 *
 * @export
 * @class Cliente
 */
export class Cliente {
  vendedores: Vendedore[];
  vendedor_cliente: Vendedore[];
  documento_cobros: Documentocobro[];
  _id: string;
  rut: string;
  clasificacionFinanciera: string;
  tipoCartera: string;
  ciudad: string;
  provincia: string;
  clasificacion: string;
  condicionPago: string;
  credito: number;
  creditoUtilizado: number;
  formaPago: string;
  giros: Giro[];
  nombre: string;
  segmento?: string;
  subSegmento?: string;
  correos?: Correo[];
  telefonos?: any;
  direcciones?: Direccione[];
  clasificacionCLPot: string;
  estado: string;
  contactos: Contacto[];
  createdAt: string;
  updatedAt: string;
  tipo_cliente: number;
  recid: number;
  CAT_FUGA: string;
  recomendacionClienteObject: RecomendacionClienteObject[];
  transforma_recomendacion: boolean;
  cod_region?: string;
  creditoM? : number;
  ventaM? : number;
  latitud? : number;
  longitud? : number;
  distancia?:number;
  oportunidades?: number;
}

/**
 * @ignore
 */
export class RecomendacionClienteObject {
  cluster: number;
  rut: string;
  producto: string;
  probabilidad: number;
  tipo: number;
  articulosObject: ArticulosObject[];
}

/**
 * @ignore
 */
export class ArticulosObject {
  _id: string;
  sku: string;
  categoria: string;
  linea: string;
  marca: string;
  nombre: string;
  numero_parte: string;
  uen: string;
  unit_id: string;
}

/**
 * @ignore
 */
export class Contacto {
  nombre: string;
  apellido: string;
  contactoDe: string;
  correo: string;
  contactoId: string;
  telefono?: string;
}

/**
 * @ignore
 */
export class Direccione {
  calle: string;
  comuna: string;
  direccionCompleta: string;
  tipo: string;
  numero?: string;
}

/**
 * @ignore
 */
export class Telefono {
  telefono: string ="";
  constructor(){
    this.telefono ="";
  }
}

/**
 * @ignore
 */
export class Correo {
  correo: string;
}

/**
 * @ignore
 */
export class Giro {
  codigo: string;
  nombre: string;
}

/**
 * @ignore
 */
export class Documentocobro {
  tienda: string;
  nota_venta: string;
  folio: string;
  fechaCreacion: string;
  fechaVencimiento: string;
  monto: number;
  abono: number;
  saldo: number;
  estado: string;
}
/**
 * @ignore
 */
export class Vendedore {
  _id: number;
  clasificacion: string;
  nombre: string;
}
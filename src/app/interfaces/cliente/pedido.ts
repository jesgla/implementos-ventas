/**
 *
 *
 * @export
 * @ignore
 * @interface Pedido
 */
export interface Pedido {
  rutVendedor: string;
  rutCliente: string;
  nombreCliente: string;
  fecha: string;
  folio: string;
  Venta: number;
  saldo: number;
  estado: string;
  ordenC?: string;
}
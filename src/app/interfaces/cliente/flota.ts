/**
 *
 *
 * @export
 * @ignore
 * @interface Flota
 */
export interface Flota {
  rutCliente: string;
  Buses: number;
  Camiones: number;
  Maquinarias: number;
  Remolques: number;
  
}

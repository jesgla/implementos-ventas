import { NgModule, LOCALE_ID } from '@angular/core';
/* import es from '@angular/common/locales/es'; */
import { registerLocaleData } from '@angular/common';
import localeEsCl from '@angular/common/locales/es-CL';

registerLocaleData(localeEsCl);

import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';

import { CallNumber } from '@ionic-native/call-number/ngx';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClienteService } from './services/cliente/cliente.service';
import { EventosComponent } from './pages/clientePage/components/eventos/eventos.component';
import { VendedorService } from './services/vendedor/vendedor.service';
import { EventoService } from './services/evento/evento.service';
import { SeccionesService } from './services/secciones/secciones.service';
import { IonicStorageModule } from '@ionic/storage';
import { DataLocalService } from './services/dataLocal/data-local.service';
import { AcercaDePage } from './pages/Modals/acerca-de/acerca-de.page';
import { AcercaDePageModule } from './pages/Modals/acerca-de/acerca-de.module';
import { Network } from '@ionic-native/network/ngx';
import { Insomnia } from '@ionic-native/insomnia/ngx';
import { Ng2Rut } from 'ng2-rut';
import { BasicAuthInterceptor } from './class/basic-auth-interceptor';
import { FCM } from '@ionic-native/fcm/ngx';
import { ComponentsModule } from 'src/app/components/components.module';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReciboMercanciasDiscrepanciaPage } from './pages/bodegaPages/pages/recibo-mercancias-discrepancia/recibo-mercancias-discrepancia.page';
import { ReciboMercanciasDiscrepanciaPageModule } from './pages/bodegaPages/pages/recibo-mercancias-discrepancia/recibo-mercancias-discrepancia.module';
import { ReciboMercanciasResumenConfirmPageModule } from './pages/bodegaPages/pages/recibo-mercancias-resumen-confirm/recibo-mercancias-resumen-confirm.module';
import { ModalImagenPageModule } from './pages/bodegaPages/pages/modal-imagen/modal-imagen.module';
import { ModalOvPageModule } from './pages/bodegaPages/pages/modal-ov/modal-ov.module';

/**
 *Declaracion de modulos
 * @export
 * @class AppModule
 */
@NgModule({
  declarations: [
    AppComponent,
  ],
  entryComponents: [
    AcercaDePage
  ],
  imports: [
    IonicStorageModule.forRoot(),
    HttpClientModule,
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    AcercaDePageModule,
    ReciboMercanciasDiscrepanciaPageModule,ReciboMercanciasResumenConfirmPageModule,ModalImagenPageModule,ModalOvPageModule,
    ComponentsModule,
    BrowserAnimationsModule,
    Ng2Rut
  ],
  providers: [
    BackgroundMode,
    CallNumber,
    LaunchNavigator,
    SocialSharing,
    Geolocation,
    Camera,
    FileTransfer,
    NativeGeocoder,
    StatusBar,
    SplashScreen,
    ClienteService,
    VendedorService,
    EventoService,
    DataLocalService,
    SeccionesService,
    Network,
    Insomnia,
    FCM,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: LOCALE_ID, useValue: 'es-CL' },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}

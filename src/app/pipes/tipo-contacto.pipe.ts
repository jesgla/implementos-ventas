import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tipoContacto'
})
export class TipoContactoPipe implements PipeTransform {

  transform(tipoContacto: string, color?: boolean): string {
    
    if(!color){
      if(tipoContacto === 'Llamada') return 'call';
      if(tipoContacto === 'Visita') return 'car';
      if(tipoContacto === 'Whatsapp') return 'logo-whatsapp';
      if(tipoContacto === 'Videoconferencia') return 'videocam';
    }

    if(tipoContacto === 'Llamada') return 'warning';
    if(tipoContacto === 'Visita') return 'tertiary';
    if(tipoContacto === 'Whatsapp') return 'success';
    if(tipoContacto === 'Videoconferencia') return 'danger';

    return '';
  }

}

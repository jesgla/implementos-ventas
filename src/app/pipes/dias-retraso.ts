import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import business from 'moment-business';



@Pipe({
  name: 'diasRetraso'
})
export class diasRetrasoPipe implements PipeTransform {

  transform(fecha: Date, ): string {
    let resp;

    try{
      let hoy = moment(new Date()).set({ hour: 20, minute: 0, second: 0 });;
      let fechaCompromiso = moment(fecha).set({ hour: 0, minute: 0, second: 0 });    
      resp = business.weekDays(fechaCompromiso,hoy);
    }catch(e){
      console.log('No se ha podido calcular los dias de retraso de la fecha ingresada');
    }
    return resp.toString();
  }

}

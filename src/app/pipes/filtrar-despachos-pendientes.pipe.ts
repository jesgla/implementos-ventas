import { Pipe, PipeTransform } from '@angular/core';

// interfaces
import { despachoPendiente } from 'src/app/interfaces/bodega/interfaces';

@Pipe({
  name: 'filtrarDespachosPendientes'
})
export class FiltrarDespachosPendientesPipe implements PipeTransform {

  transform(despachosPendientes: despachoPendiente[], filtro: string): any {
    if(!despachosPendientes  || despachosPendientes.length < 1) return [];
    if(filtro === 'TODOS') return despachosPendientes;

    let despachosPendientesFiltrados: despachoPendiente[] = [];

    for(let despachoPendiente of despachosPendientes){
      if(despachoPendiente.tipo === filtro) despachosPendientesFiltrados.push(despachoPendiente);
    }

    return despachosPendientesFiltrados;
  }

}

//angular
import { Pipe, PipeTransform } from '@angular/core';
/**
 *Permite filtrar por caracter ingresado
 * @export
 * @class FiltroPipe
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'filtro'
})

export class FiltroPipe implements PipeTransform {

  /**
   *Funcion que busca coincidencia en un arreglo
   * @param {any[]} arreglo
   * @param {string} texto
   * @param {string} columna
   * @returns {any[]} arreglo con coincidencias
   * @memberof FiltroPipe
   */
  transform(arreglo: any[],
    texto: string,
    columna: string,
    columna2): any[] {
 
    if (texto.trim().length === 0) {
      return arreglo;
    }

    texto = texto.toLowerCase();


    return arreglo.filter(item => {
      return item[columna].toLowerCase()
        .includes(texto) 
        ||
        item[columna2].includes(texto)
    });

  }
}

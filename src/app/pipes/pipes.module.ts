import { diasRetrasoPipe } from './dias-retraso';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatoMonedaPipe } from './formato-moneda.pipe';
import { FiltroPipe } from './filtro.pipe';
import { FormatoKmPipe } from './formato-km.pipe';
import { IVAPipe } from './iva.pipe';
import { MontoAbreviadoPipe } from './monto-abreviado.pipe';
import { ValFechasPipe } from './val-fechas.pipe';
import { DiasFaltantesPipe } from './dias-faltantes.pipe';
import { ImageSanitizerPipe } from './image-sanitizer.pipe';
import { TipoContactoPipe } from './tipo-contacto.pipe';
import { FiltrarDespachosPendientesPipe } from './filtrar-despachos-pendientes.pipe';

@NgModule({
  declarations: [
    FormatoMonedaPipe,
    FiltroPipe,
    FormatoKmPipe,
    IVAPipe,
    MontoAbreviadoPipe,
    ValFechasPipe,
    DiasFaltantesPipe,
    ImageSanitizerPipe,
    TipoContactoPipe,
    FiltrarDespachosPendientesPipe,
    diasRetrasoPipe,
  ],
  imports: [
    CommonModule,
   

  ],
  exports:[
    FormatoMonedaPipe,
    FiltroPipe,
    FormatoKmPipe,
    IVAPipe,
    MontoAbreviadoPipe,
    ValFechasPipe,
    DiasFaltantesPipe,
    ImageSanitizerPipe,
    TipoContactoPipe,
    FiltrarDespachosPendientesPipe,
    diasRetrasoPipe,
  ]
})
export class PipesModule { }

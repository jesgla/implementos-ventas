import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'valFechas'
})
export class ValFechasPipe implements PipeTransform {

  transform(fecha: string ): string {
    // Si la fecha es menor a 1950, entonces devolvemos un string en blanco.
    let ano = new Date(fecha);

    if(ano.getFullYear() > 1950){
      return fecha;
    }else{
      return '';
    }
  }

}

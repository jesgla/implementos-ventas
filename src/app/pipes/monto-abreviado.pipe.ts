import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'montoAbreviado'
})
export class MontoAbreviadoPipe implements PipeTransform {

  transform(value: any, tipo: boolean = true): string {
    let tipoMoneda = tipo ? 'MM' : ''; 

    let monto = value / 1000000;
    return String((monto).toFixed(1))+tipoMoneda; 
  }

}
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'iVA'
})
export class IVAPipe implements PipeTransform {

  transform(value: number, tipo: string, cantidad?: number): number {
    let precio: number; 

    if(value > 0 && tipo === 'con'){
      cantidad?  precio = Math.round( (value * cantidad) * 1.19) :  precio = Math.round( value * 1.19);
    }

    if(value > 0 && tipo === 'sin'){
      precio = Math.round( value / 1.19);
    }

    return precio;
  }

}

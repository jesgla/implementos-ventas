import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'diasFaltantes'
})
export class DiasFaltantesPipe implements PipeTransform {

  transform(fecha: Date, ): string {
    let diasFaltantes: string = '0';

    if(fecha){
      let fecha_limite = moment(fecha);
      let hoy = moment(new Date());
  
      diasFaltantes = String(fecha_limite.diff(hoy,'days'));
    }
    return diasFaltantes;
  }

}

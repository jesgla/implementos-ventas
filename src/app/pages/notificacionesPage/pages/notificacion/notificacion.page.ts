import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-notificacion',
  templateUrl: './notificacion.page.html',
  styleUrls: ['./notificacion.page.scss'],
})
export class NotificacionPage implements OnInit {
  price: any = '';
  constructor(private route: ActivatedRoute) {
    this.price = this.route.snapshot.params['price'];
  }

  ngOnInit() {
  }

}

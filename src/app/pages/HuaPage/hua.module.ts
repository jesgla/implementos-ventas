import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PipesModule } from 'src/app/pipes/pipes.module'

import { IonicModule } from '@ionic/angular';

import { HuaPage } from './hua.page';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { DetalleHuaPageModule } from 'src/app/pages/Modals/detalle-hua/detalle-hua.module'; 
import { DetalleHuaPage } from 'src/app/pages/Modals/detalle-hua/detalle-hua.page';
import { DetalleArticuloPageModule } from 'src/app/pages/Modals/detalle-articulo/detalle-articulo.module';
import { DetalleArticuloPage } from 'src/app/pages/Modals/detalle-articulo/detalle-articulo.page';

const routes: Routes = [
  {
    path: '',
    component: HuaPage
  }
];

@NgModule({
  imports: [
    PipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleHuaPageModule,
    DetalleArticuloPageModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [DetalleHuaPage, DetalleArticuloPage],
  providers: [BarcodeScanner],
  declarations: [
    HuaPage],
  exports: [HuaPage]
})
export class HuaPageModule {}

// Angular
import { Component, OnInit, OnDestroy} from '@angular/core';
import { Subscription } from 'rxjs';

//ionic
import { ModalController, Platform, NavController  } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

// Interfaces
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';

// Servicios
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

// Modal
import  {DetalleHuaPage } from 'src/app/pages/Modals/detalle-hua/detalle-hua.page';
import { DetalleArticuloPage } from 'src/app/pages/Modals/detalle-articulo/detalle-articulo.page';

@Component({
  selector: 'app-hua',
  templateUrl: './hua.page.html',
  styleUrls: ['./hua.page.scss'],
})
export class HuaPage implements OnInit,  OnDestroy {

  articuloEscaneado: any;
  tituloHeader: string = 'Detalles HUA';
  usuarioObservable$: Subscription;
  showDetalleVentas: boolean = false;
  busquedaHuaSKU: string = '';

  tipoBusqueda: string = 'Tienda';
  sucursales: any[] = [];
  scanningCancelled: boolean;
  codSucursal: string =  '';

  constructor(private _authService: AutentificacionService, 
              private _dataLocal: DataLocalService,
              private barcodeScanner: BarcodeScanner,
              private _articuloService: ArticulosService,
              public _estadoTiendaService: EstadoTiendaService,
              private _loading: LoadingService,
              public platform: Platform,
              private navCtr: NavController,
              private modalController: ModalController,
              private _UsuarioService: UsuarioService) 
     { 

      this.platform.backButton.subscribeWithPriority(10, () =>{
        if(this._estadoTiendaService.GetScanningStatus()){
          this._estadoTiendaService.setScanningStatus(false);
          return;
        }else{
          this.navCtr.pop();
        }
      })

     } 

  async ngOnInit() {
    await this._estadoTiendaService.getBodegas();
    await this.setSelectSucursales();
  }

  ngOnDestroy(){
   
  }

  /*
 *permite cerrar la aplicacion
  * @memberof ObjetivosTabPage
  */
  cerrarSesion() {
    this._authService.logout();
  }

  // Metodo utilizado para activar la deceteccion de codigo desde el plugin de cordova.
  async scanProduct(){

    this.barcodeScanner.scan().then( barcodeData => {
      if(barcodeData.cancelled){
          this._estadoTiendaService.setScanningStatus(true);
      }else{
        this._estadoTiendaService.setScanningStatus(false);
        if(barcodeData && barcodeData.text.length > 0){
          this.busquedaHuaSKU = barcodeData.text;
          this.ShowProductDetail(barcodeData.text);  
        }else{
          this._dataLocal.presentToast("No ha sido posible escanear el codigo del producto.", "danger");
        }
      }
     }).catch(err => {
        this._dataLocal.presentToast("Se ha producido un error al intentar escanear el codigo del producto.", "danger");
        console.log('Error', err);
     }); 
  }
  
  /**
   * Permite visualizar el detalle del articulo seleccionado
   * @param {*} articulo
   * @memberof CarroCompraPage
   */
  async ShowProductDetail(sku: string) {
    if(sku.length > 0){
      sku = sku.toUpperCase();

      await this._loading.presentLoading('Cargando Datos');

      if(this.codSucursal && this.codSucursal.length > 0){
        if(this.tipoBusqueda === 'Tienda'){
          this.getDetallesHua(sku);
        }else{
          this.showFichaProductoModal(sku);
        }
      }else{
        this._loading.hideLoading();
        this._dataLocal.presentToast('Debe seleccionar una sucursal.', 'warning');
      }
    }else{
      this._dataLocal.presentToast("Debe Ingresar el sku de un producto", "warning");
    }
  }

  // Funcion que permite obtener los datos de la hua y validar si el sku ingresado existe, sino el modal no será presentado.
  async getDetallesHua(sku: string){

    let detalleHua =  await this._estadoTiendaService.getDetalleHua(sku, this.codSucursal);

    if(detalleHua && detalleHua.sku){
      detalleHua.img = `https://images.implementos.cl/img/250/${detalleHua.sku}-1.jpg`;
      this._loading.hideLoading();
      this.showHuaModal(detalleHua)
    }else{
      this._loading.hideLoading();
      this._dataLocal.presentToast("No se ha encontrado el articulo ingresado.",'warning');
    }
  }

  // Metodo utilizado para mostrar modal asociado a los datos del estado de producto.
  async showHuaModal(detalleHua){
    const modal = await this.modalController.create({
      component: DetalleHuaPage,
      componentProps: {
        detalleHua: detalleHua,
        sucursal: this.codSucursal,
        scanProduct: true,
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();

    if (data.reset) {
      this.busquedaHuaSKU = '';
    } 
  }

  // Metodo utilizado par mostrar modal asociado a la ficha del producto.
  async showFichaProductoModal(sku: string){

    // obtenemos la ficha del producto desde el sku ingresado.
    let articulo = await this._articuloService.buscarSku({'sku': [sku], 'sucursal': this.codSucursal});
    this._loading.hideLoading();

    // Si datos existen, entonces presentamos el modal de detalle de articulos.
    if(articulo[0] && articulo[0].sku){
      const modal = await this.modalController.create({
        component: DetalleArticuloPage,
        componentProps: {
          articulo: articulo[0],
          boton: false,
          cantidad: 1,
          scanProducto: true, 
          }
        },
      );

      await modal.present();
      const { data } = await modal.onWillDismiss();

      /* data.recargar ? await this.obtenerCarro() : null; */
    
    }else{
      this._dataLocal.presentToast("No se ha encontrado el articulo ingresado.",'warning');
    }
  }

  async setSelectSucursales(){
    let vendedor: Vendedor = this._UsuarioService.getUserLoged();
    await this._estadoTiendaService.setSucursales(vendedor.codBodega, vendedor.codSucursal);

    if(!this._UsuarioService.isZonalAccount()){
      this.codSucursal = this._estadoTiendaService.selectSucurales[0].codigo;
    }else{
      let savedCodSucursal = await this._dataLocal.getItem('selectedSucursalHua');
      this.codSucursal = savedCodSucursal? savedCodSucursal : '';
    }
  }

  async saveSelectedSucursal(sucursal: string){
    await this._dataLocal.setItem('selectedSucursalHua', sucursal);
  }
}

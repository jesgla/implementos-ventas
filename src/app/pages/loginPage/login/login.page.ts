//angular
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Platform } from '@ionic/angular';

//services
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';
/**
 *Login aplicacion
 * @export
 * @class LoginPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})


export class LoginPage {

  /**
   * Formulario de login
   * @type {FormGroup}
   * @memberof LoginPage
   */
  loginForm: FormGroup;

  /**
   *Creates an instance of LoginPage.
   * @param {AutentificacionService} authService
   * @param {FormBuilder} formBuilder
   * @param {Router} router
   * @memberof LoginPage
   */
  constructor(public authService: AutentificacionService, private formBuilder: FormBuilder, private router: Router, public platform: Platform) {
    this.loginForm = this.formBuilder.group({
      usuario: [''],
      contrasenia: [''],
    });
  }

  ionViewWillEnter(){
    // Si usuario esta logeado, se le redirecciona a la pagina "home". Esto, con el objetivo de evitar que usuario sea enviado al login al presionar boton back del dispositivo.
    if(this.authService.isAuthenticated()){
      this.router.navigate(['/home']);
    }
  }

  /**
   * Permite obtener login para iniciar sesion en la aplicacion
   * @memberof LoginPage
   */
  logForm() {
    let { usuario, contrasenia } = this.loginForm.value
  
    let usuarioLogin = {
      strUsuario: usuario,
      strPassword: contrasenia
    }
    this.authService.login(usuarioLogin);

  }
}

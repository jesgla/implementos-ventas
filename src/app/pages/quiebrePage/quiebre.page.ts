import { Component, OnInit } from '@angular/core';

// Servicios
import { QuiebreStockService } from 'src/app/services/quiebreStock/quiebreStock.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';
import { FiltrosQuiebresService } from 'src/app/services/filtrosQuiebres/filtros-quiebres.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

// interfaces
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { ArticuloQuiebre, SucursalQuiebre } from 'src/app/interfaces/articuloQuiebre/articuloQuiebre';

@Component({
  selector: 'app-quiebre',
  templateUrl: './quiebre.page.html',
  styleUrls: ['./quiebre.page.scss'],
})
export class QuiebrePage implements OnInit {

  vendedor: Vendedor;
  ArticulosQuiebre: ArticuloQuiebre[] = [];
  SucursalesQuiebre: SucursalQuiebre[] = [];
  tipoCategoria: string = '';
  SucursalQuiebre: SucursalQuiebre;
  showSucursales: boolean = true;
  codSucursal: string = '';

  constructor(private _quiebreStock: QuiebreStockService, 
              private _dataLocal: DataLocalService, 
              public _estadoTiendaService: EstadoTiendaService,
              private _LoadingService: LoadingService,
              public _filtrosQservice: FiltrosQuiebresService) { }

  async ngOnInit() {
    await this._LoadingService.presentLoading('Cargando Datos..');
    await this.obtenerUsuario();    
    this.SucursalesQuiebre = await this._quiebreStock.getQuiebreSucursales();

    // Si solo existe una sucursal en el select de sucursales, entonces mostrarmos directamente la informacion de la sucursal.
    if(this._estadoTiendaService.selectSucurales && this._estadoTiendaService.selectSucurales.length === 1){
      if(this.vendedor && this.vendedor.codBodega){
        this.MostrarQuiebre(this.vendedor.codBodega);
      }
    }else{
      this.codSucursal = '';
    }
    this._LoadingService.hideLoading();
  }

  async obtenerUsuario() {
    let usuario = await this._dataLocal.getItem('auth-token');
    this.vendedor = JSON.parse(usuario);
  }

  // Metodo utilizado para setear la sucursal seleccionada por el select y luego mostrar los datos.
  MostrarQuiebre(codSucursal: string){
    this.SucursalQuiebre = null;
    
    let sucursal = this.SucursalesQuiebre.filter(sucursal => sucursal._id === codSucursal)[0];
    sucursal._id && sucursal._id? this.SucursalQuiebre = sucursal : '';
    this.showSucursales = false;
  }

  ionViewWillEnter(){
    if(this._estadoTiendaService.selectSucurales && this._estadoTiendaService.selectSucurales.length > 1){
      this.SucursalQuiebre = null;
      this.showSucursales = true;
      this._filtrosQservice.showFiltros = false;
    }
  }

  HideSucursalSelect(estado?: boolean){
    this.showSucursales = !estado;
    this.SucursalQuiebre = null;
  }

  mostrarFiltros(event){
    this._filtrosQservice.showMenuFiltrosQuiebre(event);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QuiebrePage } from './quiebre.page';

// modulos 
import { ComponentsModule } from 'src/app/components/components.module'

// componentes
import { ItemQuiebreViewComponent } from './components/item-quiebre-view/item-quiebre-view.component';
import { SucursalQuiebreViewComponent } from './components/sucursal-quiebre-view/sucursal-quiebre-view.component';
import { popOverFiltrosQuiebreComponent } from 'src/app/components/popover-filtros-quiebre/popover-filtros-quiebre.component';

import { DetalleHuaPageModule } from 'src/app/pages/Modals/detalle-hua/detalle-hua.module'; 
import { DetalleHuaPage } from 'src/app/pages/Modals/detalle-hua/detalle-hua.page';

// pipes
import { PipesModule } from 'src/app/pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: QuiebrePage
  }
];

@NgModule({
  imports: [
    DetalleHuaPageModule,
    ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [DetalleHuaPage, popOverFiltrosQuiebreComponent],
  declarations: [QuiebrePage, ItemQuiebreViewComponent, SucursalQuiebreViewComponent]
})
export class QuiebrePageModule {}

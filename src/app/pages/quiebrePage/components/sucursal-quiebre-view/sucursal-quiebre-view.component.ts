import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';

// interface
import { SucursalQuiebre } from 'src/app/interfaces/articuloQuiebre/articuloQuiebre';

// servicios
import { FiltrosQuiebresService } from 'src/app/services/filtrosQuiebres/filtros-quiebres.service';
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';

// COmponentes
import { ItemQuiebreViewComponent } from '../item-quiebre-view/item-quiebre-view.component';


@Component({
  selector: 'app-sucursal-quiebre-view',
  templateUrl: './sucursal-quiebre-view.component.html',
  styleUrls: ['../../quiebre.page.scss'],
})
export class SucursalQuiebreViewComponent implements OnInit, OnChanges {
  @Input() SucursalQuiebre: SucursalQuiebre;
  @Output() OcultarSucursales = new EventEmitter();
  showDetaProductos: boolean = false;

  constructor(public _FiltrosQuiebresService: FiltrosQuiebresService, public _EstadoTiendaService: EstadoTiendaService) { }

  ngOnInit() {

  }

  // Funcion que permite calcular el porcentaje entre dos valores
  getPorcentajeQuiebre(quiebre: number, total: number, disp = true){
    let result: any = (quiebre * 100) / total;

    disp? result = (100 - result).toFixed(2)  : result = result.toFixed(2) ;
    return result;
  }
  
  HideSucursalSelect(estado: boolean){
    this.OcultarSucursales.emit(estado);
  }
  
  ngOnChanges(changes: SimpleChanges) {
    this.showDetaProductos = false;
    if(this.SucursalQuiebre){
      
    }
  }


}

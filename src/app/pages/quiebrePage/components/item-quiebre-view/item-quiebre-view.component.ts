import { Component, OnInit, Input, OnDestroy, EventEmitter} from '@angular/core';
import { ArticuloQuiebre } from 'src/app/interfaces/articuloQuiebre/articuloQuiebre';
import { Subscription } from 'rxjs'

// Servicios
import { QuiebreStockService } from 'src/app/services/quiebreStock/quiebreStock.service';
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { FiltrosQuiebresService } from 'src/app/services/filtrosQuiebres/filtros-quiebres.service';

// interfaces
import { parametrosUrl } from 'src/app/interfaces/articuloQuiebre/articuloQuiebre';

// ionic
import { ModalController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';

// Modal
import  {DetalleHuaPage } from 'src/app/pages/Modals/detalle-hua/detalle-hua.page';

// componentes
import { popOverFiltrosQuiebreComponent } from 'src/app/components/popover-filtros-quiebre/popover-filtros-quiebre.component';

@Component({
  selector: 'app-item-quiebre-view',
  templateUrl: './item-quiebre-view.component.html',
  styleUrls: ['./item-quiebre-view.component.scss'],
})
export class ItemQuiebreViewComponent implements OnInit, OnDestroy {
  @Input() sucursal: string;
  itemQuiebres: ArticuloQuiebre[] = [];
  paginascroll: number = 2;
  paginas: number = 1;
  paginasTotal: number = 1;
  filtrosQuiebre: any;
  subFiltrosQuiebre: any;
  tipologiasDisponibles: string[] = [];
  filtroAplicado: any;
  textoFiltros: string =  '';
  parametros: parametrosUrl = { page: 1, 
                                sucursal: '',
                                categoria: '',
                                quebrados: 1,
                                filtro: 1,
                              }


  showPopOverSubscription$: Subscription;

  constructor(private _quiebreStockService: QuiebreStockService,
              private modalController: ModalController,
              private _estadoTiendaService: EstadoTiendaService,
              private _loading: LoadingService,
              private _dataLocal: DataLocalService,
              private _FiltrosQuiebresService: FiltrosQuiebresService,
              public PopoverController: PopoverController
              )
  { 
    // Subscripcion que permite presentar el popover asociado a los filtros, cuando se presiona el boton que se encuentra en otro quiebre.page
    this.showPopOverSubscription$ = this._FiltrosQuiebresService.menuFiltrosQuiebreObservable$.subscribe( ( event : any ) => {
      this.MostrarPopOverFiltros(event);
    })
  }

  async ngOnInit() {
    await this.getProductosQuiebre(1);
  }

  async getProductosQuiebre(pagina){

    pagina === 1? await this._loading.presentLoading("Cargando Productos..") : '';

    // Seteamos la pagina en 1 y la sucursal.
    this.parametros.sucursal = this.sucursal;
    this.parametros.page = pagina;

    this.itemQuiebres.push(... await this._quiebreStockService.getProductosQuiebre(this.parametros));
    this.paginasTotal = this._quiebreStockService.totalPaginas;
    
    this._FiltrosQuiebresService.showFiltros = true;
    this.setTextoFiltros();
    pagina === 1? this._loading.hideLoading() : '';
  }

  ngOnDestroy(){
    this.paginascroll = 2;
    this.paginasTotal = 1;
    this.itemQuiebres = [];
    this.showPopOverSubscription$? this.showPopOverSubscription$.unsubscribe() : '';
  }

  // Funcion emitida cuando se ejecuta el infinity scroll de la lista de productos con quiebre.
  async loadData(event){
    if(this.paginascroll <= this.paginasTotal){
      await this.getProductosQuiebre(this.paginascroll);
      this.paginas++;
      this.paginascroll++;
      event.target.complete();
    }else{
      event.target.complete();
    }
  }

  // Funcion que permite obtener los datos de la hua y validar si el sku ingresado existe, sino el modal no será presentado.
  async getDetallesHua(sku: string){
    
    await this._loading.presentLoading('Cargando Datos');

    let detalleHua =  await this._estadoTiendaService.getDetalleHua(sku, this.sucursal);

    if(detalleHua && detalleHua.sku){
      detalleHua.img = `https://images.implementos.cl/img/250/${detalleHua.sku}-1.jpg`;
      this._loading.hideLoading();
      this.showHuaModal(detalleHua)
    }else{
      this._loading.hideLoading();
      this._dataLocal.presentToast("No se ha encontrado el articulo ingresado.",'warning');
    }
  }

  // Metodo utilizado para mostrar modal asociado a los datos del estado de producto.
  async showHuaModal(detalleHua){
    const modal = await this.modalController.create({
      component: DetalleHuaPage,
      componentProps: {
        detalleHua: detalleHua,
        sucursal: this.sucursal,
        scanProduct: false,
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();

    if (data.reset) {

    } 
  }

  /**
  * @author ignacio zapata  \"2020-08-27\
  * @desc Se ejecuta cuando se presiona el boton que se encarga de mostrar los filtros para los productos quebrados
  * @params elemento html desde donde se hace el llamado al popOver
  * @return
  */
  async MostrarPopOverFiltros(ev: any) {
      let setFiltrosEvent = new EventEmitter< any >();
      setFiltrosEvent.subscribe(
        (parametros: parametrosUrl )   => { 
          this.parametros = parametros; 
          this.paginascroll = 2;
          this.itemQuiebres = [];
          this.getProductosQuiebre(1);
        }  
      );

    const popover = await this.PopoverController.create({
      component: popOverFiltrosQuiebreComponent,
      translucent: true,
      animated: true,
      componentProps: {
        parametros: this.parametros,
        setFiltrosEvent: setFiltrosEvent
      },
      event: ev,
      mode: "ios"
    });
    return await popover.present();
  }

  /**
  * @author ignacio zapata  \"2020-09-21\
  * @desc Metodo utilizado para setear el string que se mostrará para detallar cuales son los filtros con los cuales se estan mostrando los items.
  * @params 
  * @return
  */
  setTextoFiltros(){
    this.textoFiltros = 'productos';
    if(this.parametros.filtro == 1) this.textoFiltros = this.textoFiltros.concat(' del Assortment ');
    if(this.parametros.filtro == 2) this.textoFiltros = this.textoFiltros.concat(' de tipología tienda 1 y 2 ');
    if(this.parametros.filtro == 3) this.textoFiltros = this.textoFiltros.concat(' de tipología Compañía 1 y 2 en ');
    this.textoFiltros = this.parametros.quebrados ?  this.textoFiltros.concat(' en estado de quiebre') : this.textoFiltros.concat(' aun sin quiebre');
    this.textoFiltros = this.parametros.categoria ? this.textoFiltros.concat(' para la categoría '+this.parametros.categoria) : this.textoFiltros;
    this.textoFiltros = this.textoFiltros+'.';
  }

  getSemanaVenta(item){
   let result = item.cantidad / ((item.avg+item.sd ) *5.5);
   if (isFinite(result) && !isNaN(result)) {
    return result.toFixed(2);
   }else{
     return 0
   }
  }

  getPromedioSemanaVenta(item){
    let result = ((item.avg + item.sd) *5.5);
    if (isFinite(result) && !isNaN(result)) {
      return result.toFixed(1);
     }else{
       return 0
     }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CarroCompraPage } from './carro-compra.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { CotizacionPage } from 'src/app/pages/Modals/cotizacion/cotizacion.page';
import { CotizacionPageModule } from 'src/app/pages/Modals/cotizacion/cotizacion.module';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { DetalleArticuloPage } from 'src/app/pages/Modals/detalle-articulo/detalle-articulo.page';
import { DetalleArticuloPageModule } from 'src/app/pages/Modals/detalle-articulo/detalle-articulo.module';

const routes: Routes = [
  {
    path: '',
    component: CarroCompraPage
  }
];

@NgModule({
  entryComponents:[CotizacionPage,DetalleArticuloPage],
  imports: [
    PipesModule,
    ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CotizacionPageModule,
    DetalleArticuloPageModule
  ],
  declarations: [CarroCompraPage]
})
export class CarroCompraPageModule {}

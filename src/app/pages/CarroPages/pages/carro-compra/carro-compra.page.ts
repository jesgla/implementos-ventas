//angular
import { Component, OnInit, ɵConsole } from '@angular/core';

//ionic
import { ModalController, Events } from '@ionic/angular';

//components
import { CotizacionPage } from 'src/app/pages/Modals/cotizacion/cotizacion.page';

//services 
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';
import { CarroService } from 'src/app/services/carro/carro.service';

//interfaces
import { Articulo } from 'src/app/interfaces/articulo/articulo';
import { Cliente } from '../../../../interfaces/cliente/cliente';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor'

@Component({
  selector: 'app-carro-compra',
  templateUrl: './carro-compra.page.html',
  styleUrls: ['./carro-compra.page.scss'],
})

/**
 * Componente que permite visualizar los objetos para registrar cotizacion
 * @export
 * @class CarroCompraPage
 * @implements {OnInit}
 */
export class CarroCompraPage implements OnInit {
  /**
   * listado con articulos
   * @type {Articulo[]}
   * @memberof CarroCompraPage
   */
  carro: Articulo[];

  /**
   *Monto de los articulos cotizados
   * @type {number}
   * @memberof CarroCompraPage
   */
  total: number;

    /**
   * Objeto de vendedor 
   * @type {Cliente}
   * @memberof CarroCompraPage
   */
    cliente: Cliente;

  /**
   *Monto sin iva de los articulos cotizados
   * @type {number}
   * @memberof CarroCompraPage
   */
  subTotal: number;

  /**
   *Monto del IVA
   * @type {number}
   * @memberof CarroCompraPage
   */
  iva: number;

  /**
   *Creates an instance of CarroCompraPage.
   * @param {ModalController} modalController
   * @param {DataLocalService} dataLocal
   * @param { CarroService } carroService
   * @memberof CarroCompraPage
   */
  constructor(public events : Events ,
    public modalController: ModalController, private dataLocal: DataLocalService, private _ArticulosService: ArticulosService, private carroService: CarroService) {
   }

  /**
   *Metodo que permite ejecutar funciones al inicio del componente
   *
   * @memberof CarroCompraPage
   */
  async ngOnInit() {
    window.scrollTo({ top: 0});
    await this.obtenerCliente();
    await this.obtenerCarro();
  }

  /**
   *Metodo que se ejecuta cada vez que se llama al componente
   * @memberof CarroCompraPage
   */
  async ionViewWillEnter() {
    await this.obtenerCliente();
    await this.obtenerCarro();
  }

  async obtenerCarro() {
    let consulta: any = await this.carroService.obtenerCarro()
    
    this.carro = consulta.carro;

    this.total = consulta.total;
    this.subTotal = consulta.subTotal;
    this.iva = consulta.iva;

    return this.carro;
  }

  /**
   *Permite abrir modal para registrar cotizacion
   *
   * @returns
   * @memberof CarroCompraPage
   */
  async abrirModal() {
    const modal = await this.modalController.create({
      component: CotizacionPage,
      componentProps: {
        total: this.total,
        subTotal: this.subTotal,
        iva: this.iva,
        carro: this.carro
      }
    });
    return await modal.present();
  }


  /**
   * Permite eliminar elementos del carro de compra
   * @param {*} articulo
   * @memberof CarroCompraPage
   */
  async eliminarArticulo(articulo) {
    this.events.publish('actualizar',true);
    await this.dataLocal.eliminarArticulo(articulo.sku);
    await this.obtenerCarro();
    
  }

  async obtenerCliente(){
    this.cliente = await this.dataLocal.getCliente()
  }
}

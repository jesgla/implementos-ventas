import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarroCompraPage } from './carro-compra.page';

describe('CarroCompraPage', () => {
  let component: CarroCompraPage;
  let fixture: ComponentFixture<CarroCompraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarroCompraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarroCompraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

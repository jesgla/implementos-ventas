import { LoadingService } from './../../../../services/loading/loading.service';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';

//ionic
import { Platform } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';


// interfaces
import { DocumentoRespaldo } from 'src/app/interfaces/respaldoDocs/respaldo';

//services
import { RespaldoDocsService } from 'src/app/services/respaldoDocs/respaldo-docs.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';

@Component({
  selector: 'app-search-document',
  templateUrl: './search-document.component.html',
  styleUrls: ['./search-document.component.scss'],
})
export class SearchDocumentComponent implements OnInit, OnDestroy {
  @Input() tipo: string;
  @Output() documentoEventEmitter =  new EventEmitter();
  
  tipoDocumento: string;
  documento: DocumentoRespaldo;

  constructor(
    private _RespaldoDocsService: RespaldoDocsService,
    private _dataLocal: DataLocalService,
    public platform: Platform,
    private barcodeScanner: BarcodeScanner,
    public _estadoTiendaService: EstadoTiendaService,
    private _LoadingService: LoadingService) { }

  ngOnInit() {
    this.tipoDocumento = this.tipo != 'OV'? 'GDEL' : 'OV';
  }

  ngOnDestroy(){
    this.tipoDocumento = null;
    this.documento = null;
  }

  async scanProduct(){

    this.barcodeScanner.scan().then( barcodeData => {
      if(barcodeData.cancelled){
        this._estadoTiendaService.setScanningStatus(true);
      }else{
        this._estadoTiendaService.setScanningStatus(true);
        if(barcodeData && barcodeData.text.length > 0){
          let folio = this.getDocumentFolio( barcodeData.text );
          if(folio) this.getDocumentDetails(folio);
        }else{
          this.errorScanningDocument();
        }
      }
     }).catch(err => {
        this.errorScanningDocument();
     }); 
  }
  
  async getDocumentDetails(folio: any){
    if(!folio){
      this._dataLocal.presentToast('Debe ingresar el número de folio', 'warning'); 
      return;
    } 

    await this._LoadingService.presentLoading(`Buscando ${this.tipoDocumento}`);
    this.documento = await this._RespaldoDocsService.getDocumentoDisponibleDetails(this.tipoDocumento, folio);

    if(!this.documento || !this.documento.strSalesID){
      this.errorDocumentFormat();
      this._LoadingService.hideLoading();
      return;
    }
    
    this.returnDocument();
    this._LoadingService.hideLoading();
  }

  getDocumentFolio(DocumentId){
    let folio = DocumentId.split('-')[1]? Number(DocumentId.split('-')[1]) : null;
    if(!folio) this.errorScanningDocument();

    return folio;
  }

  errorScanningDocument(){
    this._dataLocal.presentToast("Se ha producido un error al intentar escanear el codigo del documento.", "danger");
  }

  errorDocumentFormat(){
    this._dataLocal.presentToast("No se encontró el documento. Verifíque los datos ingresados.", "danger");
  }

  returnDocument(){
    if(!this.documento){
      this.errorDocumentFormat();
      return;
    }

    this.documentoEventEmitter.emit(this.documento);
    this.ngOnDestroy();
  }
}

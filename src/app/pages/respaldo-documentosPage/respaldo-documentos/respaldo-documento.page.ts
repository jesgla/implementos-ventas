import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

// interfaces
import { Respaldo, DocumentoRespaldo } from 'src/app/interfaces/respaldoDocs/respaldo';

// services
import { UsuarioService } from '../../../services/usuario/usuario.service';
import { RespaldoDocsService } from 'src/app/services/respaldoDocs/respaldo-docs.service';
import { LoadingService } from '../../../services/loading/loading.service';
import { DataLocalService } from '../../../services/dataLocal/data-local.service';

@Component({
  selector: 'app-respaldo-documento',
  templateUrl: './respaldo-documento.page.html',
  styleUrls: ['./respaldo-documento.page.scss'],
})
export class RespaldoDocumentoPage implements OnInit, OnDestroy{
  tipo: string;
  respaldo: Respaldo;
  paso: number = 1;
  documento: DocumentoRespaldo;
  documentoRespaldadoMsj: string;
  documentoRespaldado: boolean;

  constructor(
    private _UsuarioService: UsuarioService,
    private _RespaldoDocsService: RespaldoDocsService,
    private _DataLocalService: DataLocalService,
    private _loadingService: LoadingService,
    private router: Router) { }

  ngOnInit() {
    this.tipo = this.router.url === '/respaldo-compras'? 'OV' : null;
    this.initDocumento();
  }

  initDocumento(){
    this.respaldo = {
      intRegistro: null,
      intConductor: this._UsuarioService.getUserLoged()? this._UsuarioService.getUserLoged().codEmpleado : 0,
      intEstadoDocumento: 10,
      intPagoCaja: 1,
      strTipoDocumento: null,
      strImagen64: [],
    }
  }

  ngOnDestroy(){
    this.respaldo = null;
  }
  
  setImage(image){
    if(image){
      this.respaldo.strImagen64 = image.imageData;
      this.paso = 2;
    }
  }

  setDocumento(documento: DocumentoRespaldo){
    this.documento = documento;
    this.respaldo.intRegistro = this.documento.intRegistro;
    this.respaldo.strTipoDocumento = this.documento.strTipoDoc;
    this.paso = 3;
  }

  async guardarRespaldo(){
    if(!this.respaldo.strImagen64){
      this._DataLocalService.presentToast('La foto adjunta no es valida', 'danger'); 
      return;
    }

    if(!this.respaldo.intRegistro){
      this._DataLocalService.presentToast('El folio del documento no es valido', 'danger');
      return;
    }
    
    await this._loadingService.presentLoading('Guardando Respaldo.');
    let consulta = await this._RespaldoDocsService.saveRespaldo(this.respaldo);
    this.documentoRespaldadoMsj = consulta? 'Documento respaldado Exitosamente' : 'Error al respaldar documento';
    this.documentoRespaldado = consulta;
    this._loadingService.hideLoading();
  }

  cancelar(){
    this.paso = 1;
    this.documento = null;
    this.documentoRespaldadoMsj = null;
    this.documentoRespaldado = null;
    this.initDocumento();
  }
}

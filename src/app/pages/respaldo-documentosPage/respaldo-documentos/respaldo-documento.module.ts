import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ComponentsModule } from '../../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RespaldoDocumentoPage } from './respaldo-documento.page';

// components
import { SearchDocumentComponent } from '../components/search-document/search-document.component';
import { PipesModule } from 'src/app/pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: RespaldoDocumentoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PipesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RespaldoDocumentoPage, SearchDocumentComponent],
  providers: [BarcodeScanner],
})
export class RespaldoDocumentoPageModule {}

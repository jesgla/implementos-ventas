import { Component, OnInit, OnDestroy } from '@angular/core';

// servicios
import { RespaldoDocsService } from 'src/app/services/respaldoDocs/respaldo-docs.service';
import { UsuarioService } from './../../../services/usuario/usuario.service';
import { LoadingService } from './../../../services/loading/loading.service';

// interface
import { DocumentoRespaldo } from 'src/app/interfaces/respaldoDocs/respaldo';

@Component({
  selector: 'app-historial-respaldos',
  templateUrl: './historial-respaldos.page.html',
  styleUrls: ['./historial-respaldos.page.scss'],
})
export class HistorialRespaldosPage implements OnInit, OnDestroy {
  respaldos: DocumentoRespaldo[] = [];
  datosCargados: boolean = false;
  viewDetail: boolean = false;
  respaldoSelected: DocumentoRespaldo;

  constructor(private _UsuarioService: UsuarioService, private _RespaldoDocsService: RespaldoDocsService, private _LoadingService: LoadingService) { }

  async ngOnInit() {
    await this._LoadingService.presentLoading('Cargando Respaldos');
    await this.getMisRespaldos();
    this._LoadingService.hideLoading();
  }

  ngOnDestroy(){
    this.ocultarDetalleRespaldo();
    this.datosCargados = false;
  }

  async getMisRespaldos(){
    this.respaldos = await this._RespaldoDocsService.getMisRespaldos(this._UsuarioService.getUserLoged().codEmpleado);
    this.datosCargados = true;
  }

  async getDetalleDoc(intRegistro: number, tipo: string){
    await this._LoadingService.presentLoading('Cargando Respaldo');
    this.respaldoSelected = await this._RespaldoDocsService.getDetalleDocumentoRespaldado(intRegistro, tipo);
    this.viewDetail = true;
    this._LoadingService.hideLoading();
  }

  ocultarDetalleRespaldo(){
    this.viewDetail = false;
    this.respaldoSelected = null;
  }

}

import { Component, OnInit, EventEmitter, ViewChild} from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { IonSlides } from '@ionic/angular';
import * as moment from 'moment';


//ionic
import { ModalController } from '@ionic/angular';

// servicios
import { OportunidadesService } from 'src/app/services/oportunidades/oportunidades.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

// interfaces
import { Oportunidad, ResumenOportunidades} from 'src/app/interfaces/oportunidades/oportunidades';

// componentes
import { PopoverFiltrosOportunidadesComponent } from './components/popover-filtros-oportunidades/popover-filtros-oportunidades.component';


@Component({
  selector: 'app-oportunidades',
  templateUrl: './oportunidades.page.html',
  styleUrls: ['./oportunidades.page.scss'],
})
export class OportunidadesPage implements OnInit {
  @ViewChild(IonSlides, { static: false }) slidesOportunidades: IonSlides;

  oportunidades: Oportunidad[] = [];
  ResumenOportunidades: ResumenOportunidades;
  cargando: boolean = true;
  indexActual: number = 0;

  paginas = [
    { name: 'Resumen', index: 0, cheked: true, filtro: '' },
    { name: 'Detalles', index: 1, cheked: false, filtro: '' },
  ];

  constructor(
      public _oportunidadesService: OportunidadesService,
      private popoverController: PopoverController,
      private _LoadingService: LoadingService,
      public _UsuarioService: UsuarioService) 
  { }


  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.resetFilters();
    await this.getOportunidades();
  }

  ionSlidesDidLoad(){
    this.slidesOportunidades.lockSwipes(true);
  }

  resetFilters(): void{
    this._oportunidadesService.filtros.estado = 'abiertas';
    this._oportunidadesService.filtros.periodo = 'todos';
    this._oportunidadesService.filtros.orderBy = 'fecha_limite';
  }

  /**
  * @author ignacio zapata  \"2020-11-24\
  * @desc metodo utilizado para obtener los datos de las oportunidades enviando como parametros el rut del vendedor y la fecha de inicio "el primero del mes actual"
  * @params 
  * @return
  */
  async getOportunidades(){
    this.oportunidades = [];
    await this._LoadingService.presentLoading('Cargando oportunidades');
    let vendedor = await this._UsuarioService.getUserLoged();

    let filtros = this.setFiltros();
    
    let data = {
      rutVendedor: vendedor.rut.replace('.','').replace('.',''),
      desde: filtros.desde,
      estado: filtros.estado,
      orderBy: filtros.orderBy,
      tipoContacto: filtros.tipoContacto,
    }

    this.oportunidades = await this._oportunidadesService.getOportunidades(data);
    this.ResumenOportunidades = await this._oportunidadesService.getResumenOportunidadVendedor(vendedor.rut.replace('.','').replace('.',''), moment(new Date()).format('YYYY/MM/DD'));
    this.cargando = false;
    this._LoadingService.hideLoading();
  }

  async presentPopoverFiltros(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverFiltrosOportunidadesComponent,
      cssClass: 'filrosOpor',
      event: ev,
      mode: 'ios',
      animated: true,
      translucent: false
    });

     popover.onDidDismiss().then((data) => {
      if(data && data.data === 'filtrar') this.getOportunidades();
    });

    return await popover.present();
  }

  setFiltros(){
    let periodo: number = this._oportunidadesService.filtros.periodo === 'todos'? undefined : this._oportunidadesService.filtros.periodo === 'actual'? 0 : 3;
    let estado: boolean = this._oportunidadesService.filtros.estado === 'abiertas'? true : this._oportunidadesService.filtros.estado === 'cerradas'? false : null;
    let tipoContacto: string = this._oportunidadesService.filtros.tipoContacto  === 'todos'? undefined : this._oportunidadesService.filtros.tipoContacto;

    return {desde: this.setFiltroDesde(periodo) , estado, orderBy: this._oportunidadesService.filtros.orderBy, tipoContacto};
  }

  cambioPagina(index, slide) {
    this.indexActual = index;

    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false : pag.cheked=true
    });
 
    slide.lockSwipes(false);
    slide.slideTo(index);
    slide.lockSwipes(true);

    window.scrollTo({ top: 0});
 
   }

   setFiltroDesde(periodo: number){
    let fecha = new Date();
    fecha.setMonth(fecha.getMonth() - periodo);
    let desde =  periodo  != undefined? fecha.getFullYear()+'/'+( fecha.getMonth()+1)+'/01' : null;
    return desde;
   }
}

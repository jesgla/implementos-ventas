import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { IonicModule } from '@ionic/angular';

import { OportunidadesPage } from './oportunidades.page';

//components
import { ListaOportunidadesComponent } from './components/lista-oportunidades/lista-oportunidades.component';
import { PopoverFiltrosOportunidadesComponent } from './components/popover-filtros-oportunidades/popover-filtros-oportunidades.component';
import { OportunidadItemComponent } from './components/oportunidad-item/oportunidad-item.component';
import { SetOvListComponent } from './components/set-ov-list/set-ov-list.component';
import { ResumenOportunidadesComponent } from './components/resumen-oportunidades/resumen-oportunidades.component';
import { DetalleConceptoComponent } from './components/resumen-oportunidades/components/detalle-concepto/detalle-concepto.component';

const routes: Routes = [
  {
    path: '',
    component: OportunidadesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    OportunidadesPage,
    ListaOportunidadesComponent,
    PopoverFiltrosOportunidadesComponent,
    OportunidadItemComponent,
    SetOvListComponent,
    ResumenOportunidadesComponent,
    DetalleConceptoComponent],
  entryComponents: [PopoverFiltrosOportunidadesComponent, SetOvListComponent ],
})
export class OportunidadesPageModule {}

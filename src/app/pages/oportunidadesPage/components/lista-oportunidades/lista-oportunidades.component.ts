import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

// interfaces
import { Oportunidad, oportunidades_conceptos } from 'src/app/interfaces/oportunidades/oportunidades'

// servicios
import { OportunidadesService } from 'src/app/services/oportunidades/oportunidades.service';

@Component({
  selector: 'app-lista-oportunidades',
  templateUrl: './lista-oportunidades.component.html',
  styleUrls: ['./lista-oportunidades.component.scss'],
})
export class ListaOportunidadesComponent implements OnInit {
  @Input() oportunidades: Oportunidad[]; 
  @Output() reloadOportunidadesEmitter = new EventEmitter()

  conceptosCierre: oportunidades_conceptos[] = [];

  constructor(private _oportunidades: OportunidadesService) { }

  async ngOnInit() {
    this.conceptosCierre = await this._oportunidades.getOportunidadesConceptos('cierre');
  }

  reloadOportunidades(){
    this.reloadOportunidadesEmitter.emit();
  }
}

import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

// modals
import { SetOvListComponent } from '../set-ov-list/set-ov-list.component';

// interface
import { Oportunidad, oportunidades_conceptos } from 'src/app/interfaces/oportunidades/oportunidades';

// servicios
import { OportunidadesService } from 'src/app/services/oportunidades/oportunidades.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

@Component({
  selector: 'app-oportunidad-item',
  templateUrl: './oportunidad-item.component.html',
  styleUrls: ['./oportunidad-item.component.scss'],
})
export class OportunidadItemComponent implements OnInit {
  @Input('oportunidad') oportunidad:  Oportunidad;
  @Input('conceptosCierre') conceptosCierre: oportunidades_conceptos[] = [];
  @Output() finalizarEmitter = new EventEmitter()

  finalizarOportunidad: boolean = false;
  cerrarOportunidadForm : FormGroup;
  optionsSelectConcretada: any = {header: 'Oportunidad Concretada?'};
  optionsSelectConcepCierre: any = {header: 'Razón de Cierre?'};
  ovFolio: string = null;
  listaSku: any[] = [];
  borrandoItem: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private _OportunidadesService: OportunidadesService,
              private _DataLocalService: DataLocalService,
              public modalController : ModalController,
              public alertController: AlertController 
            ){ 

    this.cerrarOportunidadForm = formBuilder.group({
      selectOporConcretada: [null, Validators.required],
      selectConceptCierre: [null],
      listaOv: [null, Validators.required],
      productos: [null, Validators.required],
    });
  }

  async ngOnInit() {

  }

  /**
  * @author ignacio zapata  \"2020-11-26\
  * @desc Metodo utilizado para hacer el llamado que se encarga de cambiar el estado a la oportunidad. cerrandola.
  * @params 
  * @return
  */
  async cerrarOportunidad(){
    this.oportunidad.fecha_cierre = new Date();
    this.oportunidad.OportunidadOpen = false;
    this.oportunidad.oportunidadConcretada = this.cerrarOportunidadForm.controls['selectOporConcretada'].value;

    if(this.cerrarOportunidadForm.controls['selectOporConcretada'].value === 'false' && this.cerrarOportunidadForm.controls['selectConceptCierre'].value){
      this.oportunidad.concepto_cierre = this.cerrarOportunidadForm.controls['selectConceptCierre'].value.trim();
      this.oportunidad.productos = this.cerrarOportunidadForm.controls['productos'].value? this.cerrarOportunidadForm.controls['productos'].value : null;
    }else if(this.cerrarOportunidadForm.controls['selectOporConcretada'].value === 'true' && this.cerrarOportunidadForm.controls['listaOv'].value){
      this.oportunidad.notaVenta = this.cerrarOportunidadForm.controls['listaOv'].value? this.cerrarOportunidadForm.controls['listaOv'].value : null;
    } 

    let consulta: any = await this._OportunidadesService.finalizarOportunidad(this.oportunidad);
    if(!consulta.error) this.finalizarEmitter.emit();
  }

  /**
  * @author ignacio zapata  \"2020-11-26\
  * @desc Metodo que se utiliza para setear algunos validadores del formulario de forma dinamica. especificamente cuando se selecciona que la oportunidad no fue concretada.
  * @params value asociado al select selectOporConcretada
  * @return
  */
  setValidadores(ConcretadaValue: string){
    if(ConcretadaValue === 'false'){
      // si no fue concretada, se setean validadores al select de concepto de cierre
      this.addValidator('selectConceptCierre');
      this.addValidator('productos');

      // y se quitan validadores al ovFolio.
      this.removeValidators('listaOv');
      this.ovFolio = null;
    }else{
      // si oportunidad fue concretada, entonces se quitan validadores al selectConceptoCierre y lista de productos
      this.removeValidators('selectConceptCierre');
      this.removeValidators('productos');
      this.listaSku = [];

      // y se setean validadores a ovFolio y productos.
      this.addValidator('listaOv');
    }
  }

  /**
  * @author ignacio zapata  \"2020-11-26\
  * @desc Metodo utilizado para cancelar el procedimiento de cierre de una oportunidad. reseteando variables y ocultando botones.
  * @params 
  * @return
  */
  cancelarCierre(){
    this.cerrarOportunidadForm.controls['selectOporConcretada'].setValue(null);
    this.removeValidators('selectConceptCierre');
    this.removeValidators('listaOv');
    this.removeValidators('productos');
    this.finalizarOportunidad = false;
    this.borrandoItem = false;
    this.ovFolio = null;
    this.listaSku = [];
  }

  removeValidators(control: string){
    this.cerrarOportunidadForm.controls[control].setValue(null);
    this.cerrarOportunidadForm.controls[control].clearValidators();
    this.cerrarOportunidadForm.controls[control].updateValueAndValidity();
  }

  addValidator(control: string){
    this.cerrarOportunidadForm.controls[control].setValidators([Validators.required]);
    this.cerrarOportunidadForm.controls[control].updateValueAndValidity();
  }

  async presentPopoverOv(ev: any) {
    const popover = await this.modalController.create({
      component: SetOvListComponent,
      mode: 'ios',
      animated: true,
      componentProps: {
        alreadySelectedOV: this.cerrarOportunidadForm.controls['listaOv'].value,
        rutVendedor: this.oportunidad.rutVendedor,
        rutCliente: this.oportunidad.rutCliente,
        nombreCliente: this.oportunidad.clienteNombre
      }
    });

     popover.onDidDismiss().then((data) => {
      if(data && data.data && data.data.length > 0){
        let listaOv: any[] = [];

        listaOv = data.data.map( item => {
          return item.folio;
        });
        this.ovFolio = listaOv.join(' ');
        this.cerrarOportunidadForm.controls['listaOv'].setValue(data.data);  
      }else{
        this.ovFolio = '';
        this.cerrarOportunidadForm.controls['listaOv'].setValue(null);  
      }
    });

    return await popover.present();
  }

  async AlertAddProduct() {
    if(!this.borrandoItem){
      const alert = await this.alertController.create({
        inputs: [
          {
            name: 'SKU',
            type: 'text',
            placeholder: 'Ej: WUXACC0001'
          }],    
        header: 'Ingresar SKU',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Agregar',
            handler: (alertData) => {  
              alertData.SKU && String(alertData.SKU).length > 0? this.listaSku.push({sku: String(alertData.SKU).toUpperCase()}) : this._DataLocalService.presentToast('Debe ingresar algun SKU.','danger');
              this.setProductosControl();
            }
          }
        ]
      });
  
      await alert.present();
    }else{
      this.borrandoItem = false;
    }
  }

  removeSku(index: number){
    this.borrandoItem = true;
    this.listaSku.splice(index, 1);
    this.setProductosControl();
  }

  setProductosControl(){
    this.listaSku && this.listaSku.length > 0? this.cerrarOportunidadForm.controls['productos'].setValue(this.listaSku) : this.cerrarOportunidadForm.controls['productos'].setValue(null);
  }
}

import { Component, OnInit, Input } from '@angular/core';
import {PopoverController, NavParams} from '@ionic/angular';

// services
import { OportunidadesService } from 'src/app/services/oportunidades/oportunidades.service';

@Component({
  selector: 'app-popover-filtros-oportunidades',
  templateUrl: './popover-filtros-oportunidades.component.html',
  styleUrls: ['./popover-filtros-oportunidades.component.scss'],
})
export class PopoverFiltrosOportunidadesComponent implements OnInit {


  constructor(private PopoverController: PopoverController, public _oporServices: OportunidadesService) { }

  ngOnInit() {

  }

  setFiltros(estado: string, periodo: string, orderBy: string, tipoContacto: string){
    this._oporServices.filtros.estado  = estado;
    this._oporServices.filtros.periodo = periodo;
    this._oporServices.filtros.orderBy = orderBy;
    this._oporServices.filtros.tipoContacto = tipoContacto;

    this.PopoverController.dismiss('filtrar')
  }

  cerrar(){
    this.PopoverController.dismiss();
  }

}

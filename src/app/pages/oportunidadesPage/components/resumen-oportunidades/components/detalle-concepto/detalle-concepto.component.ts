import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-detalle-concepto',
  templateUrl: './detalle-concepto.component.html',
  styleUrls: ['../../resumen-oportunidades.component.scss'],
})
export class DetalleConceptoComponent implements OnInit {
  @Input() conceptos: any;
  @Input() concepName: string;

  viewDetail: boolean = false;

  constructor() { }

  ngOnInit() {}

}

import { Component, OnInit, Input } from '@angular/core';

// interfaces
import { ResumenOportunidades } from 'src/app/interfaces/oportunidades/oportunidades';

@Component({
  selector: 'app-resumen-oportunidades',
  templateUrl: './resumen-oportunidades.component.html',
  styleUrls: ['./resumen-oportunidades.component.scss'],
})
export class ResumenOportunidadesComponent implements OnInit {
  @Input() ResumenOportunidades: ResumenOportunidades

  viewConceptosCreacionAbiertas: boolean = false;
  viewConceptosCreacionCerradas: boolean = false;
  viewConceptosCierreCerradas: boolean = false;

  constructor() { }

  ngOnInit() {}

}

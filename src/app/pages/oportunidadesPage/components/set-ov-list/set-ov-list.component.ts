import { Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as moment from 'moment';

// servicios
import { ClienteService } from 'src/app/services/cliente/cliente.service'
import { LoadingService } from 'src/app/services/loading/loading.service'

// interfaces
import { Pedido } from 'src/app/interfaces/cliente/pedido';


@Component({
  selector: 'app-set-ov-list',
  templateUrl: './set-ov-list.component.html',
  styleUrls: ['./set-ov-list.component.scss'],
})
export class SetOvListComponent implements OnInit {
  @Input() rutCliente: string;
  @Input() rutVendedor: string;
  @Input() nombreCliente: string;
  @Input() alreadySelectedOV: any[];
  @Output() ovEventEmitter = new EventEmitter();

  listaPedidos: Pedido[] = [];
  cargado: boolean = false;

  constructor(private ModalController: ModalController, private _clienteService: ClienteService, private _LoadingService: LoadingService) { }

  async ngOnInit() {
    await this._LoadingService.presentLoading('Cargando lista de OV..');
    await this.getOVList();
    this.cargado = true;
    this._LoadingService.hideLoading();
  }

  closeModal(){
    this.ModalController.dismiss();
  }

  async getOVList(){
    let listaPedidos = await this._clienteService.obtenerPedidos({rut: this.rutVendedor}, { rut: this.rutCliente});
    
    // Filtramos solo las OV.
    for(let pedido of listaPedidos){
      let folio = pedido.folio.split('-');
      if(folio[0] && folio[0] === 'OV' && pedido.estado != 'CANCELADO'){
        let fecha = pedido.fecha.split(' ');
        fecha = fecha[0].split('-');
        pedido.fecha =  moment(fecha[1]+'/'+fecha[0]+'/'+fecha[2]).locale('es').format('DD MM YYYY');

        let selected = this.valSelectedOV(pedido.folio);
        let dato = {
          ...pedido,
          selected: selected,
        }
        this.listaPedidos.push(dato);
      }
    }
  }

  /**
  * @author ignacio zapata  \"2020-12-03\
  * @desc metodo utilizado para devolver del modal, un objeto con el folio y venta de todas los pedidos seleccionados por el usuario.
  * @params 
  * @return objeto con propiedad folio y venta
  */
  retornarOVs(){
    let selectedOV: any[] = [];

    this.listaPedidos.forEach( (ov : any) => {
      if(ov.selected) selectedOV.push({folio: ov.folio, total: ov.Venta });
    })

    this.ModalController.dismiss(selectedOV);
  }

  valSelectedOV(pedido){
    if(this.alreadySelectedOV){
      for(let ov of this.alreadySelectedOV){
        if(ov.folio === pedido) return true;
      }
    }
    return false;
  }

}

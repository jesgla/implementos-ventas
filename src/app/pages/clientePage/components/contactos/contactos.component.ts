import { Component, OnInit, Input } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Contacto } from 'src/app/interfaces/contacto/contacto';
import { IonSlides } from '@ionic/angular';
/**
 *Componente que muestra los contactos del cliente
 * @export
 * @class ContactosComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.scss'],
})
export class ContactosComponent implements OnInit {

  /**
   *elemento slider
   * @type {IonSlides}
   * @memberof ContactosComponent
   */
  @Input() slides:IonSlides;

  /**
   *listado de contactos
   * @type {*}
   * @memberof ContactosComponent
   */
  @Input() agenda : any;
  
  /**
   *Creates an instance of ContactosComponent.
   * @param {ClienteService} clienteService
   * @memberof ContactosComponent
   */
  constructor(private clienteService : ClienteService) { }

  /**
   *Metodo que se ejecuta al iniciar el componente
   * @memberof ContactosComponent
   */
  async ngOnInit() {

  }

  /**
   *Permite ir al inicio del slider
   * @param {*} value
   * @memberof ContactosComponent
   */
  evento(value){
 
    //value? this.slides.slidePrev(): this.slides.slideNext();
    this.slides.slideTo(1)
  }
}

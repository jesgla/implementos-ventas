import { Component, OnInit, Input } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Evento } from 'src/app/interfaces/cliente/evento';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

/**
 *Permite visualizar los eventos del cliente
 * @export
 * @class EventosComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.scss'],
})
export class EventosComponent implements OnInit {

  /**
   *arreglo de eventos
   * @type {Evento[]}
   * @memberof EventosComponent
   */
  listadoEventos: Evento[];

  /**
   *Elemento slider
   * @type {*}
   * @memberof EventosComponent
   */
  @Input() slides:any;

  /**
   *Objeto vendedor
   * @type {*}
   * @memberof EventosComponent
   */
  vendedor: any;

  /**
   *objeto cliente
   * @type {*}
   * @memberof EventosComponent
   */
  detalleCliente: any;

  /**
   *estado de data recibida
   * @type {boolean}
   * @memberof EventosComponent
   */
  obtener: boolean;

  /**
   *Creates an instance of EventosComponent.
   * @param {ClienteService} clienteService
   * @param {DataLocalService} dataLocal
   * @memberof EventosComponent
   */
  constructor(private clienteService: ClienteService, private dataLocal: DataLocalService, ) {
    this.obtener = true
  }

  /**
   *Metodo que se ejecuta al iniciar el componente
   * @memberof EventosComponent
   */
  async ngOnInit() {
    await this.obtenerUsuario();
    await this.obtenerEventos();
  }

  /**
   *Permite obtener la sesion de usuario
   * @memberof EventosComponent
   */
  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
    let cliente = await this.dataLocal.getCliente()
    this.vendedor = JSON.parse(usuario);
    this.detalleCliente = cliente;
 
  }

  /**
   *Permite obtener los eventos del cliente
   * @returns listado de eventos
   * @memberof EventosComponent
   */
  async obtenerEventos(){

    this.listadoEventos= await this.clienteService.obtenerEventos(this.vendedor,this.detalleCliente);
    this.obtener=false;
    return this.listadoEventos;
  }
  
  /**
   *Permite cambiar la vista de slider
   * @param {*} value
   * @memberof EventosComponent
   */
  evento(value){
    value? this.slides.slidePrev(): this.slides.slideNext();
  }
}

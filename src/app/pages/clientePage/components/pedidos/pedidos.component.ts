import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Pedido } from 'src/app/interfaces/cliente/pedido';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { DetalleOVPage } from 'src/app/pages/Modals/detalle-ov/detalle-ov.page';
import { ModalController } from '@ionic/angular';

/**
 *permite ver los pedidos de un cliente
 * @export
 * @class PedidosComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss'],
})
export class PedidosComponent implements OnInit {

  /**
   *listado de pedidos
   * @type {Pedido[]}
   * @memberof PedidosComponent
   */
  listadoPedido: Pedido[];
  listadoPedidoBackUp: Pedido[];

  /**
   *elemento slider
   * @type {*}
   * @memberof PedidosComponent
   */
  @Input() slides: any;

  /**
   *objeto cliente
   * @type {Cliente}
   * @memberof PedidosComponent
   */
  @Input() detalleCliente: Cliente;

  /**
   *objeto vendedor
   * @type {Vendedor}
   * @memberof PedidosComponent
   */
  vendedor: Vendedor;

  /**
   *estado de informacion
   * @type {boolean}
   * @memberof PedidosComponent
   */
  obtener: boolean;

  busqueda: string = '';

  /**
   *Creates an instance of PedidosComponent.
   * @param {ClienteService} clienteService
   * @param {DataLocalService} dataLocal
   * @memberof PedidosComponent
   */
  constructor(private clienteService: ClienteService, private dataLocal: DataLocalService, public modalController: ModalController, ) {
    this.obtener = true
  }

  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof PedidosComponent
   */
  async ngOnInit() {
    await this.obtenerUsuario();
    await this.obtenerPedidos();
  }

  /**
   *permite obtener el listado de los pedidos de un cliente
   * @returns listado de cliente
   * @memberof PedidosComponent
   */
  async obtenerPedidos() {
    this.listadoPedido = await this.clienteService.obtenerPedidos(this.vendedor, this.detalleCliente);
    this.obtener = false;

    await this.setOrdenCompra();
    this.listadoPedidoBackUp = this.listadoPedido;

    return this.listadoPedido;
  }

  /**
   *Permite abrir modal para registrar cotizacion
   *
   * @returns
   * @memberof CarroCompraPage
   */
  async abrirModal(orden) {
    const modal = await this.modalController.create({
      component: DetalleOVPage,
      componentProps: {
        orden: orden
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
   
    data.recargar ? await this.ngOnInit() : null;
  }
  /**
   *permite obtener usuario
   * @memberof PedidosComponent
   */
  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
    let cliente = await this.dataLocal.getCliente()
    this.vendedor = JSON.parse(usuario);
    this.detalleCliente = cliente;

  }

  /**
  * @author ignacio zapata  \"2020-09-14\
  * @desc Metodo que se utiliza para dividir el valor del campo folio, cuando viene con una orden de compra.
  * @params 
  * @return
  */
  setOrdenCompra(){
    for(let pedido of this.listadoPedido){
      let folio = pedido.folio.split('/');
      pedido.folio = folio[0];
      pedido.ordenC = folio[1] ?  folio[1] : '';
    }  
  }

  /**
  * @author ignacio zapata  \"2020-09-15\
  * @desc Metodo que se ejecuta cada ves que el value del ion-search cambia. filtrando la lista de productos.
  * @params 
  * @return
  */
  buscar(){
    if(this.busqueda.length > 0){
      this.listadoPedido = this.listadoPedidoBackUp.filter( pedido => {
        if(pedido.folio){
           return (pedido.folio.toLowerCase().match(this.busqueda.toLowerCase()) ||  pedido.ordenC.toLowerCase().match(this.busqueda.toLowerCase()))
        }
      })
    }else{
      this.listadoPedido = this.listadoPedidoBackUp;
    }
  }

}

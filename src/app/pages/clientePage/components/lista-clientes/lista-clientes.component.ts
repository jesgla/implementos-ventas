import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { NavController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

/**
 *Permite visualizar el listado de clientes
 * @export
 * @class ListaClientesComponent
 * @implements {OnInit}
 * @implements {OnChanges}
 */
@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.scss'],
})
export class ListaClientesComponent implements  OnChanges {

  /**
   *listado de clientes
   * @type {Cliente[]}
   * @memberof ListaClientesComponent
   */
  @Input() listaClientes: Cliente[];

  /**
   *permite realizar la busqueda de cliente
   * @type {string}
   * @memberof ListaClientesComponent
   */
  @Input() textoBuscar: string;

  /**
   *estado de informacion
   * @type {boolean}
   * @memberof ListaClientesComponent
   */
  @Input() obtener: boolean;

  /**
   *Creates an instance of ListaClientesComponent.
   * @param {ClienteService} clienteService
   * @param {NavController} navCtrl
   * @param {DataLocalService} dataLocal
   * @memberof ListaClientesComponent
   */
  constructor(private clienteService: ClienteService, private navCtrl: NavController, private dataLocal: DataLocalService) {
  

  }
  /**
   *permite obtener los cambios en las variables
   * @param {SimpleChanges} changes
   * @memberof ListaClientesComponent
   */
  async ngOnChanges(changes: SimpleChanges) {
    
  }

  /**
   *permite ver el detalle del cliente
   * @param {*} cliente
   * @memberof ListaClientesComponent
   */
  async verDertalleCliente(cliente) {
 
    await this.dataLocal.setCliente(cliente);
    this.navCtrl.navigateForward(['/detalle-cliente'/* , JSON.stringify(cliente) */]);
    //this.clienteService.detalleCliente(cliente);
  }

}

import { Component, Input } from '@angular/core';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { ToastController, NavController } from '@ionic/angular';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Router } from '@angular/router';

/**
 *Componente que permite visualizar los datos comerciales
 * @export
 * @class ComercioComponent
 */
@Component({
  selector: 'app-comercio',
  templateUrl: './comercio.component.html',
  styleUrls: ['./comercio.component.scss'],
})
export class ComercioComponent {
  /**
   *elemento slider
   * @type {*}
   * @memberof ComercioComponent
   */
  @Input() slides: any;

  /**
   *objeto detalle cliente
   * @type {Cliente}
   * @memberof ComercioComponent
   */
  @Input() detalleCliente: Cliente;

  /**
   *arreglo acciones con plugins
   * @memberof ComercioComponent
   */
  acciones = [
    { icono: 'call', funcion: 'Llamar', color: 'primary' },
    { icono: 'logo-whatsapp', funcion: 'Whatsapp', color: 'success' },
    { icono: 'mail', funcion: 'Correo', color: 'primary' },
    { icono: 'calendar', funcion: 'Agendar', color: 'primary' },
    { icono: 'navigate', funcion: 'Visitar', color: 'primary' }
  ];

  /**
   *Creates an instance of ComercioComponent.
   * @param {CallNumber} callNumber
   * @param {ToastController} toastController
   * @param {SocialSharing} socialSharing
   * @param {LaunchNavigator} launchNavigator
   * @param {NavController} navCtrl
   * @memberof ComercioComponent
   */
  constructor(private callNumber: CallNumber,
    public toastController: ToastController,
    private socialSharing: SocialSharing,
    private launchNavigator: LaunchNavigator,
    private navCtrl: NavController
  ) { }

  /**
   *Permite ir al inicio del slides
   * @param {*} accionesCliente
   * @memberof ComercioComponent
   */
  slidesDidLoad(accionesCliente) {
    accionesCliente.startAutoplay();
  }

  /**
   *Muestra mensaje al usuario
   * @memberof ComercioComponent
   */
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'CLIENTE NO TIENE UBICACIÓN ENCONTRADA. NO ES POSIBLE USAR EL MAPA!!',
      duration: 2000
    });
    toast.present();
  }
}

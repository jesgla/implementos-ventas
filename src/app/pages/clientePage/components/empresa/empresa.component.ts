import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { PluginService } from 'src/app/services/plugin/plugin.service';
import { Evento } from 'src/app/interfaces/evento/evento';
import { EventoService } from 'src/app/services/evento/evento.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

/**
 *Permite visualizar los datos de la empresa
 * @export
 * @class EmpresaComponent
 * @implements {OnInit}
 * @implements {OnChanges}
 */
@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.scss'],
})
export class EmpresaComponent {

  /**
   *Elemento slider
   * @type {*}
   * @memberof EmpresaComponent
   */
  @Input() slides: any;

  /**
   *Objeto de cliente
   * @type {Cliente}
   * @memberof EmpresaComponent
   */
  @Input() detalleCliente: Cliente;

  /**
   *Objeto evento
   * @type {Evento}
   * @memberof EmpresaComponent
   */
  registrarCheckIn: Evento;
  vendedor: any;

  /**
   *Creates an instance of EmpresaComponent.
   * @param {Geolocation} geolocation
   * @param {DataLocalService} dataLocal
   * @param { Router } Router
   * @param { NavController } navCtrl
   * @memberof EmpresaComponent
   */
  constructor(
    private pluginService: PluginService,
    private router: Router) {
    this.detalleCliente = new Cliente();
    this.registrarCheckIn = new Evento();
  }

  /**
   *permite ejecutar los plugins nativos
   * @param {*} funcion
   * @memberof EmpresaComponent
   */
  ejecutarFuncion(funcion) {

    this.pluginService.ejecutarSocialShare(funcion, this.detalleCliente);
  }

  /**
   *Permite ejecutar la aplicacion de navegacion
   * @memberof EmpresaComponent
   */
  abrirNavegacion() {
    this.pluginService.abrirNavegacion(this.detalleCliente)
  }

  /**
   *Permite ir a la pantalla de check-in
   * @memberof EmpresaComponent
   */
  async checkIn() {
    this.router.navigate(['/registro-visita']);    
  }
}

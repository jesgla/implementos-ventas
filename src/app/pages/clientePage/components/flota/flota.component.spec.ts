import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlotaComponent } from './flota.component';

describe('FlotaComponent', () => {
  let component: FlotaComponent;
  let fixture: ComponentFixture<FlotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlotaComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

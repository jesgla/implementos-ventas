import { Component, OnInit, Input } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Flota } from 'src/app/interfaces/cliente/flota';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Cliente } from 'src/app/interfaces/cliente/cliente';

/**
 *Permite visualizar la flota de un cliente
 * @export
 * @class FlotaComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-flota',
  templateUrl: './flota.component.html',
  styleUrls: ['./flota.component.scss'],
})
export class FlotaComponent implements OnInit {

  /**
   *arreglo de flota
   * @type {Flota[]}
   * @memberof FlotaComponent
   */
  listadoFlota: Flota[];

  /**
   *elemento slider
   * @type {*}
   * @memberof FlotaComponent
   */
  @Input() slides: any;

  /**
   *flota del cliente
   * @type {Flota}
   * @memberof FlotaComponent
   */
  flota: Flota;

  /**
   *objeto vendedor
   * @type {Vendedor}
   * @memberof FlotaComponent
   */
  vendedor: Vendedor;

  /**
   *estado de informacion
   * @type {boolean}
   * @memberof FlotaComponent
   */
  obtener: boolean;

  /**
   *objeto de cliente
   * @type {Cliente}
   * @memberof FlotaComponent
   */
  detalleCliente: Cliente;

  /**
   *Creates an instance of FlotaComponent.
   * @param {ClienteService} clienteService
   * @param {DataLocalService} dataLocal
   * @memberof FlotaComponent
   */
  constructor(private clienteService: ClienteService, private dataLocal: DataLocalService, ) {
    this.obtener = true
  }

  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof FlotaComponent
   */
  async ngOnInit() {
    await this.obtenerCliente();
    await this.obtenerFlota();
  }

  /**
   *permite obtener la flota del cliente
   * @returns
   * @memberof FlotaComponent
   */
  async obtenerFlota() {
    this.listadoFlota = await this.clienteService.obtenerFlota(this.detalleCliente);
   
    this.obtener = false;
    this.flota = this.listadoFlota[0];
    return this.flota;
  }

  /**
   *permite obtener el cliente
   * @memberof FlotaComponent
   */
  async obtenerCliente() {
    let cliente = await this.dataLocal.getCliente()
    this.detalleCliente = cliente;

  }

}

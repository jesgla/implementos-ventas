import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaClientesComponent } from './lista-clientes/lista-clientes.component';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { EventosComponent } from './eventos/eventos.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { FlotaComponent } from './flota/flota.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { ContactosComponent } from './contactos/contactos.component';
import { DireccionesComponent } from './direcciones/direcciones.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { ComercioComponent } from './comercio/comercio.component';

import { DetalleOVPage } from '../../Modals/detalle-ov/detalle-ov.page';
import { DetalleOVPageModule } from '../../Modals/detalle-ov/detalle-ov.module';

import { FormsModule } from '@angular/forms';

@NgModule({
  entryComponents:[DetalleOVPage],
  declarations: [
    ListaClientesComponent,
    EventosComponent,
    PedidosComponent,
    FlotaComponent,
    ContactosComponent,
    DireccionesComponent,
    EmpresaComponent,
    ComercioComponent
  ],
  exports:[
    ListaClientesComponent,
    EventosComponent,
    PedidosComponent,
    FlotaComponent,
    ContactosComponent,
    DireccionesComponent,
    EmpresaComponent,
    ComercioComponent
  ],
  imports: [
    DetalleOVPageModule,
    ComponentsModule,
    PipesModule,
    CommonModule,
    IonicModule,
    RouterModule,
    FormsModule
  ]
})
export class ClienteComponentsModule { }

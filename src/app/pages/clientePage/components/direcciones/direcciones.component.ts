import { Component, OnInit, Input } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Direccion } from 'src/app/interfaces/direccion/direccion';
import { PluginService } from 'src/app/services/plugin/plugin.service';

/**
 *Permite visualizar las direcciones del cliente
 * @export
 * @class DireccionesComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-direcciones',
  templateUrl: './direcciones.component.html',
  styleUrls: ['./direcciones.component.scss'],
})
export class DireccionesComponent implements OnInit {

  /**
   *elemento slider
   * @type {*}
   * @memberof DireccionesComponent
   */
  @Input() slides:any;

  /**
   *Listado de direcciones del cliente
   * @type {*}
   * @memberof DireccionesComponent
   */
  @Input() listadoDireccion: any;

  /**
   *Creates an instance of DireccionesComponent.
   * @param {ClienteService} clienteService
   * @memberof DireccionesComponent
   */
  constructor(private clienteService : ClienteService,private pluginService : PluginService) { }

  /**
   *Metodo que se ejecuta al iniciar el componente
   * @memberof DireccionesComponent
   */
  async ngOnInit() {
    //await this.obtenerDirecciones();
  }

  /**
   *Permite cambiar la vista del componente
   * @param {*} value
   * @memberof DireccionesComponent
   */
  evento(value){
    value? this.slides.slidePrev(): this.slides.slideNext();
  }
  abrirDireccion(direccion){
    this.pluginService.abrirDireccion(direccion)
  }
}

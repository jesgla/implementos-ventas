import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { Subscription } from 'rxjs';


// services
import { LoadingService } from 'src/app/services/loading/loading.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

/**
 *componente de cliente 
 * @export
 * @class ClientesPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit, OnChanges, OnDestroy{

  /**
   *permite buscar un cliente en el listado de clientes
   * @type {string}
   * @memberof ClientesPage
   */
  textoBuscar :string = '';

  /**
   *listado clientes
   * @type {Cliente[]}
   * @memberof ClientesPage
   */
  @Input() listaClientes: Cliente[];

  /**
 *listado clientes
  * @type { Cliente[] }
  * @memberof ClientesPage
  */
  listaClientesAsignados: Cliente[];
  vendedorReloadSubscription$: Subscription;

  /**
   *Creates an instance of ClientesPage.
   * @param {ClienteService} clienteService
   * @param { LoadingService } _loadingService
   * @param { DataLocalService } _dataLocal
   * @memberof ClientesPage
   */
  constructor(private clienteService : ClienteService, private _LoadingService: LoadingService, private _dataLocal: DataLocalService, private _UsuarioService: UsuarioService) {
    // Observable que nos sirve para verificar el cambio de vendedor desde el tab vendedores, utilizado desde el perfil de zonales
    this.vendedorReloadSubscription$ = this._UsuarioService.userReload$.subscribe( (resp: any) => {
      this.listaClientesAsignados = null;
      this.listaClientes = [];
    })
  }
  
  @Input() obtener:boolean
  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof ClientesPage
   */
  ngOnInit() {
    
  }

  ngOnDestroy(){
    this.vendedorReloadSubscription$? this.vendedorReloadSubscription$.unsubscribe() : '';
  }


  async ngOnChanges(changes: SimpleChanges){
    if((changes.listaClientes && !this.listaClientesAsignados) ){
      this.listaClientesAsignados = changes.listaClientes.currentValue;
    }
  }
  
  /**
   *permite buscar cliente 
   * @param {*} event
   * @memberof ClientesPage
   */
  buscar( event ) {
    this.textoBuscar = event.detail.value;
  }

  /**
   *permite buscar cliente por rut o nombre
   * @param {*} rut
   * @memberof ClientesPage
   */
  async buscarCliente(rut: string){
    if(rut.length > 0){
      try{

        await this._LoadingService.presentLoading('Buscando Clientes..');
        let cliente ={
          rutCliente : rut
        }

        this.listaClientes= await this.clienteService.obtenerCliente(cliente);
        this.textoBuscar='';
      }catch(e){
        this._LoadingService.hideLoading();
        this._dataLocal.presentToast('Error al intentar obtener los datos de los clientes', 'danger');
      }
      this._LoadingService.hideLoading();
    }else{
      this.listaClientesAsignados?  this.listaClientes = this.listaClientesAsignados : '';
    }
  }

  // Metodo que se ejecuta cuando se presiona el boton cancelar asociado al input buscador y se encarga de mostrar nuevamente la lista de clientes asignados al vendedor
  cancelarBusqueda(){
    this.listaClientesAsignados?  this.listaClientes = this.listaClientesAsignados: '';
  }
  
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetalleClientePage } from './detalle-cliente.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ClienteComponentsModule } from '../../components/cliente-components.module';

const routes: Routes = [
  {
    path: '',
    component: DetalleClientePage
  }
];

@NgModule({
  imports: [
    ClienteComponentsModule,
    PipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DetalleClientePage
  ]
})
export class DetalleClientePageModule {}

import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { ToastController, IonSlides } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

/**
 *permite ver el detalle del cliente
 * @export
 * @class DetalleClientePage
 * @implements {OnInit}
 * @implements {AfterViewInit}
 */
@Component({
  selector: 'app-detalle-cliente',
  templateUrl: './detalle-cliente.page.html',
  styleUrls: ['./detalle-cliente.page.scss'],
})
export class DetalleClientePage implements OnInit, AfterViewInit {
  /**
   *objeto cliente
   * @type {Cliente}
   * @memberof DetalleClientePage
   */
  detalleCliente: Cliente;

  /**
   *elemento slider
   * @type {IonSlides}
   * @memberof DetalleClientePage
   */
  @ViewChild(IonSlides, { static: false }) theSlides: IonSlides;

  /**
   *color de elemento
   * @type {*}
   * @memberof DetalleClientePage
   */
  color: any;

  /**
   *arreglo de paginas de slider
   * @memberof DetalleClientePage
   */
  paginas = [
    { svg: 'person', index: 0 },
    { svg: 'list-box', index: 1 },
    { svg: 'calendar', index: 2 },
    { svg: 'cube', index: 3 },
    { svg: 'bus', index: 4 },
    { svg: 'contacts', index: 5 },
    { svg: 'pin', index: 6 },
  ];

  /**
   *configuracion de slider
   * @memberof DetalleClientePage
   */
  slideOpts = {
    autoHeight: true
  };

  /**
   *Creates an instance of DetalleClientePage.
   * @param {ClienteService} clienteService
   * @param {ToastController} toastController
   * @param {ActivatedRoute} route
   * @param { Router } router 
   * @param {DataLocalService} dataLocal
   * @memberof DetalleClientePage
   */
  constructor(private clienteService: ClienteService,
    public toastController: ToastController,
    private route: ActivatedRoute,
    private router: Router,
    private dataLocal: DataLocalService
  ) {
    this.detalleCliente = new Cliente();
  }

  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof DetalleClientePage
   */
  async ngOnInit() {
    this.detalleCliente = await this.dataLocal.getCliente();

    if(!this.detalleCliente){
      this.router.navigate(['tabs', 'objetivos']);
    }
  }

  /**
   *metodo que se ejecuta despues de iniciar la vista
   * @memberof DetalleClientePage
   */
  ngAfterViewInit() {
    this.theSlides.lockSwipes(true);

  }

  /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(event, slide) {
    slide.lockSwipes(false);
    slide.slideTo(event.detail.value.index);
    slide.lockSwipes(true);

  }

}

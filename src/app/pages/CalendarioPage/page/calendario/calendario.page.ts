//angular
import { Component, ViewChild, OnInit, Inject, LOCALE_ID, Input, OnDestroy } from '@angular/core';
import { formatDate } from '@angular/common';
import { Subscription } from 'rxjs';

//ionic
import { AlertController, ModalController } from '@ionic/angular';

//components
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { AgendarPage } from 'src/app/pages/Modals/agendar/agendar.page';

//services
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { EventoService } from 'src/app/services/evento/evento.service';
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';

//interfaces
import { Evento } from 'src/app/interfaces/cliente/evento';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { Cliente } from 'src/app/interfaces/cliente/cliente'

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.page.html',
  styleUrls: ['./calendario.page.scss'],
})

/**
 * Componente que permite visualizar las fechas agendadas por el vendedor
 * @export
 * @class CalendarioPage
 * @implements {OnInit}
 * @implements {OnChanges}
 */
export class CalendarioPage implements OnInit, OnDestroy {
  /**
   * variable de estado del cliente seleccionado
   * @type {boolean}
   * @memberof CalendarioPage
   */
  @Input() clienteSeleccionado: boolean;

  /**
   *Objeto del tipo evento calendario
   * @memberof CalendarioPage
   */
  event = {
    title: '',
    desc: '',
    startTime: '',
    endTime: '',
    allDay: false
  };

  /**
   * Contiene fecha minima para agendar visita
   * @type {string}
   * @memberof CalendarioPage
   */
  minDate: string;

    /**
   * Contiene fecha minima para agendar visita
   * @type {Cliente}
   * @memberof CalendarioPage
   */
  cliente: Cliente;

  /**
   * Listado de eventos calendarizados en la agenda
   * @memberof CalendarioPage
   */
  eventSource = [];
  /**
   * Titulo de vista agenda
   * @type {string}
   * @memberof CalendarioPage
   */
  viewTitle: string;
  /**
   * Titulo de agenda 
   * @type {string}
   * @memberof CalendarioPage
   */
  title: string;

  /**
   * Objeto de calendario para definir tipo de presentacion 
   * @memberof CalendarioPage
   */
  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };

  /**
   * Elemento calendario
   * @type {CalendarComponent}
   * @memberof CalendarioPage
   */
  @ViewChild(CalendarComponent, { static: true }) myCal: CalendarComponent;

  /**
   * Listado de eventos agendados por mes
   * @type {Evento[]}
   * @memberof CalendarioPage
   */
  eventos: Evento[];
  eventosAux: Evento[];
  /**
   *Objeto vendedor
   * @type {Vendedor}
   * @memberof CalendarioPage
   */
  vendedor: Vendedor;
  fecha: number;
  termino: number;
  reloadClienteSubcription$: Subscription;

  /**
   *Creates an instance of CalendarioPage.
   * @param {EventoService} eventoService
   * @param {AlertController} alertCtrl
   * @param {string} locale
   * @param {ModalController} modalController
   * @param {DataLocalService} dataLocal
   * @memberof CalendarioPage
   */

  constructor(
    private eventoService: EventoService,
    private alertCtrl: AlertController,
    @Inject(LOCALE_ID) private locale: string,
    public modalController: ModalController,
    private dataLocal: DataLocalService) {
    this.vendedor = new Vendedor();
    this.minDate = new Date().toISOString();
    this.eventos = [];
    this.eventosAux = [];
  }

  /**
   *Metodo que permite ejecutar funciones al inicio del componente
   * @memberof CalendarioPage
   */
  async ngOnInit() {
    await this.obtenerUsuario();
    await this.obtenerCliente();
    await this.resetEvent();

    this.reloadClienteSubcription$ = this.dataLocal.clienteReload$.subscribe( () => {
      this.obtenerCliente();
    })
   
    let fechas = await this.obtenerMes(new Date());
    this.eventosAux = await this.eventoService.obtenerEventosCliente(this.vendedor, fechas, '');

    this.setearEvento(this.eventosAux)
    //await this.ValidarCliente();
  }

  ngOnDestroy(){
    this.reloadClienteSubcription$? this.reloadClienteSubcription$.unsubscribe() : '';
  }

  
  
  obtenerMes(fecha) {
    let date = new Date(fecha);
    let primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
    let ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    let anio = date.getFullYear();

    let inicio = new Date(primerDia.toDateString() + -+ anio.toString());
    let termino = new Date(ultimoDia.toDateString() + -+ anio.toString());
    let fechas = {
      inicio: inicio,
      termino: termino
    }

    return fechas;
  }
  /**
   * Funcion que permite obtener vendedor logueado
   * @memberof CalendarioPage
   */
  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
    this.vendedor = JSON.parse(usuario);

  }

  /**
   *  Permite obtener los valores del cliente seleccionado
   */
  async obtenerCliente(){
    this.cliente = await this.dataLocal.getCliente();
  }

  /**
   * Permite almacenar evento en listado de eventos
   * @param {Evento[]} eventos
   * @memberof CalendarioPage
   */
  setearEvento(eventos: Evento[]) {
    eventos.map(evento => {

      let fechaCorregida = evento.fechaProgramada.split(" ");
      let hora = fechaCorregida[1];
      let horaTermino = this.horaTerminoVisita(hora);

      fechaCorregida = fechaCorregida[0].split("-")
      let fechaD = new Date(`${fechaCorregida[1]}-${fechaCorregida[0]}-${fechaCorregida[2]} ${hora}`);
      let fechaT = new Date(`${fechaCorregida[1]}-${fechaCorregida[0]}-${fechaCorregida[2]} ${horaTermino}`);
      let eventCopy = {
        title: evento.nombreCliente.toLowerCase(),
        startTime: new Date(fechaD),
        endTime: new Date(fechaT),
        allDay: false,
        desc: evento.Observaciones,

      }
      this.eventSource.push(eventCopy);
      evento.fechaCorregida = `${fechaCorregida[1]}-${fechaCorregida[0]}-${fechaCorregida[2]}`
    })

    this.myCal.loadEvents();
  }

  /**
   *
   * @param {string} hora
   * @returns
   * @memberof CalendarioPage
   */
  horaTerminoVisita(hora: string) {
    let arregloHora = hora.split(":");
    let min = Number(arregloHora[1]);
    min += 30;
    arregloHora[1] = min.toString();
    if (min > 60) {
      arregloHora[1] = "00"
    }
    let horaF = `${arregloHora[0]}:${arregloHora[1]}:${arregloHora[2]}`
    return horaF
  }

  /**
   * Funcion que permite limpiar instancia evento
   * @memberof CalendarioPage
   */
  resetEvent() {
    this.event = {
      title: '',
      desc: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    };
  }



  /**
   * Funcion que permite crear un evento en el calendario
   * @memberof CalendarioPage
   */
  addEvent() {
    let eventCopy = {
      title: this.event.title,
      startTime: new Date(this.event.startTime),
      endTime: new Date(this.event.endTime),
      allDay: this.event.allDay,
      desc: this.event.desc
    }

    if (eventCopy.allDay) {
      let start = eventCopy.startTime;
      let end = eventCopy.endTime;

      eventCopy.startTime = new Date(Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate()));
      eventCopy.endTime = new Date(Date.UTC(end.getUTCFullYear(), end.getUTCMonth(), end.getUTCDate() + 1));
    }

    this.eventSource.push(eventCopy);
    this.myCal.loadEvents();
    this.resetEvent();
  }

  /**
   * Permite cambiar al siguiente mes
   *
   * @memberof CalendarioPage
   */
  next() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slideNext();
  }

  /**
   * Permite devolverse un mes
   * @memberof CalendarioPage
   */
  back() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slidePrev();
  }

  /**
   *Cambia el modo de vista a semanas mes dias
   *
   * @param {*} mode
   * @memberof CalendarioPage
   */
  changeMode(mode) {
    this.calendar.mode = mode;
  }

  /**
   *Permite obtener la fecha actual
   * @memberof CalendarioPage
   */
  today() {
    this.calendar.currentDate = new Date();
  }

  /**
   * Selected date reange and hence title changed
   * @param {*} title
   * @memberof CalendarioPage
   */
  async onViewTitleChanged(title) {
    await this.obtenerUsuario()
    this.viewTitle = title.replace('Week', 'Semana');
    let titulo = title.split(' ');
    this.title = titulo[0];
    this.eventSource = [];
    let fechas = await this.obtenerMes(new Date(title));
    this.eventosAux = await this.eventoService.obtenerEventosCliente(this.vendedor, fechas, '');

    this.setearEvento(this.eventosAux)
  }

  /**
   *Calendar event was clicked
   * @param {*} event
   * @memberof CalendarioPage
   */
  async onEventSelected(evento) {
    const alert = await this.alertCtrl.create({
      header: evento.nombreCliente,
      subHeader: 'Ejecución: ' + evento.fechaEjecucion,
      message: 'Observación: <br> ' + evento.Observaciones,
      buttons: ['Aceptar']
    });
    alert.present();
  }

  /**
   *Time slot was clicked
   * @param {*} ev
   * @memberof CalendarioPage
   */
  onTimeSelected(ev) {

    let selected = new Date(ev.selectedTime);
    let inicio = new Date(selected.setHours(0, 0, 0))
    let fin = new Date(inicio).setHours(23, 59, 59)
    this.event.startTime = selected.toISOString();
    selected.setHours(selected.getHours() + 1);
    this.event.endTime = (selected.toISOString());

    let fitro = this.eventosAux.filter(evento => inicio.getTime() == new Date(evento.fechaCorregida).getTime())
   
    this.eventos = fitro;
  }

  /**
   * Permite abrir modal para agendar visitas
   * @returns modal para agendar visita
   * @memberof CalendarioPage
   */
  async abrirModal() {
    const modal = await this.modalController.create({
      component: AgendarPage,
      componentProps: {
        eventSource: this.eventSource,
        myCal: this.myCal
      }
    });
    return await modal.present();
  }
}




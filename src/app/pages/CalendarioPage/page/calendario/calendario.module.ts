import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalendarioPage } from './calendario.page';
import { NgCalendarModule  } from 'ionic2-calendar';
import { AgendarPage } from 'src/app/pages/Modals/agendar/agendar.page';
import { AgendarPageModule } from 'src/app/pages/Modals/agendar/agendar.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  entryComponents:[AgendarPage],
  exports:[CalendarioPage],
  imports: [
    AgendarPageModule,
    CommonModule,
    FormsModule,
    IonicModule,
    NgCalendarModule,
    ComponentsModule
  ],
  
  declarations: [CalendarioPage]
})
export class CalendarioPageModule {}

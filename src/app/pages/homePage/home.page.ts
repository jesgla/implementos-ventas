// Angular
import { Component, OnInit } from '@angular/core';
import { HostListener } from "@angular/core";

// Ionic
import { Platform } from '@ionic/angular';

// services
import { SeccionesService } from 'src/app/services/secciones/secciones.service';
import {  DataLocalService } from 'src/app/services/dataLocal/data-local.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit {

  innerWidth: number;
  innerHeight: number;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
      this.innerWidth = window.innerWidth;
      this.innerHeight = window.innerHeight;
  }

  constructor(public platform: Platform, 
              private _seccionService: SeccionesService,
              private _dataLocal: DataLocalService) { }

  ngOnInit() {
  }

  // Metodo ejecutado cuando se selecciona un tipo de sección.
  async selectSeccion(tipo: string){
    await this._dataLocal.setSeccion(tipo);
    await this._seccionService.selectedSeccion(true);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatosVendedorComponent } from './datos-vendedor/datos-vendedor.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { Ng2Rut } from 'ng2-rut';
import { ObjetivosVendedorComponent } from './objetivos-vendedor/objetivos-vendedor.component';
import { VentasVendedorComponent } from './ventas-vendedor/ventas-vendedor.component';



@NgModule({
  declarations: [
    DatosVendedorComponent,
    ObjetivosVendedorComponent,
    VentasVendedorComponent
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    CommonModule,
    IonicModule,
    RouterModule,
    NgxEchartsModule,
    Ng2Rut
  ],
  exports: [
    DatosVendedorComponent,
    ObjetivosVendedorComponent,
    VentasVendedorComponent
  ]
})
export class VendedorComponentsModule { }

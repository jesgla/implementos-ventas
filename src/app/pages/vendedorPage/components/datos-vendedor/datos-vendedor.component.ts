import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';

@Component({
  selector: 'app-datos-vendedor',
  templateUrl: './datos-vendedor.component.html',
  styleUrls: ['./datos-vendedor.component.scss'],
})
export class DatosVendedorComponent implements OnInit, OnChanges {
  meses =['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
  @Input() vendedor: Vendedor;
  @Input() grafico: any;
  @Input() restaMes: number;
  option = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        crossStyle: {
          color: '#999'
        }
      }
    },
    legend: {
      data: ['Meta', 'Venta']
    },
    xAxis: [
      {
        type: 'category',
        data: ['8', '9', '10', '11', '12', 1],
        axisPointer: {
          type: 'shadow'
        },
        axisLabel: {
          rotate:  45
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: 'Millones',
   /*      min: 0,
        max: 250,
        interval: 50, */
        axisLabel: {
          formatter: '{value}'
        }
      },

    ],
    series: [
      {
        name: 'Meta',
        type: 'bar',
        data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
      },
      {
        name: 'Venta',
        type: 'bar',
        data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
      }
    ]
  };
  obtener: boolean;


  constructor() {
    this.obtener = true;
    this.vendedor = new Vendedor();
  }

  ngOnInit() { }
  ngOnChanges(changes: import("@angular/core").SimpleChanges) {

    if (this.grafico) {
      this.obtener = true;
      this.generarGrafico();
    }
  }

  generarGrafico() {
    let fecha = new Date();
    let mes = new Date().getMonth();
    let anio = new Date().getFullYear();
    let label = [];
    let venta = [];
    let meta = [];
    label.push(`${this.meses[mes]}-${anio}`)
  
    for (let index = 0; index < this.grafico.length; index++) {
      const element = this.grafico[index];
      let mesLabel = mes - 1;
      let seP = fecha.setMonth(fecha.getMonth() - 1);
    
      if (index < this.grafico.length - 1) {
        label.push(`${this.meses[new Date(seP).getMonth()]}-${new Date(seP).getFullYear()} `)
      }
      meta.push(Math.trunc(element.Meta/1000000));
      venta.push(Math.trunc(element.Venta/1000000));
    }
    label.reverse();

    this.option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          crossStyle: {
            color: '#999'
          }
        }
      },
      legend: {
        data: ['Meta', 'Venta']
      },
      xAxis: [
        {
          type: 'category',
          data: label,
          
          axisPointer: {
            type: 'shadow'
          },
          axisLabel: {
            rotate:  45
          }
        },
        
      ],
      
      yAxis: [
        {
          type: 'value',
          name: 'Millones',
         /*  min: 0,
          max: 250,
          interval: 50, */
          axisLabel: {
            formatter: '{value}'
          }
        },

      ],
      series: [
        {
          name: 'Meta',
          type: 'bar',
          data: meta,
          
        },
        
        {
          name: 'Venta',
          type: 'bar',
          data: venta
        }
      ]
    };
    this.obtener = false;

  }
}

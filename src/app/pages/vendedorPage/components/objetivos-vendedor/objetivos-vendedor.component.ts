import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Objetivos } from 'src/app/interfaces/vendedor/objetivos';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { Evento } from 'src/app/interfaces/cliente/evento';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { LoadingController, NavController } from '@ionic/angular';
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { EventoService } from 'src/app/services/evento/evento.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';

@Component({
  selector: 'app-objetivos-vendedor',
  templateUrl: './objetivos-vendedor.component.html',
  styleUrls: ['./objetivos-vendedor.component.scss'],
})
export class ObjetivosVendedorComponent implements  OnChanges {


  /**
   *arreglo de objetivos
   * @type {Objetivos[]}
   * @memberof ObjetivosTabPage
   */
  @Input() listaObjetivos: Objetivos[];
  @Input() cambio: boolean;
  @Input() vendedor: Vendedor;
  /**
   *objeto de objetivos vendedor
   * @type {Objetivos}
   * @memberof ObjetivosTabPage
   */
  objetivos: Objetivos;

  /**
   *fecha actual
   * @type {string}
   * @memberof ObjetivosTabPage
   */
  fecha: string;

  /**
   *menta del mes 
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  metaMes: number;

  /**
   *meta que debe llevar hasta hoy
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  metaAcumulada: number;

  /**
   *procentaje de meta mes
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  porcentajeMetaMes: number;

  /**
   *venta del dia
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  ventaDia: number;

  /**
   *venta acumulada del dia
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  ventaDiaAcumulado: number;

  /**
   *porcentaje de venta dia
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  porcentajeDia: number;

  /**
   *porcentaje de meta mes 
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  porcentajeMetaMesGrafico: number = 0;

  /**
   *visita a clientes
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  visitaConcretadas: number = 0;

  /**
   *visitas agendadas
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  visitasAgendadas: number;

  /**
   *porcentaje de visitas
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  porcentajeVisita: number;

  /**
   *venta del dia acumulada
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  ventaDiaAcumulada: number;

  /**
   *arreglo de eventos
   * @type {Evento[]}
   * @memberof ObjetivosTabPage
   */
  eventos: Evento[];



  /**
   *meta venta de hoy
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  mentaHoy: number;

  /**
   *venta acumulada hasta hoy
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  ventaAcumulada: number;
  /**
   *porcentaje de ventas
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  ventaAcumuladaProcentaje: number;

  /**
   *porcentaje venta 
   * @type {number}
   * @memberof ObjetivosTabPage
   */
  ventaDiaPorcentaje: number;

  /**
   *Creates an instance of ObjetivosTabPage.
   * @param {VendedorService} vendedorService
   * @param {LoadingController} loadingController
   * @param {AutentificacionService} authService
   * @param {DataLocalService} dataLocal
   * @param {EventoService} eventoService
   * @param {ClienteService} clienteService
   * @param {NavController} navCtrl
   * @memberof ObjetivosTabPage
   */
  constructor(
    private vendedorService: VendedorService,
    public loadingController: LoadingController,
    private authService: AutentificacionService,
    private dataLocal: DataLocalService,
    private eventoService: EventoService,
    private clienteService: ClienteService,
    private navCtrl: NavController
  ) {
    this.vendedor = new Vendedor();
    this.eventos = [];
    this.listaObjetivos = [];
    
  }
  /**
   *objeto grafico
   * @memberof ObjetivosTabPage
   */
  option = {
    series: [
      {
        name: 'Mentas Vendedor',
        type: 'pie',
        radius: ['50%', '70%'],
        avoidLabelOverlap: true,
        label: {
          normal: {
            show: false,
            position: 'center'
          },
          emphasis: {
            show: true,
            textStyle: {
              fontSize: '10',
              fontWeight: 'bold'
            }
          }
        },
        labelLine: {
          normal: {
            show: true
          }
        },
        data: [
          { value: 100, name: '', itemStyle: { color: '#7eb9f5' }, show: true },
          { value: this.porcentajeMetaMes * 100, name: '', itemStyle: { color: '#005db9' }, show: true },
        ]
      }
    ]
  };
 
  async ngOnChanges() {
    
    if(this.cambio){
     
      this.vendedor = await this.dataLocal.getItem('vendedor');
      await this.obtenerObjetivos();
      await this.obtenerValores();
  
      let fechas = await this.obtenerMes(new Date());
      this.eventos = await this.eventoService.obtenerEventosCliente(this.vendedor, fechas,'PROGRAMADA');
  
      this.fecha = await new Date().toDateString();
    }
   /*   */
  }
  /**
   *permite obtener los objetivos del vendedor
   * @memberof ObjetivosTabPage
   */
  async obtenerValores() {

    let dia = new Date().getDate();

    this.metaMes = await this.listaObjetivos[0].Meta;

    this.metaAcumulada = this.listaObjetivos[2].MetaAcumulada;
    this.ventaAcumulada = this.listaObjetivos[0].Venta;
    this.ventaAcumuladaProcentaje = this.ventaAcumulada / this.metaAcumulada;
    this.porcentajeMetaMesGrafico = Math.round(this.ventaAcumuladaProcentaje * 100);


    this.ventaAcumulada = Number((this.ventaAcumulada / 1000000).toFixed(1));
    this.metaAcumulada = Number((this.metaAcumulada / 1000000).toFixed(1));

    this.mentaHoy = this.listaObjetivos[2].Meta;
    this.ventaDia = this.listaObjetivos[2].Venta;
    this.ventaDiaPorcentaje = this.ventaDia / this.mentaHoy;
    this.mentaHoy = Number((this.mentaHoy / 1000000).toFixed(1));
    this.ventaDia = Number((this.ventaDia / 1000000).toFixed(1));
    this.visitaConcretadas = this.listaObjetivos[0].VisitasConfirmadas;
    this.visitasAgendadas = this.listaObjetivos[0].VisitasCreadas;
    this.porcentajeVisita = this.visitaConcretadas / this.visitasAgendadas;

    this.option = {
      series: [
        {
          name: 'Mentas Vendedor',
          type: 'pie',
          radius: ['50%', '70%'],
          avoidLabelOverlap: true,
          label: {
            normal: {
              show: false,
              position: 'center'
            },
            emphasis: {
              show: true,
              textStyle: {
                fontSize: '10',
                fontWeight: 'bold'
              }
            }
          },
          labelLine: {
            normal: {
              show: true
            }
          },
          data: [
            { value: 100, name: '', itemStyle: { color: '#7eb9f5' }, show: true },
            { value: this.porcentajeMetaMesGrafico, name: '', itemStyle: { color: '#005db9' }, show: true },
          ]
        }
      ]
    };
  }

  /**
   *permite consultar los objetivos mensuales
   * @returns
   * @memberof ObjetivosTabPage
   */
  async obtenerObjetivos() {
    this.vendedor.rut = await this.vendedor.rutEmpleado;

    this.listaObjetivos = await this.dataLocal.getItem('listaObjetivos');
 


    return this.listaObjetivos;
  }

  /**
   *permite obtener un cliente
   *
   * @param {*} evento
   * @memberof ObjetivosTabPage
   */
  async obtenerCliente(evento) {
    let respuesta = await this.clienteService.obtenerCliente(evento);
 
    await this.dataLocal.setCliente(respuesta[0]);
    await this.navCtrl.navigateForward(['/detalle-cliente']);

  }

  /**
   *permite cerrar la aplicacion
   * @memberof ObjetivosTabPage
   */
  cerrarSesion() {
    this.authService.logout();
  }
  obtenerMes(fecha) {
    let date = new Date(fecha);
    let primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
    let ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    let anio = date.getFullYear();

    let inicio = new Date(primerDia.toDateString() + -+ anio.toString());
    let termino = new Date(ultimoDia.toDateString() + -+ anio.toString());
    let fechas = {
      inicio: new Date(),
      termino: termino
    }

    return fechas;
  }

}

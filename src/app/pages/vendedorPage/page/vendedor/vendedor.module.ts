import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VendedorPage } from './vendedor.page';
import { Ng2Rut } from 'ng2-rut';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { PopoverFiltroZonasComponent } from 'src/app/components/popover-filtro-zonas/popover-filtro-zonas.component';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: VendedorPage
  }
];

@NgModule({
  imports: [
    Ng2Rut,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    PipesModule,
    ComponentsModule,
  ],
  exports:[VendedorPage],
  declarations: [VendedorPage],
  entryComponents: [ PopoverFiltroZonasComponent ],
})
export class VendedorPageModule {}

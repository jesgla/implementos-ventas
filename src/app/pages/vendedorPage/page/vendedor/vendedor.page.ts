import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { PopoverController } from '@ionic/angular';
import { PopoverFiltroZonasComponent } from 'src/app/components/popover-filtro-zonas/popover-filtro-zonas.component';

// servicios
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-vendedor',
  templateUrl: './vendedor.page.html',
  styleUrls: ['./vendedor.page.scss'],
})
export class VendedorPage implements OnInit {
  
  lstVendedores : any [];
  listVendedoresRespaldo: any[];
  obtener: boolean;
  textoBuscar: string;
  zonas: any[] = [];
  nombreZona: string = 'TODAS';
  mostrarFiltroZona: boolean = false;

  constructor(
    private vendedorService : VendedorService,
     private dataLocal : DataLocalService,
     public popoverController: PopoverController,
     private _UsuarioService: UsuarioService,
     private router : Router) { 
    this.lstVendedores =[];
    this.textoBuscar='';
  }
  ngOnInit(){
  
  }
 /**
   *Metodo que se ejecuta cada vez que se llama el componente
   *
   * @memberof ClientesTabPage
   */
  async ionViewWillEnter() {
 
    this.obtener = true;
    
    this.lstVendedores = await this.vendedorService.obtenerVendedores();
    this.zonas = [];
    let zonasVT: string[] = [];

    this.lstVendedores.map(vt=>{
      if(vt){
        let arr = vt.nombreCompleto.split(' ');
        vt.nombreCompleto = `${arr[2]} ${arr[3]?arr[3]:''} ${arr[0]} ${arr[1]} `;
        vt.Vendedorzona? zonasVT.push(vt.Vendedorzona) : null;
      }
    })
    zonasVT = Array.from(new Set(zonasVT));
    zonasVT.unshift('TODAS');

    zonasVT.forEach(zona => {
      this.zonas.push({nombre: zona , selected: false});
    })
    this.listVendedoresRespaldo = this.lstVendedores;

    this.obtener = false;

  }
  async verResumen(vendedor: Vendedor){
    await this.dataLocal.eliminarDatos('vendedor');
    
    await this.dataLocal.setItem('vendedor',vendedor);
    await this._UsuarioService.reloadUser();
    await this.router.navigate(['detalle-vendedor'])
  }
  /**
   *permite buscar cliente 
   * @param {*} event
   * @memberof ClientesPage
   */
  buscar( event ) {
    this.textoBuscar = event.detail.value;
  }

  filtrarZonales(zona){
    this.lstVendedores = zona != 'TODAS' ? this.listVendedoresRespaldo.filter(vendedor => vendedor.Vendedorzona === zona) : this.listVendedoresRespaldo;
    window.scrollTo({ top: 0});
    this.nombreZona = zona;
  }
  
  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverFiltroZonasComponent,
      cssClass: 'zonas-popover',
      event: ev,
      componentProps: {
        zonas: this.zonas
      },
      animated: true,
      mode: 'ios',
      translucent: true,
    });
    
    popover.onDidDismiss().then((data) => {
      if(data && data.data && data.data.length > 0)  this.filtrarZonales(data.data);
    });

    return await popover.present();
  }
}



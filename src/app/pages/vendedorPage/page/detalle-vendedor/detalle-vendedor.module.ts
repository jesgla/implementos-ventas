import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetalleVendedorPage } from './detalle-vendedor.page';
import { VendedorComponentsModule } from '../../components/vendedor-components.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: DetalleVendedorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    VendedorComponentsModule,
    PipesModule
  ],
  declarations: [DetalleVendedorPage]
})
export class DetalleVendedorPageModule {}

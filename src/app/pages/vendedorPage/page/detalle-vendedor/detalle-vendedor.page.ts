import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { IonSlides, ToastController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';

@Component({
  selector: 'app-detalle-vendedor',
  templateUrl: './detalle-vendedor.page.html',
  styleUrls: ['./detalle-vendedor.page.scss'],
})
export class DetalleVendedorPage implements OnInit, AfterViewInit {


  /**
   *elemento slider
   * @type {IonSlides}
   * @memberof DetalleClientePage
   */
  @ViewChild(IonSlides, { static: false }) slidesVendedor: IonSlides;

  /**
   *color de elemento
   * @type {*}
   * @memberof DetalleClientePage
   */
  color: any;

  /**
   *arreglo de paginas de slider
   * @memberof DetalleClientePage
   */
  paginas = [
    { svg: 'person', index: 0 },
    { svg: 'stats', index: 1 },
    { svg: 'cube', index: 2 },
  ];
 
  /**
   *configuracion de slider
   * @memberof DetalleClientePage
   */
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  vendedor: Vendedor;
  cambio: boolean;
  detalle: any;
  grafico: any;
  restaMes: number;
  indiceActual: number = 0;

  /**
   *Creates an instance of DetalleClientePage.
   * @param {ClienteService} clienteService
   * @param {ToastController} toastController
   * @param {ActivatedRoute} route
   * @param {DataLocalService} dataLocal
   * @memberof DetalleClientePage
   */
  constructor(
    public toastController: ToastController,
    private dataLocal: DataLocalService,
    private vendedorService : VendedorService
  ) {
    this.vendedor = new Vendedor();
    this.detalle=null;
    this.grafico=null;
  }

  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof DetalleClientePage
   */
  async ngOnInit() {
    this.vendedor=await this.dataLocal.getItem('vendedor');
    this.vendedor.rut = await this.vendedor.rutEmpleado;
    let listaObjetivos = await this.vendedorService.obtenerObjetivos(this.vendedor);
   
    this.cambio = true;
    await this.dataLocal.setItem('listaObjetivos',listaObjetivos);
    this.grafico = await this.vendedorService.obtenerEstadisticaVendedor(this.vendedor.rut,6,'M',new Date().toDateString());
    this.restaMes = 6;
   
    this.detalle =await  this.vendedorService.obtenerDetalleVendedor(this.vendedor.rut,new Date().toDateString());
   

    

  
  }

  /**
   *metodo que se ejecuta despues de iniciar la vista
   * @memberof DetalleClientePage
   */
  ngAfterViewInit() {
    this.slidesVendedor.lockSwipes(true);
    this.indiceActual = 0;
  }

  /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(event, slide) {
    this.indiceActual = event.detail.value.index;    
    slide.lockSwipes(false);
    slide.slideTo(event.detail.value.index);
    slide.lockSwipes(true);

  }

}

//angular
import { Component, OnInit, OnChanges, SimpleChanges, EventEmitter, OnDestroy} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//ionic
import { Geolocation, GeolocationOptions, Geoposition} from '@ionic-native/geolocation/ngx';
import { ModalController } from '@ionic/angular';

//Modals 
import { ComentarioPage } from 'src/app/pages/Modals/comentario/comentario.page';
import { CotizacionPage } from 'src/app/pages/Modals/cotizacion/cotizacion.page';
import { TareaPage } from 'src/app/pages/Modals/tarea/tarea.page';
import { OportunidadPage } from 'src/app/pages/Modals/oportunidad/oportunidad.page';

//services
// import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { EventoService } from 'src/app/services/evento/evento.service';
import { PluginService } from 'src/app/services/plugin/plugin.service';
import { CotizacionService } from 'src/app/services/evento/cotizacion.service';
import { CarroService } from 'src/app/services/carro/carro.service'
import { LoadingService } from 'src/app/services/loading/loading.service';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { OportunidadesService } from 'src/app/services/oportunidades/oportunidades.service';

//interfaces
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { Evento } from 'src/app/interfaces/evento/evento';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import {Oportunidad } from 'src/app/interfaces/oportunidades/oportunidades'

/**
 * Componente que permite registrar una visita a un cliente
 * @export
 * @class RegistroVisitaPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-registro-visita',
  templateUrl: './registro-visita.page.html',
  styleUrls: ['./registro-visita.page.scss'],
})


export class RegistroVisitaPage implements OnInit, OnDestroy{

  vendedor: Vendedor;
  registrarCheckIn: Evento;
  detalleCliente: Cliente;
  comentarioEmitido: string;
  cotizacionEmitida: any;
  origenCheckIn: string;

  oportunidadEmitida: Oportunidad;

  eventos = [
    { icon: 'briefcase', titulo: 'Oportunidad'},
    { icon: 'chatbubbles', titulo: 'Comentario'},
    { icon: 'clipboard', titulo: 'Cotización'},
  ];


  constructor(
    public modalController: ModalController,
    private eventoService: EventoService,
    private pluginService: PluginService,
    private cotizacionService: CotizacionService,
    private loading: LoadingService,
    private carroService: CarroService,
    private route: Router,
    private geolocation: Geolocation,
    private _VendedorService: VendedorService,
    private dataLocal: DataLocalService,
    private _oportunidadesService: OportunidadesService) {
  }

  /**
   *Metodo que se ejecuta al iniciar el componente
   * @memberof RegistroVisitaPage
   */
  async ngOnInit() {
     this.detalleCliente = await this.dataLocal.getCliente(); //JSON.parse(this.route.snapshot.paramMap.get('cliente')); */

     if(!this.detalleCliente){
      this.route.navigate(['/tabs/objetivos']);
     }
  }

  /**
  *Metodo que se ejecuta al eliminar el componente
  * @memberof RegistroVisitaPage
  */
  ngOnDestroy(){
    this.origenCheckIn = null;
  } 

  /**
   *Permite abrir un modal dependiendo de la opcion que se seleccione en la vista
   * @param {*} modalEvento
   * @memberof RegistroVisitaPage
   */
  async abrirModal(modal) {
    switch(modal){
      case 'Comentario':{
        this.presentComentarioModal();
        break;
      }
      case 'Cotización':{
        this.presentCotizacionModal();
        break;
      }
      case 'Oportunidad':{
        this.presentOportunidadModal();
        break;
      }
    }
  }

  async presentComentarioModal(){
    let ComentarioEvent = new EventEmitter< any >();
    ComentarioEvent.subscribe( (resp) => {
      this.comentarioEmitido = resp;
    });

    const modal = await this.modalController.create({
      component: ComentarioPage,
      componentProps: {
        cliente: this.detalleCliente,
        checkIn: true,
        ComentarioEvent: ComentarioEvent,
        comentarioAnterior: this.comentarioEmitido ? this.comentarioEmitido : null,
      }
      });
      await modal.present();
  }

  async presentCotizacionModal(){
     // Verificamos si existe una cotizacion, con el objetivo de eliminar subscricion a la observable desde la cotizacion existente.
     if(this.cotizacionEmitida){
      this.cotizacionEmitida = null;
    }

    let carro  = await this.obtenerCarro();

    let cotizacionEvent = new EventEmitter< any >();
    cotizacionEvent.subscribe( (resp) => {
      this.cotizacionEmitida = resp;
    });

    const modal = await this.modalController.create({
      component: CotizacionPage,
      componentProps: {
        carro: carro.carro,
        total: carro.total,
        subTotal: carro.subTotal,
        iva: carro.iva,
        checkIn: true,
        cotizacionEvent: cotizacionEvent,
      }
      });
      await modal.present();
  }

  async presentOportunidadModal(){
    let oportunidadEvent = new EventEmitter< any >();
      oportunidadEvent.subscribe( (resp: Oportunidad ) => {
        this.oportunidadEmitida = resp;
      });

      let usuario = await this.dataLocal.getItem('auth-token');
      this.vendedor = JSON.parse(usuario);

      let zonaVendedor = await this._VendedorService.getZonaVendedor(this.vendedor.codBodega);

      const modal = await this.modalController.create({
        component: OportunidadPage,
        componentProps: {
          rutCliente: this.detalleCliente.rut? this.detalleCliente.rut : null,
          codEmpleado: this.vendedor.codEmpleado?  this.vendedor.codEmpleado : null,
          nombreCliente: this.detalleCliente.nombre? this.detalleCliente.nombre : null, 
          checkIn: true,
          oportunidadEvent: oportunidadEvent,
          oportunidad: this.oportunidadEmitida? this.oportunidadEmitida : null,
          rutVendedor: this.vendedor.rut,
          zonaVendedor: zonaVendedor? zonaVendedor : null,
          sucursalVendedor: this.vendedor.codBodega? this.vendedor.codBodega : null,
          vendedorNombre: this.vendedor.nombre? this.vendedor.nombre : '',
        }
        });
        await modal.present();
  }

  async checkIn() {

    await this.loading.presentLoading('Registrando Check-In..');

    this.geolocation.getCurrentPosition({ timeout: 50000, enableHighAccuracy: true }).then(async (resp) => {
      this.registrarCheckIn = new Evento();

      let usuario = await this.dataLocal.getItem('auth-token');
      let evento = await this.dataLocal.getItem('evento');

      this.vendedor = JSON.parse(usuario);

      let zona = await this._VendedorService.getZonaVendedor(this.vendedor.codBodega);

      this.registrarCheckIn.idEvento =evento? evento.idEvento:'';
      this.registrarCheckIn.latitud = resp.coords.latitude.toString();
      this.registrarCheckIn.longitud = resp.coords.longitude.toString();
      this.registrarCheckIn.tipoEvento = 'VISITA';
      this.registrarCheckIn.fechaCreacion = new Date().toString();
      this.registrarCheckIn.fechaEjecucion = new Date().toString();
      this.registrarCheckIn.fechaProgramada = "";
      this.registrarCheckIn.observaciones = "CHECK-IN";
      this.registrarCheckIn.rutEmisor = this.vendedor.rut.replace('.','').replace('.','');
      this.registrarCheckIn.rutCliente = this.detalleCliente.rut;
      this.registrarCheckIn.rutVendedor = this.vendedor.rut.replace('.','').replace('.','');
      this.registrarCheckIn.estado = "EJECUTADA";
      this.registrarCheckIn.sucursal = this.vendedor.codBodega;
      this.registrarCheckIn.comentario = this.comentarioEmitido? this.comentarioEmitido : null;
      this.registrarCheckIn.zona = zona;
      this.registrarCheckIn.nombreVendedor = this.vendedor.nombre;
      this.registrarCheckIn.fechaCreacionMongo = new Date().toString();
      this.registrarCheckIn.nombreCliente = this.detalleCliente? this.detalleCliente.nombre : '';
      this.registrarCheckIn.direccion = this.detalleCliente.direcciones.length>0? this.detalleCliente.direcciones[0].direccionCompleta:'sin direccion';
      this.registrarCheckIn.origenEvento = this.origenCheckIn;
      
      let idReferencia = await this.eventoService.generarEvento(this.registrarCheckIn);

      // Se genera el comentario y se asocia al check-in.
      await this.dataLocal.setIdReferencia(idReferencia._id); 

      // Validamos si VT ha creado una oportunidad. entonces la creamos en mongo.
      if(this.oportunidadEmitida){
        this.oportunidadEmitida.id_visita = idReferencia && idReferencia._id? idReferencia._id : null;
        this.oportunidadEmitida.origenEvento = this.registrarCheckIn.origenEvento ? this.registrarCheckIn.origenEvento : null;
        await this._oportunidadesService.setNewOportunidad(this.oportunidadEmitida);
      }
  
      // Verificamos si existe cotizacion generada desdes la pagina de check-in.
        if(this.cotizacionEmitida){

        this.cotizacionEmitida.parametros.idVisita = idReferencia && idReferencia._id? idReferencia._id : null;
        let respuesta = await this.cotizacionService.generarCotizacion(this.cotizacionEmitida.parametros);

        if(this.cotizacionEmitida && this.cotizacionEmitida.accion == 'crear'){
          this.cotizacionService.confirmarCotizacion( respuesta, this.vendedor)
        }
      } 
  
      this.dataLocal.eliminarDatos('evento'); 
      this.loading.hideLoading(); 

      this.pluginService.ejecutarSocialShare('Check-in', this.detalleCliente);

    }).catch((error) => {
      this.loading.hideLoading();
      this.dataLocal.presentToast("Ha ocurrido un error intentando obtener información desde el GPS. Favor revisar si la ubicacion se encuentra activada", 'danger', 3000)
    });
  }
  
  /**
 *Permite obtener el carro de compras almacenado en la localStorage
  * @returns
  * @memberof CarroCompraPage
  */
  async obtenerCarro() {
    let consulta: any = await this.carroService.obtenerCarro();
    return consulta;
  }
  

  getTotalCotizacion(){
    return this.cotizacionService.getTotalCotizacion(this.cotizacionEmitida.parametros.detalle)
  }

/**
* @author ignacio zapata  \"2020-11-23\
* @desc Metodo utilizado para remover una oportunidad creada.
* @params 
* @return
*/
removeOportunidad(){
  this.oportunidadEmitida = null;
  this.dataLocal.presentToast('Oportunidad borrada correctamente.', 'success');
}

setOrigenCheckIn(origen: string){
  origen ? this.origenCheckIn = origen : null;
}

}

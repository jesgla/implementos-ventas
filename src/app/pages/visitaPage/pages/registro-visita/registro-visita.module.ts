import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegistroVisitaPage } from './registro-visita.page';
import { ComentarioPage } from 'src/app/pages/Modals/comentario/comentario.page';
import { ComentarioPageModule } from 'src/app/pages/Modals/comentario/comentario.module';
import { ReAgendarPage } from 'src/app/pages/Modals/re-agendar/re-agendar.page';
import { ReAgendarPageModule } from 'src/app/pages/Modals/re-agendar/re-agendar.module';
import { TareaPage } from 'src/app/pages/Modals/tarea/tarea.page';
import { TareaPageModule } from 'src/app/pages/Modals/tarea/tarea.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

// Modals
import { CotizacionPage } from 'src/app/pages/Modals/cotizacion/cotizacion.page';
import { CotizacionPageModule } from 'src/app/pages/Modals/cotizacion/cotizacion.module';
import { OportunidadPageModule } from 'src/app/pages/Modals/oportunidad/oportunidad.module';
import { OportunidadPage } from 'src/app/pages/Modals/oportunidad/oportunidad.page'; 

const routes: Routes = [
  {
    path: '',
    component: RegistroVisitaPage
  }
];

@NgModule({
  entryComponents:[
    ComentarioPage,
    CotizacionPage,
    OportunidadPage,
    ReAgendarPage,
    TareaPage
  ],
  imports: [
    TareaPageModule,
    ReAgendarPageModule,
    CotizacionPageModule,
    ComentarioPageModule,
    OportunidadPageModule,
    CommonModule,
    PipesModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegistroVisitaPage]
})
export class RegistroVisitaPageModule {}

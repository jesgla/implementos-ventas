import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Articulo } from '../../interfaces/articulo/articulo';
import { ArticulosService } from '../../services/articulos/articulos.service';
import { Vendedor} from '../../interfaces/vendedor/vendedor';
import { Cliente } from '../../interfaces/cliente/cliente';

@Component({
  selector: 'app-propuesta',
  templateUrl: './propuesta.page.html',
  styleUrls: ['./propuesta.page.scss'],
})

export class PropuestaPage implements OnInit {

  tipoPropuesta: any[] = [
    {value: 'ofrecer', label: 'Combos'},
    {value: 'nocompra', label: 'Ya no Compra'},
    {value: 'promo', label: 'Prod. Nuevos'},        
  ];

  loadingCategoria: any[] = [1,1,1];

  items: number = 0;
  productos_recomendados: Articulo[] = [];
  productos_nocompra: Articulo[] = [];
  productos_promo: Articulo[] = [];
  vendedor: Vendedor;
  cliente: Cliente;

  slideOpts = {
    autoHeight: true
  };

  propuestaActual: string = 'ofrecer';

  constructor(private _DataLocalService:DataLocalService, private _ArticulosService: ArticulosService, public router: Router) { }

  async ngOnInit() {
    await this.obtenerCarro();
    this.vendedor = await this.obtenerUsuario();
    this.cliente = await this.obtenerCliente();
    
    if(!this.cliente){
      this.router.navigate(['tabs/clientes']);
    }
    await this.obtenerPropuesta({ rut: this.cliente.rut, sucursal: this.vendedor.codBodega, tipo: 1 });
    await this.obtenerPropuesta({ rut: this.cliente.rut, sucursal: this.vendedor.codBodega, tipo: 2 });
    await this.obtenerPropuesta({ rut: this.cliente.rut, sucursal: this.vendedor.codBodega, tipo: 3 });
  }

  async obtenerUsuario() {
    let usuario = await this._DataLocalService.getItem('auth-token');
    return JSON.parse(usuario);
  }

  async obtenerCliente(){
    return await this._DataLocalService.getCliente()
  }

  async ionViewWillEnter() {
    this.propuestaActual = 'ofrecer';
    await this.obtenerCarro();
    this.vendedor = await this.obtenerUsuario();
    this.cliente = await this.obtenerCliente();
  }

  // Metodo que se encarga de lanzar una a una las 3 consultas que obtienen los datos de cada tipo de propuesta.
  async obtenerPropuesta(parametros){
    let consulta: any = await this._ArticulosService.obtenerPropuesta(parametros);
    if(!consulta.error){
      if(parametros.tipo == 1 && consulta.data.items) this.productos_recomendados = consulta.data.items;
      if(parametros.tipo == 2 && consulta.data.items) this.productos_nocompra = consulta.data.items;
      if(parametros.tipo == 3 && consulta.data.items) this.productos_promo = consulta.data.items;
    }
    this.hideLoading(parametros.tipo);
  }

  // se encarga de ocultar el item "loading", que aparece en cada lista de items de las propuestas.
  hideLoading(cat: number){
    this.loadingCategoria[cat - 1] = 0;
  }

  // metodo con el cual se obtienen los datos del carro desde el localstorage de ionic.
  async obtenerCarro() {
    let carro = await this._DataLocalService.getCarroCompra();
    this.items = carro.length;
  }

  // Metodo lanzado cuando seleccionas un tab asociado a un tipo de propuesta
  changetipoPropuesta(event){
    window.scrollTo({ top: 0});
    this.propuestaActual  = event.detail.value;
  }

}

import { Component, OnInit } from '@angular/core';

// interfaces
import {MotivoNoEntrega} from 'src/app/interfaces/respaldoDocs/respaldo';

//services
import { UsuarioService } from './../../services/usuario/usuario.service';
import { RespaldoDocsService } from 'src/app/services/respaldoDocs/respaldo-docs.service';

@Component({
  selector: 'app-asignacion-documentos',
  templateUrl: './asignacion-documentos.page.html',
  styleUrls: ['./asignacion-documentos.page.scss'],
})
export class AsignacionDocumentosPage implements OnInit {
  index: number = 0;
  motivosNoEntrega: MotivoNoEntrega[] = [];
  motivosRecepcionParcial: MotivoNoEntrega[] = []
  tipoAcceso: number; // 0 no puede asingar - 1 solo signarse a uno mismo - 2 asignar a otros.

  paginas: any[] = [];

  constructor(private _RespaldoDocsService: RespaldoDocsService, private _UsuarioService: UsuarioService) { }

  async ngOnInit() {
    await this.valAccesoAsignarDocumento();
    await this.isAsignarVisible();
    await this.cargarMotivos();
  }

  cambioPagina(index: number){
    this.index = index;
    this.updateCheckedPag();
  }

  async cargarMotivos(){
    this.motivosNoEntrega = await this._RespaldoDocsService.getMotivoNoEntrega(2);
    this.motivosRecepcionParcial = await this._RespaldoDocsService.getMotivoNoEntrega(1);
  }

  async valAccesoAsignarDocumento(){
    // 0 no acceso, 1 asignarme y 2 asignar a mi o otros.
    if(!this._UsuarioService.getUserLoged().codPerfil) return;

    let codPerfil = this._UsuarioService.getUserLoged().codPerfil;
    this.tipoAcceso = codPerfil === 6? 0 : codPerfil === 7? 1 : codPerfil === 5?  2 : 0;
  }

  isAsignarVisible(){
    this.paginas.push({ name: 'Mis Documentos', index: 0, cheked: true });

    if(this.tipoAcceso > 0){
      this.paginas.push({ name: 'Asignar', index: 1, cheked: false });
    }
  }

  updateCheckedPag(){
    for(let page of this.paginas){
      if(this.index === page.index){
        page.cheked = true;
      }else{
        page.cheked = false;
      }
    }
  }

  reloadMisDocumentos(){
    this._RespaldoDocsService.reloadMisDocumentos();
  }

}

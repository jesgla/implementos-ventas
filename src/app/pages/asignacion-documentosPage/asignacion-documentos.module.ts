import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from './../../components/components.module';

import { AsignacionDocumentosPage } from './asignacion-documentos.page';

// components
import { AsignarComponent } from './components/asignar/asignar.component';
import { MisAsignadosComponent } from './components/mis-asignados/mis-asignados.component';


const routes: Routes = [
  {
    path: '',
    component: AsignacionDocumentosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AsignacionDocumentosPage, MisAsignadosComponent, AsignarComponent]
})
export class AsignacionDocumentosPageModule {}

import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Component, OnInit, Input } from '@angular/core';

// services
import { RespaldoDocsService } from 'src/app/services/respaldoDocs/respaldo-docs.service';
import { LoadingService } from './../../../../services/loading/loading.service';
import { DataLocalService } from './../../../../services/dataLocal/data-local.service';

// interfaces
import { DocumentoRespaldo, Transportista, Respaldo } from 'src/app/interfaces/respaldoDocs/respaldo';

@Component({
  selector: 'app-asignar',
  templateUrl: './asignar.component.html',
  styleUrls: ['./asignar.component.scss'],
})
export class AsignarComponent implements OnInit {
  @Input() tipoAcceso: number;

  tipoDocumento: string = 'GDEL';
  documento: DocumentoRespaldo;
  transportistas: Transportista[] = [];
  transportistasFiltered: Transportista[] = [];
  isItemAvailable = false;
  searchingTransportista: boolean = false;
  selectedTransportista: number;
  selectedTransportistaNombre: string;

  constructor(
    private _RespaldoDocsService: RespaldoDocsService,
    private _dataLocal: DataLocalService,
    private _LoadingService: LoadingService,
    private _UsuarioService: UsuarioService) { }

  ngOnInit() {
    this.getTransportistas("");
  }

  async getDocumentDetails(folio: any){
    if(!folio){
      this._dataLocal.presentToast('Debe ingresar el número de folio', 'warning'); 
      return;
    } 

    await this._LoadingService.presentLoading(`Buscando ${this.tipoDocumento}`);
    let documento = await this._RespaldoDocsService.getDocumentoDisponibleDetails(this.tipoDocumento, folio);

    if(!documento){
      this._LoadingService.hideLoading();
      this._dataLocal.presentToast('No se encontró el documento. Verifíque los datos ingresados', 'warning', 2100);
      return;
    }

    if(documento.intEstadoDocumento == 1){
      this.documento = documento;
      this.tipoAcceso === 1? this.selectedTransportista = this._UsuarioService.getUserLoged().codEmpleado : null;
      this.tipoAcceso === 1? this.selectedTransportistaNombre = this._UsuarioService.getUserLoged().nombre : null;
      this._LoadingService.hideLoading();
      return;
    }

    this._LoadingService.hideLoading();
    this._dataLocal.presentToast('El documento ya se encuentra asignado a: \n' + documento.strNombreConductor, 'warning', 2100);
  }

  async getTransportistas(texto: string){
    this.transportistas = await this._RespaldoDocsService.getTransportistas(texto);
  }

  autoCompleteTransportista(ev: any) {
    this.transportistasFiltered = [];
    const val = ev.target.value;

    this.searchingTransportista = val && val.length > 0?  true : false;

    if (val && val.trim() !== '') {
        this.isItemAvailable = true;
        this.transportistasFiltered = this.transportistas.filter((item: Transportista) => {
            return (item.strNombreUsuario.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
    } else {
        this.isItemAvailable = false;
    }
  }

  setTransportista(transportista: Transportista){
    this.searchingTransportista = false;
    this.selectedTransportistaNombre = transportista.strNombreUsuario;
    this.selectedTransportista = transportista.intCodEmpleado;
  }

  removeSelectedTransportista(){
    this.isItemAvailable = false;
    this.transportistasFiltered = [];
    this.selectedTransportista = null;
    this.selectedTransportistaNombre = null;
  }

  cancelAction(){
    console.log('tipoacceso', this.tipoAcceso);
    if((!this.selectedTransportista && this.documento) || this.tipoAcceso === 1){
      this.documento = null;
      return;
    }

    if(this.selectedTransportista && this.documento){
      this.selectedTransportista = null;
      return;
    }
  }

  async asignarDocumento(){
    if(!this.selectedTransportista){
      this._dataLocal.presentToast('Debe seleccionar un transportista para asignar el documento', 'danger',2000);
      return;
    }

    if(!this.documento){
      this._dataLocal.presentToast('no existe documento a asignar.','danger', 2000);
      return;
    }

    await this._LoadingService.presentLoading('Asignando Documento..');

    let dataDocumento: Respaldo = {
			intRegistro: this.documento.intRegistro,
			intConductor: this.selectedTransportista,
			intEstadoDocumento: 2,
			strTipoDocumento: this.tipoDocumento,
			intPagoCaja: 0
		}

    let respuesta: boolean = await this._RespaldoDocsService.saveRespaldo(dataDocumento, 'asignado');
    this.resetAsignacion();
    this._LoadingService.hideLoading();
  }

  resetAsignacion(){
    this.documento = null;
    this.removeSelectedTransportista();
  }

}

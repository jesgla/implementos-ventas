import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Subscription } from 'rxjs';

//servicios
import { RespaldoDocsService } from 'src/app/services/respaldoDocs/respaldo-docs.service';
import { UsuarioService } from './../../../../services/usuario/usuario.service';
import { LoadingService } from './../../../../services/loading/loading.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

// interface
import { DocumentoRespaldo, Respaldo, MotivoNoEntrega} from 'src/app/interfaces/respaldoDocs/respaldo';

@Component({
  selector: 'app-mis-asignados',
  templateUrl: './mis-asignados.component.html',
  styleUrls: ['./mis-asignados.component.scss'],
})
export class MisAsignadosComponent implements OnInit, OnDestroy {
  @Input() motivosNoEntrega: MotivoNoEntrega[] = [];
  @Input() motivosRecepcionParcial: MotivoNoEntrega[] = [];

  misDocumentos: DocumentoRespaldo[];
  docCargados: boolean = false;
  selectedDocument: DocumentoRespaldo;
  MotivosAusar: MotivoNoEntrega[] = [];
  finalizandoRuta: boolean = false;
  resultadoEntrega: string;
  img64: any[];
  observacion: string;
  motivoId: number;
  tipoEntregaId: number;
  gettingImg: boolean = false;
  respaldando: boolean = false;
  reloadSubcription: Subscription;
  
  constructor(
    private _RespaldoDocsService: RespaldoDocsService,
    private _usuarioService: UsuarioService,
    private _LoadingService: LoadingService,
    public alertController: AlertController,
    private _DataLocalService: DataLocalService) { 

    this.reloadSubcription = this._RespaldoDocsService.misDocumentosReload$.subscribe(() => {
      this.docCargados = false;
      this.getMisDocumentos();
    })
  }

  ngOnInit() {
    this.getMisDocumentos();
  }

  ngOnDestroy(){
    this.reloadSubcription? this.reloadSubcription.unsubscribe() : null;
  }

  async getMisDocumentos(){
    let codUsuario = await this._usuarioService.getUserLoged().codEmpleado;
    this.misDocumentos = await this._RespaldoDocsService.getMisDocumentos(codUsuario);
    this.docCargados = true;
  }

  setSelectedDocument(documento: DocumentoRespaldo){
    this.selectedDocument = documento;
  }

  async confirmarSalida(){
    await this._LoadingService.presentLoading('Cambiando estado del documento.');
    let codEmpleado = await this._usuarioService.getUserLoged().codEmpleado;

    let data: Respaldo = {
			intEstadoDocumento : 3,
			intRegistro: this.selectedDocument.intRegistro,
			intConductor: codEmpleado,
			intPagoCaja: 0,
			strTipoDocumento: this.selectedDocument.strTipoDoc,
		}

    let consulta = await this._RespaldoDocsService.saveRespaldo(data, 'confirmado en ruta');
    if(consulta){
      this.selectedDocument.intEstadoDocumento = 3;
      this.reloadSelectedDocument();
    }
    this._LoadingService.hideLoading();
  }

  async presentAlertConfirmarEntrega() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Ruta Finalizada?',
      inputs: [
        {
          type: 'radio',
          label: 'Entregado',
          value: 'entregado'
        },
        {
          type: 'radio',
          label: 'No Entregado',
          value: 'motivosNoEntrega'
        },
        {
          type: 'radio',
          label: 'Entrega parcial',
          value: 'motivosRecepcionParcial'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            
          }
        }, {
          text: 'Aceptar',
          handler: (resultadoEntrega) => {
            this.finalizandoRuta = true;
            this.ShowMotivos(resultadoEntrega);
          }
        }
      ]
    });

    await alert.present();
  }

  ShowMotivos(resultadoEntrega: string){
    this.resultadoEntrega = resultadoEntrega;
    if(resultadoEntrega === 'entregado'){
      this.tipoEntregaId = 4;
      this.tomarImg();
      return;
    }

    if(this.resultadoEntrega === 'motivosRecepcionParcial') this.tipoEntregaId = 5;
    if(this.resultadoEntrega === 'motivosNoEntrega') this.tipoEntregaId = 6;


    this.MotivosAusar = eval('this.'+this.resultadoEntrega);
  }

  setMotivosResultadoEntrega(motivo: any,){
      motivo? this.motivoId = motivo : '';
  }

  setObservacion(observacion: string){
    this.observacion = observacion;
  }

  backtoMisDocumentos(){
    this.cancelFinalizarRuta();
    this.selectedDocument = null;
  }

  cancelFinalizarRuta(){
    this.finalizandoRuta = false;
    this.resultadoEntrega = null;
    this.MotivosAusar = [];
    this.motivoId = null;
    this.observacion = null;
    this.img64 = null;
    this.tipoEntregaId = null;
    this.gettingImg = false;
    this.respaldando = false;
  }

  valListoParaGuardar(){
    if(this.resultadoEntrega == 'entregado' && this.img64) return true;
    if(this.resultadoEntrega != 'entregado' && this.motivoId > 0 && (this.observacion && this.observacion.length > 0)) return true;

    return false;
  }

  tomarImg(){
    this.gettingImg = true;
  }

  setImage(image: any){
    image? this.img64 = image.imageData : null;
    this.gettingImg = false;
  }

  async guardarEntregado(){
    let codEmpleado = await this._usuarioService.getUserLoged().codEmpleado;
    
    if(codEmpleado){

      await this._LoadingService.presentLoading('Finalizando Ruta..');

      let respaldo: Respaldo = {
        intRegistro : this.selectedDocument.intRegistro,
        intConductor: codEmpleado,
        strTipoDocumento: this.selectedDocument.strTipoDoc,
        intPagoCaja: 0,
        intEstadoDocumento: this.tipoEntregaId,
      }
  
      if(this.tipoEntregaId === 4){
        if(!this.img64 || this.img64.length < 1){
          this._DataLocalService.presentToast('Falta agregar la imagen', 'danger', 2000);
          return;
        }
  
        respaldo = {
          ...respaldo,
          strImagen64: this.img64        
        }
  
      }
  
      if(this.tipoEntregaId === 5 || this.tipoEntregaId === 6){
        if(!this.motivoId || !this.observacion){
          this._DataLocalService.presentToast('Falta agregar motivo o observacion', 'danger', 2000);
          return;
        }
  
        respaldo = {
          ...respaldo,
          strObservacion: this.observacion,
          intCodMotivo: this.motivoId,    
        }
      }
      let consulta = await this._RespaldoDocsService.saveRespaldo(respaldo);
      if(consulta){
        this.respaldando = true;
        await this.reloadSelectedDocument();
      }
    }
    this._LoadingService.hideLoading();
  }

  async reloadSelectedDocument(){
    this.selectedDocument = await this._RespaldoDocsService.getDocumentoDisponibleDetails(this.selectedDocument.strTipoDoc, Number(this.selectedDocument.strFolioDoc));
    await this.getMisDocumentos();
    this.cancelFinalizarRuta();
  }
  
}

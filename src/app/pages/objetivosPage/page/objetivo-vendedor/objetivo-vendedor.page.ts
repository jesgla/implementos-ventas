import { Component, OnInit, Input } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { Objetivos } from 'src/app/interfaces/vendedor/objetivos';
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';
import { Evento } from 'src/app/interfaces/cliente/evento';
import { EventoService } from 'src/app/services/evento/evento.service';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { SupervisorService } from 'src/app/services/supervisor/supervisor.service';

@Component({
  selector: 'app-objetivo-vendedor',
  templateUrl: './objetivo-vendedor.page.html',
  styleUrls: ['./objetivo-vendedor.page.scss'],
})
export class ObjetivoVendedorPage{

    /**
     *arreglo de objetivos
     * @type {Objetivos[]}
     * @memberof ObjetivosTabPage
     */
    @Input() listaObjetivos: Objetivos[];

    /**
     *objeto de objetivos vendedor
     * @type {Objetivos}
     * @memberof ObjetivosTabPage
     */
    objetivos: Objetivos;

    /**
     *fecha actual
     * @type {string}
     * @memberof ObjetivosTabPage
     */
    fecha: string;

    /**
     *menta del mes 
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    metaMes: number;

    /**
     *meta que debe llevar hasta hoy
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    metaAcumulada: number;

    /**
     *procentaje de meta mes
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    porcentajeMetaMes: number = 0;

    /**
     *venta del dia
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    ventaDia: number;

    /**
     *venta acumulada del dia
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    ventaDiaAcumulado: number;

    /**
     *porcentaje de venta dia
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    porcentajeDia: number;

    /**
     *porcentaje de meta mes 
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    porcentajeMetaMesGrafico: number;

    /**
     *visita a clientes
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    visitaConcretadas: number;

    /**
     *visitas agendadas
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    visitasAgendadas: number;

    /**
     *porcentaje de visitas
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    porcentajeVisita: number;

    /**
     *venta del dia acumulada
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    ventaDiaAcumulada: number;

    /**
     *arreglo de eventos
     * @type {Evento[]}
     * @memberof ObjetivosTabPage
     */
     eventos: Evento[];

    /**
     *objeto vendedor
     *
     * @type {Vendedor}
     * @memberof ObjetivosTabPage
     */
    vendedor: Vendedor;

    /**
     *meta venta de hoy
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    mentaHoy: number;

    /**
     *venta acumulada hasta hoy
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    ventaAcumulada: number;
    /**
     *porcentaje de ventas
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    ventaAcumuladaProcentaje: number;

    /**
     *porcentaje venta 
     * @type {number}
     * @memberof ObjetivosTabPage
     */
    ventaDiaPorcentaje: number;

    tiendaMetaAcumulada : number = 0;
    tiendaVentaDia: number = 0;
    tiendaVentaPorcentaje: number = 0;


    /**
     *Creates an instance of ObjetivosTabPage.
     * @param {VendedorService} vendedorService
     * @param {LoadingController} loadingController
     * @param {AutentificacionService} authService
     * @param {DataLocalService} dataLocal
     * @param {EventoService} eventoService
     * @param {ClienteService} clienteService
     * @param {NavController} navCtrl
     * @memberof ObjetivosTabPage
     */
    constructor(
        private vendedorService: VendedorService,
        public loadingController: LoadingController,
        private authService: AutentificacionService,
        private dataLocal: DataLocalService,
        private eventoService: EventoService,
        private clienteService: ClienteService,
        private navCtrl: NavController,
        private _SupervisorService: SupervisorService,
    ) {
        this.vendedor = new Vendedor();
        this.eventos = [];
    }
    /**
     *objeto grafico
     * @memberof ObjetivosTabPage
     */
    option = {
        series: [
            {
                name: 'Mentas Vendedor',
                type: 'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: true,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '10',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: true
                    }
                },
                data: [
                    { value: 100, name: '', itemStyle: { color: '#7eb9f5' }, show: true },
                    { value: this.porcentajeMetaMes * 100, name: '', itemStyle: { color: '#005db9' }, show: true },
                ]
            }
        ]
    };

    /**
     *metodo que se ejecuta al llamar el componente
     * @memberof ObjetivosTabPage
     */
    async ionViewWillEnter() {

        await this.obtenerUsuario();
        this.getMetaTiendas();

        await this.obtenerObjetivos();
        await this.obtenerValores();
    
        let fechas = await this.obtenerMes(new Date());
        this.eventos = await this.eventoService.obtenerEventosCliente(this.vendedor,fechas,'PROGRAMADA');
      
        this.fecha = await new Date().toDateString();
    }
   

    /**
     *permite obtener al usuario logueado
     * @memberof ObjetivosTabPage
     */
    async obtenerUsuario() {
        let usuario = await this.dataLocal.getItem('auth-token');

        this.vendedor = JSON.parse(usuario);
  
    }

    /**
     *permite obtener los objetivos del vendedor
     * @memberof ObjetivosTabPage
     */
    async obtenerValores() {

        let dia = new Date().getDate();

        this.metaMes = await this.listaObjetivos[0].Meta;

        this.metaAcumulada = this.listaObjetivos[2].MetaAcumulada;
        this.ventaAcumulada = this.listaObjetivos[0].Venta;
        this.ventaAcumuladaProcentaje = this.ventaAcumulada / this.metaAcumulada;
        this.porcentajeMetaMesGrafico = Math.round(this.ventaAcumuladaProcentaje * 100);
        
        this.porcentajeMetaMesGrafico =  this.porcentajeMetaMesGrafico && isFinite(this.porcentajeMetaMesGrafico)? this.porcentajeMetaMesGrafico : 0;
     

        this.ventaAcumulada = Number((this.ventaAcumulada / 1000000).toFixed(1));
        this.metaAcumulada = Number((this.metaAcumulada / 1000000).toFixed(1));

        this.mentaHoy = this.listaObjetivos[2].Meta;
        this.ventaDia = this.listaObjetivos[2].Venta;
        this.ventaDiaPorcentaje = this.ventaDia / this.mentaHoy;
        this.mentaHoy = Number((this.mentaHoy / 1000000).toFixed(1));
        this.ventaDia = Number((this.ventaDia / 1000000).toFixed(1));
        this.visitaConcretadas = this.listaObjetivos[0].VisitasConfirmadas;
        this.visitasAgendadas = this.listaObjetivos[0].VisitasCreadas;
        this.porcentajeVisita = this.visitaConcretadas / this.visitasAgendadas;

        this.option = {
            series: [
                {
                    name: 'Mentas Vendedor',
                    type: 'pie',
                    radius: ['50%', '70%'],
                    avoidLabelOverlap: true,
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '10',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: true
                        }
                    },
                    data: [
                        { value: 100, name: '', itemStyle: { color: '#7eb9f5' }, show: true },
                        { value: this.porcentajeMetaMesGrafico, name: '', itemStyle: { color: '#005db9' }, show: true },
                    ]
                }
            ]
        };
    }

    /**
     *permite consultar los objetivos mensuales
     * @returns
     * @memberof ObjetivosTabPage
     */
    async obtenerObjetivos() {
        const loading = await this.loadingController.create({
            message: 'Cargando...'
        });
        await loading.present();
        this.listaObjetivos = await this.vendedorService.obtenerObjetivos(this.vendedor);

        await loading.dismiss();

        return this.listaObjetivos;
    }

    /**
     *permite obtener un cliente
     *
     * @param {*} evento
     * @memberof ObjetivosTabPage
     */
    async obtenerCliente(evento) {
        let respuesta = await this.clienteService.obtenerCliente(evento);
        await this.dataLocal.setItem('evento',evento);
        await this.dataLocal.setCliente(respuesta[0]);
        await this.navCtrl.navigateForward(['/detalle-cliente']);

    }

    /**
     *permite cerrar la aplicacion
     * @memberof ObjetivosTabPage
     */
    cerrarSesion() {
      this.authService.logout();
  }
  obtenerMes(fecha) {
    let date = new Date(fecha);
    let primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
    let ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    let anio = date.getFullYear();

    let inicio = new Date(primerDia.toDateString() + -+ anio.toString());
    let termino = new Date(ultimoDia.toDateString() + -+ anio.toString());
    let fechas = {
      inicio: new Date(),
      termino: termino,
    }
  
    return fechas;
  }

    /**
    * @author ignacio zapata  \"2020-09-16\
    * @desc  metodo utilizado para obtener las ventas y meta de la tienda asociada al vendedor.
    * @params 
    * @return
    */
    async getMetaTiendas(){
        if(this.vendedor.codBodega && this.vendedor.codBodega.length > 0){
            let zona = await this.vendedorService.getZonaVendedor(this.vendedor.codBodega);

            let consulta = await this._SupervisorService.obtenerZonas(zona, 'SUCURSAL');
            let sucursal = consulta.filter( sucursal => sucursal.nombreVendedor === this.vendedor.codSucursal);

            if(sucursal[0]){
                this.tiendaMetaAcumulada = Number((sucursal[0].MetaAcumulada / 1000000).toFixed(1));
                this.tiendaVentaDia = Number((sucursal[0].Venta / 1000000).toFixed(1)); 

                let porcentajeD =Math.trunc( this.tiendaVentaDia / this.tiendaMetaAcumulada * 100);
        
                if (isFinite(porcentajeD) && !isNaN(porcentajeD)) {
                this.tiendaVentaPorcentaje = ( porcentajeD / 100);
                
                }else{
                    this.tiendaVentaPorcentaje = 0;
                }        
            }     
        }
    }

}

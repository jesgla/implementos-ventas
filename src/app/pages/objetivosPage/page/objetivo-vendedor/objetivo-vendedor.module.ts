import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ObjetivoVendedorPage } from './objetivo-vendedor.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { NgxEchartsModule } from 'ngx-echarts';

const routes: Routes = [
  {
    path: '',
    component: ObjetivoVendedorPage
  }
];
@NgModule({
  imports: [
    ComponentsModule,
    PipesModule,
    NgxEchartsModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    IonicModule
  ],
  exports:[ObjetivoVendedorPage],
  declarations: [ObjetivoVendedorPage]
})
export class ObjetivoVendedorPageModule {}

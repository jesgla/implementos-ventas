import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ObjetivoAdministradorPage } from './objetivo-administrador.page';

const routes: Routes = [
  {
    path: '',
    component: ObjetivoAdministradorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,

  ],
  exports:[ObjetivoAdministradorPage],
  declarations: [ObjetivoAdministradorPage]
})
export class ObjetivoAdministradorPageModule {}

import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { SupervisorService } from 'src/app/services/supervisor/supervisor.service';
import { Objetivos } from 'src/app/interfaces/vendedor/objetivos';

@Component({
  selector: 'app-objetivo-administrador',
  templateUrl: './objetivo-administrador.page.html',
  styleUrls: ['./objetivo-administrador.page.scss'],
})
export class ObjetivoAdministradorPage implements  AfterViewInit {
  listaObjetivos: Objetivos[];
  /**
   *elemento slider
   * @type {IonSlides}
   * @memberof DetalleClientePage
   */
  @ViewChild(IonSlides, { static: false }) slidesAdmin: IonSlides;

  /**
   *color de elemento
   * @type {*}
   * @memberof DetalleClientePage
   */
  color: any;

  /**
   *arreglo de paginas de slider
   * @memberof DetalleClientePage
   */
  paginas = [
    { svg: './assets/svg/ubicacion.svg', index: 0, cheked: true },
    { svg: './assets/svg/tiendas.svg', index: 1, disabled: true, cheked: false },
    { svg: './assets/svg/canal.svg', index: 2, disabled: true, cheked: false },
    { svg: './assets/svg/vendedor.svg', index: 3, disabled: true, cheked: false },

  ];

  /**
   *configuracion de slider
   * @memberof DetalleClientePage
   */
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  indice: number;
  totalVenta: number;
  totalMeta: number;
  promedio: number;
  sucursales: Objetivos[];
  canales: Objetivos[];
  vendedores: Objetivos[];
  totalVentaSuc: number;
  totalMetaSuc: number;
  promedioSuc: number;
  totalVentaCan: number;
  totalMetaCan: number;
  promedioCan: number;
  promedioVen: number;
  totalMetaVen: number;
  totalVentaVen: number;

  constructor(private supervisorService: SupervisorService) {
    this.indice = 1;
    this.totalVenta = 0;
    this.totalMeta = 0;
    this.promedio = 0;
    this.totalVentaSuc = 0
    this.totalMetaSuc = 0
    this.promedioSuc = 0
    this.totalVentaCan = 0
    this.totalMetaCan = 0
    this.promedioCan = 0
    this.totalVentaVen=0
    this.totalMetaVen=0
    this.promedioVen=0

  }
 /**
     *metodo que se ejecuta al llamar el componente
     * @memberof ObjetivosTabPage
     */
    async ionViewWillEnter() {
    

    this.listaObjetivos = await this.supervisorService.obtenerZonas('', 'ZONA');
    this.listaObjetivos.map(async (zona) => {
      this.totalVenta += zona.Venta;
      this.totalMeta += zona.MetaAcumulada;
      this.promedio = this.totalVenta / this.totalMeta * 100;
    })
  
    }
  /* async ngOnInit() {
    
  } */
 

  async habilitar(indice, slide, zona, tipo) {

    switch (tipo) {
      case 'SUCURSAL':
        this.sucursales = await this.supervisorService.obtenerZonas(zona.nombreVendedor, tipo)
        this.sucursales.map(async (zona) => {
          this.totalVentaSuc += zona.Venta;
          this.totalMetaSuc += zona.MetaAcumulada;
          this.promedioSuc = this.totalVentaSuc / this.totalMetaSuc * 100;
        })
        break;
      case 'CANAL':
        this.canales = await this.supervisorService.obtenerZonas(zona.nombreVendedor, tipo)
        this.canales.map(async (zona) => {
          this.totalVentaCan += zona.Venta;
          this.totalMetaCan += zona.MetaAcumulada;
          this.promedioCan = this.totalVentaCan / this.totalMetaCan * 100;
        })
        break;
      case 'VENDEDOR':
        this.vendedores = await this.supervisorService.obtenerZonas(zona.nombreVendedor, tipo);
        this.vendedores.map(async (zona) => {
          this.totalVentaVen += zona.Venta;
          this.totalMetaVen += zona.MetaAcumulada;
          this.promedioVen = this.totalVentaVen / this.totalMetaVen * 100;
        })
        break;




    }

    this.paginas[indice].disabled = false;
    this.paginas[indice].cheked = true;
    let index = this.paginas[indice].index;
    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false : null
    })
    slide.lockSwipes(false);
    slide.slideTo(indice);
    slide.lockSwipeToNext(true);
    slide.lockSwipes(true);
  }
  /**
    *metodo que se ejecuta despues de iniciar la vista
    * @memberof DetalleClientePage
    */
  ngAfterViewInit() {
    this.slidesAdmin.lockSwipes(true);

  }

  /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(event, slide) {
    slide.lockSwipes(false);
    slide.slideTo(event.detail.value.index);
    slide.lockSwipes(true);

  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArticulosPage } from './articulos.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { DetalleArticuloPageModule } from 'src/app/pages/Modals/detalle-articulo/detalle-articulo.module';
import { DetalleArticuloPage } from 'src/app/pages/Modals/detalle-articulo/detalle-articulo.page';
import { ComponentsModule } from '../../../../components/components.module'

const routes: Routes = [
  {
    path: '',
    component: ArticulosPage
  }
];

@NgModule({
  entryComponents:[DetalleArticuloPage],
  imports: [
    DetalleArticuloPageModule,
    PipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  exports:[ArticulosPage],
  declarations: [ArticulosPage]
})
export class ArticulosPageModule {}

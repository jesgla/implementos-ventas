//angular 
import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

//ionic
import { ModalController, AlertController, NavController, Events } from '@ionic/angular';

//components
import { DetalleArticuloPage } from 'src/app/pages/Modals/detalle-articulo/detalle-articulo.page';

//services
import { ArticulosService } from 'src/app/services/articulos/articulos.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { CategoriasArticulosService } from 'src/app/services/categoriasArticulos/categorias-articulos.service';

//interfaces
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { Articulo } from 'src/app/interfaces/articulo/articulo';
/**
 * Permite vizualizar articulos 
 * @export
 * @class ArticulosPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.page.html',
  styleUrls: ['./articulos.page.scss'],
})


export class ArticulosPage implements OnInit, OnDestroy {

  /**
   * variable que permite saber si obtuvo el listado de articulos
   * @type {boolean}
   * @memberof ArticulosPage
   */
  obtener: boolean;
  busqueda: string;
  
  /**
   * Listado de articulos 
   * @type {Articulo}
   * @memberof ArticulosPage
   */
  listadoArticulo: Articulo[] = [];

  /**
   * Objeto de vendedor 
   * @type {Vendedor}
   * @memberof ArticulosPage
   */
  vendedor: Vendedor;

    /**
   * Objeto de Cliente 
   * @type {Cliente}
   * @memberof ArticulosPage
   */
  cliente: Cliente;

  /**
 * Objeto de Subscription
 * @type { Subscription }
 * @memberof ArticulosPage
 */
  menuCategorySubcription: Subscription;

  items: number = 0;
  paginascroll: number = 2;
  paginasTotal: number = 1;
  mensajeError: string = '';

  /**
   *Creates an instance of ArticulosPage.
   * @param {ArticulosService} articuloService
   * @param {ModalController} modalController
   * @param {AlertController} alertController
   * @param {NavController} navCtrl
   * @param { MenuController } menu
   * @param {DataLocalService} dataLocal
   * @param { CategoriasArticulosService } _menuCategorias
   * @memberof ArticulosPage
   */
  constructor(
    public events: Events,
    private articuloService: ArticulosService,
    public modalController: ModalController,
    public alertController: AlertController,
    private navCtrl: NavController,
    private _menuCategorias: CategoriasArticulosService,
    private dataLocal: DataLocalService) {
    this.obtener = false;

    this.events.subscribe('actualizar', (data) => {
    });
  }

  /**
   * Permite ejecutar funciones al iniciar el componente.
   * @memberof ArticulosPage
   */
  async ngOnInit() {
    this.menuCategorySubcription = this._menuCategorias.buscarMenuObserver$.subscribe(( filtros: any ) => {
      if(filtros && filtros.categoria.length > 0){
        this.buscarDesdeMenu();
      }else{
        this.resetPaginado();
        if(this.busqueda != ''){
          this.buscar();
        }
      }
    })
  }

  ngOnDestroy(){
    this.menuCategorySubcription? this.menuCategorySubcription.unsubscribe() : '';
    this._menuCategorias.resetFiltros();
  }

  async ionViewWillEnter() {
    // Reseteamos los filtros
    this._menuCategorias.resetFiltros();
    
    this.listadoArticulo = [];
    this.busqueda = '';
    this.paginascroll = 2;
    this.paginasTotal = 1;
    
    await this.obtenerUsuario();
    await this.obtenerCliente()
    await this.obtenerCarro();
  }

  /**
   * Permite obtener usuario logueado en la aplicacion
   * @memberof ArticulosPage
   */

  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
    this.vendedor = JSON.parse(usuario);
  }
  
  async obtenerCliente(){
    this.cliente = await this.dataLocal.getCliente()
  }

  /**
   * Permite buscar articulos ingresando su SKU
   * @param {string} articulo
   * @memberof ArticulosPage
   */

  async buscar(event?) {
    if ((event && event.key === "Enter") || (!event)) { 
      this._menuCategorias.resetFiltros();

      if(this.busqueda && this.busqueda.length > 0){
        this.obtener = true;

        // reseteamos el array de articulos.
        this.listadoArticulo = [];
        this.paginascroll = 2;
    
        await this.getProductos();
    
        if(this.listadoArticulo.length === 1){
          this.buscarMatrizProducto();
        } 
      }else{
        this.resetPaginado();
        this._menuCategorias.resetFiltros();
        this.listadoArticulo = [];
      }
    }
  }

  /**
   * Permite abrir modal para visualizar un detalle del articulo seleccionado
   * @param {*} articulo
   * @returns modal detalle articulos
   * @memberof ArticulosPage
   */
  async abrirModal(articulo) {
    const modal = await this.modalController.create({
      component: DetalleArticuloPage,
      componentProps: {
        articulo,
        boton: false,
        cantidad: 1,
      }
    },
    );

    await modal.present();
    const { data } = await modal.onWillDismiss();

    data.recargar ? await this.obtenerCarro() : null;
  }

  async obtenerCarro() {
    
    let carro = await this.dataLocal.getCarroCompra();
    carro? this.items = carro.length : this.items = 0;
  }

  // metodo ejecutado al activar el inifity scroll.
  async loadData(event){
    if(!this.obtener && this.paginascroll <= this.paginasTotal){
      await this.getProductos(this.paginascroll)
      this.paginascroll ++;
      event.target.complete();
    }else{
      event.target.complete();
    }
  }

  // metodo utilizado para realizar la llamada al servicio obtener articulos. se usa al iniciar la vista, realizar busqueda o el inifity scroll.
  async getProductos(pagina: number = 1){

    let consulta = await this.articuloService.obtenerArticulos(this.setParametros(pagina)).then( consulta =>{
      this.obtener = false;
      this.setItems(consulta);
    }).catch(error => {
      this.obtener = false;
      this.mensajeError = 'No se ha podido establecer la conexión con el servidor.'
    });
  }

  setParametros(pagina:number = 1){
    let bodega = this.vendedor.codBodega == "" || this.vendedor.codBodega == "AREA ADMINISTRACION" ? "CDD-CD1" : this.vendedor.codBodega;
    
    let parametros = { 
      "descripcion": this.busqueda,
      "sucursal": bodega,
      "pageElastic": pagina,
      "usuario": this.vendedor.codUsuario,
      "sku": [],
      "categoria": this._menuCategorias.selectedCategoria,
      "filtros": this._menuCategorias.setFiltroUrl()
    }
    
    return parametros;
  }

  // Metodo utilizado para setear la lista de articulos cuando el api de articulos genera la respuesta.
  setItems(resp: any){
    if(resp && resp.data.items){
      this.listadoArticulo.push(...resp.data.items);

      // Seteamos los items que se mostrarán en el menu de filtros de busqueda en el catalogo.
      this._menuCategorias.setCategoriasXbusqueda(resp.categorias);
      this._menuCategorias.setFiltrosXbusqueda(resp.filtros);

      this.paginasTotal = resp.totalPages;
    }else{
      this.mensajeError = 'No se han encontrado productos para la busqueda ingresada.';
    }
  }

  // Metodo que permite buscar los productos matriz de un producto buscado por sku
  async buscarMatrizProducto(){
    this.obtener = true;
    let arreglo: any[] = this.listadoArticulo;
    this.listadoArticulo = [];
    let consultaMatrix = await this.articuloService.getMatrizProducto(arreglo[0].sku, this.setParametros());
    arreglo.push(...consultaMatrix);
    this.listadoArticulo = arreglo;
    this.obtener = false;
  }

  // Metodo utilizado para emitir señal desde observable para que se abra el menu de categorias.
  openMenuCategorias(){
    this._menuCategorias.mostrarMenu();
  }

  // Metodo ejecutado cuando se selecciona una categoria o filtro desde el menu de categorias.
  buscarDesdeMenu(){
    this.resetPaginado();

    this.obtener = true;
    this.listadoArticulo = [];
    this.getProductos();
  }

  resetPaginado(){
    // reseteamos el array de articulos.
    this.listadoArticulo = [];
    this.paginascroll = 2;
    this.paginasTotal = 1;
  }
}

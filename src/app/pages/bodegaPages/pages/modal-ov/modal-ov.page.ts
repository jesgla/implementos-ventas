import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { OperacionesBodegaService } from 'src/app/services/bodega/operaciones-bodega.service';
import { AppUtilsService } from 'src/app/services/dataLocal/app-utils.service';

@Component({
  selector: 'app-modal-ov',
  templateUrl: './modal-ov.page.html',
  styleUrls: ['./modal-ov.page.scss'],
})
export class ModalOvPage implements OnInit {

  @Input() data: any;

  showBtn:boolean = true;
  
  constructor(private modalCtrl: ModalController,
              private actionSheetController: ActionSheetController,
              private appUtilsService:AppUtilsService,              
              private operBodegaService: OperacionesBodegaService ) { }

  ngOnInit() {
  }

  async recibirOv(){
    console.log('se recibe');
    await this.confirmacion();
  }

  cancelar(){
    console.log('se cancela');
    this.modalCtrl.dismiss();
  }

  async confirmacion(){
          
      console.log(' Recibir OV');

      await this.appUtilsService.presentAlertConfirm( 'Confirmar Recepción',
        "Con esta acción <strong>se notifica al cliente</strong> que su compra ya está lista para ser retirada",
        async()=>{
          await this.appUtilsService.presentLoading('Espere un momento');
          await this.recibirOV();
          this.appUtilsService.closeLoading();
          ;},
        ()=>{console.log('Cancelar');},
        )
  }

  
  async recibirOV(){
    console.log('modal-ov.recibirOV[INI]');
    let resp = await this.operBodegaService.recibirOV(this.data.guias[0].folio_guia,this.data.documento, this.data.almacen_envia, this.data.almacen_recibe);
    
    if(resp){
      await this.appUtilsService.presentToast(`Recepción exitosa`,'success');
      await this.appUtilsService.presentarAlertaSimple('Recepción exitosa','<div style="text-align:center"><strong>Se ha enviado una notificación al cliente!</strong><br/>Vendrá a retirar su compra en cualquier momento.</div>');
      this.showBtn=false;
    }

    console.log(resp);
  }  


}

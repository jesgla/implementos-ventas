import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReciboMercanciasDiscrepanciaPage } from './recibo-mercancias-discrepancia.page';

const routes: Routes = [
  {
    path: '',
    component: ReciboMercanciasDiscrepanciaPage
  }
];

@NgModule({
  entryComponents:[ReciboMercanciasDiscrepanciaPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReciboMercanciasDiscrepanciaPage]
})
export class ReciboMercanciasDiscrepanciaPageModule {}

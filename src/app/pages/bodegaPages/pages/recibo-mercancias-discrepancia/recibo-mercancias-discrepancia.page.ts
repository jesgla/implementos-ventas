import { Component, OnInit, Input } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
//import { File } from '@ionic-native/file';
import { ModalController } from '@ionic/angular';
import { AppUtilsService } from 'src/app/services/dataLocal/app-utils.service';
import { OperacionesBodegaService } from 'src/app/services/bodega/operaciones-bodega.service';
import { ModalImagenPage } from '../modal-imagen/modal-imagen.page';
declare var window:any;

@Component({
  selector: 'app-recibo-mercancias-discrepancia',
  templateUrl: './recibo-mercancias-discrepancia.page.html',
  styleUrls: ['./recibo-mercancias-discrepancia.page.scss'],
})
export class ReciboMercanciasDiscrepanciaPage implements OnInit {
 
  @Input() producto: any;
  @Input() documento: any;

  cantidadTemporal: number;
  deterioroTemporal: number;

  ordenTransporteTemporal: string;
  tipoDeterioroTemporal: string;
  descripcionTemporal: string;

  tempImages: any[] = []; 

  constructor( private modalCtrl: ModalController,
               private appUtilsService: AppUtilsService,
               private bodegaService: OperacionesBodegaService,
               private camera: Camera,
               private fileTransfer: FileTransfer
    ) { }

  ngOnInit() {
    console.log('desde modal producto', this.producto);
    console.log('desde modal documento', this.documento);

    this.cantidadTemporal = this.producto.cantidadUsuario;
    this.deterioroTemporal = this.producto.deterioroUsuario;
    this.tipoDeterioroTemporal = this.producto.tipoDeterioro;
    this.ordenTransporteTemporal = this.producto.ordenTransporte;
    this.descripcionTemporal = this.producto.descripcionDeterioro;

    if ( this.producto.tempImages ){
      this.tempImages = this.producto.tempImages;
    }
  }
  

  async aceptar(){
    console.log('Se guardan discrepancias');
    //validaciones de los datos ingresados
    //se graba la discrepancia. se sube el listado de fotos agregadas

    /**
     * Validaciones
     */
    let valido = true;
    let msg = [];

    if( !this.cantidadTemporal || !this.deterioroTemporal ){
      valido = false;
      msg.push('Ingrese la cantidad a recibir y cuántos se declaran dañados');
    }

    if( this.cantidadTemporal < this.deterioroTemporal ){
      valido = false;
      msg.push('La cantidad de productos dañados no puede ser mayor a la cantidad que se está recibiendo');
    }

    if(!this.ordenTransporteTemporal){
      valido = false;
      msg.push('Indique el nro de Orden de Transporte (OT)');
    }


    if(!this.tipoDeterioroTemporal){
      valido = false;
      msg.push('Señale el origen del daño: Transporte o Bodega Origen');
    }

    if(this.tempImages.length <= 0){
      valido = false;
      msg.push('Debe adjuntar evidencia fotográfica de los productos dañados');
    }

    if(!valido){
      let txt = '-'+msg.join('<br/><br/>-  ');
      await this.appUtilsService.presentarAlertaSimple( 'Debe considerar lo siguiente:', txt, 'Aceptar' );//,'alert-danger'
      return
    }

    //La informacion ingresada es válida, se procede a guardar
    /**
     * Se almacena
     */    
    this.producto.cantidadUsuario = this.cantidadTemporal;
    this.producto.deterioroUsuario = this.deterioroTemporal;
    this.producto.tipoDeterioro = this.tipoDeterioroTemporal;
    this.producto.ordenTransporte = this.ordenTransporteTemporal;
    this.producto.descripcionDeterioro = this.descripcionTemporal;
    
    
    /*
    //generar un hash aleatoreo
    let hash = this.bodegaService.generarUuid();
    console.log('se usa el hash:',hash);
    */
   //generar un hash que sea unico para el grupo de imagenes agregadas, con esto se determina si se cambio el contenido o no
   //si es diferente el hash, significa que se modificaron las imagenes cargadas, por lo que se suben las imagenes actuales y se descartan las anteriores
    let hash = this.toHash(this.tempImages);
    console.log('hash que ya existia:',this.producto.reciboHash);
    console.log('hash calculado:',hash);
    hash = Math.abs(hash);
    
    //si cambio el hash, se sube el set de imagenes
    if( hash != this.producto.reciboHash ){
      await this.appUtilsService.presentLoading('Espere un momento');
    
      //console.log('cant arrImagenes:',this.tempImages.length);
      //console.log('arrImagenes:',this.tempImages);
      let err=false;
      let m=0;
      for(let elem of this.tempImages){
         console.log('iteracion ['+m+']:',elem);
         //si la imagen ya se subio con este hash, no se vuelve a subir
        if(elem.cargada && elem.hashSubida == hash){
          console.log('imagen ya subida, la saltamos')
          continue;
        }
        console.log('imagen no se habia subido, se sube al server')
         let resp = await this.subirImagen(elem.imageData , hash.toString() );
         if(!resp){
           err=true;
           elem.cargada=false
           break;
         }
         else{
           elem.cargada=true;
           elem.hashSubida=hash;
         }
         m++;
      }
      this.appUtilsService.closeLoading();
      if(err){
        console.log("Ocurrio un error, se subieron:",m+1," de ",this.tempImages.length);
        await this.appUtilsService.presentToast('Ha ocurrido un error al enviar las imágenes, por favor intente de nuevo','danger');
        return;
      }
      
      console.log('ok');

      for(let x of  this.tempImages) {delete x.cargada; delete x.hashSubida}
      
      this.producto.tempImages = this.tempImages;
     

      //se almacena el hash en la info del item
      this.producto.reciboHash = hash;
    }else{
      console.log('Las imágenes son las mismas, no se guardan');
    }

    this.modalCtrl.dismiss();
  }

  cancelar(){
    console.log('preguntar si cancela');
    console.log('se cancela');
    this.modalCtrl.dismiss();
  }

  //un hash calculado segun el contenido del arreglo de imagenes. facilita revisar si se modificó el array de imagenes
  toHash(valor){
    let val = JSON.stringify(valor);
    return val.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
  }


  camara(){

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.CAMERA
    };

    this.procesarImagen(options);

    /*this.camera.getPicture(options).then( ( imageData ) => {
      const img = window.Ionic.WebView.convertFileSrc( imageData );
      console.log(img);

      this.tempImages.push({"image":img,"imageData":imageData});
    }, (err) => {
      console.error(err);
    });*/

  }

  libreria(){

      const options: CameraOptions = {
        quality: 60,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
      };
      
      this.procesarImagen(options);

  }

  procesarImagen(options:CameraOptions){
    
    this.camera.getPicture(options).then( ( imageData ) => {
      const img = window.Ionic.WebView.convertFileSrc( imageData );
      console.log(img);

      this.tempImages.push({"image":img,"imageData":imageData});
    }, (err) => {
      console.error(err);
    });

  }

  async mostrarImagen(img:any, index:number){

    let modal = await this.modalCtrl.create({
      component: ModalImagenPage,
      componentProps: {
        image: img,
        arrImagenes: this.tempImages,
        index: index
      }
    });
    await modal.present();
    await modal.onDidDismiss().then( console.log );
    modal.remove();

  }
  
  async subirImagen( imgData:string , hash:string){

    let dataInfo = {   documentoNro: this.documento.documentoNro
                      ,documentoRaiz: this.documento.documentoRaiz
                      ,tipoDocumento: this.documento.tipoDocumento
                      ,itemSku: this.producto.item
                      ,iventTransId: this.producto.inventtransid
                      ,hash:hash
                    }

    let rr = await this.bodegaService.subirImagenTemporalRecibo(imgData,dataInfo);
    console.log('rr',rr);
    if(!rr){
      console.error('Ha ocurrido un error al subir la imagen');
      return false;
    }
    return true;
  }




}

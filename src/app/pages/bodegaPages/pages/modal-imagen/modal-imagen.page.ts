import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-imagen',
  templateUrl: './modal-imagen.page.html',
  styleUrls: ['./modal-imagen.page.scss'],
})
export class ModalImagenPage implements OnInit {

  @Input() image: any;
  @Input() arrImagenes: [];
  @Input() index: number;

  constructor(private modalCtrl : ModalController) { }

  ngOnInit() {
  }

  quitarImagen(){
    console.log()
    this.arrImagenes.splice(this.index, 1);
    this.modalCtrl.dismiss();
    return;
  }
  aceptar(){
    this.modalCtrl.dismiss();
    return;
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-registro-sobrantes',
  templateUrl: './registro-sobrantes.page.html',
  styleUrls: ['./registro-sobrantes.page.scss'],
})
export class RegistroSobrantesPage implements OnInit {

  items = [{
    codigoItem: 'SKU123124',
    nombreItem: 'Foco Led 12V Frontal derecho Nissan',
    cantidadUsuario: 12
  },
  {
    codigoItem: 'SKU123125',
    nombreItem: 'Foco Led 12V Frontal derecho Toyota',
    cantidadUsuario: 12
  },
  {
    codigoItem: 'SKU123126',
    nombreItem: 'Foco Led 12V Frontal derecho Chevrolet',
    cantidadUsuario: 12
  }];

  constructor() { }

  ngOnInit() {
  }

  boton1(prod?){
    /* console.log('prod',prod); */
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegistroSobrantesPage } from './registro-sobrantes.page';

const routes: Routes = [
  {
    path: '',
    component: RegistroSobrantesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegistroSobrantesPage]
})
export class RegistroSobrantesPageModule {}

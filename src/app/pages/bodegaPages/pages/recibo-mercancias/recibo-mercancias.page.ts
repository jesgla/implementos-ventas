import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { LoadingController, AlertController, ModalController, IonSlides, ActionSheetController, IonInput, IonContent, IonItemSliding } from '@ionic/angular';
import { fromEvent } from 'rxjs';
import { ReciboMercanciasDiscrepanciaPage } from 'src/app/pages/bodegaPages/pages/recibo-mercancias-discrepancia/recibo-mercancias-discrepancia.page';
import { ReciboMercanciasResumenConfirmPage } from 'src/app/pages/bodegaPages/pages/recibo-mercancias-resumen-confirm/recibo-mercancias-resumen-confirm.page';
import { LecturaRecibo } from 'src/app/interfaces/bodega/interfaces';
import { AppUtilsService } from 'src/app/services/dataLocal/app-utils.service';
import { OperacionesBodegaService } from 'src/app/services/bodega/operaciones-bodega.service';
import xml2js from 'xml2js';
import { ModalOvPage } from '../modal-ov/modal-ov.page';



@Component({
  selector: 'app-recibo-mercancias',
  templateUrl: './recibo-mercancias.page.html',
  styleUrls: ['./recibo-mercancias.page.scss'],
})
export class ReciboMercanciasPage implements OnInit {

  @ViewChild(IonSlides, { static: false }) slidesRecibo: IonSlides;
  @ViewChild(IonContent, {static: false}) ionContent: any;

  //@ViewChild(IonItemSliding, {static: false}) slidingItems: IonItemSliding[];
  @ViewChild("ionInputCodigoProducto", {static: false}) ionInputCodigoProducto: IonInput;

  data = 
  [];
  

  docsLocal = [];


  documentacion=[];

  documentos=[];
  productos=[];
  
  simulacionPaso=1;


  slideOpts = {
    initialSlide: 0,
    speed: 400,
    centerSlides: false
  };
  pestaniaActual=0;


  transportistas = ["SAMEX", "BEETRACK", "OTRO"];
  
  empresaTransportista: string;
  patenteCamion: string;
  inputCodigoProducto:string;
  inputCodigoProductoDisable:boolean=false;

  //src1$;
  constructor(private loadingCtrl : LoadingController,
              private alertController: AlertController,
              private modalCtrl : ModalController,   
              private appUtilsService : AppUtilsService,
              private actionSheetController: ActionSheetController,
              private operBodegaService: OperacionesBodegaService,
              ) { }

  ngOnInit() {


  }

  ngAfterViewInit() {
    this.slidesRecibo.lockSwipes(true);



    let click$ = fromEvent<any>( document ,'click'); 
    click$.subscribe( async ({x,y} ) => { 
      console.log('click - X:',x);
      console.log('click - Y:',y);

    });
    /**
     * se configura el campo para ingresar codigo del producto y buscar en los que estan cargados
     */
    const inputCodigoProducto_ : any = document.querySelector('#inputCodigoProducto');    
    //se configura para que se limpie el contenido del campo cada vez que se pone el foco
    let codProdFocus$ = fromEvent<any>( inputCodigoProducto_ ,'focusin'); 
    codProdFocus$.subscribe( async (a ) => { 
      console.log('focusin',a);
      if(a.target.value.length > 0){
        console.log('se limpia');
        a.target.value='';
      }

    });


    let codProdEv$ = fromEvent<any>( inputCodigoProducto_ ,'keypress'); 
    codProdEv$.subscribe( async (a ) => { 
          
          console.log(a);
          if(a.keyCode == 13){
            //se presionó el salto de linea
            console.log('ENTER');
            this.inputCodigoProductoDisable=true;

            console.log(a.target.value);
            let lectura = a.target.value;
            if( lectura.length > 30 ){
              await this.appUtilsService.presentToast(`Código no encontrado`,'precaucion');
              this.inputCodigoProducto='';
              this.inputCodigoProductoDisable=false;
              return;
            }
            
            //se revisa si la lectura coincide con alguno de los codigos de los items cargados en pantalla
            lectura = lectura.toUpperCase().trim();
            let filas =  this.docsLocal.reduce((acc,el) => {
                let itemsCoincide = el.items.reduce((acc2,el2)=>{
                      if(el2.barcodes.includes(lectura)){
                        //el codigo ingresado coincide con uno de los que tiene este producto
                        console.log('el',el);
                        acc2.push({ ...el2
                                    ,documento:el.documentoNro
                                    ,documentoRaiz:el.documentoRaiz
                                    ,refDocu: el
                                  });
                      }
                      return acc2;
                    },[]);
                acc = acc.concat(itemsCoincide);
                return acc;

            }, [] );

            console.log('RESULTADO',filas);

            this.inputCodigoProducto='';
            this.inputCodigoProductoDisable=false;
            a.target.value='';

            
            switch(filas.length){
              case 0: 
                await this.appUtilsService.presentToast(`Código: <strong>${lectura}</strong> no encontrado`,'precaucion');
                //return;
                break;
              case 1:
                let txtSku = lectura != filas[0].item ? `<br/>[sku: <strong>${filas[0].item}</strong>]`: '';                                
                let txtDocu = filas[0].documento != filas[0].documentoRaiz ? `<strong>${filas[0].documento} || ${filas[0].documentoRaiz}</strong>` : `<strong>${filas[0].documento}</strong>`;
                let txt = `Código <strong>${lectura}</strong> encontrado! ${txtSku}<br/>En el documento: ${txtDocu}`;

                await this.appUtilsService.presentToast( txt ,'success');

                await this.posicionarEnProducto(filas[0]);

                break;
              default:
                //si el producto se encuentra en mas de 1 item  
                await this.appUtilsService.presentToast( `El código coincide con <strong>${filas.length}</strong> productos cargados en pantalla` ,'precaucion');
                
                //se crean los botones para armar un actionsheet y el usuario seleccione a qué producto apuntar
                let botones = [];
                let funciones=[];
                for(let i=0;i<filas.length; i++){
                  let fila=filas[i];
                  
                  let txtSku = lectura != filas[0].item ? `<br/>[sku: <strong>${filas[0].item}</strong>]`: '';                                
                  let txtDocu = filas[0].documento != filas[0].documentoRaiz ? `<strong>${filas[0].documento} || ${filas[0].documentoRaiz}</strong>` : `<strong>${filas[0].documento}</strong>`;
                  let txt = `Código <strong>${lectura}</strong> encontrado! ${txtSku}<br/>En el documento: ${txtDocu}`;

                  let txtBtn = (fila.documento != fila.documentoRaiz ? `${fila.documento} || ${fila.documentoRaiz}` : `${fila.documento}`) + ` [${fila.item}]`;
                  
                  funciones[i]= async ()=>{
                    await this.appUtilsService.presentToast( txt ,'success');
                    this.posicionarEnProducto(fila);                    
                  }

                  let boton = {
                      text: `${txtBtn}`,
                      //icon: 'play',
                      handler: funciones[i]
                    }
                  botones.push(boton);
                }

                const actionSheet = await this.actionSheetController.create({
                  header: `El código leído coincide con más de un producto, seleccione a cuál dirigirse`,
                  cssClass: 'asheet-multiples-filas',
                  mode:"md",
                  buttons: botones
                });
                await actionSheet.present();

            }
          }
          
        } ); 
  }

  async posicionarEnProducto(fila){
      fila.refDocu.check=true;
      await this.timeout(100);

      let idd=`${fila.documento}_${fila.inventtransid}`;
      let elem = document.getElementById(idd);

      let iddParent = `docu_${fila.documento}`;
      let elemParent = document.getElementById(iddParent);
      
      let yOffset = elem.offsetTop;
      console.log('offsetTop:',yOffset);
      let yOffsetParent = elemParent.offsetTop;
      console.log('yOffsetParent:',yOffsetParent); 

      let yOffsetParentHeight = elemParent.offsetHeight;
      console.log('yOffsetParentHeight:',yOffsetParentHeight);
      
      document.getElementById('contenidoProductos').scrollTop = yOffset + yOffsetParent + 145;
      console.log('se hizo scroll');

      let elemItem: IonItemSliding = elem as unknown as IonItemSliding;
      if(fila.cant_a_recibir<=0 || fila.cant_pendiente_entrega<=0){
        return;
      }
      await this.timeout(100);
      await elemItem.open('end'); 
      (await this.ionInputCodigoProducto.getInputElement()).blur();
  }


  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  segmentEvent($event){
    console.log('segmentEvent',$event);
    let pestania = $event.detail.value ? $event.detail.value : 0;
    this.cambiarSeccion(pestania);
  }
  cambiarSeccion(pestania){    
    this.slidesRecibo.lockSwipes(false);
    this.slidesRecibo.slideTo(pestania);
    this.slidesRecibo.lockSwipes(true);
    this.pestaniaActual=pestania;

    if(pestania==1 && this.docsLocal.length==0){
        this.presentPromptManual();
    }
  }

  scrollListaTo(idElement:string) {
    console.log('idElement:',idElement);
    //let yOffset = document.getElementById(idElement).offsetTop;
    
    console.log('id-element:',idElement);
    //console.log('offsetItem',yOffset);

   // document.getElementById('contenidoProductos').scrollTop = yOffset;
    //document.getElementById('contenidoProductos').scrollTop = yOffset;
    document.getElementById('contenidoProductos').scrollTop = parseInt(idElement);


    //this.getContent().scrollToPoint(0, yOffset, 1000);  
  }
  focusBuscarProducto(){
    //this.getContent().scrollToPoint(0, 0, 200);  
    //
    this.ionInputCodigoProducto.setFocus();
  }
  getContent() {
    console.log(document.querySelector('ion-content'));
    //return document.querySelector('ion-content');
    return document.querySelector('ion-content');
  }
  
  async clickLecturaManual(){
    await  this.presentPromptManual();
  }

 
  async presentPromptManual() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Ingreso Guía/OC/LPN',
      inputs: [
        // multiline input.
        {
          name: 'codigo',
          type: 'text',
          placeholder: 'Nro Guía / OC / LPN (DGV..)'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Aceptar',
          handler: (data) => {
            console.log('Confirm Ok');
            //console.log('data',data);
            
            (async () => {
              await this.gestionarIngresoManual(data.codigo);
              console.info('Se retorna');
              //se vuelve a abrir alert de lectura
              //this.presentPromptManual();
            })();
          
          }
        }
      ]
    });
    //presenta alert y posiciona el cursor en el textarea
    await alert.present().then(() => {
      //al abrirse el modal se deja el foco en el primer input
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();

      //capturar la tecla enter para accionar el click en boton aceptar
      const botonAceptar : any = document.querySelector('ion-alert button:nth-child(2)');
      let src2$ = fromEvent(firstInput,'keyup'); 
      src2$.subscribe( ({ keyCode} ) => { 
            console.log(keyCode); 
            if(keyCode == 13){
              botonAceptar.click();
            }
          } ); 

      
      return;
    });
  }
  async gestionarIngresoManual( codigo:string ){
    console.log('gestionarIngresoManual[ini] codigo',codigo);
    let lect:LecturaRecibo;
    //@TODO: Ordenar
    
    //se revisa la cadena ingresada
    //si comienza con caracter '<' se determina trata de un xml leido con la pistola y se intenta interpretar como xml.
    let tipo:string;

    if( /^</.test(codigo) && codigo.length > 12 ){
      console.log('se trata de un xml. se interpreta');
      console.log('lectura',codigo);
     
      try{
        let res = await this.parseXML(codigo);
        console.log(res);
        if(
             res['TED']['DD'][0]['RE'][0] == '78924030-2' //si el emisor es implementos
           ){

            //si el receptor tambien es implementos, se trata de una guia de una TR
            if( res['TED']['DD'][0]['RE'][0] == res['TED']['DD'][0]['RR'][0] ){
              //en este caso se trata de una guia de despacho de una TR de implementos
              console.log('Guia TR:',res['TED']['DD'][0]['F'][0])
              lect =  {tipo:'GUIA-TR' ,numeroDocumento:res['TED']['DD'][0]['F'][0]};
            }                     
            else{
              //en este caso se trata de una guia de despacho de una ORDEN DE VENTA OV  
              console.log('Orden de Venta. factura:',res['TED']['DD'][0]['F'][0])
              lect =  {tipo:'OV',numeroDocumento:res['TED']['DD'][0]['F'][0]};
            
            }
             console.log('doc',res['TED']['DD'][0]['F'][0]);
             await this.atenderLectura(lect);
             return;
           }
           throw new Error('No se pudo interpretar documento');
      }catch(e){
        console.error('No se pudo interpretar el XML',e);
        await this.appUtilsService.presentToast(`Lectura no pudo ser procesada`,'precaucion');
        return;
      }
      
    
      
    }
    //A este punto 
    codigo=codigo.toLocaleUpperCase();
    
    //se revisa si el codigo es una OC
    if(/^OC-/i.test(codigo)){
      console.log('consultar por OC:',codigo);
      lect =  {tipo:"OC",numeroDocumento:codigo};
      await this.atenderLectura(lect);
      return;
    }

    if( /^DGV|^000|^[a-zA-Z]/i.test(codigo) ){
      console.log('consultar por LPN:',codigo);
      lect =  {tipo:"LPN",numeroDocumento:codigo};
      await this.atenderLectura(lect);
      return;
    }
    
    else{
      lect =  {tipo:'GUIA-TR',numeroDocumento:codigo};
      await this.atenderLectura(lect);
      return;
    }
    
    
  }



  parseXML(data)
   {
      return new Promise(resolve =>
      {             
         let parser = new xml2js.Parser(
             {
                trim: true,
                explicitArray: true
             });

         parser.parseString(data, function (err, result) 
         {
            resolve(result);
         });
        });
    }
    
  asignarCantUsuario(prod , cantidad:number){
    console.log('asignarCantUsuario[INI] prod',prod,'cantidad',cantidad);
    prod.cantidadUsuario = cantidad; 
  }
  async slideItemAsignarCantUsuario(prod , cantidad:number, itemSlideId: string){
    this.asignarCantUsuario(prod , cantidad);

    
    let elemItem: IonItemSliding = document.getElementById(itemSlideId) as unknown as IonItemSliding;
    console.log(elemItem);
    await elemItem.close(); 
    await this.timeout(1000);
    await this.ionInputCodigoProducto.setFocus();

  } 
  
  
  restart(){
    this.docsLocal=[];
  }

  marcarQuitarGuiaRecibida(docu){
    console.log('marcarQuitarGuiaRecibida',docu);
    //this.ejecutarFiltro();
  }

  async modalDiscrepancias(producto,documento){
    let modal = await this.modalCtrl.create({
      component: ReciboMercanciasDiscrepanciaPage,
      componentProps: {
        producto: producto,
        documento: documento
      }
    });
    await modal.present();
    await modal.onDidDismiss().then( console.log );
    modal.remove();

  }

  async modalResumenConfirm( recibos ){
    const modal = await this.modalCtrl.create({
      component: ReciboMercanciasResumenConfirmPage,
      componentProps: {
        recibos: recibos 
      }
    });
    await modal.present();
  }

  async modalGestionOV( dataOV ){
    const modal = await this.modalCtrl.create({
      component: ModalOvPage,
      componentProps: {
        data : dataOV
      }
    });
    await modal.present();
  }

  cantidadDocumentosMarcados(){
    return this.docsLocal.reduce((acc,el) => {
      if(el.check){
        acc++;
      }
      return acc;
      }
    , 0 );
  }

  cantidadProductosIngresados(docItems){
    if(!docItems) return 0;
    return docItems.reduce((acc,el) => {
      if(el.cantidadUsuario ){
        acc++;
      }
      return acc;
      }
    , 0 );  
  }

  cantFilasSinCompletar(items){
    if(!items) return 0;
    let cantPend = items.reduce((acc,el) => {
      if( el.cant_a_recibir ){
        acc++;
      }
      return acc;
      }
    , 0 );  
    return cantPend;
  }

  /**
   * 
   * @param documento 
   * Se determina un estado del documento, segun los items que contiene, y cuandos de estos han sido recibidos 
   */
  determinarEstado(documento){
    let cantFilas=0;
    let accum={
      cantFilas:0,
      filasConAlgunDeteriorado:0,
      filasSinRecibo:0, 
      filasCompletadas:0
    }
    switch(documento.tipoDocumento){
       case 'GUIA-TR':
           documento.items.reduce( (acc,el) => {
              acc.cantFilas++;
              if(el.cant_a_recibir==0) acc.filasCompletadas++;
              if(el.cant_recibida==0) acc.filasSinRecibo++;
              if(el.deterioro>0) acc.filasConAlgunDeteriorado++;
              return acc;
           },accum)
         break;
       case 'OC':
            documento.items.reduce( (acc,el) => {
              acc.cantFilas++;
              if(el.cant_pendiente_entrega==0) acc.filasCompletadas++;
              if(el.cant_recibida==0) acc.filasSinRecibo++;
              return acc;
          },accum)
         break;
    }
    if(accum.cantFilas == accum.filasCompletadas) 
    {
      return {estado:'RECIBIDO_TOTAL', text:'Recepción ya fue completada',color:"danger"}; 
    }
    if(accum.cantFilas == accum.filasSinRecibo) 
    {      
      return {estado:'SIN_RECIBOS', text:'Nada recibido aún',color:"primary"};
    }
    else{
      return  {estado:'RECIBIDO_PARCIAL',text:'Recibido parcialmente',color:"warning"};
    }
    
  }

  async gestionarIngresoLector( lectura:string ){

      //se determina qué es lo que se leyó
      //var resultado = this.recepcionService.analizarCodigo(lectura);
      //console.info('resp analisis',resultado);
      let lect:LecturaRecibo = { tipo:'GUIA-TR' , numeroDocumento: lectura };// '811736' };
      await this.atenderLectura(lect);

  }

  async opcionesDeDocumento(doc) {
    console.log(doc);
    let articulosIngresadosPeroSinRecibir=0;
    let titulo='';
    let textoConfirmacion;
    switch(doc.tipoDocumento){
      case 'GUIA-TR':
            articulosIngresadosPeroSinRecibir = doc.items.reduce( (acc,el) => { 
              if(el.cantidadUsuario) acc++;
              return acc;            
            }
          ,0);
          titulo=`CERRAR GUIA: ${doc.documentoNro}`;
          textoConfirmacion=`Con esta acción se completa la recepción de la <br/>
                              <ion-badge class="cuadro" color="tertiary">Guía:  ${doc.documentoNro}</ion-badge>,
                              perteneciente a la <ion-badge color="success">${doc.documentoRaiz}</ion-badge> <br/> 
                              y se declaran como <strong>faltantes</strong> las mercancías 
                              que no fueron recibidas`;

        break;
      case 'OC':
        break;
    }

    const actionSheet = await this.actionSheetController.create({
      header: `${doc.tipoDocumentoNombre} : ${doc.documentoNro}`,
      cssClass: 'alert-confirm-cierre',
      buttons: [{
        text: 'Cerrar Recibo',
        icon: 'lock-closed-outline',
        handler: () => {
          
          (async()=>{
          
          console.log('Cerrar Recibo');
          if(articulosIngresadosPeroSinRecibir>0){
            await this.appUtilsService.presentarAlertaSimple('Favor Revisar','Tiene cantidades ingresadas que no han sido recibidas.');
            return;
          }

          await this.appUtilsService.presentAlertConfirm( 'Cerrar Recibo',
            textoConfirmacion,
            async()=>{
              await this.cerrarGuia(doc);
              ;},
            ()=>{console.log('hola, NO');},
            )
          })();
        }
      }]
    });
    await actionSheet.present();
  }

  async cerrarGuia(doc:any){

    await this.appUtilsService.presentLoading(`Cerrando Recibo de GUÍA: ${doc.documentoNro}`);
    let respCierre = await this.operBodegaService.realizarCierreGuia(doc);
    if(respCierre){
      //la guia se cerró correctamente actualizamos la informacion local
      console.log('se recibió exitosamente lo referente a la guia:',doc.documentoNro);
      //se actualiza la data local
      for(let itt of doc.items){
        itt.cant_recibida = itt.cant_enviada;
        itt.cant_a_recibir = 0;
        itt.cantidadUsuario = 0;
        itt.deterioroUsuario = 0;
      }
      
      await this.appUtilsService.presentToast(`Recibos de GUIA: ${doc.documentoNro} procesado exitósamente`,'success');
    }
    await this.appUtilsService.closeLoading();

  }
  async cerrarOC(doc:any){

  }

  
  async atenderLectura( resultado:LecturaRecibo ){
       
    let dd; 
    switch(resultado.tipo){
      case 'GUIA-TR': 
            //revisa si la guia está cargada en la info actual, en cuyo caso debe marcarse como seleccionada 
            dd = this.docsLocal.find((el) => {
              return el.documentoNro ===  resultado.numeroDocumento;
            });
            if(dd){
              console.log('El documento ya está en pantalla solo hay q seleccionarlo',dd);
              dd.check=true;
              break;
            }
            console.log('El documento hay q ir a buscarlo a la API');
            //si no esta aún cargada en la recepción actual, se trae desde la api la TR a la que pertenece la guía en cuestión.
            await this.appUtilsService.presentLoading('Espere un momento');
            let dataTR = await this.operBodegaService.obtenerTrFromNroGuia(resultado.numeroDocumento); 
            this.appUtilsService.closeLoading();
            console.log('dataTR',dataTR);
            if(!dataTR){
              //this.appUtilsService.presentToast("Ha ocurrido un error","danger");
              return;
            }
            //Puede que la guia por la que se consultó pertenezca a una OV. se le da tratamiento especial
            if(dataTR.recurso=='OV'){
              await this.modalGestionOV(dataTR);
              return;
            }

            this.data.push(dataTR);

            //this.appUtilsService.presentToastWithOptions();
            this.appUtilsService.presentToast("Documento reconocido","success");

            const docs = dataTR.guias.reduce( (acc,el) => {
              let data={ 
                         tipoDocumento        : 'GUIA-TR',
                         tipoDocumentoNombre  : 'Guía Despacho',
                         documentoRaiz        : dataTR.documento,
                         documentoNro         : el.folio_guia,
                         dataTR               : dataTR,
                         ...el 
                        }
              if( data.folio_guia.toString() === resultado.numeroDocumento ) data.check=true;          
              acc.push( data );  
              return acc;
            }, [] );
            console.log(typeof docs,' docs',docs);
            //se agregan los documentos leudos al arreglo local
            this.docsLocal = this.docsLocal.concat( docs );
            console.log('this.docsLocal',this.docsLocal);
            break;
      case 'OV':
          console.log('El documento hay q ir a buscarlo a la API');
            //si no esta aún cargada en la recepción actual, se trae desde la api la TR a la que pertenece la guía en cuestión.
            await this.appUtilsService.presentLoading('Espere un momento');
            let dataOV = await this.operBodegaService.obtenerTrFromNroGuia(resultado.numeroDocumento); 
            this.appUtilsService.closeLoading();
            console.log('dataOV',dataOV);
            if(!dataOV){
              //this.appUtilsService.presentToast("Ha ocurrido un error","danger");
              return;
            }
            await this.modalGestionOV(dataOV);

            break;
      case 'OC':
            //@TODO: revisar si la guia está cargada en la info actual, en cuyo caso debe marcarse como seleccionada 
            dd = this.docsLocal.find((el) => {
              return el.documentoNro ===  resultado.numeroDocumento;
            });
            if(dd){
              console.log('El documento ya está en pantalla solo hay q seleccionarlo',dd);
              dd.check=true;
              break;
            }
            console.log('El documento hay q ir a buscarlo a la API');
            //si no esta aún cargada en la recepción actual, se trae desde la api la TR a la que pertenece la guía en cuestión.
            await this.appUtilsService.presentLoading('Espere un momento');
            let dataOC = await this.operBodegaService.obtenerOrdenDeCompra(resultado.numeroDocumento); 
            this.appUtilsService.closeLoading();
            if(!dataOC){
              return;
            }
            console.log('dataOC',dataOC);
            this.data.push(dataOC);

            let data={ 
              tipoDocumento        : 'OC',
              tipoDocumentoNombre  : 'Orden de Compra',
              documentoRaiz        : dataOC.documento,
              documentoNro         : dataOC.documento,
              ...dataOC 
             }
            if( data.documentoNro.toString() === resultado.numeroDocumento ) data.check=true; 
            //se agregan los documentos leudos al arreglo local
            this.docsLocal.push( data );
            console.log('this.docsLocal',this.docsLocal);         

            break;
      case 'LPN':
            //revisa si la guia está cargada en la info actual, en cuyo caso debe marcarse como seleccionada       

            console.log('El documento hay q ir a buscarlo a la API');
            //si no esta aún cargada en la recepción actual, se trae desde la api la TR a la que pertenece la guía en cuestión.
            await this.appUtilsService.presentLoading('Espere un momento');
            let dataLPN = await this.operBodegaService.obtenerGuiasLPN(resultado.numeroDocumento); 
            this.appUtilsService.closeLoading();
            console.log('dataLPN',dataLPN);
            if(!dataLPN){
              //this.appUtilsService.presentToast("Ha ocurrido un error","danger");
              return;
            }

            //Puede que el LPN por la que se consultó pertenezca a una OV. se le da tratamiento especial
            if(dataLPN.recurso=='OV'){
              await this.modalGestionOV(dataLPN);
              return;
            }

            //this.data.push(dataLPN);

            //this.appUtilsService.presentToastWithOptions();
            this.appUtilsService.presentToast("LPN reconocido","success");

            const docus = dataLPN.guias.reduce( (acc,el) => {
              let data={ 
                         tipoDocumento        : 'GUIA-TR',
                         tipoDocumentoNombre  : 'Guía Despacho',
                         documentoRaiz        : dataLPN.documento,
                         documentoNro         : el.folio_guia,
                         dataTR               : dataLPN,
                         ...el 
                        }
              //if( data.folio_guia.toString() === resultado.numeroDocumento ) data.check=true;
              data.check=true;    
              
              //antes de agregar la guia al listado, revisamos si ya estaba cargada, en ese caso, la seleccionamos, en caso contrario la agregamos
              dd = this.docsLocal.find((el) => {
                return el.documentoNro ===  data.documentoNro;
              });
              if(dd){
                console.log('El documento ya está en pantalla solo hay q seleccionarlo',dd);
                dd.check=true;
              }else{
                acc.push( data );
              }
              return acc;
            }, [] );
            console.log(typeof docus,' docus',docus);
            //se agregan los documentos leudos al arreglo local
            this.docsLocal = this.docsLocal.concat( docus );
            console.log('this.docsLocal',this.docsLocal);
            break;

      case 'PRODUCTO':
      default:
          console.log('No se pudo interpretar');
          break;
    }
    //determinar qué hacer segun lo que se leyó    
  }

  async recibirItems(){
    console.log('recibirItems[INI]');
    //validaciones
    //se valida que se haya ingresado informacion del transportista
    let valido=true;
    if(!this.empresaTransportista){
      valido=false;
      await this.appUtilsService.presentToast(`Seleccione empresa Transportista`,'precaucion');
    }

    if(!this.patenteCamion){
      valido=false;
      await this.appUtilsService.presentToast(`Ingrese Patente del Camión`,'precaucion');
    }

   if(!valido){
      console.error('No pasa validaciones');
      return;
    }

      let reciboResumen = this.docsLocal.reduce((acc,el) => {
          
          let itemsRecibirGuia = el.items.reduce( (acc2,el2) =>  {
                                    if( el2.cantidadUsuario && el2.cantidadUsuario > 0 ){
                                        acc2.push(el2);
                                    }
                                    return acc2;
                                  }, []);
          //si hay items configurados para hacer la recepción, se agrega la guia al acumulador
          if ( itemsRecibirGuia.length > 0 ){
              let documento = {
                  documento : el,
                  itemsRecibir : itemsRecibirGuia,
                  patenteCamion: this.patenteCamion.toUpperCase(),
                  empresaTransportista: this.empresaTransportista
              }
              acc.push(documento);
          }
          return acc;
      }
    , [] );
    

    if( reciboResumen.length <=0 ){
      valido=false;
      await this.appUtilsService.presentToast(`No hay cantidades para recibir`,'precaucion');
    }

   if(!valido){
      console.error('No pasa validaciones');
      return;
    }
    
    console.log('reciboResumen',reciboResumen);
    this.modalResumenConfirm(reciboResumen);
    /*
    for(let elem of reciboResumen){
      for(let item of elem.itemsRecibir){
        console.log('item',item);
        item.cant_recibida = item.cantidadUsuario;
        item.cant_a_recibir = (item.cant_enviada - item.cant_recibida) < 0 ? 0 : item.cant_enviada - item.cant_recibida;
      }
    }*/
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReciboMercanciasPage } from './recibo-mercancias.page';
import { ReciboMercanciasDiscrepanciaPageModule } from '../recibo-mercancias-discrepancia/recibo-mercancias-discrepancia.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { ReciboMercanciasDiscrepanciaPage } from '../recibo-mercancias-discrepancia/recibo-mercancias-discrepancia.page';

const routes: Routes = [
  {
    path: '',
    component: ReciboMercanciasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    //ReciboMercanciasDiscrepanciaPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReciboMercanciasPage]
})
export class ReciboMercanciasPageModule {}

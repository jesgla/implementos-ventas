import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { retiroPendientePage } from './retiro-pendiente-page';

describe('retiroPendientePage', () => {
  let component: retiroPendientePage;
  let fixture: ComponentFixture<retiroPendientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ retiroPendientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(retiroPendientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

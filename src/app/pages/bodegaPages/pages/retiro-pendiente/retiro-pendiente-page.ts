import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';

//servicios
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { ReteiroPendienteService } from 'src/app/services/retiroPendiente/reteiro-pendiente.service';
import { UsuarioService } from './../../../../services/usuario/usuario.service';
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';

//interfaces
import { detalleOV, retiroPendiente} from 'src/app/interfaces/bodega/interfaces';

@Component({
  selector: 'retiro-pendiente-page',
  templateUrl: './retiro-pendiente-page.html',
  styleUrls: ['./retiro-pendiente-page.scss'],
})
export class retiroPendientePage implements OnInit {
  detallePedido: detalleOV[];
  folioOV: string;
  detalleRetiroPendiente: retiroPendiente;

  constructor(
    private _clienteService: ClienteService,
    private route: ActivatedRoute,
    private _ReteiroPendienteService: ReteiroPendienteService,
    public _UsuarioService : UsuarioService,
    private _estadoTiendaService: EstadoTiendaService,
    private router: Router,) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.folioOV = await this.route.snapshot.paramMap.get('OV');
    this.getDetalleOV();
  }

  async getDetalleOV(){
    this.detallePedido = await this._clienteService.obtenerDetallePedido(this.folioOV);
    let consulta = await this._ReteiroPendienteService.obtenerRetiroPendiente(this.folioOV);
    this.detalleRetiroPendiente = consulta? consulta[0] : [];
    this.getProductosUbicaciones();
  }

  async setResponsableRetiroPendiente(_id: string){
    await this._ReteiroPendienteService.setResponsableRetiroPendiente(_id, this._UsuarioService.getUserLoged().nombre);
    await this.getDetalleOV();
    this._ReteiroPendienteService.reloadBandejaPicking();
  }

  async setListoParaRetiro(_id: string, folio: string){
    let respuesta: boolean = await this._ReteiroPendienteService.setListoParaRetiro(_id, folio);
    if(respuesta){
      await this.getDetalleOV();
      this._ReteiroPendienteService.reloadBandejaPicking(); 
    }
  }

  async getProductosUbicaciones(){
    for(let producto of this.detallePedido){
      let direccion = await this._estadoTiendaService.getProductoUbicacion(this.detalleRetiroPendiente.codBodega, producto.sku);
      producto.direccion = direccion;
    }
  }

  backToPickingBandeja(){
    this._ReteiroPendienteService.reloadBandejaPicking();
    this.router.navigate(['/picking-pendientes']);
  }
}

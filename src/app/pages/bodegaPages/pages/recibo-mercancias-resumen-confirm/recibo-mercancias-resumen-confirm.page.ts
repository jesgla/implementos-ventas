import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppUtilsService } from 'src/app/services/dataLocal/app-utils.service';
import { OperacionesBodegaService } from 'src/app/services/bodega/operaciones-bodega.service';

@Component({
  selector: 'app-recibo-mercancias-resumen-confirm',
  templateUrl: './recibo-mercancias-resumen-confirm.page.html',
  styleUrls: ['./recibo-mercancias-resumen-confirm.page.scss'],
})
export class ReciboMercanciasResumenConfirmPage implements OnInit {

  @Input() recibos: any;

  constructor(  private modalCtrl: ModalController, 
                private appUtilsService : AppUtilsService,
                private bdgaService : OperacionesBodegaService
               ) { }

  ngOnInit() {
    console.log('recibos',this.recibos);
  }


  async confirmar(){
    console.log(this.recibos);

    let resp; 
    //por cada grupo de items se realiza un recibo
    for(let recib of this.recibos){
      switch( recib.documento.tipoDocumento ){
       case 'GUIA-TR':
            let rr = {
              "identificador" : recib.documento.documentoNro,
              "documento" : recib.documento.documentoRaiz,
              "items" : recib.itemsRecibir.map( (x)=> {return {"id":x.inventtransid,"codigo":x.item,"cantidad":x.cantidadUsuario, "deterioro": !x.deterioroUsuario ? 0 : x.deterioroUsuario, "nombreItem":x.nombre_item , "hash":x.reciboHash,"tipoDeterioro":x.tipoDeterioro,"ordenTransporte":x.ordenTransporte,"descripcionDeterioro":x.descripcionDeterioro} } ),
              "almacen_envia":recib.documento.dataTR.almacen_envia,
              "almacen_recibe":recib.documento.dataTR.almacen_recibe,
              "patenteCamion":recib.patenteCamion,
              "empresaTransportista":recib.empresaTransportista
            }
            console.log('consumir',rr);
            await this.appUtilsService.presentLoading(`Recibiendo items de GUÍA: ${recib.documento.documentoNro}`);
            let respGuia = await this.bdgaService.recibirItemsGuia(rr);
      
            console.log('de vuelta ', respGuia);
            if(respGuia && !respGuia.error){
              //en este punto el recibo fue exitoso. actualizar data local
              console.log('se recibió exitosamente lo referente a la guia:',recib.documento.documentoNro);
              //se actualiza la data local
              for(let itt of recib.itemsRecibir){
                itt.cant_recibida = itt.cant_recibida + itt.cantidadUsuario;
                itt.cant_a_recibir = (itt.cant_a_recibir-itt.cantidadUsuario) <= 0 ? 0 : (itt.cant_a_recibir-itt.cantidadUsuario);
                itt.cantidadUsuario = 0;
                itt.deterioroUsuario = 0;

                //se eliminan las imagenes que estaban cargadas como dañados del item
                delete itt.tempImages;
                delete itt.tipoDeterioro;
                delete itt.ordenTransporte; 
                delete itt.descripcionDeterioro;

              }
              
              await this.appUtilsService.presentToast(`Recibos de GUIA: ${recib.documento.documentoNro} procesado exitósamente`,'success');
            }
            else{
              //await this.appUtilsService.presentToast(`Error al recibir GUIA: ${recib.documento.documentoNro}`,'danger');
              console.error(`Error al recibir GUIA: ${recib.documento.documentoNro}`);
            }
            
            await this.appUtilsService.closeLoading();

          break;

       case 'OC':
          let roc = {
            "identificador" : recib.documento.documentoNro,
            "documento" : recib.documento.documentoNro,
            "items" : recib.itemsRecibir.map( (x)=> {return {"id":x.inventtransid,"codigo":x.item,"cantidad":x.cantidadUsuario, "deterioro": !x.deterioroUsuario ? 0 : x.deterioroUsuario,"nombreItem":x.nombre_item , "hash":x.reciboHash} } ),
            "nombre_proveedor": recib.documento.nombre_proveedor,
            "rut_proveedor": recib.documento.rut_proveedor,
            "almacen_recibe":recib.documento.almacen_recibe,
            "patenteCamion":recib.patenteCamion,
            "empresaTransportista":recib.empresaTransportista
          }
          console.log('consumir',roc);

          await this.appUtilsService.presentLoading(`Recibiendo items de Orden de Compra: ${recib.documento.documentoNro}`);
          let respOc = await this.bdgaService.recibirItemsOc(roc);
          console.log('de vuelta ', respOc);
            if(respOc){
              //en este punto el recibo fue exitoso. actualizar data local
              console.log('se recibió exitosamente lo referente a la Orden de Compra:',recib.documento.documentoNro);
              //se actualiza la data local
              for(let itt of recib.itemsRecibir){
                itt.cant_recibida = itt.cant_recibida + itt.cantidadUsuario;
                itt.cant_pendiente_entrega = (itt.cant_pendiente_entrega-itt.cantidadUsuario) <= 0 ? 0 : (itt.cant_pendiente_entrega-itt.cantidadUsuario);
                itt.cantidadUsuario = 0;
                itt.deterioroUsuario = 0;

                //se eliminan las imagenes que estaban cargadas como dañados del item
                delete itt.tempImages;
              }
              
              await this.appUtilsService.presentToast(`Recibo de Orden de Compra: ${recib.documento.documentoNro} procesado exitósamente`,'success');
            }
            else{
              //await this.appUtilsService.presentToast(`Error al recibir Orden de Compra: ${recib.documento.documentoNro}`,'danger');
              console.error(`Error al recibir Orden de Compra: ${recib.documento.documentoNro}`);
            }
            await this.appUtilsService.closeLoading();


          break;
          default:
            break;
      }

  }
  this.modalCtrl.dismiss();
  }
  cancelar(){
    console.log('Se cancela');
    this.modalCtrl.dismiss();
  }



}

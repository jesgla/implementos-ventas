import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BdgaInicioPagePage } from './bdga-inicio-page.page';

const routes: Routes = [
  {
    path: '',
    component: BdgaInicioPagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BdgaInicioPagePage]
})
export class BdgaInicioPagePageModule {}

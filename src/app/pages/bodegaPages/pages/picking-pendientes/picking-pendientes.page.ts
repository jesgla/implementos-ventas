import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

// servicios 
import { DespachosPendientesService } from 'src/app/services/bodega/despachos-pendientes.service';
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';
import { ReteiroPendienteService } from '../../../../services/retiroPendiente/reteiro-pendiente.service';

// interfaces
import { despachoPendiente, retiroPendiente } from 'src/app/interfaces/bodega/interfaces';

//components
import { FiltrosPopoverComponent } from './components/filtros-popover/filtros-popover.component';

@Component({
  selector: 'app-picking-pendientes',
  templateUrl: './picking-pendientes.page.html',
  styleUrls: ['./picking-pendientes.page.scss'],
})
export class PickingPendientesPage implements OnInit, OnDestroy {
  despachosPendientes: despachoPendiente[] = [];
  retirosPendientes: retiroPendiente[] = [];
  loadData: boolean = false;
  sucursales: {codigo: string, nombre: string, selected: boolean}[] = [];
  selectedSucursal: string;
  filtroDespachosPendientes: string = 'TODOS';
  subcripions: Subscription;

  constructor(
    private _DespachosPendientesService: DespachosPendientesService,
    private _DataLocalService: DataLocalService,
    private _EstadoTiendaService:EstadoTiendaService,
    private popoverController: PopoverController,
    private _ReteiroPendienteService: ReteiroPendienteService,
    private router: Router,) 
  { 
    this.subcripions = this._ReteiroPendienteService.pickingReloadObservable$.subscribe( () => {
      this.loadData = false;
      this.getRetirosPendientes();
      this.loadData = true;
    })

  }

  ngOnDestroy(){
    this.subcripions ? this.subcripions.unsubscribe() : null;
  }

  async ngOnInit() {
    await this._EstadoTiendaService.getBodegas();
    await this.setSelectedBodega();
    await this.getPickingPendientes();
  }

  async setSelectedBodega(){
    this.sucursales = this._EstadoTiendaService.sucursales;

    let savedSucursal = await this._DataLocalService.getItem('selectedSucursalDesPend');
    this.selectedSucursal = savedSucursal? savedSucursal : null;
  }

  async saveSelectedSucursal(sucursal: string){
    await this._DataLocalService.setItem('selectedSucursalDesPend', sucursal);
  }

  async getPickingPendientes(){
    this.loadData = false;
    await this.getDespachosPendientes();
    await this.getRetirosPendientes();
    this.loadData = true;
  }

  async getDespachosPendientes(){
    this.despachosPendientes = [];
    this.despachosPendientes = await this._DespachosPendientesService.getDespachosPendientes(this.selectedSucursal);
  }

  async getRetirosPendientes(){
    this.retirosPendientes = [];
    this.retirosPendientes = await this._ReteiroPendienteService.getRetirosPendientesByBodega(this.selectedSucursal, false);
  }

  async presentPopoverFiltros(ev: any) {
    const popover = await this.popoverController.create({
      component: FiltrosPopoverComponent,
      cssClass: 'filrosOpor',
      event: ev,
      mode: 'ios',
      componentProps: { filtro: this.filtroDespachosPendientes },
      animated: true,
      translucent: false
    });

     popover.onDidDismiss().then((data) => {
      if(data && data.data) this.filtrarResultados(data.data);
    });

    return await popover.present();
  }


  filtrarResultados(filtro: any){
    this.filtroDespachosPendientes = filtro;
  }

  showRetiroPendienteDetail(ordenVenta: string){
    this.router.navigate([`retiros-pendientes/${ordenVenta}`]);
  }
}

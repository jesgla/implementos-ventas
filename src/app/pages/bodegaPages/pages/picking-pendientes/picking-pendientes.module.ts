import { retiroPendientePageModule } from '../retiro-pendiente/retiro-pendiente-page.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { IonicModule } from '@ionic/angular';

import { PickingPendientesPage } from './picking-pendientes.page';

// components
import { FiltrosPopoverComponent } from './components/filtros-popover/filtros-popover.component';
import { ItemDespachoPendienteComponent } from './components/item-despacho-pendiente/item-despacho-pendiente.component';
import { ItemRetiroPendienteComponent } from './components/item-retiro-pendiente/item-retiro-pendiente.component';

const routes: Routes = [
  {
    path: '',
    component: PickingPendientesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PickingPendientesPage,
    ItemDespachoPendienteComponent,
    FiltrosPopoverComponent,
    ItemRetiroPendienteComponent,
  ],
  entryComponents:[
    FiltrosPopoverComponent
  ]
})
export class PickingPendientesPageModule {}

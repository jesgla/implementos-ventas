import {  retiroPendiente } from 'src/app/interfaces/bodega/interfaces';
import { Component, Input, OnInit } from '@angular/core';

//servicios
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { ReteiroPendienteService } from 'src/app/services/retiroPendiente/reteiro-pendiente.service';

@Component({
  selector: 'app-item-retiro-pendiente',
  templateUrl: './item-retiro-pendiente.component.html',
  styleUrls: ['./item-retiro-pendiente.component.scss'],
})
export class ItemRetiroPendienteComponent implements OnInit {
  @Input() retiroPendiente: retiroPendiente;
  showDetails: boolean = false;

  constructor(public _UsuarioService: UsuarioService, private _ReteiroPendienteService: ReteiroPendienteService) { }

  ngOnInit() {}


  async setResponsableRetiroPendiente(_id: string){
    await this._ReteiroPendienteService.setResponsableRetiroPendiente(_id, this._UsuarioService.getUserLoged().nombre);
    this._ReteiroPendienteService.reloadBandejaPicking();

  }

  async setListoParaRetiro(_id: string, folio: string){
    let respuesta = await this._ReteiroPendienteService.setListoParaRetiro(_id, folio);
    if(respuesta) this._ReteiroPendienteService.reloadBandejaPicking();
  }

}

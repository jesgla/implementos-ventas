import { Component, OnInit, Input } from '@angular/core';

// interfaces
import { despachoPendiente } from 'src/app/interfaces/bodega/interfaces'

@Component({
  selector: 'app-item-despacho-pendiente',
  templateUrl: './item-despacho-pendiente.component.html',
  styleUrls: ['./item-despacho-pendiente.component.scss'],
})
export class ItemDespachoPendienteComponent implements OnInit {
  @Input() despachoPendiente: despachoPendiente;
  showDetails: boolean = false;

  constructor() { }

  ngOnInit() {}

}

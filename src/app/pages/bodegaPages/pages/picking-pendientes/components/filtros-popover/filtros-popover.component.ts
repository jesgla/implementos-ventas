import { Component, OnInit, Input } from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-filtros-popover',
  templateUrl: './filtros-popover.component.html',
  styleUrls: ['./filtros-popover.component.scss'],
})
export class FiltrosPopoverComponent implements OnInit {
  @Input() filtro: string;
  selectFiltroText: string = 'TODOS';

  constructor(private PopoverController: PopoverController) { }

  ngOnInit() {
    this.filtro && this.filtro.length > 0? this.selectFiltroText = this.filtro : null; 
  }

  setFiltro(filtro: string){
    this.selectFiltroText = filtro;
    this.filtrarBandeja();
  }

  filtrarBandeja(){
    this.PopoverController.dismiss(this.selectFiltroText);
  }
}

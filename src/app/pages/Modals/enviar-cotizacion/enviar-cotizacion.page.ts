import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';
import { Cliente } from 'src/app/interfaces/cliente/cliente';

@Component({
  selector: 'app-enviar-cotizacion',
  templateUrl: './enviar-cotizacion.page.html',
  styleUrls: ['./enviar-cotizacion.page.scss'],
})
export class EnviarCotizacionPage implements OnInit {
  @Input() orden: any
  correoForm: FormGroup;
  html = '';
  vendedor: Vendedor;
  tipoDocumento: string = '';
  cliente : Cliente;

  constructor(
    private formBuilder: FormBuilder,
    public modalController: ModalController,
    private dataLocal : DataLocalService,
    private vendedorService:VendedorService) {
    this.correoForm = this.formBuilder.group({
      para: new FormControl('', [Validators.required, ,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      cc: new FormControl(''),
      cuerpo: new FormControl(''),
    });
  }

  async ngOnInit() {
    this.obtenerUsuario();

    // Obtenemos el tipo de documento.
    this.orden.folio.split('-')[0]  === 'CO'? this.tipoDocumento = 'Cotización' :  this.tipoDocumento = 'Orden de Venta';

    this.cliente = await this.dataLocal.getCliente();

    // Seteamos el correo del cliente.
    let correo = this.cliente.correos[0] && this.cliente.correos[0].correo  ? this.cliente.correos[0].correo : '';
    this.correoForm.get("para").setValue(correo);
  }

  async enviarCotizacion() {
    
    const { para, cc, bcc, cuerpo } = this.correoForm.value;

    // Construimos el url
    let url = `https://replicacion.implementos.cl/apicarro/api/carro/documentos?numero=${window.btoa(this.orden.folio)}`

    let parametros = {
      "tipo": this.tipoDocumento.trim(),
      "email": String(para).toLowerCase().trim(),
      "bbc": String(cc).toLowerCase().trim(),
      "cliente": this.orden.nombreCliente.trim(),
      "folio": this.orden.folio.trim(),
      "cuerpo": cuerpo.trim(),
      "vendedor": this.vendedor.nombre.trim(),
      "link": url.trim(),
    }

    let respuesta = await this.vendedorService.enviarCotizacion(parametros);

   if(respuesta && !respuesta.error && respuesta.msg === 'ok'){
      this.cerrarModal();
      this.dataLocal.presentToast('Se envio con exito la cotizacion','success')
    }else{
      this.dataLocal.presentToast('No se logro enviar la cotización','danger')
    }
  }

   /**
   *permite cerrar el modal
   * @memberof ComentarioPage
   */
  cerrarModal() {

    this.modalController.dismiss();
  }

  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
    this.vendedor = JSON.parse(usuario);
  }
}

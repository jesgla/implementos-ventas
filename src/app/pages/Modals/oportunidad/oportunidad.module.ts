import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { IonicModule } from '@ionic/angular';

import { OportunidadPage } from './oportunidad.page';

const routes: Routes = [
  {
    path: '',
    component: OportunidadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
  ],
  declarations: [OportunidadPage],
})
export class OportunidadPageModule {}

import { Component, OnInit, Output, Input, EventEmitter} from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

// servicios
import { OportunidadesService } from 'src/app/services/oportunidades/oportunidades.service';

//interfaces
import { oportunidades_conceptos, Oportunidad } from 'src/app/interfaces/oportunidades/oportunidades';

@Component({
  selector: 'app-oportunidad',
  templateUrl: './oportunidad.page.html',
  styleUrls: ['./oportunidad.page.scss'],
})
export class OportunidadPage implements OnInit {
  @Output() oportunidadEvent = new EventEmitter;
  @Input() checkIn: boolean = false;
  @Input() nombreCliente: string;
  @Input() rutCliente: string;
  @Input() oportunidad: any;
  @Input() rutVendedor: string;
  @Input() codEmpleado: string;
  @Input() zonaVendedor: string;
  @Input() sucursalVendedor: string;
  @Input() vendedorNombre: string;

  oportunidadForm : FormGroup;
  showMontoPresup: boolean = false;
  oportunidadConceptos: oportunidades_conceptos[] = [];
  minDate: string;

  constructor(private router: Router,
    public modalController: ModalController,
    public alertController: AlertController,
    private formBuilder: FormBuilder,
    private _oportunidades: OportunidadesService) 
    { 
      let fecha = new Date();
      this.minDate = fecha.getUTCFullYear()+'-'+("0" + (fecha.getMonth() + 1)).slice(-2)+'-'+("0" + fecha.getDate()).slice(-2);

      this.oportunidadForm = this.formBuilder.group({
        tipoOportunidad: [null, Validators.required],
        montoMagnitud: [null, Validators.required],
        fechaLimite: [null, Validators.required],
        probabilidad: [null, Validators.required],
    });  
  }

  async ngOnInit() {
    this.oportunidadConceptos = await this._oportunidades.getOportunidadesConceptos('creacion');
    await new Promise(resolve => setTimeout(resolve, (700)));
    this.oportunidad? this.setOportunidad() : null;
  }

  guardarOportunidad(){
    let { tipoOportunidad, montoMagnitud, fechaLimite, probabilidad, ventaRecurrente } = this.oportunidadForm.value;

    let oportunidad: Oportunidad = {
      id_visita: null,
      monto_magnitud: montoMagnitud? montoMagnitud : null,
      concepto_creacion: tipoOportunidad? tipoOportunidad.trim() : null,
      concepto_cierre: null,
      fecha_limite: fechaLimite? fechaLimite: null,
      fecha_cierre: null,
      fecha_creacion: new Date(),
      rutVendedor: this.rutVendedor? this.rutVendedor.replace('.','').replace('.','') : null,
      rutCliente: this.rutCliente? this.rutCliente.replace('.','').replace('.','') : null,
      clienteNombre: this.nombreCliente? this.nombreCliente : null,
      OportunidadOpen: true,
      probabilidad: probabilidad? probabilidad.trim() : null,
      oportunidadConcretada: null,
      zonaVendedor: this.zonaVendedor? this.zonaVendedor : null,
      sucursalVendedor: this.sucursalVendedor? this.sucursalVendedor : null,
      notaVenta: null,
      productos: null,
      codEmpleado: this.codEmpleado,
      vendedorNombre: this.vendedorNombre,
    }
    this.oportunidadEvent.emit(oportunidad);
    this.cerrarModal();
  }

  cerrarModal() {
    this.modalController.dismiss();
  }

  /**
  * @author ignacio zapata  \"2020-11-18\
  * @desc Funcion utilizada para setear la oportunidad cuando viene desde el input.
  * @params 
  * @return
  */
  setOportunidad(){
    this.oportunidadForm.controls['tipoOportunidad'].setValue(this.oportunidad.concepto_creacion);
    this.oportunidadForm.controls['montoMagnitud'].setValue(this.oportunidad.monto_magnitud);
    this.oportunidadForm.controls['fechaLimite'].setValue(this.oportunidad.fecha_limite);
    this.oportunidadForm.controls['probabilidad'].setValue(this.oportunidad.probabilidad);
  }

}

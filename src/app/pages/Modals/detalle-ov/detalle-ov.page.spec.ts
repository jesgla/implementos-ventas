import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleOVPage } from './detalle-ov.page';

describe('DetalleOVPage', () => {
  let component: DetalleOVPage;
  let fixture: ComponentFixture<DetalleOVPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleOVPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleOVPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

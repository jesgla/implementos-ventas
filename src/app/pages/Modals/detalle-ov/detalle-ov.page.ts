import { Component, OnInit, Input } from '@angular/core';
import { DetalleArticuloPage } from '../detalle-articulo/detalle-articulo.page';
import { CotizacionPage } from '../cotizacion/cotizacion.page';
import { Events, ModalController, AlertController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Articulo } from 'src/app/interfaces/articulo/articulo';
import { ClienteService } from 'src/app/services/cliente/cliente.service';

import { Router } from '@angular/router';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { EnviarCotizacionPage } from '../enviar-cotizacion/enviar-cotizacion.page';

@Component({
  selector: 'app-detalle-ov',
  templateUrl: './detalle-ov.page.html',
  styleUrls: ['./detalle-ov.page.scss'],
})
export class DetalleOVPage implements OnInit {

  /**
   * listado con articulos
   * @type {Articulo[]}
   * @memberof CarroCompraPage
   */
  carro: Articulo[];

  /**
   *Monto de los articulos cotizados
   * @type {number}
   * @memberof CarroCompraPage
   */
  total: number;

  /**
   *Monto sin iva de los articulos cotizados
   * @type {number}
   * @memberof CarroCompraPage
   */
  subTotal: number;

  /**
   *Monto del IVA
   * @type {number}
   * @memberof CarroCompraPage
   */
  iva: number;
  /**
    *objeto articulo
    * @type {Articulo}
    * @memberof DetalleArticuloPage
    */
  @Input() orden: any;
  detallePedido: any;
  articulo: Articulo;
  vendedor: any;
  tipoDocumento: string = '';
  ordenC: string = '';

  /**
   *Creates an instance of CarroCompraPage.
   * @param {ModalController} modalController
   * @param {DataLocalService} dataLocal
   * @memberof CarroCompraPage
   */
  constructor(
    public events: Events,
    public modalController: ModalController,
    private dataLocal: DataLocalService,
    public alertController: AlertController,
    private router: Router,
    private vendedorService: VendedorService,
    private clienteService: ClienteService) {

      this.carro = [];
  }

  /**
   *Metodo que permite ejecutar funciones al inicio del componente
   *
   * @memberof CarroCompraPage
   */
  async ngOnInit() {

    this.obtenerUsuario();

    this.total = Math.round(this.orden.Venta * 1.19);
    this.subTotal = this.orden.Venta;
    this.iva = Math.round( this.total - this.subTotal);
    this.orden.folio.split('-')[0]  === 'CO'? this.tipoDocumento = 'Cotización' :  this.tipoDocumento = 'Orden de Venta';
 
    this.detallePedido = await this.clienteService.obtenerDetallePedido(this.orden.folio);
    this.generarArreglo(this.detallePedido);  
  }

  /**
   *obtener usuario logueado
   * @memberof CotizacionPage
   */
  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
    this.vendedor = JSON.parse(usuario);

  }

  generarArreglo(data) {
    this.carro = [];
    data.map(async art => {
      this.articulo = new Articulo();
      this.articulo.sku = art.sku;

      this.articulo.p_minimo = String(Math.round(art.precio / 1.19 ))
      this.articulo.cantidad = art.cantidad;

      await this.carro.push(this.articulo);
    });
    //this.calcularMontos(this.carro)
  }

  /**
   * Permite calcular el monto de los productos cargados al carro
   * @param {*} carro
   * @memberof CarroCompraPage
   */
  calcularMontos(carro) {
    this.total = 0;
    this.subTotal = 0;
    this.iva = 0;
    carro.map(articulo => {

      let precioSIva = Math.round(Number(articulo.p_minimo) / 1.19);
      
      this.subTotal += Math.round(precioSIva * articulo.cantidad);

    })

    this.total = Math.round(this.subTotal + this.iva);
  }

  /**
   * Permite eliminar elementos del carro de compra
   * @param {*} articulo
   * @memberof CarroCompraPage
   */
  async eliminarArticulo(articulo) {
    this.events.publish('actualizar', true);
    await this.dataLocal.eliminarArticulo(articulo.sku);


  }

  /**
   *permite cerrar el modal
   * @memberof DetalleArticuloPage
   */
  async cerrarModal() {

    await this.modalController.dismiss({
      recargar: false
    });
  }
  async convertir() {
     if( this.orden.estado!=="ENVIADO"  ){
      this.dataLocal.presentToast('No se puede convertir esta cotización', 'danger');
      return
    } 
    const alert = await this.alertController.create({
      header: 'Confirmación!',
      message: '¿Desea convertir la cotización en orden de venta?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

            this.modalController.dismiss();
          }
        }, {
          text: 'Aceptar',
          handler: async () => {
       
            let folio =[{resultado:this.orden.folio }];
            let convertir = await this.vendedorService.convertirCotizacion(this.vendedor,folio );
           
            if (convertir) {
              this.dataLocal.presentToast('Cotización convertida exitosamente', 'success');
              await this.modalController.dismiss({
                recargar: true
              });
            }
         

          }
        }
      ]
    });

    await alert.present();

  }

  async enviarCotizacion(){
    const modal = await this.modalController.create({
      component: EnviarCotizacionPage,
      componentProps: {
        orden:this.orden
      }
    });
    return await modal.present();
  }
}

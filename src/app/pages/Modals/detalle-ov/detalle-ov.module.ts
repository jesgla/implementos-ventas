import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetalleOVPage } from './detalle-ov.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { EnviarCotizacionPage } from '../enviar-cotizacion/enviar-cotizacion.page';
import { EnviarCotizacionPageModule } from '../enviar-cotizacion/enviar-cotizacion.module';


@NgModule({
  entryComponents:[EnviarCotizacionPage],
  imports: [
    ComponentsModule,
    PipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    EnviarCotizacionPageModule
  ],
  declarations: [DetalleOVPage]
})
export class DetalleOVPageModule {}

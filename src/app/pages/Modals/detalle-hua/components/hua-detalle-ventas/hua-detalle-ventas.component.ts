import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

// services
import { ArticulosService } from 'src/app/services/articulos/articulos.service';
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';

// interfaces 
import { detalleHua } from 'src/app/interfaces/hua/hua';

@Component({
  selector: 'app-hua-detalle-ventas',
  templateUrl: './hua-detalle-ventas.component.html',
  styleUrls: ['../../detalle-hua.page.scss'],
})
export class HuaDetalleVentasComponent implements OnInit {
  articulosRelacionados: detalleHua[] = [];

  @Input() detalleHua;
  @Input() sucursal;
  @Input() mostrarRelacionados: boolean = false;
  @Input() showingItemRelacionado: boolean = false;
  @Output() closeDetail = new EventEmitter();
  @Output() ocultarDetails = new EventEmitter();
  @Output() ShowDetalleRelacionado = new EventEmitter();
  
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    slidesPerView: 3,
    freeMode: true,
  };
  
  constructor(private _articuloService: ArticulosService, public _estadoTiendaService: EstadoTiendaService) { }

  ngOnInit() {
    this.getProductosRelacionados();
  }

  closeDetailFunction(){
    this.closeDetail.emit();
  }

  ocultarDetalles(){
    this.ocultarDetails.emit();
  }

  /**
  * @author ignacio zapata  \"2020-09-02\
  * @desc Metodo que permite obtener los estados de los articulos asociados a los productos de la matriz del art. consultado.
  * @params 
  * @return
  */
  async getProductosRelacionados(){
    let arreglo: string[] = [];
    this.articulosRelacionados = [];

    arreglo = await this._articuloService.getMatrizProducto(this.detalleHua.sku);
    
    console.log(this.sucursal);

    if(arreglo && arreglo.length > 0){
       arreglo.forEach( async item => {
        let detalleHuaRelacionado =  await this._estadoTiendaService.getDetalleHua(item, this.sucursal);

        if(detalleHuaRelacionado && detalleHuaRelacionado.sku){
          detalleHuaRelacionado.img = `https://images.implementos.cl/img/150/${detalleHuaRelacionado.sku}-1.jpg`;
          
          this.articulosRelacionados.push(detalleHuaRelacionado);
        }else{
          console.log("No se ha encontrado el articulo ingresado.");
        }
      })
    }
  }

  /**
  * @author ignacio zapata  \"2020-09-02\
  * @desc Metodo que permite ejecutar el cambio de datos entre el articulo consultado y el articulo relacionado seleccionado.
  * @params 
  * @return
  */
  mostrarDetalleRelacionado(articulo: detalleHua){
    this.ShowDetalleRelacionado.emit(articulo);
  }
}

// angular
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

// ionic
import { ModalController, AlertController, Events } from '@ionic/angular';

// interfaces 
import { detalleHua } from 'src/app/interfaces/hua/hua';

// Service
import { EstadoTiendaService } from 'src/app/services/hua/estadoTienda.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-detalle-hua',
  templateUrl: './detalle-hua.page.html',
  styleUrls: ['./detalle-hua.page.scss'],
})
export class DetalleHuaPage implements OnInit, OnDestroy{

  @Input() detalleHua: detalleHua;
  @Input() sucursal: string;
  @Input() scanProduct: boolean;

  // Variable utilizada para guardar los datos del producto consultado ,cuando se desea mostrar el detalle de un producto de la matriz.
  detalleHuaProdConsultado: detalleHua; 

  showDetalleVentas: boolean = false;
  showItemRelacionado: boolean = false;
  showMatrizLIst: boolean = false;


  constructor(  public modalController: ModalController,
                public alertController: AlertController,
                public _EstadoTiendaService: EstadoTiendaService,
                private _loadingService: LoadingService) { }

  async ngOnInit() {
    this.detalleHuaProdConsultado = this.detalleHua;
  }

  ngOnDestroy(){
    this.resetProducto();
  }

  async cerrarModal() {
    if(!this.showItemRelacionado){
      await this.modalController.dismiss({
        reset: true
      });
    }else{
      this.ocultarProductoRelacionado();
    }
  }

  /**
  * @author ignacio zapata  \"2020-09-02\
  * @desc Metodo utilizado para ocultar los datos de un producto relacionado.
  * @params 
  * @return
  */
  ocultarProductoRelacionado(){
    this.showMatrizLIst = true;
    this.detalleHua = this.detalleHuaProdConsultado;
    this.showItemRelacionado = false;
    this.showDetalleVentas = true;
  }

  /**
  * @author ignacio zapata  \"2020-09-02\
  * @desc Permite resetear las variables del modal.
  * @params 
  * @return
  */
  resetProducto(){
    this.detalleHua = null;
    this.showDetalleVentas = false;
    this.showItemRelacionado = false;
    this.detalleHuaProdConsultado = null;
  }

  /**
  * @author ignacio zapata  \"2020-09-02\
  * @desc Se ejecuta cuando desde el detalle del hua, se selecciona un producto relacionado y este metodo permite mostrar los datos de ese producto desde el mismo modal.
  * @params DetalleHua
  * @return
  */
  async showRelacionado(articulo: detalleHua){
    await this._loadingService.presentLoading("Cargando Producto Relacionado..");
    this.showItemRelacionado = true;
    this.showDetalleVentas = false;
    this.detalleHuaProdConsultado = this.detalleHua;
    this.detalleHua = articulo;
    setTimeout( () => { this._loadingService.hideLoading(); }, 1000);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { IonicModule } from '@ionic/angular';

import { DetalleHuaPage } from './detalle-hua.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { HuaDetalleVentasComponent } from './components/hua-detalle-ventas/hua-detalle-ventas.component';

@NgModule({
  imports: [
    PipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [DetalleHuaPage, HuaDetalleVentasComponent]
})
export class DetalleHuaPageModule {}

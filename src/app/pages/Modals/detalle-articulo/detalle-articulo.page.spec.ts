import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleArticuloPage } from './detalle-articulo.page';

describe('DetalleArticuloPage', () => {
  let component: DetalleArticuloPage;
  let fixture: ComponentFixture<DetalleArticuloPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleArticuloPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleArticuloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

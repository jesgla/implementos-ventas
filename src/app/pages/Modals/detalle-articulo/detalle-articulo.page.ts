import { Component, OnInit, Input } from '@angular/core';
import { Articulo } from 'src/app/interfaces/articulo/articulo';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor'
import { ModalController, AlertController, Events } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';
import { ToastController, IonSlides } from '@ionic/angular';

/**
 *permite ver el detalle del articulo
 * @export
 * @class DetalleArticuloPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-detalle-articulo',
  templateUrl: './detalle-articulo.page.html',
  styleUrls: ['./detalle-articulo.page.scss'],
})
export class DetalleArticuloPage implements OnInit {

  /**
   *objeto articulo
   * @type {Articulo}
   * @memberof DetalleArticuloPage
   */
  nuevoArticulo: Articulo;

  /**
   *objeto articulo
   * @type {Articulo}
   * @memberof DetalleArticuloPage
   */
  @Input() articulo: Articulo;

  /**
   *estado para habilitar boton cotizar
   * @type {boolean}
   * @memberof DetalleArticuloPage
   */
  @Input() boton: boolean;

  /**
   *arreglo de articulos
   * @memberof DetalleArticuloPage
   */
  @Input() obtenerCarro;

  /**
 *arreglo de articulos
  * @memberof DetalleArticuloPage
  */
  @Input() scanProducto;

  /**
   *cantidad de productos
   * @type {number}
   * @memberof DetalleArticuloPage
   */
  @Input() cantidad: number;

  /**
   *precio venta del vendedor
   * @type {number}
   * @memberof DetalleArticuloPage
   */
  precioVenta: number;

  /**
   *permite agregar o quitar productos
   * @type {boolean}
   * @memberof DetalleArticuloPage
   */
  estadoCantidad: boolean;

  /**
   *estado del cliente
   *
   * @type {boolean}
   * @memberof DetalleArticuloPage
   */
  clienteSeleccionado: boolean;

  /**
   * propiedad en donde se seteara el stock de las demas tiendas
   * @type {*}
   * @memberof DetalleArticuloPage
   */
  stockTiendas: any;

    /**
   * propiedad que permite ocultar o mostrar el stock de las otras tiendas
   * @type {boolean}
   * @memberof DetalleArticuloPage
   */
  verStockTiendas: boolean = false;

  /**
   *Creates an instance of DetalleArticuloPage.
   * @param {ModalController} modalController
   * @param {AlertController} alertController
   * @param {DataLocalService} dataLocal
   * @param {ArticulosService} articuloService
   * @memberof DetalleArticuloPage
   */
  constructor(
    public events : Events,
    public modalController: ModalController,
    public alertController: AlertController,
    private dataLocal: DataLocalService,
    private articuloService: ArticulosService,
    public toastController: ToastController,) {

    this.estadoCantidad = true;
    this.clienteSeleccionado = true;
  }

  /**
   * booleano para esconder o mostrar precios escala
   * @type {boolean}
   * @memberof DetalleArticuloPage
   */
  showPrecioEscala: boolean = false;

  /**
   *  array donde se guardan precios escalas
   * @type {Array}
   * @memberof DetalleArticuloPage
   */
    preciosEscala: any[] = [];

  /**
   *  array donde se guardan precios escalas
   * @type {number}
   * @memberof DetalleArticuloPage
   */
  precioEscala: number;

  /**
   *metodo que se ejecuta al iniciar el componente
   *
   * @memberof DetalleArticuloPage
   */
  ngOnInit() {

    this.precioEscala = this.articulo.precioNeto;
    this.ValidarCliente();
    this.precioVenta = this.articulo.precio.precio;
    this.obtenerStock();

    // primero validamos si el producto existe o no en el carro.
    this.valItemOnCart();

    if(this.articulo.precio_escala){
      this.getListaPreciosEscala();
    }
  }

  /**
   *permite obtener stock por almacen
   * @memberof DetalleArticuloPage
   */
  async obtenerStock() {
    let parametros = {
      sku: this.articulo.sku
    }
    this.stockTiendas = await this.articuloService.obtenerStockTiendas(parametros);

  }

  /**
   *permite validar si selecciono un cliente
   * @memberof DetalleArticuloPage
   */
  async ValidarCliente() {
    let cliente = await this.dataLocal.getCliente();
   

    if (!cliente) {
      this.clienteSeleccionado = false
    }

  }

  /**
   *permite cerrar el modal
   * @memberof DetalleArticuloPage
   */
  async cerrarModal() {

    await this.modalController.dismiss({
      recargar: false
    });
  }

  /**
   *permite saber la cantidad de articulos a agregar
   * @param {*} estado
   * @memberof DetalleArticuloPage
   */
  cantidadArt(estado) {
    this.estadoCantidad = estado;

    if (this.estadoCantidad) {
      this.cantidad ++;
    } else {
      this.cantidad --;
    }
  }

  /**
   *permite cambiar el precio del producto
   * @memberof DetalleArticuloPage
   */
  async cambiarPrecio() {
   /*  const alert = await this.alertController.create({
      header: 'Ingrese precio acordado neto',
      inputs: [
        {
          name: 'precio',
          type: 'number',
          placeholder: this.articulo.p_minimo,
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
        
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            if(data.precio >= this.articulo.p_minimo ){
              this.precioVenta = data.precio;
            }else{
              this.alertPrecioMinimo();
            }
          }
        }
      ]
    });

    await alert.present(); */
  }

  /**
   *permite agregar al carro el producto
   * @memberof DetalleArticuloPage
   */
  async agregarCarro() {

    if (this.cantidad <= 0) {
      this.dataLocal.presentToast('Ingrese cantidad mayor a 0','danger')
      return
    }

    this.articulo.cantidad = this.cantidad;
    this.articulo.precioEscaladoNeto = this.precioEscala;

    await this.dataLocal.setCarroCompra(this.articulo, this.estadoCantidad);
    this.modalController.dismiss({
      recargar: true
    });

  }

  /**
   *permite validar si desea eliminar un producto
   * @param {*} articulo
   * @memberof DetalleArticuloPage
   */
  async confirmarEliminacion(articulo) {
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: '¿Desea eliminar del carro?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
          cssClass: 'secondary',
          handler: (blah) => {
  
          }
        }, {
          text: 'Confirmar',
          handler: () => {
          
            this.eliminarArticulo(articulo)
          }
        }
      ]
    });

    await alert.present();
  }

  /**
   *elimina un articulo del carro
   *
   * @param {*} articulo
   * @memberof DetalleArticuloPage
   */
  async eliminarArticulo(articulo) {
    await this.dataLocal.eliminarArticulo(articulo.sku);
    this.events.publish('actualizar',true);
    this.modalController.dismiss({
      recargar: true
    });
  }

  async alertPrecioMinimo(){
    const toast = await this.toastController.create({
      message: 'El precio ingresado no puede ser menor al precio minimo.',
      duration: 2000,
      color: 'danger',
      animated: true,
      position: 'top',

    });
    toast.present();
  }

  
  /**
   *obtiene un array con todos los precios escala posibles para el articulo
   *
   * @memberof DetalleArticuloPage
   */
  async getListaPreciosEscala(){

    let vendedor: Vendedor = JSON.parse( await this.dataLocal.getItem('auth-token'));
  
    let consulta: any = await this.articuloService.getListPreciosEscala(this.articulo.sku, vendedor.codBodega);
    if(!consulta.error){
      if(consulta.data.length > 0){
        this.preciosEscala = consulta.data;
      }
    } 
  }

  
  /**
   *obtiene el precio escala para la cantidad ingresada
   *
   * @memberof DetalleArticuloPage
   */
  async getPrecioEscala(){
    if(this.articulo.precio_escala){
      let vendedor: Vendedor = JSON.parse( await this.dataLocal.getItem('auth-token'));

      let consulta: number = await this.articuloService.getPrecioEscala(this.articulo.sku, vendedor.codBodega, this.cantidad);
      if(consulta){
        if(this.articuloService.getSinIva(consulta) < this.articulo.precioNeto){
          this.precioEscala = this.articuloService.getSinIva(consulta);
        }else{
          this.precioEscala = this.articulo.precioNeto;
        }
      }
    } 
  }

/**
 * Metodo que permite validar si el producto ya existe en el carro, si existe, se reemplaza por el que esta en el carro.
 *
 * @memberof DetalleArticuloPage
 */
  async valItemOnCart(){
    let consulta: any = await this.dataLocal.valItemOnCart(this.articulo);
    if(consulta > 1){
      this.cantidad = consulta;
      if(this.articulo.precio_escala){
        this.getPrecioEscala();
      }
    }
  }

  getPrecioLength(precio , cantidad, iva?: string){
    iva? precio = Math.round( precio * 1.19 ) : precio = precio;
    let total: string = String(precio * cantidad);
    return total.length;
  }
}

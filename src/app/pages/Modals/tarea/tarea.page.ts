import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
/**
 *componente tareas
 * @export
 * @class TareaPage
 */
@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.page.html',
  styleUrls: ['./tarea.page.scss'],
})
export class TareaPage {
  /**
   *formulario tarea
   * @type {FormGroup}
   * @memberof TareaPage
   */
  tareaForm: FormGroup;

  /**
   *arreglo usuario
   * @type {any[]}
   * @memberof TareaPage
   */
  users: any[] = [
    {
      id: 1,
      first: 'Alice',
      last: 'Smith',
    },
    {
      id: 2,
      first: 'Bob',
      last: 'Davis',
    },
    {
      id: 3,
      first: 'Charlie',
      last: 'Rosenburg',
    }
  ];

  /**
   *Creates an instance of TareaPage.
   * @param {ModalController} modalController
   * @param {FormBuilder} formBuilder
   * @memberof TareaPage
   */
  constructor(public modalController: ModalController, private formBuilder: FormBuilder) {
    this.tareaForm = this.formBuilder.group({
      asignar: ['', Validators.required],
      observacion: ['', Validators.required],
      fecha: ['', Validators.required],
    });
  }

  /**
   *permite cerrar modal
   * @memberof TareaPage
   */
  cerrarModal() {
    this.modalController.dismiss();
  }

  /**
   *permite obtener datos del formulario
   * @memberof TareaPage
   */
  logForm() {

  }

  /**
   *funcion para filtrar opcion de usuario
   * @memberof TareaPage
   */
  compareWithFn = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  };

  /**
   *filtro
   * @memberof TareaPage
   */
  compareWith = this.compareWithFn;

}

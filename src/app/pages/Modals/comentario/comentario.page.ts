import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventoService } from 'src/app/services/evento/evento.service';
import { Evento } from 'src/app/interfaces/evento/evento';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { Subscription } from 'rxjs';


/**
 *permite generar un comentario del check-in
 * @export
 * @class ComentarioPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-comentario',
  templateUrl: './comentario.page.html',
  styleUrls: ['./comentario.page.scss'],
})
export class ComentarioPage implements OnInit {
  /**
   *objeto cliente
   * @type {Cliente}
   * @memberof ComentarioPage
   */
  @Input() cliente: Cliente;
  @Input() comentarioAnterior: string;

    /**
   *objeto boolean
   * @type {boolean}
   * @memberof ComentarioPage
   */
  @Input() checkIn: boolean = false;

  /**
 *objeto Output
  * @type {Output}
  * @memberof ComentarioPage
  */
  @Output() ComentarioEvent = new EventEmitter;

  /**
   *formulario cliente
   * @type {FormGroup}
   * @memberof ComentarioPage
   */
  comentarioForm: FormGroup;

  /**
   *objeto evento
   * @type {Evento}
   * @memberof ComentarioPage
   */
  registrarComentario: Evento;

  /**
   *arreglo de comentarios
   * @memberof ComentarioPage
   */
  autoComentario = [
    { comentario: 'Cliente no esta disponible' },
    { comentario: 'Cliente no necesita nada' },
    { comentario: 'Cliente compro en la competencia' },
  ];

  /**
   *id de referencia check-in
   * @type {*}
   * @memberof ComentarioPage
   */
  idReferencia: any;
  vendedor: any;

  /**
 *id de referencia check-in
  * @type {Subscription}
  * @memberof ComentarioPage
  */
  ComentarioCheckin$: Subscription;

  /**
   *Creates an instance of ComentarioPage.
   * @param {ModalController} modalController
   * @param {FormBuilder} formBuilder
   * @param {EventoService} eventoService
   * @param {ToastController} toastController
   * @param {DataLocalService} dataLocal
   * @memberof ComentarioPage
   */
  constructor(public modalController: ModalController,
    private formBuilder: FormBuilder,
    private eventoService: EventoService,
    public toastController: ToastController,
    private dataLocal: DataLocalService) {
    this.comentarioForm = this.formBuilder.group({
      comentario: ['', Validators.required]
    });

    this.registrarComentario = new Evento();
  }

  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof ComentarioPage
   */
  async ngOnInit() {
    this.idReferencia = await this.dataLocal.getItem('referencia');
    // Validamos si viene un comentario desde input comentarioAnterior, si es asi entonces lo asignamos al formulario.
    this.comentarioAnterior && this.comentarioAnterior.length > 0 ? this.comentarioForm.controls['comentario'].setValue(this.comentarioAnterior) : null;
  }

  /**
   *permite cerrar el modal
   * @memberof ComentarioPage
   */
  cerrarModal() {
    this.modalController.dismiss();
  }

  /**
   *permite obtener datos del formulario
   * @memberof ComentarioPage
   */
  async logForm(emitEvent = false) {
    if(this.checkIn && !emitEvent){
      this.ComentarioEvent.emit(this.comentarioForm.get('comentario').value);
      this.cerrarModal();
    }else{
      let usuario = await this.dataLocal.getItem('auth-token');
      this.vendedor = JSON.parse(usuario);
      this.registrarComentario.tipoEvento = 'COMENTARIO';
      this.registrarComentario.fechaCreacion = new Date().toString();
      this.registrarComentario.fechaEjecucion = new Date().toString();
      this.registrarComentario.fechaProgramada = "";
      this.registrarComentario.observaciones = this.comentarioForm.get('comentario').value;
      this.registrarComentario.rutEmisor = this.vendedor.rut.replace('.','').replace('.','');
      this.registrarComentario.rutCliente = this.cliente.rut;
      this.registrarComentario.rutVendedor = this.vendedor.rut.replace('.','').replace('.','');
      this.registrarComentario.estado = "";
      this.registrarComentario.refVisita = this.idReferencia;
      this.registrarComentario.referenciaEvento = this.idReferencia

      let respuesta = await this.eventoService.generarEvento(this.registrarComentario);
      if (respuesta[0].resultado) {
        if(!emitEvent){
          await this.dataLocal.presentToast('Comentario regitrado exitosamente','success')
          this.cerrarModal();
        }
      } else {
        await this.dataLocal.presentToast('No se logro registrar comentario','danger')
      }
    }
  }


  /**
   *permite agregar calendario
   * @param {*} texto
   * @memberof ComentarioPage
   */
  addComentario(texto) {
    let comentario = this.comentarioForm.get('comentario').value;
    comentario += ` ${texto.comentario}`;
    this.comentarioForm.controls['comentario'].setValue(comentario)
  }  
}

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 *componente que permite reagendar visitas
 * @export
 * @class ReAgendarPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-re-agendar',
  templateUrl: './re-agendar.page.html',
  styleUrls: ['./re-agendar.page.scss'],
})
export class ReAgendarPage {
  /**
   *formulario agenda
   * @type {FormGroup}
   * @memberof ReAgendarPage
   */
  reAgendarForm: FormGroup;

  /**
   *Creates an instance of ReAgendarPage.
   * @param {ModalController} modalController
   * @param {FormBuilder} formBuilder
   * @memberof ReAgendarPage
   */
  constructor(public modalController: ModalController, private formBuilder: FormBuilder) {
    this.reAgendarForm = this.formBuilder.group({
      comentario: ['', Validators.required],
      fecha: ['', Validators.required]
    });
  }

  /**
   *permite cerrar el modal
   * @memberof ReAgendarPage
   */
  cerrarModal() {
    this.modalController.dismiss();
  }

  /**
   *permite obtener datos del formulario
   * @memberof ReAgendarPage
   */
  logForm() {

  }
}

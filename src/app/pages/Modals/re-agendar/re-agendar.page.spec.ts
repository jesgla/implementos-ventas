import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReAgendarPage } from './re-agendar.page';

describe('ReAgendarPage', () => {
  let component: ReAgendarPage;
  let fixture: ComponentFixture<ReAgendarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReAgendarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReAgendarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

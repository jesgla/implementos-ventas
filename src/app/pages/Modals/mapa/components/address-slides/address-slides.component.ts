import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import { Platform, IonSlides } from '@ionic/angular';

// Interfaces
import { Cliente } from 'src/app/interfaces/cliente/cliente';

@Component({
  selector: 'app-address-slides',
  templateUrl: './address-slides.component.html',
  styleUrls: ['./address-slides.component.scss'],
})
export class AddressSlidesComponent implements OnInit, OnChanges {;
  @Input() Clientes: Cliente[] = [];
  @Input() showFiltros: boolean = false;
  @Output() ocultarFiltro = new EventEmitter();
  @Output() selectedCliente = new EventEmitter();
  @Output() resetViewEvent = new EventEmitter();

  listaClientes: Cliente[] = [];

  busqueda: string = '';

  slideOpts = {
    freeMode: true,
    slidesPerView: 1,
    spaceBetween: 10,
    // Responsive breakpoints
    breakpoints: {
      // when window width is >= 578px
      578: {
        slidesPerView: 2,
        spaceBetween: 5
      },
      // when window width is >= 1024px
      1024: {
        slidesPerView: 4,
        spaceBetween: 20
      },
    }
  };

  constructor(private platform: Platform) { }

  ngOnInit() {
    this.platform.resize.subscribe(async () => {
      if(this.platform.width() > 568){
        console.log("entre al resize", this.platform.width());
      }
    });
  }

  ngOnChanges(changes: SimpleChanges){
    if(changes.Clientes){
      this.Clientes = changes.Clientes.currentValue;
      this.listaClientes = this.Clientes;
    }
  }

  /**
  * @author ignacio zapata  \"2020-09-03\
  * @desc metodo que permite filtrar los cards asociados a los clientes.
  * @params string obtenido desde la searchbar de los filtros
  * @return
  */
  filtrar(event: string){
    if(event.length == 0){
      this.listaClientes = this.Clientes;
    }else{
      this.listaClientes = this.Clientes.filter(cliente => {
        if(cliente.nombre){
          return cliente.nombre.toLowerCase().match(event.toLowerCase())
        }else{
          return false;
        }
      })
    }
  }

  /**
  * @author ignacio zapata  \"2020-09-03\
  * @desc Metodo que permite ocultar el div que muestra los cards de las direcciones de los clientes.
  * @params 
  * @return
  */
  OcultarFiltro(){
    this.showFiltros = false;
    this.ocultarFiltro.emit();
  }

  /**
  * @author ignacio zapata  \"2020-09-03\
  * @desc Metodo utilizado para emitir evento que permitirá mover el mapa hacia la direccion del cliente.
  * @params 
  * @return
  */
  MostrarAddress(cliente: Cliente){
    this.selectedCliente.emit(cliente);
  }

  /**
  * @author ignacio zapata  \"2020-09-07\
  * @desc Metodo que permite resetear la vista del mapa desde la ventana de filtros.
  * @params 
  * @return
  */
  resetView(){
    this.resetViewEvent.emit();
  }



}

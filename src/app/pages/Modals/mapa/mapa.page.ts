import { Component, OnInit, ChangeDetectorRef, NgZone, OnDestroy, ViewChild, EventEmitter, Input} from '@angular/core';
import { MapsAPILoader, AgmMap} from '@agm/core';
import { Observable, of , Subscription } from 'rxjs';
import { from } from 'rxjs';
import {  tap, map} from 'rxjs/operators';


// Servicios
import { GeoLocationService } from 'src/app/services/geo-location/geo-location.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

// ionic
import { ModalController, AlertController, NavController } from '@ionic/angular';

// interfaces 
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';

declare var google: any; 
@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})
export class MapaPage implements OnInit, OnDestroy {
  @ViewChild('map', {static: false}) map: AgmMap; 
  @Input() tipoSolicitud: string;
  @Input() rutVendedor: string;
  @Input() nombreVendedor: string;
  @Input() checkIns: any[];
  @Input() sucursal: string;

  geocoder: any;
  lat: number = -39.088320814101984;
  lng: number =  -70.73097506832782;
  latUser: number = 0;
  lngUser: number = 0;
  zoom: number = 4;
  title: string = 'Mapa Clientes';
  showSearchBar: boolean = true;
  autocompletado: boolean = true;
  mapready: boolean = false;
  searchFilter: boolean = false;
  tiendasImplementos : any[] = [];

  labelOptions = {
    color: 'white',
    fontFamily: '',
    fontSize: '14px',
    fontWeight: 'bold',
    text: "some text"
}

  clientes: Cliente[];
  vendedor: Vendedor;

  direcciones: any[] = [];
  
  Vendedoricon = { url: '/assets/svg/vendedor.svg', scaledSize: {height: 40, width: 40}};
  Sucursalicon = { url: '/assets/logos/implementos-logo2.png', scaledSize: {height: 35, width: 35}}

  geoServiceSubcription$: Subscription;

  constructor(  
              private mapLoader: MapsAPILoader, 
              public modalController: ModalController,
              public alertController: AlertController,
              private ref: ChangeDetectorRef,  
              private mapsAPILoader: MapsAPILoader,
              private _geoServicio: GeoLocationService,
              private _clienteService: ClienteService,
              private _dataLocalService: DataLocalService,
              private navCtrl: NavController,
              private ngZone: NgZone) 
{ 
  this.geoServiceSubcription$ = this._geoServicio.getPosition().subscribe(
    (pos: Position) => {
      this.latUser =  pos.coords.latitude;
      this.lngUser = pos.coords.longitude;
    });
}

  ngOnInit() {
    this.getCurrentPosition();
    
    this.mapsAPILoader.load().then(() => {
      switch(this.tipoSolicitud){
        case 'direccionClientes':{
          this.title = 'Mapa Clientes';
          this.getDireccionClientes();
          break;
        }
        case 'checkinVendedor':{
          this.title = 'Mapa Visitas';
          this.setCheckIn();
        }
      }
    });

  }

  /**
  * @author ignacio zapata  \"2020-08-29\
  * @desc Metodo que se ejecuta cuando el mapa esta cargado completamente y ahi hacemos que sea visible "sin este codigo, el mapa se carga en blanco en googlechrome y movil"
  * @params 
  * @return
  */
  mapLoad(event){
    setTimeout(() => {
      this.mapready = true;
    }, 50);
  }
  
  ngOnDestroy(){
    this.geoServiceSubcription$? this.geoServiceSubcription$.unsubscribe() : '';
  }
  
  async cerrarModal() {

    await this.modalController.dismiss({
      reset: true
    });
  }

  private initGeocoder() {
 
    this.geocoder = new google.maps.Geocoder();
  }

  private waitForMapsToLoad(): Observable<boolean> {
    if(!this.geocoder) {
      return from(this.mapLoader.load())
      .pipe(
        tap(() => this.initGeocoder()),
        map(() => true)
      );
    }
    return of(true);
  }

  getCurrentPosition(){
    this.latUser = this._geoServicio.lat;
    this.lngUser = this._geoServicio.lng;
  }
  

  /**
  * @author ignacio zapata  \"2020-09-03\
  * @desc Funcion que permite markar el mapa con pins asociados a las lat y long asociadas a los vendedores
  * @params 
  * @return
  */
  async getDireccionClientes(){
    let clientesConDireccion: Cliente[] = [];
    await this.obtenerUsuario();
    this.clientes = await this._clienteService.obtenerClientes(this.vendedor);
    
    if(this.clientes.length > 0){
      this.clientes.forEach(cliente => {
        if(cliente.latitud && cliente.longitud){
          clientesConDireccion.push(cliente);
          this.direcciones.push({latitude: cliente.latitud, longitude:cliente.longitud, nombre: cliente.nombre})
        }
      })
      this.clientes = clientesConDireccion;
    }
  }

  async setCheckIn(){
    this.tiendasImplementos = await this._geoServicio.getTiendas(this.sucursal);
    this.adjustcheckinPlace();
  }

  /**
 * Permite obtener el usuario logueado
 * @memberof MapaPage
 */
  async obtenerUsuario() {
    let usuario = await this._dataLocalService.getItem('auth-token');
    let vendedor: Vendedor = JSON.parse(usuario);
    this.vendedor = vendedor;

    // Validamos si la persona que esta viendo el mapa es un zonal, ya que si lo es, se traerá los datos del vendedor seleccionado para mostrar sus clientes.
    if(vendedor.codBodega == 'AREA ADMINISTRACION' || vendedor.codBodega == ''){
      let vendedorSelected = await this._dataLocalService.getItem('vendedor');
      vendedorSelected? this.vendedor.codEmpleado = vendedorSelected._id : '';
    }
  }

  mapClicked(event){
  
  }

  /**
  * @author ignacio zapata  \"2020-09-03\
  * @desc Metodo que se ejecuta al hacer click encima de un pin en el mapa y luego se hace zoom a ese pin
  * @params 
  * @return
  */
  markerClicked(event){
    this.AcercarMapa(Number(event.latitude), Number(event.longitude), 13);
  }

  /**
  * @author ignacio zapata  \"2020-09-03\
  * @desc Metodo que permite restablecer la vista del mapa a la del inicio "zoom y posicion."
  * @params 
  * @return
  */
  resetVista(){
    this.AcercarMapa(-39.088320814101984, -70.73097506832782, 4 );
  }

  /**
  * @author ignacio zapata  \"2020-09-03\
  * @desc Metodo que se ejecuta cuando desde el filtro de direcciones, se emite un evento con la lat y lng a mostrar en el mapa.
  * @params 
  * @return
  */
  MostrarEspecificAddress(event: Cliente){
    this.AcercarMapa(Number(event.latitud), Number(event.longitud), 14);
  }

  /**
  * @author ignacio zapata  \"2020-09-03\
  * @desc metodo que permite hacer zoom directo a la posicion en el mapa del vendedor
  * @params 
  * @return
  */
   showMyPosition(){
     this.AcercarMapa(this.latUser, this.lngUser);
   }

    /**
   * @author ignacio zapata  \"2020-09-03\
   * @desc Metodo que recibe lat, lgn y zoom, precediento a mover el mapa a estas coordenadas enviadas como parametros.
   * @params lat, lgn y zoom, todas de tipo number
   * @return
   */
   AcercarMapa(lat: number, lng: number, zoom: number = 16){
    this.lat = lat;
    this.lng = lng;
    this.zoom > 10 && zoom != 4? null : this.zoom = zoom;
   }

    /**
   * @author ignacio zapata  \"2020-09-07\
   * @desc Metodo que permite abrir un popover con la informacion de una direccion clickeada
   * @params 
   * @return
   */
   onMouseOver(infoWindow, gm) {

    if (gm.lastOpen != null) {
        gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }

  /**
  * @author ignacio zapata  \"2020-09-07\
  * @desc Meetodo que permite mostrar el detalle de un cliente al hacer clic en el popover de un pin asociado a un cliente.
  * @params objeto de tipo Cliente
  * @return
  */
  async verDertalleCliente(cliente: Cliente) {
    if(cliente.rut){
      this.cerrarModal();
      await this._dataLocalService.setCliente(cliente);
      this.navCtrl.navigateForward(['/detalle-cliente']);
    }
  }

  /**
  * @author ignacio zapata  \"2020-11-10\
  * @desc Funcion utilizada para evitar que dos markets aparezcan en la misma posicion.
  * @params 
  * @return
  */
  async adjustcheckinPlace() {
    if (this.checkIns && this.checkIns.length > 0) {
        for (let visita of this.checkIns) {
          visita.latitud = String( Number(visita.latitud) -  (Math.floor(Math.random() * 50) / 100000));
          visita.longitud = String( Number(visita.longitud) -  (Math.floor(Math.random() * 50) / 100000));
        }
    }
  }
}

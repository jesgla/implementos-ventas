import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { AgmCoreModule } from '@agm/core';

import { MapaPage } from './mapa.page';

import { AddressSlidesComponent } from './components/address-slides/address-slides.component';

const routes: Routes = [
  {
    path: '',
    component: MapaPage
  }
];

@NgModule({
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD_HuwF5F8X8fOSR_1Ai_hFT115caUq4vI',
      libraries: ['places']
    }),
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [MapaPage, AddressSlidesComponent]
})
export class MapaPageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CotizacionPage } from './cotizacion.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { EnviarCotizacionPage } from '../enviar-cotizacion/enviar-cotizacion.page';
import { EnviarCotizacionPageModule } from '../enviar-cotizacion/enviar-cotizacion.module';


@NgModule({
  imports: [
    PipesModule,
    ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    EnviarCotizacionPageModule
  ],
  declarations: [CotizacionPage],
  entryComponents: [EnviarCotizacionPage]
})
export class CotizacionPageModule {}

import { LoadingService } from './../../../services/loading/loading.service';
import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { EnviarCotizacionPage } from '../enviar-cotizacion/enviar-cotizacion.page';

// servicios
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { EventoService } from 'src/app/services/evento/evento.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { CotizacionService }  from 'src/app/services/evento/cotizacion.service';

//interfaces
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { Articulo } from 'src/app/interfaces/articulo/articulo';
import { Vendedor } from 'src/app/interfaces/vendedor/vendedor';

/**
 *permite generar una cotizacion
 * @export
 * @class CotizacionPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-cotizacion',
  templateUrl: './cotizacion.page.html',
  styleUrls: ['./cotizacion.page.scss'],
})
export class CotizacionPage implements OnInit {

  ordenCompra: string = '';

  /**
   *monto de la cotizacion
   * @type {number}
   * @memberof CotizacionPage
   */
  @Input() total: number;

  /**
   *monto de la cotizacion sin iva
   * @type {number}
   * @memberof CotizacionPage
   */
  @Input() subTotal: number;

  /**
   *monto iva
   * @type {number}
   * @memberof CotizacionPage
   */
  @Input() iva: number;

  /**
   *arreglo de articulos
   * @type {Articulo[]}
   * @memberof CotizacionPage
   */
  @Input() carro: Articulo[];

  /**
   *objeto cliente
   * @type {Cliente}
   * @memberof CotizacionPage
   */
  cliente: Cliente;

  TipoPagos: any[];

  /**
   *objeto pago
   * @type {*}
   * @memberof CotizacionPage
   */
  pago: any;

  /**
 *objeto boolean
  * @type {boolean}
  * @memberof CotizacionPage
  */
  @Input() checkIn: boolean = false;

  /**
 *objeto Output
  * @type {Output}
  * @memberof CotizacionPage
  */
  @Output() cotizacionEvent = new EventEmitter;

  /**
   *id de referencia
   * @type {*}
   * @memberof CotizacionPage
   */
  idReferencia: any;

  /**
   *objeto vendedor
   * @type {Vendedor}
   * @memberof CotizacionPage
   */
  vendedor: Vendedor;

  /**
 *objeto vendedor
  * @type {Vendedor}
  * @memberof CotizacionPage
  */
  mostrarProductos: boolean = true;

  generandoCoti: boolean = false;
  enviarDocumento: boolean = false;
  docFolio: string = '';
  nombreDocu: string = 'Cotización';

  /**
   *Creates an instance of CotizacionPage.
   * @param {Router} router
   * @param {ModalController} modalController
   * @param {ClienteService} clienteService
   * @param {EventoService} eventoService
   * @param {DataLocalService} dataLocal
   * @param { CotizacionService } cotizacionService
   * @memberof CotizacionPage
   */
  constructor(
    private router: Router,
    public modalController: ModalController,
    public alertController: AlertController,
    private clienteService: ClienteService,
    private cotizacionService: CotizacionService,
    private dataLocal: DataLocalService,
    private _ArticulosService: ArticulosService,
    private _LoadingService: LoadingService) {

  }

  /**
   *metodo que se ejecuta al iniciar componente
   * @memberof CotizacionPage
   */
  async ngOnInit() {
    await this.obtenerUsuario();
    this.cliente = await this.dataLocal.getCliente();

    this.idReferencia = await this.dataLocal.getItem('referencia');
    let recid = this.cliente.recid? this.cliente.recid : 0;

    this.TipoPagos = await this.clienteService.obtenerFormaPago(recid);
  }

  /**
   *obtener usuario logueado
   * @memberof CotizacionPage
   */
  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
    this.vendedor = JSON.parse(usuario);

  }

  /**
   *permite generar cotizacion a un cliente
   * @memberof CotizacionPage
   */
  async generarCotizacion(checkInEvent = false, datos?) {
    await this._LoadingService.presentLoading('Generando cotización');
    this.generandoCoti = true;
    
    let lstArticulos = await this.prepararArticulos(this.carro);
    let parametros;

    parametros = {
      codTipoFormaPago: this.pago.codigo,
      rutVendedor: this.vendedor.rut.replace('.', '').replace('.', ''),
      rutCliente: this.cliente.rut.replace('.', '').replace('.', ''),
      rutEmisor: this.vendedor.rut.replace('.', '').replace('.', ''),
      codSucursal: this.vendedor.codSucursal,
      detalle: lstArticulos,
      idVisita: this.idReferencia ? this.idReferencia.toString() : '',
      codOC: this.ordenCompra?  this.ordenCompra : '',
    }

    let respuesta;


    if(!this.checkIn){
      respuesta = await this.cotizacionService.generarCotizacion(parametros);
      if(respuesta[0]){
        this.docFolio = respuesta[0].resultado;
        this.enviarDocumento = true;
      }
    }
    this._LoadingService.hideLoading();
    this.confirmarCotizacion(respuesta, parametros);
  }


  /**
   *permite cerrar un modal
   * @memberof CotizacionPage
   */
  async confirmarCotizacion(respuesta, parametros?) {

    let total: number = 0;
    
    if(this.checkIn){
      total = this.cotizacionService.getTotalCotizacion(parametros.detalle);
    }

    const alert = await this.alertController.create({
      header: 'Confirmación!',
      message: '¿Desea convertir la cotización en orden de venta?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            if(!this.checkIn){
              /* this.router.navigate(['tabs', 'catalogo']); */
            }else{
              this.cotizacionEvent.emit({'accion': 'cancelar', 'parametros': parametros, 'total': total });
              this.modalController.dismiss();
            } 
          }
        }, {
          text: 'Aceptar',
          handler: async () => {
            
            if(!this.checkIn){
              await this._LoadingService.presentLoading('Creando orden de venta.');
              let consulta = await this.cotizacionService.confirmarCotizacion( respuesta, this.vendedor);
              if(consulta[0]){
                console.log('documento generado', consulta);
                this.docFolio = consulta[0].resultado;
                this.nombreDocu = 'Orden de Venta';
              }
              this._LoadingService.hideLoading();
            }else{
              this.cotizacionEvent.emit({'accion': 'crear', 'parametros': parametros, 'total': total });
              this.modalController.dismiss();
            }
          }
        }
      ]
    });

    await alert.present();
    this.generandoCoti = false;

  }

  /**
   *permite obtener forma de pago
   * @param {*} event
   * @memberof CotizacionPage
   */
  changeEnvironment(event): void {
    this.pago = event.detail.value

  }

  /**
   *permite concatenar los articulos en un string
   * @param {*} carro
   * @returns
   * @memberof CotizacionPage
   */
  prepararArticulos(carro) {
    let cadenaArticulo = "|";

    carro.map( (articulo: Articulo) => {

      if(!articulo.precio_escala){   
        cadenaArticulo += `${articulo.sku}@${articulo.cantidad}@${( Math.round(articulo.precioNeto * 1.19 ))}|`
      }else{
        cadenaArticulo += `${articulo.sku}@${articulo.cantidad}@${( Math.round(articulo.precioEscaladoNeto * 1.19 ))}|`
      }
    
    });
    return cadenaArticulo
  }

 /**
 *permite cerrar el modal
 * @memberof AgendarPage
 */
  cerrarModal() {
    // Si fue creada una cotizacion, entonces al cerrar el modal se enviará directo al
    if(this.enviarDocumento){
      this.router.navigate(['tabs', 'clientes']);
      this.modalController.dismiss();
    }else{
      this.modalController.dismiss();
    }
  }


  async enviarDocumentoFunc(){
    const modal = await this.modalController.create({
      component: EnviarCotizacionPage,
      componentProps: {
        orden: {folio: this.docFolio, nombreCliente: this.cliente.nombre}
      }
    });
    return await modal.present();
  }

  obtenerCarro(){
    
  }

}

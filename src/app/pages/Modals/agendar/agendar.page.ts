import { Component, OnInit, ViewChild, Inject, LOCALE_ID, Input, OnChanges } from '@angular/core';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { formatDate } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Cliente } from 'src/app/interfaces/cliente/cliente';
import { Evento } from 'src/app/interfaces/evento/evento';
import { EventoService } from 'src/app/services/evento/evento.service';

/**
 *componente que permite agendar una visita
 * @export
 * @class AgendarPage
 * @implements {OnInit}
 */
@Component({
  selector: 'app-agendar',
  templateUrl: './agendar.page.html',
  styleUrls: ['./agendar.page.scss'],
})
export class AgendarPage implements OnInit {
 
  monthShortNames = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
  /**
   *objeto de agenda
   * @type {Evento}
   * @memberof AgendarPage
   */
  generarVisita: Evento;

  /**
   *objeto de evento
   * @memberof AgendarPage
   */
  event = {
    title: '',
    desc: '',
    startTime: '',
    endTime: '',
    allDay: false
  };

  /**
   *limite fecha minima
   * @memberof AgendarPage
   */
  minDate = new Date('MM/DD/YYYY HH:mm').toString();

  /**
   *evento calendar
   * @memberof AgendarPage
   */
  @Input() eventSource;

  /**
   *elemento calendario
   * @memberof AgendarPage
   */
  @Input() myCal;

  /**
   *configuracion calendario
   *
   * @memberof AgendarPage
   */
  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };

  /**
   *estado de cliente
   * @type {boolean}
   * @memberof AgendarPage
   */
  clienteSeleccionado: boolean;

  /**
   *objeto cliente
   *
   * @type {Cliente}
   * @memberof AgendarPage
   */
  cliente: Cliente;

  /**
   *objeto evento
   * @type {Evento}
   * @memberof AgendarPage
   */
  agendar: Evento;

  /**
   *objeto vendedor
   * @type {*}
   * @memberof AgendarPage
   */
  vendedor: any;

  /**
   *Creates an instance of AgendarPage.
   * @param {ModalController} modalController
   * @param {string} locale
   * @param {DataLocalService} dataLocal
   * @param {EventoService} eventoService
   * @memberof AgendarPage
   */
  constructor(
    public modalController: ModalController,
    @Inject(LOCALE_ID) private locale: string,
    private dataLocal: DataLocalService,
    private eventoService: EventoService) {
    this.generarVisita = new Evento();
  }

  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof AgendarPage
   */
  async ngOnInit() {
    await this.obtenerUsuario();
    this.resetEvent();
    this.cliente = await this.dataLocal.getCliente();
    this.event.title = this.cliente.nombre
    this.event.desc = "VISITA";
  }

  /**
   *premite obtener usuario
   * @memberof AgendarPage
   */
  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');

    this.vendedor = JSON.parse(usuario);

  }

  /**
   *reinicia objeto evento
   * @memberof AgendarPage
   */
  resetEvent() {
    this.event = {
      title: '',
      desc: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    };
  }



  /**
   *permite crear un nuevo evento
   *
   * @memberof AgendarPage
   */
  async addEvent() {
    let eventCopy = {
      title: this.event.title,
      startTime: new Date(this.event.startTime),
      endTime: new Date(this.event.endTime),
      allDay: this.event.allDay,
      desc: this.event.desc
    }
    this.generarVisita.tipoEvento = 'VISITA';
    this.generarVisita.fechaCreacion = this.event.startTime;
    this.generarVisita.fechaProgramada = this.event.startTime;
    this.generarVisita.observaciones = this.event.desc;
    this.generarVisita.rutEmisor = this.vendedor.rut.replace('.','').replace('.','');
    this.generarVisita.rutCliente = this.cliente.rut.replace('.','').replace('.','');
    this.generarVisita.rutVendedor = this.vendedor.rut.replace('.','').replace('.','');
    this.generarVisita.nombreCliente = this.cliente.nombre;
    this.generarVisita.estado = "PROGRAMADA";


    let respuesta = await this.eventoService.generarEvento(this.generarVisita);

    if (respuesta[0].resultado) {

      this.dataLocal.eliminarDatos('cliente');
      this.dataLocal.presentToast('Visita creada existosamente','success');
      this.eventSource.push(eventCopy);
      this.cerrarModal();

      this.myCal.loadEvents();
      this.resetEvent();
    } else{
      this.dataLocal.presentToast('No se logro registrar la visita','danger');
    }

  }

  /**
   *permite cerrar el modal
   * @memberof AgendarPage
   */
  cerrarModal() {
    this.modalController.dismiss();
  }

  actualizarFecha(event) {
  
    if(this.event.startTime){
      let fecha = new Date(this.event.startTime);
      fecha.setMinutes(fecha.getMinutes() + 15);
      this.event.endTime =fecha.toISOString()
    }
  }

}

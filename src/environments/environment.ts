export const environment = {
  production: false,
  endPoint:'http://replicacion.implementos.cl/wsOmnichannel/Capa_jsonServices',
  endPointB2B :'https://b2b-api.implementos.cl',
  endPointApiCarro :'https://b2b-api.implementos.cl/api/carro',
  endPointApiCatalogo :'https://b2b-api.implementos.cl/api/catalogo',
  endPointApiLogistica :'https://b2b-api.implementos.cl/api/logistica',
  endPointApiCms :'https://b2b-api.implementos.cl/cms',
  endPointApiMovil: 'https://b2b-api.implementos.cl/api/mobile',
  endPointApiNotificaciones: 'https://dev-api.implementos.cl/api/notificaciones',
  endPointApiLocalizacion: 'https://b2b-api.implementos.cl/api/localizacion',
  apiImplementosReplicacion: "http://replicacion.implementos.cl/webapi2/API/",
};
